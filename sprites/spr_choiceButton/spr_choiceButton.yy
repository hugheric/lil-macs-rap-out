{
    "id": "48d67fad-5268-4316-aa80-70b5a2f113ca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_choiceButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "983b30ff-c0cd-4a52-a485-df9404942764",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48d67fad-5268-4316-aa80-70b5a2f113ca",
            "compositeImage": {
                "id": "4595fcc4-eda2-49e8-a7d4-c6155988641b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "983b30ff-c0cd-4a52-a485-df9404942764",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f646596b-5f60-4884-b17a-ddb00674a2e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "983b30ff-c0cd-4a52-a485-df9404942764",
                    "LayerId": "f724ffea-536a-4897-bc3b-41b31a0827c7"
                }
            ]
        },
        {
            "id": "b90ae20d-635b-4a74-bc78-26dc129088b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48d67fad-5268-4316-aa80-70b5a2f113ca",
            "compositeImage": {
                "id": "4fd5dcbe-d2f1-4ccd-a595-5b9ac59406de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b90ae20d-635b-4a74-bc78-26dc129088b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b4dc078-ceb0-40d5-ad46-7e8e8cfbef83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b90ae20d-635b-4a74-bc78-26dc129088b2",
                    "LayerId": "f724ffea-536a-4897-bc3b-41b31a0827c7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "f724ffea-536a-4897-bc3b-41b31a0827c7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "48d67fad-5268-4316-aa80-70b5a2f113ca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 23,
    "yorig": 18
}