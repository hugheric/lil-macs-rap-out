{
    "id": "eb039aea-ce5a-418a-8bea-0e3ef6dcb603",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lilMac",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "893634c3-0a7c-4849-bc96-97183bd8496d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb039aea-ce5a-418a-8bea-0e3ef6dcb603",
            "compositeImage": {
                "id": "1b0667bb-d8e3-4a06-b894-479b6dc0914a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "893634c3-0a7c-4849-bc96-97183bd8496d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32893c9e-4376-480f-8780-4da684092103",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "893634c3-0a7c-4849-bc96-97183bd8496d",
                    "LayerId": "5b4cca4b-51fc-426a-8ff3-7916e4bf3f65"
                }
            ]
        },
        {
            "id": "c03d8395-df80-4980-88e5-ce53456d45b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb039aea-ce5a-418a-8bea-0e3ef6dcb603",
            "compositeImage": {
                "id": "22cf6f90-be22-45af-a636-3ed58d3e39e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c03d8395-df80-4980-88e5-ce53456d45b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "804124b5-6da4-4d99-b63a-83ee71feec1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c03d8395-df80-4980-88e5-ce53456d45b5",
                    "LayerId": "5b4cca4b-51fc-426a-8ff3-7916e4bf3f65"
                }
            ]
        },
        {
            "id": "42b29db4-8ffa-4490-a4ba-4940eb6d34f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb039aea-ce5a-418a-8bea-0e3ef6dcb603",
            "compositeImage": {
                "id": "ff2ad58e-3947-4f23-9dc4-990d20900d2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42b29db4-8ffa-4490-a4ba-4940eb6d34f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8431fa1-4ec5-4227-9f50-050cc3efb0b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42b29db4-8ffa-4490-a4ba-4940eb6d34f2",
                    "LayerId": "5b4cca4b-51fc-426a-8ff3-7916e4bf3f65"
                }
            ]
        },
        {
            "id": "02ee3ade-5344-4a5b-9f57-a4a0facd1390",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb039aea-ce5a-418a-8bea-0e3ef6dcb603",
            "compositeImage": {
                "id": "ed0b22be-399e-467b-823b-50b6930d7947",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02ee3ade-5344-4a5b-9f57-a4a0facd1390",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3727142-428b-49af-a90b-38c119824a40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02ee3ade-5344-4a5b-9f57-a4a0facd1390",
                    "LayerId": "5b4cca4b-51fc-426a-8ff3-7916e4bf3f65"
                }
            ]
        },
        {
            "id": "cf2a68e3-ca2d-448f-8642-f69ddd5b4818",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb039aea-ce5a-418a-8bea-0e3ef6dcb603",
            "compositeImage": {
                "id": "cc73aca5-284c-4c14-b171-49b4ee64d8b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf2a68e3-ca2d-448f-8642-f69ddd5b4818",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d6a8784-98db-4bb3-9a2d-463dd919de90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf2a68e3-ca2d-448f-8642-f69ddd5b4818",
                    "LayerId": "5b4cca4b-51fc-426a-8ff3-7916e4bf3f65"
                }
            ]
        },
        {
            "id": "78ecdfac-9276-4b18-8a4b-316e00d94a12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb039aea-ce5a-418a-8bea-0e3ef6dcb603",
            "compositeImage": {
                "id": "c8ec38f1-0b2e-424d-956c-1e516166e852",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78ecdfac-9276-4b18-8a4b-316e00d94a12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ef50dd5-8f10-4799-97af-4b36158a5277",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78ecdfac-9276-4b18-8a4b-316e00d94a12",
                    "LayerId": "5b4cca4b-51fc-426a-8ff3-7916e4bf3f65"
                }
            ]
        },
        {
            "id": "67ffdbb3-41b4-4980-ac85-d4394eb20cc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb039aea-ce5a-418a-8bea-0e3ef6dcb603",
            "compositeImage": {
                "id": "29c4ecb2-71f3-491c-95b4-037f9a968731",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67ffdbb3-41b4-4980-ac85-d4394eb20cc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c6ddf09-c7b8-44ef-864e-44d77e49a3f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67ffdbb3-41b4-4980-ac85-d4394eb20cc2",
                    "LayerId": "5b4cca4b-51fc-426a-8ff3-7916e4bf3f65"
                }
            ]
        },
        {
            "id": "268bb4cf-c375-43c1-85a4-fc7731aa2dad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb039aea-ce5a-418a-8bea-0e3ef6dcb603",
            "compositeImage": {
                "id": "8772c0f5-f1f1-4cc0-b4f5-6f611a555314",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "268bb4cf-c375-43c1-85a4-fc7731aa2dad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f6bab6d-93d9-4716-a433-9c80b98e4239",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "268bb4cf-c375-43c1-85a4-fc7731aa2dad",
                    "LayerId": "5b4cca4b-51fc-426a-8ff3-7916e4bf3f65"
                }
            ]
        },
        {
            "id": "82ec57f5-ed9d-423a-a450-b4c341e9c5d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb039aea-ce5a-418a-8bea-0e3ef6dcb603",
            "compositeImage": {
                "id": "18f2816f-b2fc-45a1-83d4-3a375e3534b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82ec57f5-ed9d-423a-a450-b4c341e9c5d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0602e975-1d06-425f-96f5-7c0aa0d9308a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82ec57f5-ed9d-423a-a450-b4c341e9c5d7",
                    "LayerId": "5b4cca4b-51fc-426a-8ff3-7916e4bf3f65"
                }
            ]
        },
        {
            "id": "2fac4de4-7279-470a-974f-db5d101d307c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb039aea-ce5a-418a-8bea-0e3ef6dcb603",
            "compositeImage": {
                "id": "b212adee-fa76-4283-be0c-b68e93e276d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fac4de4-7279-470a-974f-db5d101d307c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06475363-02a5-4308-b00b-84451d247e39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fac4de4-7279-470a-974f-db5d101d307c",
                    "LayerId": "5b4cca4b-51fc-426a-8ff3-7916e4bf3f65"
                }
            ]
        },
        {
            "id": "59f2aceb-86cd-4e78-9407-ccc24af4b973",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb039aea-ce5a-418a-8bea-0e3ef6dcb603",
            "compositeImage": {
                "id": "14deda50-2ba6-4faa-91d0-f6ab6a98ffda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59f2aceb-86cd-4e78-9407-ccc24af4b973",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0907591-e5cf-43e2-9b39-d35813bb8a4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59f2aceb-86cd-4e78-9407-ccc24af4b973",
                    "LayerId": "5b4cca4b-51fc-426a-8ff3-7916e4bf3f65"
                }
            ]
        },
        {
            "id": "6a442844-a693-4a7e-97db-6906cd99d209",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb039aea-ce5a-418a-8bea-0e3ef6dcb603",
            "compositeImage": {
                "id": "b8d7367b-07f4-49b8-a90c-12eef1d04ca1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a442844-a693-4a7e-97db-6906cd99d209",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b4c7fab-92e0-4a73-9c4c-638d8a08bf3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a442844-a693-4a7e-97db-6906cd99d209",
                    "LayerId": "5b4cca4b-51fc-426a-8ff3-7916e4bf3f65"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "5b4cca4b-51fc-426a-8ff3-7916e4bf3f65",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eb039aea-ce5a-418a-8bea-0e3ef6dcb603",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 0,
    "yorig": 0
}