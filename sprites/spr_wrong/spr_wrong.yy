{
    "id": "f2f7eda7-1f89-46ac-81be-6e0ee3f1ed1b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wrong",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3cfdd87e-5333-4eff-bf85-517dbd71bb5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2f7eda7-1f89-46ac-81be-6e0ee3f1ed1b",
            "compositeImage": {
                "id": "b60919f9-af44-463e-9922-6ae022058a06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cfdd87e-5333-4eff-bf85-517dbd71bb5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74658545-0e34-4a17-94e1-e1651681ea92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cfdd87e-5333-4eff-bf85-517dbd71bb5b",
                    "LayerId": "1a92e737-c582-4d59-b961-48129c7f78cb"
                }
            ]
        },
        {
            "id": "33a1ebf3-f8bc-48a3-b9e3-0639a0993501",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2f7eda7-1f89-46ac-81be-6e0ee3f1ed1b",
            "compositeImage": {
                "id": "44a4c74f-7acc-404e-813c-74c127c7f40f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33a1ebf3-f8bc-48a3-b9e3-0639a0993501",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df47f9b8-99c8-4435-8df0-a41144b855f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33a1ebf3-f8bc-48a3-b9e3-0639a0993501",
                    "LayerId": "1a92e737-c582-4d59-b961-48129c7f78cb"
                }
            ]
        },
        {
            "id": "b4ddb42e-dca3-4fbb-829e-c084f0c7c786",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2f7eda7-1f89-46ac-81be-6e0ee3f1ed1b",
            "compositeImage": {
                "id": "2a14d628-175d-4f14-b446-9da458240ef4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4ddb42e-dca3-4fbb-829e-c084f0c7c786",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3567469-21ba-43cf-92df-a9f392aeab7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4ddb42e-dca3-4fbb-829e-c084f0c7c786",
                    "LayerId": "1a92e737-c582-4d59-b961-48129c7f78cb"
                }
            ]
        },
        {
            "id": "79c50294-83f5-4e92-a7ea-7850b243f51d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2f7eda7-1f89-46ac-81be-6e0ee3f1ed1b",
            "compositeImage": {
                "id": "e943bcda-7b2d-47a8-b2a8-790dc245e655",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79c50294-83f5-4e92-a7ea-7850b243f51d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13a36765-36e3-4faa-94e3-25ae629e716f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79c50294-83f5-4e92-a7ea-7850b243f51d",
                    "LayerId": "1a92e737-c582-4d59-b961-48129c7f78cb"
                }
            ]
        },
        {
            "id": "c7f06024-3279-43ab-a49c-f0aaec4116c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2f7eda7-1f89-46ac-81be-6e0ee3f1ed1b",
            "compositeImage": {
                "id": "4244fdd2-3039-41c0-8c77-1bb36899bd4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7f06024-3279-43ab-a49c-f0aaec4116c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71469d61-7846-4c4a-a8dc-a7dcbe9cdd9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7f06024-3279-43ab-a49c-f0aaec4116c5",
                    "LayerId": "1a92e737-c582-4d59-b961-48129c7f78cb"
                }
            ]
        },
        {
            "id": "9e75bb95-11c9-4908-90f4-e286bdf0d1ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2f7eda7-1f89-46ac-81be-6e0ee3f1ed1b",
            "compositeImage": {
                "id": "47bc7fab-4561-4ffa-87f6-6b5455b78733",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e75bb95-11c9-4908-90f4-e286bdf0d1ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8de2d6d-a3cb-45bb-af53-1bc58a759a50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e75bb95-11c9-4908-90f4-e286bdf0d1ba",
                    "LayerId": "1a92e737-c582-4d59-b961-48129c7f78cb"
                }
            ]
        },
        {
            "id": "9cd4caf0-936f-492c-a213-7daeaf88c9fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2f7eda7-1f89-46ac-81be-6e0ee3f1ed1b",
            "compositeImage": {
                "id": "01632d15-0115-4358-913f-7ac3e24e0ea0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cd4caf0-936f-492c-a213-7daeaf88c9fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecf0b7f2-0683-4636-9df0-4173d1934e2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cd4caf0-936f-492c-a213-7daeaf88c9fe",
                    "LayerId": "1a92e737-c582-4d59-b961-48129c7f78cb"
                }
            ]
        },
        {
            "id": "a0f2a3f4-6ed6-49bb-b640-04b996abe4f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2f7eda7-1f89-46ac-81be-6e0ee3f1ed1b",
            "compositeImage": {
                "id": "f7818e38-6b5a-43ec-b222-2cf480f1d2dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0f2a3f4-6ed6-49bb-b640-04b996abe4f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05e2a9a7-6b6c-40a7-a2f6-4d2fa98f8592",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0f2a3f4-6ed6-49bb-b640-04b996abe4f7",
                    "LayerId": "1a92e737-c582-4d59-b961-48129c7f78cb"
                }
            ]
        },
        {
            "id": "42b2126c-6480-4be9-9e0a-8ae9db846e75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2f7eda7-1f89-46ac-81be-6e0ee3f1ed1b",
            "compositeImage": {
                "id": "ffb1aa87-3a21-4ac0-8599-3cf0887301ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42b2126c-6480-4be9-9e0a-8ae9db846e75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0349c0b-bdb2-4872-8336-2ce0d3df4d5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42b2126c-6480-4be9-9e0a-8ae9db846e75",
                    "LayerId": "1a92e737-c582-4d59-b961-48129c7f78cb"
                }
            ]
        },
        {
            "id": "4f65eb70-7914-4d9b-aa7e-438bffdf41b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2f7eda7-1f89-46ac-81be-6e0ee3f1ed1b",
            "compositeImage": {
                "id": "2ed90d59-7b76-4aee-9e65-d8e2ddcca8b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f65eb70-7914-4d9b-aa7e-438bffdf41b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "660a20ab-0d5a-400e-afbd-114245f92799",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f65eb70-7914-4d9b-aa7e-438bffdf41b1",
                    "LayerId": "1a92e737-c582-4d59-b961-48129c7f78cb"
                }
            ]
        },
        {
            "id": "195e7d87-a23b-44b9-9641-12423d0a1448",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2f7eda7-1f89-46ac-81be-6e0ee3f1ed1b",
            "compositeImage": {
                "id": "47b41101-87db-4c19-a5d7-3ce11ce921f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "195e7d87-a23b-44b9-9641-12423d0a1448",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "544c8a60-9b5b-4b13-9101-968f9944c5de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "195e7d87-a23b-44b9-9641-12423d0a1448",
                    "LayerId": "1a92e737-c582-4d59-b961-48129c7f78cb"
                }
            ]
        },
        {
            "id": "1747df8e-ded3-4297-8c56-ee9739159b1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2f7eda7-1f89-46ac-81be-6e0ee3f1ed1b",
            "compositeImage": {
                "id": "60c41d16-0536-4bf8-8c5c-cf23a894ee33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1747df8e-ded3-4297-8c56-ee9739159b1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d872b78-ec83-45ad-b7ca-afb774416007",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1747df8e-ded3-4297-8c56-ee9739159b1c",
                    "LayerId": "1a92e737-c582-4d59-b961-48129c7f78cb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "1a92e737-c582-4d59-b961-48129c7f78cb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f2f7eda7-1f89-46ac-81be-6e0ee3f1ed1b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 0
}