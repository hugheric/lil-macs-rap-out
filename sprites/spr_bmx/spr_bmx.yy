{
    "id": "801b3552-4c91-4eb4-ae09-a6126a2de793",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bmx",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "58985373-e971-4cf6-b4f3-b3187a4ebb3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "801b3552-4c91-4eb4-ae09-a6126a2de793",
            "compositeImage": {
                "id": "6cf25816-b4c9-403d-9f96-29c881ca02e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58985373-e971-4cf6-b4f3-b3187a4ebb3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d71da85-7c87-4879-9f21-9c8b1703aff8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58985373-e971-4cf6-b4f3-b3187a4ebb3c",
                    "LayerId": "f82d4da3-ecc7-4416-9fd0-06d80a3d7b3b"
                }
            ]
        },
        {
            "id": "0af581ee-17b5-43b9-8c43-3084d49457cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "801b3552-4c91-4eb4-ae09-a6126a2de793",
            "compositeImage": {
                "id": "f7164b6f-9e55-4495-8236-57b427d7803e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0af581ee-17b5-43b9-8c43-3084d49457cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c654473f-b522-4d11-9b2f-4a5bad3a8550",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0af581ee-17b5-43b9-8c43-3084d49457cf",
                    "LayerId": "f82d4da3-ecc7-4416-9fd0-06d80a3d7b3b"
                }
            ]
        },
        {
            "id": "1bf5ffbd-1186-4861-94be-9cf350c1eaa7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "801b3552-4c91-4eb4-ae09-a6126a2de793",
            "compositeImage": {
                "id": "64f900cf-6a6d-47c1-af78-8ce096dbca75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bf5ffbd-1186-4861-94be-9cf350c1eaa7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be338239-7e08-48de-9fd6-3812425e1d61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bf5ffbd-1186-4861-94be-9cf350c1eaa7",
                    "LayerId": "f82d4da3-ecc7-4416-9fd0-06d80a3d7b3b"
                }
            ]
        },
        {
            "id": "b530dc91-fd0d-450a-91ae-4d47ce93255f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "801b3552-4c91-4eb4-ae09-a6126a2de793",
            "compositeImage": {
                "id": "1aad50a1-2d7d-4c15-a865-b88697123a71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b530dc91-fd0d-450a-91ae-4d47ce93255f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c94f1d5c-236e-498a-b7fb-980756419efa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b530dc91-fd0d-450a-91ae-4d47ce93255f",
                    "LayerId": "f82d4da3-ecc7-4416-9fd0-06d80a3d7b3b"
                }
            ]
        },
        {
            "id": "d5e1c11a-7dc9-420a-85e2-89d093785be9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "801b3552-4c91-4eb4-ae09-a6126a2de793",
            "compositeImage": {
                "id": "82c571d9-1b28-41d3-aafa-a1297dafb165",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5e1c11a-7dc9-420a-85e2-89d093785be9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c969901-6b90-4db0-90c0-56e67ae5c241",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5e1c11a-7dc9-420a-85e2-89d093785be9",
                    "LayerId": "f82d4da3-ecc7-4416-9fd0-06d80a3d7b3b"
                }
            ]
        },
        {
            "id": "2b07d1a8-27e1-4ea8-94b5-9f1e4ad4f3e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "801b3552-4c91-4eb4-ae09-a6126a2de793",
            "compositeImage": {
                "id": "b986a329-e2bc-4066-8e46-858ead9f5af6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b07d1a8-27e1-4ea8-94b5-9f1e4ad4f3e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b52bb343-3854-4b47-9b83-45ff99b708ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b07d1a8-27e1-4ea8-94b5-9f1e4ad4f3e5",
                    "LayerId": "f82d4da3-ecc7-4416-9fd0-06d80a3d7b3b"
                }
            ]
        },
        {
            "id": "fee62565-678d-4d79-9436-9ab5f0dabf2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "801b3552-4c91-4eb4-ae09-a6126a2de793",
            "compositeImage": {
                "id": "21161234-8fc7-48ab-a39c-93ec49bb8607",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fee62565-678d-4d79-9436-9ab5f0dabf2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e7cd1c7-72f9-4f9e-93ab-26f595c473bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fee62565-678d-4d79-9436-9ab5f0dabf2c",
                    "LayerId": "f82d4da3-ecc7-4416-9fd0-06d80a3d7b3b"
                }
            ]
        },
        {
            "id": "c67100d0-706c-4f63-8cb6-8b0d1c08cb01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "801b3552-4c91-4eb4-ae09-a6126a2de793",
            "compositeImage": {
                "id": "b56f6cfd-a369-4603-925e-f0bff6a2884b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c67100d0-706c-4f63-8cb6-8b0d1c08cb01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "627118c6-bb33-41a0-aeb2-4f46883f75d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c67100d0-706c-4f63-8cb6-8b0d1c08cb01",
                    "LayerId": "f82d4da3-ecc7-4416-9fd0-06d80a3d7b3b"
                }
            ]
        },
        {
            "id": "b00f7e9c-30fd-4ad5-a516-812881a5fc37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "801b3552-4c91-4eb4-ae09-a6126a2de793",
            "compositeImage": {
                "id": "d1b504fa-b827-4d12-a2a5-62611149834b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b00f7e9c-30fd-4ad5-a516-812881a5fc37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1a1709e-cc9d-488a-927d-711b75086d6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b00f7e9c-30fd-4ad5-a516-812881a5fc37",
                    "LayerId": "f82d4da3-ecc7-4416-9fd0-06d80a3d7b3b"
                }
            ]
        },
        {
            "id": "db0864bd-a309-4499-ae1f-3d9ba2f0ee32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "801b3552-4c91-4eb4-ae09-a6126a2de793",
            "compositeImage": {
                "id": "3d4316aa-f4cd-4231-a22e-deea51411e0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db0864bd-a309-4499-ae1f-3d9ba2f0ee32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bc88356-858c-449c-9a3a-d99b79261952",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db0864bd-a309-4499-ae1f-3d9ba2f0ee32",
                    "LayerId": "f82d4da3-ecc7-4416-9fd0-06d80a3d7b3b"
                }
            ]
        },
        {
            "id": "907f3a9f-6da3-4291-aef7-c15229c38bab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "801b3552-4c91-4eb4-ae09-a6126a2de793",
            "compositeImage": {
                "id": "22179eee-0097-4264-8d88-16b083040213",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "907f3a9f-6da3-4291-aef7-c15229c38bab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e3ce072-f1f3-4f6f-95d4-81e800740fda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "907f3a9f-6da3-4291-aef7-c15229c38bab",
                    "LayerId": "f82d4da3-ecc7-4416-9fd0-06d80a3d7b3b"
                }
            ]
        },
        {
            "id": "ce260116-aa1a-4164-b9bc-612a23ef15ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "801b3552-4c91-4eb4-ae09-a6126a2de793",
            "compositeImage": {
                "id": "47a076d7-16b1-471c-abd9-b88da976feb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce260116-aa1a-4164-b9bc-612a23ef15ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a537634d-a141-4124-80fe-4db54b7c421c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce260116-aa1a-4164-b9bc-612a23ef15ba",
                    "LayerId": "f82d4da3-ecc7-4416-9fd0-06d80a3d7b3b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "f82d4da3-ecc7-4416-9fd0-06d80a3d7b3b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "801b3552-4c91-4eb4-ae09-a6126a2de793",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 0,
    "yorig": 0
}