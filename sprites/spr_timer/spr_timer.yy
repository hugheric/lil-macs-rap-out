{
    "id": "f4f833e6-fe97-439f-a75b-06961dd4a58d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_timer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 74,
    "bbox_left": 0,
    "bbox_right": 74,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a59c839-a92b-42a5-aeb9-05de8c625f64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4f833e6-fe97-439f-a75b-06961dd4a58d",
            "compositeImage": {
                "id": "344ec28a-84dd-47a9-aebe-d4f819e71053",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a59c839-a92b-42a5-aeb9-05de8c625f64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a928910-5bbc-4c82-a1d9-e15e4fe84f92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a59c839-a92b-42a5-aeb9-05de8c625f64",
                    "LayerId": "13671322-5095-4237-b116-47803f25c790"
                }
            ]
        },
        {
            "id": "5370e4c6-20ec-41c4-8f49-397e27788f0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4f833e6-fe97-439f-a75b-06961dd4a58d",
            "compositeImage": {
                "id": "7e7298ae-9a98-4d2e-8418-dc65d2872ed4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5370e4c6-20ec-41c4-8f49-397e27788f0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "483225d6-8cc5-4ab1-97fa-ae1ba15a74bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5370e4c6-20ec-41c4-8f49-397e27788f0d",
                    "LayerId": "13671322-5095-4237-b116-47803f25c790"
                }
            ]
        },
        {
            "id": "6f8bc125-9a41-4ba0-a6f0-ab43ef8abfc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4f833e6-fe97-439f-a75b-06961dd4a58d",
            "compositeImage": {
                "id": "4cf97246-8585-418a-aebc-01541cd5954d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f8bc125-9a41-4ba0-a6f0-ab43ef8abfc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2964931-b67f-408c-b2db-6ca9685be63a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f8bc125-9a41-4ba0-a6f0-ab43ef8abfc2",
                    "LayerId": "13671322-5095-4237-b116-47803f25c790"
                }
            ]
        },
        {
            "id": "cf153738-a0c7-4aa5-a3a7-b0d37fe400ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4f833e6-fe97-439f-a75b-06961dd4a58d",
            "compositeImage": {
                "id": "d23a2d4a-e648-4907-a73d-b20a3e5a47c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf153738-a0c7-4aa5-a3a7-b0d37fe400ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c2c5a60-12d7-4f34-92a9-fd03227a4df3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf153738-a0c7-4aa5-a3a7-b0d37fe400ec",
                    "LayerId": "13671322-5095-4237-b116-47803f25c790"
                }
            ]
        },
        {
            "id": "cc40059b-f742-494c-9c52-a0733ad960d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4f833e6-fe97-439f-a75b-06961dd4a58d",
            "compositeImage": {
                "id": "f5b62626-7f30-46b0-a841-846aef5e31db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc40059b-f742-494c-9c52-a0733ad960d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d9a6a42-ae6a-4e1d-88a2-f39afe019472",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc40059b-f742-494c-9c52-a0733ad960d5",
                    "LayerId": "13671322-5095-4237-b116-47803f25c790"
                }
            ]
        },
        {
            "id": "0b87f2a3-306f-4627-914b-8507aaa2da84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4f833e6-fe97-439f-a75b-06961dd4a58d",
            "compositeImage": {
                "id": "b012284a-b560-45b4-aedf-01d4bf6b15a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b87f2a3-306f-4627-914b-8507aaa2da84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd070aea-5de6-463e-924b-e3a9f5d2d3a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b87f2a3-306f-4627-914b-8507aaa2da84",
                    "LayerId": "13671322-5095-4237-b116-47803f25c790"
                }
            ]
        },
        {
            "id": "90474894-01af-494d-9622-f2f29d25d80b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4f833e6-fe97-439f-a75b-06961dd4a58d",
            "compositeImage": {
                "id": "653be259-66ac-401e-baba-458c95065b36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90474894-01af-494d-9622-f2f29d25d80b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "215d8cdb-9d50-4b6b-9a03-0c30f0ea5e5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90474894-01af-494d-9622-f2f29d25d80b",
                    "LayerId": "13671322-5095-4237-b116-47803f25c790"
                }
            ]
        },
        {
            "id": "b4c8ae3a-c40b-4ccf-9f39-9209160fddc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4f833e6-fe97-439f-a75b-06961dd4a58d",
            "compositeImage": {
                "id": "efe540ad-f077-4c35-86dc-8bade2f27d3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4c8ae3a-c40b-4ccf-9f39-9209160fddc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e8fda9b-a767-4718-a7b2-aa55e13e286a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4c8ae3a-c40b-4ccf-9f39-9209160fddc9",
                    "LayerId": "13671322-5095-4237-b116-47803f25c790"
                }
            ]
        },
        {
            "id": "56326dfe-b50f-443a-99af-368847e535e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4f833e6-fe97-439f-a75b-06961dd4a58d",
            "compositeImage": {
                "id": "f4afbf89-23ce-45b8-97c1-c5193e5c6ce7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56326dfe-b50f-443a-99af-368847e535e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4cc6419-703b-4537-97ae-22aa30fc5b91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56326dfe-b50f-443a-99af-368847e535e0",
                    "LayerId": "13671322-5095-4237-b116-47803f25c790"
                }
            ]
        },
        {
            "id": "d48b9a42-4b0b-4718-9ec5-8fa7ef3c740e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4f833e6-fe97-439f-a75b-06961dd4a58d",
            "compositeImage": {
                "id": "568c940b-86d3-4c88-84b6-9a8be89f0ee5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d48b9a42-4b0b-4718-9ec5-8fa7ef3c740e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb3b1777-88a5-4609-9eef-9574fb7fb82e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d48b9a42-4b0b-4718-9ec5-8fa7ef3c740e",
                    "LayerId": "13671322-5095-4237-b116-47803f25c790"
                }
            ]
        },
        {
            "id": "3f5c7e6b-7d04-472e-9bdb-5d22c99ba2f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4f833e6-fe97-439f-a75b-06961dd4a58d",
            "compositeImage": {
                "id": "b289d58a-33d4-4601-aacf-f94df14fcf67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f5c7e6b-7d04-472e-9bdb-5d22c99ba2f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "688d1ca5-98d9-485a-8b28-9a7a3aadc48f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f5c7e6b-7d04-472e-9bdb-5d22c99ba2f6",
                    "LayerId": "13671322-5095-4237-b116-47803f25c790"
                }
            ]
        },
        {
            "id": "1dbd2436-665c-4a45-97ce-0825daf26da1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4f833e6-fe97-439f-a75b-06961dd4a58d",
            "compositeImage": {
                "id": "99835043-8e69-4940-938a-04e37abd7392",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dbd2436-665c-4a45-97ce-0825daf26da1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84497c4e-be68-4a24-a2d8-1fbc040b9f53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dbd2436-665c-4a45-97ce-0825daf26da1",
                    "LayerId": "13671322-5095-4237-b116-47803f25c790"
                }
            ]
        },
        {
            "id": "2c86541f-c57a-4dc7-a37e-d1fe19a9e245",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4f833e6-fe97-439f-a75b-06961dd4a58d",
            "compositeImage": {
                "id": "797b14b0-c70c-4906-a06a-c236e931c366",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c86541f-c57a-4dc7-a37e-d1fe19a9e245",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05ae4b54-ca80-4afa-8994-58b64dd96581",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c86541f-c57a-4dc7-a37e-d1fe19a9e245",
                    "LayerId": "13671322-5095-4237-b116-47803f25c790"
                }
            ]
        },
        {
            "id": "6af11e6d-465b-455c-88c7-64f4b80abb31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4f833e6-fe97-439f-a75b-06961dd4a58d",
            "compositeImage": {
                "id": "4747bef2-d7d1-41b2-abcb-0f3d27a31c96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6af11e6d-465b-455c-88c7-64f4b80abb31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13cf00f0-26b6-4658-8c7c-64b504184d39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6af11e6d-465b-455c-88c7-64f4b80abb31",
                    "LayerId": "13671322-5095-4237-b116-47803f25c790"
                }
            ]
        },
        {
            "id": "c453d46b-aa69-4e32-a8fc-133b9cdebd69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4f833e6-fe97-439f-a75b-06961dd4a58d",
            "compositeImage": {
                "id": "203646c1-425d-4a54-8193-67456b19eda2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c453d46b-aa69-4e32-a8fc-133b9cdebd69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3db59f2-3bef-4a33-b52a-e98fd21ef284",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c453d46b-aa69-4e32-a8fc-133b9cdebd69",
                    "LayerId": "13671322-5095-4237-b116-47803f25c790"
                }
            ]
        },
        {
            "id": "cec1fc56-a834-4e9a-ab9c-f68f9991369c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4f833e6-fe97-439f-a75b-06961dd4a58d",
            "compositeImage": {
                "id": "da7458de-ffa1-460d-b5ba-e306292fd499",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cec1fc56-a834-4e9a-ab9c-f68f9991369c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "096cc303-8bc3-4ac0-ac88-a592b2fe0855",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cec1fc56-a834-4e9a-ab9c-f68f9991369c",
                    "LayerId": "13671322-5095-4237-b116-47803f25c790"
                }
            ]
        },
        {
            "id": "fb10a86a-3b08-44b6-9899-f8adeac18902",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4f833e6-fe97-439f-a75b-06961dd4a58d",
            "compositeImage": {
                "id": "3de6d240-9bdc-4e30-a7da-ed34ac32f41c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb10a86a-3b08-44b6-9899-f8adeac18902",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1f5146e-45d9-463f-a45c-b638a850338e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb10a86a-3b08-44b6-9899-f8adeac18902",
                    "LayerId": "13671322-5095-4237-b116-47803f25c790"
                }
            ]
        },
        {
            "id": "031d7de7-7d06-4fe8-88dc-226d6b8af492",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4f833e6-fe97-439f-a75b-06961dd4a58d",
            "compositeImage": {
                "id": "c9898c23-3c26-455e-82d7-bd9e5e518f70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "031d7de7-7d06-4fe8-88dc-226d6b8af492",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0430d1b2-236d-4ef3-b883-6558a91d23b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "031d7de7-7d06-4fe8-88dc-226d6b8af492",
                    "LayerId": "13671322-5095-4237-b116-47803f25c790"
                }
            ]
        },
        {
            "id": "4a84619d-4ee5-4416-b6b8-6856f2d7ba36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4f833e6-fe97-439f-a75b-06961dd4a58d",
            "compositeImage": {
                "id": "2e270854-bcc2-4c90-87de-1c639407d9c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a84619d-4ee5-4416-b6b8-6856f2d7ba36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55b5e2df-5b7c-4d80-b4c1-a8aa37e13e37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a84619d-4ee5-4416-b6b8-6856f2d7ba36",
                    "LayerId": "13671322-5095-4237-b116-47803f25c790"
                }
            ]
        },
        {
            "id": "9790ca92-559e-43cd-99ee-ac9b99473ac2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4f833e6-fe97-439f-a75b-06961dd4a58d",
            "compositeImage": {
                "id": "47e13d9e-5bf7-491b-836e-15920b86abf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9790ca92-559e-43cd-99ee-ac9b99473ac2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26d623d3-cc4f-41a5-890b-d2f966ea81d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9790ca92-559e-43cd-99ee-ac9b99473ac2",
                    "LayerId": "13671322-5095-4237-b116-47803f25c790"
                }
            ]
        },
        {
            "id": "2e17cd45-6175-44da-8d07-8d40dae43538",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4f833e6-fe97-439f-a75b-06961dd4a58d",
            "compositeImage": {
                "id": "4bf7ec27-8eca-430d-aa93-b1d2152ab719",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e17cd45-6175-44da-8d07-8d40dae43538",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a298946d-66a4-4a67-aa7a-bf6742ec7e95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e17cd45-6175-44da-8d07-8d40dae43538",
                    "LayerId": "13671322-5095-4237-b116-47803f25c790"
                }
            ]
        },
        {
            "id": "d7c0a80c-d85f-41f4-9eba-3a64ad74330a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4f833e6-fe97-439f-a75b-06961dd4a58d",
            "compositeImage": {
                "id": "a43e7b06-752a-433b-a23c-f8aba24e6da1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7c0a80c-d85f-41f4-9eba-3a64ad74330a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43921d48-af2a-462a-a6e2-029727febd58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7c0a80c-d85f-41f4-9eba-3a64ad74330a",
                    "LayerId": "13671322-5095-4237-b116-47803f25c790"
                }
            ]
        },
        {
            "id": "ed18d9c1-7ee1-4547-b3df-87d907095831",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4f833e6-fe97-439f-a75b-06961dd4a58d",
            "compositeImage": {
                "id": "43ad9e94-e45f-4472-b5be-90ef82ebcf2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed18d9c1-7ee1-4547-b3df-87d907095831",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9165f7ad-a816-445f-8a78-c866565aaaa2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed18d9c1-7ee1-4547-b3df-87d907095831",
                    "LayerId": "13671322-5095-4237-b116-47803f25c790"
                }
            ]
        },
        {
            "id": "974769ec-b5dc-4e7e-8690-7967e6e28902",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4f833e6-fe97-439f-a75b-06961dd4a58d",
            "compositeImage": {
                "id": "3d71bf87-fcf4-49ff-aef7-775611316edf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "974769ec-b5dc-4e7e-8690-7967e6e28902",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fc19b48-d764-4c7e-a075-7fb2d0d8885e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "974769ec-b5dc-4e7e-8690-7967e6e28902",
                    "LayerId": "13671322-5095-4237-b116-47803f25c790"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 75,
    "layers": [
        {
            "id": "13671322-5095-4237-b116-47803f25c790",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f4f833e6-fe97-439f-a75b-06961dd4a58d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 75,
    "xorig": 0,
    "yorig": 0
}