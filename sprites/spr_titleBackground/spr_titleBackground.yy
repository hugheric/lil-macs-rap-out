{
    "id": "7ea221dd-46ac-4445-8c09-807fff25e71c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_titleBackground",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f17ba793-a2e2-4ddc-98be-fad391bc4819",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ea221dd-46ac-4445-8c09-807fff25e71c",
            "compositeImage": {
                "id": "ea0fd9c0-ad5f-4c50-a12c-ee409110cff5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f17ba793-a2e2-4ddc-98be-fad391bc4819",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "034b3431-d05c-491f-bb57-f4101175aad7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f17ba793-a2e2-4ddc-98be-fad391bc4819",
                    "LayerId": "93d66c30-a509-4395-877a-d8ded4921b5d"
                }
            ]
        },
        {
            "id": "f5d755c4-58e2-4799-8104-ee17971a6541",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ea221dd-46ac-4445-8c09-807fff25e71c",
            "compositeImage": {
                "id": "1318fbe1-6620-4631-8ef3-01c9750dd952",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5d755c4-58e2-4799-8104-ee17971a6541",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a833e6f-fc03-4648-b57c-4fbdf5a8fd23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5d755c4-58e2-4799-8104-ee17971a6541",
                    "LayerId": "93d66c30-a509-4395-877a-d8ded4921b5d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "93d66c30-a509-4395-877a-d8ded4921b5d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7ea221dd-46ac-4445-8c09-807fff25e71c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}