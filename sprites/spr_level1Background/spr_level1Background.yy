{
    "id": "c696ab70-4992-48cd-8364-cde12704f842",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_level1Background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ee5ed15b-bef0-40a2-91c9-e226d3f1dc64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c696ab70-4992-48cd-8364-cde12704f842",
            "compositeImage": {
                "id": "17a07d65-5333-4bd5-99d5-d58e6b1d5581",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee5ed15b-bef0-40a2-91c9-e226d3f1dc64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3bde5ad-c1b0-411c-8b70-70ba8b6bc192",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee5ed15b-bef0-40a2-91c9-e226d3f1dc64",
                    "LayerId": "b7cab589-3d7c-4b0f-9a1e-afb825728da7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "b7cab589-3d7c-4b0f-9a1e-afb825728da7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c696ab70-4992-48cd-8364-cde12704f842",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}