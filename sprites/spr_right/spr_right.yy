{
    "id": "678c03b1-4cdc-45b6-b5a0-743324edc0ea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "badedbb1-acea-4b0a-b419-b8ff8eda4274",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678c03b1-4cdc-45b6-b5a0-743324edc0ea",
            "compositeImage": {
                "id": "4f3dbfff-eb92-4b19-b553-c49b15fc598c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "badedbb1-acea-4b0a-b419-b8ff8eda4274",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bf39437-0532-4c64-8e43-dd129a8c2561",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "badedbb1-acea-4b0a-b419-b8ff8eda4274",
                    "LayerId": "a3769540-aadb-44d4-8173-d61b2137c5f1"
                }
            ]
        },
        {
            "id": "5e8277cd-502b-497f-ab29-83584d5bf81b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678c03b1-4cdc-45b6-b5a0-743324edc0ea",
            "compositeImage": {
                "id": "6a2210c7-a9b6-4d0f-80c1-42fc09c21acd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e8277cd-502b-497f-ab29-83584d5bf81b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b3225ca-0d47-477b-96b4-15b02e43ce15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e8277cd-502b-497f-ab29-83584d5bf81b",
                    "LayerId": "a3769540-aadb-44d4-8173-d61b2137c5f1"
                }
            ]
        },
        {
            "id": "f69e7ba4-9134-46c1-9b6f-dd9b2f0e36a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678c03b1-4cdc-45b6-b5a0-743324edc0ea",
            "compositeImage": {
                "id": "a3793d2a-632f-4237-aef3-cde089f7263b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f69e7ba4-9134-46c1-9b6f-dd9b2f0e36a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7816a84-8401-4766-b875-5c560f212f55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f69e7ba4-9134-46c1-9b6f-dd9b2f0e36a7",
                    "LayerId": "a3769540-aadb-44d4-8173-d61b2137c5f1"
                }
            ]
        },
        {
            "id": "81eae736-dda6-47ae-a3fd-1f95fe1c3388",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678c03b1-4cdc-45b6-b5a0-743324edc0ea",
            "compositeImage": {
                "id": "111ac849-1144-47a8-9413-51ca5b7fc824",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81eae736-dda6-47ae-a3fd-1f95fe1c3388",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b378a51-e210-4605-b63d-3d54d22a5b15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81eae736-dda6-47ae-a3fd-1f95fe1c3388",
                    "LayerId": "a3769540-aadb-44d4-8173-d61b2137c5f1"
                }
            ]
        },
        {
            "id": "70058785-2c69-48ef-971c-c4c39dcfc16f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678c03b1-4cdc-45b6-b5a0-743324edc0ea",
            "compositeImage": {
                "id": "a2747e81-d5d3-43f9-9b6f-4f2e9129c2c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70058785-2c69-48ef-971c-c4c39dcfc16f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "140558f3-a2ac-4e41-a1fc-244a6421e5d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70058785-2c69-48ef-971c-c4c39dcfc16f",
                    "LayerId": "a3769540-aadb-44d4-8173-d61b2137c5f1"
                }
            ]
        },
        {
            "id": "a770996e-ae5c-4c98-8fc0-b44d5c7561f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678c03b1-4cdc-45b6-b5a0-743324edc0ea",
            "compositeImage": {
                "id": "6dfa3b43-cf92-41e2-8788-b1cfa49d7987",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a770996e-ae5c-4c98-8fc0-b44d5c7561f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a55652cc-bb35-4a55-b610-d946ee3e6a50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a770996e-ae5c-4c98-8fc0-b44d5c7561f5",
                    "LayerId": "a3769540-aadb-44d4-8173-d61b2137c5f1"
                }
            ]
        },
        {
            "id": "77dee1ce-c571-4b88-9cda-a715a41ee27b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678c03b1-4cdc-45b6-b5a0-743324edc0ea",
            "compositeImage": {
                "id": "30a5dd12-da4b-4188-b90e-d274772a16ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77dee1ce-c571-4b88-9cda-a715a41ee27b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7551be88-2cbc-4dd8-942b-b89f2302e33e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77dee1ce-c571-4b88-9cda-a715a41ee27b",
                    "LayerId": "a3769540-aadb-44d4-8173-d61b2137c5f1"
                }
            ]
        },
        {
            "id": "0dcb1e13-d05d-4199-892a-a61bcfdf083f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678c03b1-4cdc-45b6-b5a0-743324edc0ea",
            "compositeImage": {
                "id": "8b8ec144-3bcf-4e7c-8835-2acd15de821d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dcb1e13-d05d-4199-892a-a61bcfdf083f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22bbc5c4-a21d-49fc-a776-fd9d5533a677",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dcb1e13-d05d-4199-892a-a61bcfdf083f",
                    "LayerId": "a3769540-aadb-44d4-8173-d61b2137c5f1"
                }
            ]
        },
        {
            "id": "6adeeae1-4408-4535-9c09-37c5fef6889f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678c03b1-4cdc-45b6-b5a0-743324edc0ea",
            "compositeImage": {
                "id": "485321e4-9193-406b-9cd3-a67d5c94ac1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6adeeae1-4408-4535-9c09-37c5fef6889f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1629cac6-5b95-467d-ab60-a0461e9c8241",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6adeeae1-4408-4535-9c09-37c5fef6889f",
                    "LayerId": "a3769540-aadb-44d4-8173-d61b2137c5f1"
                }
            ]
        },
        {
            "id": "afb85048-e3c2-4dc3-ae7a-544a755c2113",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678c03b1-4cdc-45b6-b5a0-743324edc0ea",
            "compositeImage": {
                "id": "fa2b851d-3aa3-4b0e-944a-aec93a5244a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afb85048-e3c2-4dc3-ae7a-544a755c2113",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab1cf5e7-5096-47e2-959a-b05e34f8f669",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afb85048-e3c2-4dc3-ae7a-544a755c2113",
                    "LayerId": "a3769540-aadb-44d4-8173-d61b2137c5f1"
                }
            ]
        },
        {
            "id": "01311c80-662e-46c1-83c1-dc5e03a856e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678c03b1-4cdc-45b6-b5a0-743324edc0ea",
            "compositeImage": {
                "id": "61cef1d6-660f-4bbf-b282-c6e309435e2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01311c80-662e-46c1-83c1-dc5e03a856e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "288fc7cc-ea9b-4ef5-ba67-96d073d1223d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01311c80-662e-46c1-83c1-dc5e03a856e2",
                    "LayerId": "a3769540-aadb-44d4-8173-d61b2137c5f1"
                }
            ]
        },
        {
            "id": "1c2ff634-a89a-4aef-a768-7e0c05c97a92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678c03b1-4cdc-45b6-b5a0-743324edc0ea",
            "compositeImage": {
                "id": "b9b89987-c087-4854-b26e-ce8f907db532",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c2ff634-a89a-4aef-a768-7e0c05c97a92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fb0024e-38a6-41ac-9ea3-f2a3c67b705e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c2ff634-a89a-4aef-a768-7e0c05c97a92",
                    "LayerId": "a3769540-aadb-44d4-8173-d61b2137c5f1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "a3769540-aadb-44d4-8173-d61b2137c5f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "678c03b1-4cdc-45b6-b5a0-743324edc0ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 0
}