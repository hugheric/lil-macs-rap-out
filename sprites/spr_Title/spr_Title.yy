{
    "id": "ce8395cb-77be-46aa-8718-77bf0a1793b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_title",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1eea3c30-f00a-4277-b8e9-cb1003ce97f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce8395cb-77be-46aa-8718-77bf0a1793b0",
            "compositeImage": {
                "id": "386ef06b-0169-4834-a68d-599c3bef7550",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1eea3c30-f00a-4277-b8e9-cb1003ce97f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8485f0b3-6739-4261-83e5-8c3fad369ba1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1eea3c30-f00a-4277-b8e9-cb1003ce97f3",
                    "LayerId": "d2022b79-ae06-48c5-9922-51304dd0685b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "d2022b79-ae06-48c5-9922-51304dd0685b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ce8395cb-77be-46aa-8718-77bf0a1793b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 45,
    "yorig": 27
}