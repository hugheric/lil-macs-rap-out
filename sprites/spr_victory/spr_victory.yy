{
    "id": "279cdfcf-270b-4f59-a7ff-b1f98a59402c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_victory",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5e4c7d52-069b-4a03-b06a-4ecfc2170fb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "279cdfcf-270b-4f59-a7ff-b1f98a59402c",
            "compositeImage": {
                "id": "0c2b4126-df77-4b9f-ba5c-64d144c8fcd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e4c7d52-069b-4a03-b06a-4ecfc2170fb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d55def8-f46e-43ef-b75e-5d89a5faa499",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e4c7d52-069b-4a03-b06a-4ecfc2170fb4",
                    "LayerId": "dd157c8a-3dcd-4759-8133-d0cd1ffcf2e9"
                }
            ]
        },
        {
            "id": "90acfc08-df02-429f-a999-fb2804670157",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "279cdfcf-270b-4f59-a7ff-b1f98a59402c",
            "compositeImage": {
                "id": "0fc7173c-6940-46df-b7ae-ce8614f16438",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90acfc08-df02-429f-a999-fb2804670157",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5820aa8e-ba94-4287-8459-e40c3955798c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90acfc08-df02-429f-a999-fb2804670157",
                    "LayerId": "dd157c8a-3dcd-4759-8133-d0cd1ffcf2e9"
                }
            ]
        },
        {
            "id": "5b13f364-2bb7-4610-a2a9-530ae079655d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "279cdfcf-270b-4f59-a7ff-b1f98a59402c",
            "compositeImage": {
                "id": "4b0cc44a-1a39-479c-944f-965a61dcd802",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b13f364-2bb7-4610-a2a9-530ae079655d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c22a3d63-bc07-4d2b-948e-f76d9667fa2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b13f364-2bb7-4610-a2a9-530ae079655d",
                    "LayerId": "dd157c8a-3dcd-4759-8133-d0cd1ffcf2e9"
                }
            ]
        },
        {
            "id": "37251580-4995-45e6-8a63-da93ae01e7d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "279cdfcf-270b-4f59-a7ff-b1f98a59402c",
            "compositeImage": {
                "id": "9935b7cd-17c9-4d1d-8a8e-5d3da707078a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37251580-4995-45e6-8a63-da93ae01e7d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6de0bbc-119b-4298-92c4-81c5e7755745",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37251580-4995-45e6-8a63-da93ae01e7d3",
                    "LayerId": "dd157c8a-3dcd-4759-8133-d0cd1ffcf2e9"
                }
            ]
        },
        {
            "id": "1d976b14-ed66-416a-a977-8e0835d0f6e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "279cdfcf-270b-4f59-a7ff-b1f98a59402c",
            "compositeImage": {
                "id": "0103f504-fcf6-4685-babe-ff36ac22fa2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d976b14-ed66-416a-a977-8e0835d0f6e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66312e8e-5270-40b7-bca5-9cca68c68b7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d976b14-ed66-416a-a977-8e0835d0f6e8",
                    "LayerId": "dd157c8a-3dcd-4759-8133-d0cd1ffcf2e9"
                }
            ]
        },
        {
            "id": "7742b800-0271-49b3-b68b-5bb0b5e37a38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "279cdfcf-270b-4f59-a7ff-b1f98a59402c",
            "compositeImage": {
                "id": "fbc4a8ad-d1dc-479b-b119-73af25ca9bf5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7742b800-0271-49b3-b68b-5bb0b5e37a38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b69527e7-5ed6-4b9c-81fa-757b49e7ebc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7742b800-0271-49b3-b68b-5bb0b5e37a38",
                    "LayerId": "dd157c8a-3dcd-4759-8133-d0cd1ffcf2e9"
                }
            ]
        },
        {
            "id": "131cd105-5d47-44a3-905b-5b20ea8f9b24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "279cdfcf-270b-4f59-a7ff-b1f98a59402c",
            "compositeImage": {
                "id": "0628b842-e960-4d8d-b889-df8202970a2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "131cd105-5d47-44a3-905b-5b20ea8f9b24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3348b8a4-10b0-4b3e-87d9-73255147326f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "131cd105-5d47-44a3-905b-5b20ea8f9b24",
                    "LayerId": "dd157c8a-3dcd-4759-8133-d0cd1ffcf2e9"
                }
            ]
        },
        {
            "id": "8737fa34-fff9-47a2-aef7-55a7359af57b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "279cdfcf-270b-4f59-a7ff-b1f98a59402c",
            "compositeImage": {
                "id": "de5a169b-a769-4595-9201-0be71125d7ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8737fa34-fff9-47a2-aef7-55a7359af57b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe31d1d9-eb40-46c2-826e-af64faa9ef03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8737fa34-fff9-47a2-aef7-55a7359af57b",
                    "LayerId": "dd157c8a-3dcd-4759-8133-d0cd1ffcf2e9"
                }
            ]
        },
        {
            "id": "a5826ddc-f846-45c7-9f45-0a47a8906733",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "279cdfcf-270b-4f59-a7ff-b1f98a59402c",
            "compositeImage": {
                "id": "e4a384a1-6174-4c9b-937b-188f8c023436",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5826ddc-f846-45c7-9f45-0a47a8906733",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2a94df9-f471-4a15-aa7f-50b66be33174",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5826ddc-f846-45c7-9f45-0a47a8906733",
                    "LayerId": "dd157c8a-3dcd-4759-8133-d0cd1ffcf2e9"
                }
            ]
        },
        {
            "id": "73cf2740-a05a-474b-b8da-0a98224d0669",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "279cdfcf-270b-4f59-a7ff-b1f98a59402c",
            "compositeImage": {
                "id": "85995dd5-c9fc-4ea5-8b70-1b03575ab9b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73cf2740-a05a-474b-b8da-0a98224d0669",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fec6e55c-fedc-4cdb-80a4-0e67d3d66591",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73cf2740-a05a-474b-b8da-0a98224d0669",
                    "LayerId": "dd157c8a-3dcd-4759-8133-d0cd1ffcf2e9"
                }
            ]
        },
        {
            "id": "c993296d-bf83-44e6-b239-0a854b0d2ee2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "279cdfcf-270b-4f59-a7ff-b1f98a59402c",
            "compositeImage": {
                "id": "242f428a-1ec1-4f28-a0fc-ea2fa1051d75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c993296d-bf83-44e6-b239-0a854b0d2ee2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b4264b3-81df-4c86-9273-1a7741fe193c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c993296d-bf83-44e6-b239-0a854b0d2ee2",
                    "LayerId": "dd157c8a-3dcd-4759-8133-d0cd1ffcf2e9"
                }
            ]
        },
        {
            "id": "c13384a4-7252-4381-bfad-a3cdc7dea911",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "279cdfcf-270b-4f59-a7ff-b1f98a59402c",
            "compositeImage": {
                "id": "55dba8f5-341a-45ab-bc19-9c783d331400",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c13384a4-7252-4381-bfad-a3cdc7dea911",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d2f90a9-fa6c-4004-ac3e-874f764c420d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c13384a4-7252-4381-bfad-a3cdc7dea911",
                    "LayerId": "dd157c8a-3dcd-4759-8133-d0cd1ffcf2e9"
                }
            ]
        },
        {
            "id": "7f5b19df-082d-41a8-a645-13d58faafdd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "279cdfcf-270b-4f59-a7ff-b1f98a59402c",
            "compositeImage": {
                "id": "fd5db8d6-f1c7-49cd-8458-a1f17fab7602",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f5b19df-082d-41a8-a645-13d58faafdd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95ab93e6-780c-456e-9d93-1715ba193a1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f5b19df-082d-41a8-a645-13d58faafdd3",
                    "LayerId": "dd157c8a-3dcd-4759-8133-d0cd1ffcf2e9"
                }
            ]
        },
        {
            "id": "84d67103-e768-457e-9b76-cc30527581c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "279cdfcf-270b-4f59-a7ff-b1f98a59402c",
            "compositeImage": {
                "id": "67ecf02d-fd16-442e-a97c-c5831f5ebcdc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84d67103-e768-457e-9b76-cc30527581c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1720001-e0d8-4d28-a43a-1134a1d255f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84d67103-e768-457e-9b76-cc30527581c9",
                    "LayerId": "dd157c8a-3dcd-4759-8133-d0cd1ffcf2e9"
                }
            ]
        },
        {
            "id": "6e382363-e5f2-42db-a620-9185211b1528",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "279cdfcf-270b-4f59-a7ff-b1f98a59402c",
            "compositeImage": {
                "id": "afddf598-608b-49e7-91a1-b203c9ca4bb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e382363-e5f2-42db-a620-9185211b1528",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bed83455-4040-45db-b0f5-b705f76fabef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e382363-e5f2-42db-a620-9185211b1528",
                    "LayerId": "dd157c8a-3dcd-4759-8133-d0cd1ffcf2e9"
                }
            ]
        },
        {
            "id": "cd4f83dd-17ce-4be4-b22a-21f00fcab5b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "279cdfcf-270b-4f59-a7ff-b1f98a59402c",
            "compositeImage": {
                "id": "04976602-fb00-42ee-b622-89f0b9f04d83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd4f83dd-17ce-4be4-b22a-21f00fcab5b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91992f7a-d6c0-455a-bc82-b29460811408",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd4f83dd-17ce-4be4-b22a-21f00fcab5b1",
                    "LayerId": "dd157c8a-3dcd-4759-8133-d0cd1ffcf2e9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "dd157c8a-3dcd-4759-8133-d0cd1ffcf2e9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "279cdfcf-270b-4f59-a7ff-b1f98a59402c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}