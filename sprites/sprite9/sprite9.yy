{
    "id": "68304b57-d316-458f-b0ae-96b6d10a7720",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite9",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0e971b28-0785-426f-9c8e-fb452d0980dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68304b57-d316-458f-b0ae-96b6d10a7720",
            "compositeImage": {
                "id": "b4beb9cd-ce82-4f2f-8dd7-023e42499fe3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e971b28-0785-426f-9c8e-fb452d0980dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33718524-889a-4687-b7cf-b710ec6b3525",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e971b28-0785-426f-9c8e-fb452d0980dc",
                    "LayerId": "fc0b902f-9382-4139-ad9b-3a44721698e2"
                }
            ]
        },
        {
            "id": "52eb3b13-e707-45e3-8984-298b05c7b887",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68304b57-d316-458f-b0ae-96b6d10a7720",
            "compositeImage": {
                "id": "bafef41d-79f2-47cf-a51c-c70dcb79f6c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52eb3b13-e707-45e3-8984-298b05c7b887",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "091fb8d8-7aa3-48e0-a3e7-cb197e855563",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52eb3b13-e707-45e3-8984-298b05c7b887",
                    "LayerId": "fc0b902f-9382-4139-ad9b-3a44721698e2"
                }
            ]
        },
        {
            "id": "677c551a-a227-4796-9d84-5c26dec81e86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68304b57-d316-458f-b0ae-96b6d10a7720",
            "compositeImage": {
                "id": "8bf5ff21-712f-4857-ad09-5091dc3dabea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "677c551a-a227-4796-9d84-5c26dec81e86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a027f74-234c-4708-a147-606391fa0af7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "677c551a-a227-4796-9d84-5c26dec81e86",
                    "LayerId": "fc0b902f-9382-4139-ad9b-3a44721698e2"
                }
            ]
        },
        {
            "id": "8674d234-e86b-4f52-a904-bcd480b668c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68304b57-d316-458f-b0ae-96b6d10a7720",
            "compositeImage": {
                "id": "f36bd56a-a3e6-48b9-aea6-2ea47df228f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8674d234-e86b-4f52-a904-bcd480b668c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2eca0d89-49f3-441d-a211-05b2fb21368c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8674d234-e86b-4f52-a904-bcd480b668c3",
                    "LayerId": "fc0b902f-9382-4139-ad9b-3a44721698e2"
                }
            ]
        },
        {
            "id": "ad00464f-e26c-469f-92d8-933c3d660bf5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68304b57-d316-458f-b0ae-96b6d10a7720",
            "compositeImage": {
                "id": "b81fd7de-3a46-4f09-a7e0-1499fa76344e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad00464f-e26c-469f-92d8-933c3d660bf5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5bfdd6e-f7bc-47c2-8c03-aff99d23b36b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad00464f-e26c-469f-92d8-933c3d660bf5",
                    "LayerId": "fc0b902f-9382-4139-ad9b-3a44721698e2"
                }
            ]
        },
        {
            "id": "61d5809e-6522-4de4-b04c-db8ce7b922a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68304b57-d316-458f-b0ae-96b6d10a7720",
            "compositeImage": {
                "id": "a5c97c7d-8f69-41ca-b7ba-1fb4318fde22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61d5809e-6522-4de4-b04c-db8ce7b922a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "030672d5-8f70-4b51-baff-f1493f50edc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61d5809e-6522-4de4-b04c-db8ce7b922a9",
                    "LayerId": "fc0b902f-9382-4139-ad9b-3a44721698e2"
                }
            ]
        },
        {
            "id": "e4f60667-b3a8-474d-89cf-2861791eddbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68304b57-d316-458f-b0ae-96b6d10a7720",
            "compositeImage": {
                "id": "5905510d-72cd-4100-a38f-d7c88a8b6344",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4f60667-b3a8-474d-89cf-2861791eddbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6f3e38e-48c7-4b8b-8a64-3dc68d6222fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4f60667-b3a8-474d-89cf-2861791eddbc",
                    "LayerId": "fc0b902f-9382-4139-ad9b-3a44721698e2"
                }
            ]
        },
        {
            "id": "929d56f1-ebac-4327-84d0-104f5aa2ba13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68304b57-d316-458f-b0ae-96b6d10a7720",
            "compositeImage": {
                "id": "2bab4cdd-630e-433f-ac57-748700990a19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "929d56f1-ebac-4327-84d0-104f5aa2ba13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08bc3214-f9b8-4faf-8690-e313936c143b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "929d56f1-ebac-4327-84d0-104f5aa2ba13",
                    "LayerId": "fc0b902f-9382-4139-ad9b-3a44721698e2"
                }
            ]
        },
        {
            "id": "abfdd5b7-890b-4c8d-a504-0183e0e01885",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68304b57-d316-458f-b0ae-96b6d10a7720",
            "compositeImage": {
                "id": "67b03d59-37a2-4de7-938d-c69751cafab9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abfdd5b7-890b-4c8d-a504-0183e0e01885",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60bd4b74-04c4-4f3c-8431-32fd7997bd73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abfdd5b7-890b-4c8d-a504-0183e0e01885",
                    "LayerId": "fc0b902f-9382-4139-ad9b-3a44721698e2"
                }
            ]
        },
        {
            "id": "95d3dcd6-1bc1-487e-bf94-ae0bc8174bc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68304b57-d316-458f-b0ae-96b6d10a7720",
            "compositeImage": {
                "id": "66d34898-d414-4a67-a485-062d01f86035",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95d3dcd6-1bc1-487e-bf94-ae0bc8174bc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d2efff5-eb5d-4986-9fcd-fc3f106b62eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95d3dcd6-1bc1-487e-bf94-ae0bc8174bc8",
                    "LayerId": "fc0b902f-9382-4139-ad9b-3a44721698e2"
                }
            ]
        },
        {
            "id": "d65e693e-2785-4d48-a220-5b947f8707d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68304b57-d316-458f-b0ae-96b6d10a7720",
            "compositeImage": {
                "id": "9359c3da-0782-4288-b1af-f35da9e9bdb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d65e693e-2785-4d48-a220-5b947f8707d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ba038d8-f79a-4cea-8b80-0aaa0bdc4660",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d65e693e-2785-4d48-a220-5b947f8707d7",
                    "LayerId": "fc0b902f-9382-4139-ad9b-3a44721698e2"
                }
            ]
        },
        {
            "id": "6b24a6dc-3ee3-47ff-bf98-ef5033206bc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68304b57-d316-458f-b0ae-96b6d10a7720",
            "compositeImage": {
                "id": "55729496-fdde-4ec3-8975-b1e0714be835",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b24a6dc-3ee3-47ff-bf98-ef5033206bc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fea21d7-fcca-46ac-a7e3-d043cf66b3fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b24a6dc-3ee3-47ff-bf98-ef5033206bc9",
                    "LayerId": "fc0b902f-9382-4139-ad9b-3a44721698e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "fc0b902f-9382-4139-ad9b-3a44721698e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "68304b57-d316-458f-b0ae-96b6d10a7720",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": -32,
    "yorig": 59
}