{
    "id": "86d36553-6e1f-4ad1-9a6c-6f4047a33c8e",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font3",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Anime Ace",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "19df8be5-c5a9-44d1-97af-b7c0ff3a68a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 44,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 44,
                "y": 232
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "cefba6dd-dbc8-4317-ae52-eae18ab167e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 44,
                "offset": 2,
                "shift": 15,
                "w": 14,
                "x": 62,
                "y": 232
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "d3cceeb5-37c4-4ec7-bc50-cfeada44c069",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 44,
                "offset": 3,
                "shift": 18,
                "w": 13,
                "x": 94,
                "y": 232
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "ca5a2c7a-3688-4801-beb2-6fa7fbaf8e40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 44,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 438,
                "y": 140
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "a8a85be4-7b84-493a-b109-ac3c1653e00a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 44,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 341,
                "y": 94
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "ae0ac45c-48e3-432e-baf8-c7726488feb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 44,
                "offset": 2,
                "shift": 35,
                "w": 32,
                "x": 2,
                "y": 48
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "caf751dc-1ddc-4e7f-baf6-49cdb5353adc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 44,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 2,
                "y": 186
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "98dd3f30-49ff-45cc-a3dc-dcf047a62211",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 44,
                "offset": 3,
                "shift": 13,
                "w": 10,
                "x": 134,
                "y": 232
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "870c620f-602a-496a-9edf-c7885bd38b66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 44,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 472,
                "y": 186
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "6c4ef5d2-d8fa-4db4-9f19-79bc4cd69ed3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 44,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 450,
                "y": 186
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "482660e9-2a21-4bce-9f26-20e51df232b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 44,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 78,
                "y": 232
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "ae04f2b6-3dd8-4aa4-9417-a74dba8586c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 44,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 379,
                "y": 186
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "60e4caf5-5abc-46fd-804f-7f208702cef6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 44,
                "offset": 3,
                "shift": 15,
                "w": 9,
                "x": 146,
                "y": 232
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "f12615cd-fe84-46a1-8c5b-49fa1eb08af3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 44,
                "offset": 2,
                "shift": 28,
                "w": 23,
                "x": 354,
                "y": 186
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "67488606-1311-4290-b5e2-2b8d9548235e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 44,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 176,
                "y": 232
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "a3ec9d2e-079f-42b4-8a06-b28cb232f1a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 44,
                "offset": 1,
                "shift": 27,
                "w": 27,
                "x": 295,
                "y": 140
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "9c06069f-edeb-4941-bc03-a9352af44036",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 44,
                "offset": 2,
                "shift": 41,
                "w": 36,
                "x": 236,
                "y": 2
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "ab8940f4-b054-45e5-86b2-9154f944bda6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 44,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 109,
                "y": 232
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "31373718-671d-48aa-9271-25c56d53ef5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 44,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 371,
                "y": 94
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "f5e3a375-80d9-400c-8ee0-39dd8ad6e2a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 44,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 410,
                "y": 140
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "69291431-852b-479d-97a5-c5d0c5e57ecd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 44,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 401,
                "y": 94
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "f02d3657-d364-4775-a493-9739cd237641",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 44,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 303,
                "y": 186
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "5b991e91-983b-4a25-93b0-9b52b3cf0f07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 44,
                "offset": 2,
                "shift": 33,
                "w": 30,
                "x": 327,
                "y": 48
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "c1a589b5-ea19-4c32-873d-34e34fc59179",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 44,
                "offset": 1,
                "shift": 30,
                "w": 27,
                "x": 150,
                "y": 140
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "8213f2fb-cf39-4e14-a805-edfb4b395c15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 44,
                "offset": 2,
                "shift": 32,
                "w": 29,
                "x": 188,
                "y": 94
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "e0d89443-8e19-4cb7-ac65-2c3715416924",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 44,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 157,
                "y": 94
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "8e95f638-c488-4b9b-b947-45bf5d775bc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 44,
                "offset": 4,
                "shift": 13,
                "w": 7,
                "x": 167,
                "y": 232
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "a5495523-d1f8-40a5-87b5-0f19b30f49d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 44,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 122,
                "y": 232
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "7aa917e0-30f0-435f-888d-d26cd1e2d259",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 44,
                "offset": 0,
                "shift": 28,
                "w": 28,
                "x": 2,
                "y": 140
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "e4f07ea3-6852-4ef7-9d25-c8e838721bbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 44,
                "offset": 2,
                "shift": 30,
                "w": 25,
                "x": 222,
                "y": 186
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "7fce92df-87f1-43a1-b357-6c891d1fac44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 44,
                "offset": 1,
                "shift": 30,
                "w": 28,
                "x": 461,
                "y": 94
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "541e557d-8b26-4cf7-a498-5ebc860d815c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 44,
                "offset": 2,
                "shift": 31,
                "w": 28,
                "x": 431,
                "y": 94
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "493f97b1-d89a-4f5a-87c7-6639069b91e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 44,
                "offset": 2,
                "shift": 39,
                "w": 35,
                "x": 350,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "9ef911af-7940-4807-b58b-24f9846ff2d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 44,
                "offset": 1,
                "shift": 33,
                "w": 31,
                "x": 102,
                "y": 48
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "0df792cf-fae8-402d-b919-47be224ad205",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 44,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 2,
                "y": 94
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "9eb38983-cd8f-49f2-a70f-9d4ef12ac39b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 44,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 453,
                "y": 48
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "3f307151-14a0-4295-b74a-30950d181aea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 44,
                "offset": 2,
                "shift": 34,
                "w": 30,
                "x": 295,
                "y": 48
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "2c5361d8-3572-4cea-97e7-7b1559d7f3f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 44,
                "offset": 1,
                "shift": 29,
                "w": 28,
                "x": 62,
                "y": 140
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "5a60ff4d-9c75-49c7-b9ce-367c9d2220e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 44,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 237,
                "y": 140
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "ff46f164-cdaf-4313-abd0-8a5c43419cd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 44,
                "offset": 2,
                "shift": 36,
                "w": 33,
                "x": 387,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "a1ea5c4f-c80d-45cd-8e00-a80064988843",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 44,
                "offset": 2,
                "shift": 29,
                "w": 27,
                "x": 324,
                "y": 140
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "2e4ddfd0-a3c6-4944-bef7-e2f431cfe6c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 44,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 404,
                "y": 186
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "f47a659d-794f-414f-b98b-e970ced624f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 44,
                "offset": 2,
                "shift": 34,
                "w": 30,
                "x": 231,
                "y": 48
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "0d545212-6455-4245-a715-06d58c59ee54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 44,
                "offset": 1,
                "shift": 28,
                "w": 27,
                "x": 353,
                "y": 140
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "4dd49fc3-5618-4f54-a009-5dda1e46b89b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 44,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 141,
                "y": 186
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "311a2161-d2ad-4d0a-8f6c-8b88671e5809",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 44,
                "offset": 1,
                "shift": 40,
                "w": 38,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "987e4b20-d015-41c9-9010-440fd5a09a43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 44,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 311,
                "y": 94
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "2804c31e-08ce-4d4d-a7bf-be40da42095a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 44,
                "offset": 2,
                "shift": 41,
                "w": 36,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "a6cac01b-6358-4778-9231-37f26e79fad4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 44,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 266,
                "y": 140
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "fd5880f0-b432-46e9-b73c-465d831bc0be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 44,
                "offset": 2,
                "shift": 39,
                "w": 36,
                "x": 274,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "fd9208f4-d832-46e7-94a5-240977bd1747",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 44,
                "offset": 0,
                "shift": 30,
                "w": 29,
                "x": 391,
                "y": 48
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "05906426-7774-45f7-a397-da7403e37fa3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 44,
                "offset": 2,
                "shift": 34,
                "w": 31,
                "x": 36,
                "y": 48
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "df95dbfe-eee8-484b-ab7c-7ada65464c69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 44,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 249,
                "y": 186
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "adfbc7ac-3640-4bc6-b8a2-24fca7fd14af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 44,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 58,
                "y": 186
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "e9d70853-6093-4f4a-8251-868df479d478",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 44,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 195,
                "y": 186
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "cc5777ff-4882-49b5-87c7-25536fb640d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 44,
                "offset": 2,
                "shift": 41,
                "w": 37,
                "x": 121,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "4967dcae-0f5f-4d44-9676-daca5b831c0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 44,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 30,
                "y": 186
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "d91a24ad-6e4c-4b58-84e6-034686ac2fc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 44,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 126,
                "y": 94
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "be0bb0d6-cb20-440f-b40d-a072909bc3ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 44,
                "offset": 1,
                "shift": 30,
                "w": 29,
                "x": 219,
                "y": 94
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "acf34fb6-7f7a-47f1-8b97-f60321cba0fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 44,
                "offset": 0,
                "shift": 18,
                "w": 19,
                "x": 23,
                "y": 232
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "1aba7c0e-8c42-4d87-957d-6db96a975384",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 44,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 86,
                "y": 186
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "ccdd43f9-c2f1-4e5b-ad84-f0215ea002a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 44,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 2,
                "y": 232
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "cb8a93af-fd83-4a73-b634-2e142b8ad157",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 44,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 428,
                "y": 186
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "053a6637-0f71-4a1f-b911-9aa380f2f157",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 44,
                "offset": 1,
                "shift": 32,
                "w": 30,
                "x": 135,
                "y": 48
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "55978b05-81a9-4a12-b840-53a033ef2389",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 44,
                "offset": 8,
                "shift": 24,
                "w": 8,
                "x": 157,
                "y": 232
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d4eec376-afbc-402d-9e95-1363af44bbad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 44,
                "offset": 1,
                "shift": 33,
                "w": 31,
                "x": 69,
                "y": 48
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "db782d92-82e9-43d9-88f8-15c36bb2a44b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 44,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 64,
                "y": 94
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "a1f9f9bf-4a83-4548-b9cd-b3eb4bf9c26e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 44,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 33,
                "y": 94
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "5cf3fbc9-e909-41eb-899c-030e8534092e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 44,
                "offset": 2,
                "shift": 34,
                "w": 30,
                "x": 263,
                "y": 48
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "5c2f1fcb-e620-409d-93cc-f46fbc242e71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 44,
                "offset": 1,
                "shift": 29,
                "w": 28,
                "x": 32,
                "y": 140
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "80c3ae74-d92e-4483-b574-fecd781fa25b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 44,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 121,
                "y": 140
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "7c8450c4-3c52-408c-91ea-dc391a79008f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 44,
                "offset": 1,
                "shift": 35,
                "w": 33,
                "x": 422,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "eae6e624-f360-4045-bd41-e4b92585dece",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 44,
                "offset": 2,
                "shift": 29,
                "w": 27,
                "x": 179,
                "y": 140
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "9683bc30-9b02-47ce-bc77-590cbed6987c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 44,
                "offset": 3,
                "shift": 11,
                "w": 6,
                "x": 192,
                "y": 232
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "f48a4d36-16fc-40f2-bcdc-e1172aedf8be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 44,
                "offset": 2,
                "shift": 34,
                "w": 30,
                "x": 199,
                "y": 48
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "59c329c5-75c3-4dc4-9b86-92b9cb0809a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 44,
                "offset": 1,
                "shift": 28,
                "w": 27,
                "x": 92,
                "y": 140
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "a798b808-277f-4480-9928-b4af8d571328",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 44,
                "offset": 0,
                "shift": 24,
                "w": 25,
                "x": 276,
                "y": 186
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "a9c9724c-09f9-4cb8-9943-e2d5f51b40ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 44,
                "offset": 1,
                "shift": 40,
                "w": 38,
                "x": 42,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "7debfd3b-f899-4db7-9645-2b4a7ccf5c95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 44,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 281,
                "y": 94
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "06c79fae-755d-4c66-a87e-e8a50deba4d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 44,
                "offset": 2,
                "shift": 40,
                "w": 36,
                "x": 312,
                "y": 2
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "f4814b24-ed55-45d2-a505-09bf6ab057fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 44,
                "offset": 1,
                "shift": 28,
                "w": 27,
                "x": 208,
                "y": 140
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "dfab52d2-e0c2-4a2f-85a1-9d7868336c2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 44,
                "offset": 2,
                "shift": 39,
                "w": 36,
                "x": 198,
                "y": 2
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "4f09d589-7766-47af-bbac-bba0a3f1d66d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 44,
                "offset": 0,
                "shift": 30,
                "w": 29,
                "x": 95,
                "y": 94
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "1436bbba-f1af-4949-96f5-7b83138d1141",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 44,
                "offset": 1,
                "shift": 34,
                "w": 32,
                "x": 457,
                "y": 2
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "f1b32319-1be6-40af-a3a0-92ad3c847b36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 44,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 168,
                "y": 186
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "2046c25a-ac96-4f19-898b-87a9f425ef19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 44,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 466,
                "y": 140
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "08f661a0-7047-4768-9a47-dbaf27544819",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 44,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 114,
                "y": 186
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "65d66653-1f45-40b4-9004-7c0a0d1a69bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 44,
                "offset": 2,
                "shift": 41,
                "w": 37,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "2b6181be-3469-4ada-a807-d5f114d848ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 44,
                "offset": 1,
                "shift": 27,
                "w": 26,
                "x": 382,
                "y": 140
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "9b20361b-d4a9-47e7-b1b3-d8341e6e6c68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 44,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 422,
                "y": 48
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "1806a7d7-6a0a-4d11-84cc-6e099361d7df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 44,
                "offset": 1,
                "shift": 30,
                "w": 29,
                "x": 250,
                "y": 94
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "1e7a1848-bea9-4263-8b76-8a410c517e19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 44,
                "offset": 0,
                "shift": 31,
                "w": 30,
                "x": 359,
                "y": 48
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "170ca7e0-afca-430a-bd31-6c4f8930db64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 44,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 184,
                "y": 232
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "53e99af7-a076-4e92-a5f8-548c9d49c70c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 44,
                "offset": 1,
                "shift": 30,
                "w": 30,
                "x": 167,
                "y": 48
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "826b982a-49d8-42da-ba52-7c4b5cbaf139",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 44,
                "offset": 2,
                "shift": 28,
                "w": 23,
                "x": 329,
                "y": 186
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 30,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}