{
    "id": "c68981ba-5522-4b1a-b559-d8a82f80a0c8",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font0",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "ArmWrestler",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "e99e71ee-1bfd-42b7-85f2-541565218a0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 23,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 155,
                "y": 102
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "118d0644-a792-4b18-ade8-3f2d82e66a5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 23,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 129,
                "y": 102
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "33b48b86-5573-4750-9ba1-cc6b945c5e7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 23,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 86,
                "y": 77
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "3be44207-85ad-475e-8914-327e17313eee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 54,
                "y": 52
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "8db38b44-aafb-49df-9d0b-90a8a8567919",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 23,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 41,
                "y": 52
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "08ca20d4-ba36-4287-85b1-c3cc730a661c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 23,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "e8e8f49e-ae19-445e-8de2-cd406283e4f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 23,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "e9879e23-2468-4a53-b6cd-04659371d528",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 23,
                "offset": -1,
                "shift": 5,
                "w": 6,
                "x": 84,
                "y": 102
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "7653492c-a6f7-4b72-8c9a-009c5e67a1f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 23,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 136,
                "y": 102
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "1bbbf918-5d9a-4fef-89fe-c0db4a10b971",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 23,
                "offset": -1,
                "shift": 5,
                "w": 6,
                "x": 92,
                "y": 102
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "4710779d-8f12-4419-9b87-64d94beaf2c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 23,
                "offset": -1,
                "shift": 8,
                "w": 8,
                "x": 217,
                "y": 77
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "b99b3c70-812e-4bcb-99d7-a4d799b10168",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 166,
                "y": 77
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "15992d03-edb7-409f-bcbb-4318bda85b25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 23,
                "offset": -1,
                "shift": 5,
                "w": 6,
                "x": 68,
                "y": 102
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "d1ee2895-1414-44cc-9a23-ab15086add91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 23,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 108,
                "y": 102
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "1831c3de-a0bd-4616-a9a5-2fb3dfff27cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 23,
                "offset": -1,
                "shift": 5,
                "w": 6,
                "x": 52,
                "y": 102
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "07775166-3bd6-4a74-a26b-56c5718de1c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 23,
                "offset": -1,
                "shift": 4,
                "w": 6,
                "x": 44,
                "y": 102
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "c22abd69-81a2-4fc4-a56e-7d9bdbf37e64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 67,
                "y": 52
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "beae98c4-bd08-472a-b13a-4179df2533b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 23,
                "offset": -1,
                "shift": 8,
                "w": 8,
                "x": 177,
                "y": 77
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "71b0fd5e-e5a7-4af7-af4e-a17a75887875",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 14,
                "y": 77
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "7ee3c2d7-41ce-45ac-abbf-8057fa99b7c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 23,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 132,
                "y": 52
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "fb0dc8a6-2f84-49f9-9087-a4340f3171f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 23,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 100,
                "y": 27
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "5b1e8ebd-89d1-45ef-a21e-8294abccfe44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 23,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 179,
                "y": 27
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "d2361ddd-0d36-4a0e-8744-f998a1923fe9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 114,
                "y": 27
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "b93109c7-a586-41ba-bb60-048f6acf8177",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 23,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 119,
                "y": 52
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "d92224ae-c9ed-4117-8555-c7359eafa4c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 38,
                "y": 77
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "d7ec51cb-e72e-4cef-84c2-29e4fefff2d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 158,
                "y": 52
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "635ea291-e284-4457-8221-020aeef44d9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 23,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 115,
                "y": 102
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "7776d4cc-db6e-41d8-ab58-b48f5158c65c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 23,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 122,
                "y": 102
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "0303408f-0a2f-4e95-8278-ff2c8db122e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 23,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 76,
                "y": 102
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "7c2e660a-986c-47e1-b4a8-dd107ffd24f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 23,
                "offset": -1,
                "shift": 10,
                "w": 10,
                "x": 208,
                "y": 52
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "5f8b6c41-fe6f-4787-b7f5-8e45d389cdde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 23,
                "offset": -1,
                "shift": 6,
                "w": 7,
                "x": 11,
                "y": 102
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "29f75b9b-b963-48b7-85db-9a3cfba3c2be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 23,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 155,
                "y": 77
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "5e3ec3e4-9819-4791-b5cc-d5903e20bb6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 23,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "57ba52c5-28f4-4f23-ae35-9fabed31b650",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 23,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 154,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "f1029491-c586-4d48-b11c-e230c156cfc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 23,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 171,
                "y": 52
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "f2e886e6-13d4-43e9-bb0b-314b08c2cb66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 23,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 106,
                "y": 52
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "da6083fe-8697-4df5-8e27-90040970bc4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 23,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 145,
                "y": 52
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "a5fc7f6f-dac4-4f38-8f5d-e3097c4f63b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 50,
                "y": 77
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "703a653f-12d3-4e0f-aa68-122ab1bc68f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 62,
                "y": 77
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "74732f2b-db2c-4f98-b826-559ad07f8def",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 23,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 72,
                "y": 27
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "9f27bdd5-67df-4440-8b6f-30f22836d948",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 23,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 80,
                "y": 52
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "3a10a5e4-8240-4e4b-a143-efdf57102f1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 23,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 143,
                "y": 102
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "30e2e982-a756-4182-b854-f2e06e8102bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 23,
                "offset": -1,
                "shift": 7,
                "w": 6,
                "x": 28,
                "y": 102
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "28337d97-82f8-423e-8a46-b83710e68b90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 23,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 58,
                "y": 27
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "95f74c09-1c1b-462c-bd8f-6433311583ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 74,
                "y": 77
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "e12c6495-c4ff-434b-8b13-187952f7fc57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 23,
                "offset": 1,
                "shift": 18,
                "w": 14,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "b1350955-66ba-4c9b-95c3-ad53b5408d57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 23,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 127,
                "y": 27
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "af3c7bf1-dbac-4105-ac6f-07e84d88f075",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 23,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 86,
                "y": 27
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "0c7d21db-af14-49b6-8906-e890450a6184",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 23,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 140,
                "y": 27
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "050352a7-6fa4-49ef-9161-b6f838de79f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 23,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 30,
                "y": 27
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "b1407a81-6b23-41ec-8da1-67127938e32d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 23,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 16,
                "y": 27
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "5d2ab64c-e54b-4331-9a7d-58fb60485630",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 23,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 153,
                "y": 27
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "4423f34a-253c-42c3-8a89-c25d46fc4475",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 23,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 2,
                "y": 27
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "31b545f8-98ed-4b4c-a6f1-5f287653a65e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 23,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 166,
                "y": 27
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "1498aa48-e8b1-479d-accf-f3c121dd608f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 23,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 169,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "f14e99fb-2440-48c4-a367-adcc82ad2265",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 23,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "eaaf654c-1347-4129-8493-f6776a7cbf6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 23,
                "offset": -1,
                "shift": 13,
                "w": 13,
                "x": 199,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "61c7509f-2d8e-4497-ac4d-74c948b59554",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 23,
                "offset": -1,
                "shift": 12,
                "w": 13,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "49f4a2b4-38cd-4dc6-b948-0b50178df189",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 23,
                "offset": -1,
                "shift": 11,
                "w": 11,
                "x": 93,
                "y": 52
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "5e1f751e-e5e4-446d-b9f0-cb4ecc3908bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 23,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 60,
                "y": 102
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "3fad9266-bad8-4d6a-a3a6-6994ade5870e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 23,
                "offset": -1,
                "shift": 4,
                "w": 6,
                "x": 36,
                "y": 102
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "c33c7915-e23b-4416-ab3e-a5c742a55310",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 23,
                "offset": -1,
                "shift": 6,
                "w": 6,
                "x": 100,
                "y": 102
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "b93e3616-08f3-419b-a8ac-e20296f720bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 232,
                "y": 52
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "5c1f78f6-796b-4af5-9b2d-aad93ddcdb68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 23,
                "offset": -1,
                "shift": 8,
                "w": 10,
                "x": 98,
                "y": 77
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "2a5be822-9079-49ec-aa81-9e70dad2911f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 23,
                "offset": -5,
                "shift": 0,
                "w": 8,
                "x": 207,
                "y": 77
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "c8599bc2-59ff-4a56-937e-ce357f6f2b31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 23,
                "offset": -1,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 77
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "8f46e2a2-25c7-4c3d-a9ec-6d8d119b2a0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 23,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 192,
                "y": 27
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "4f845809-ba3d-456d-bdb6-e3fc686cb7b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 23,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 144,
                "y": 77
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "0ccb4838-72a6-4a5c-afe0-4eb114e838dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 23,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 205,
                "y": 27
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "b9cf0562-c295-4476-92cb-457366eba058",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 184,
                "y": 52
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "e16da0a3-5728-4d3f-9596-d4a5c1bcc82a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 23,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 237,
                "y": 77
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "a281a4aa-597f-4e1d-83c8-d5cbd5b1d7be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 23,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 214,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "4f8fc80f-950c-4c7b-897c-e55788f3d93a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 23,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 196,
                "y": 52
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "c26122b8-358d-4471-8343-7792a329f25c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 20,
                "y": 102
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "bc2fc11d-d63f-4c80-9475-86d6fe870f68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 23,
                "offset": -2,
                "shift": 6,
                "w": 7,
                "x": 2,
                "y": 102
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "9b40ad56-31fc-4b3c-aa4a-394a516c1ea1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 23,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 218,
                "y": 27
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "2a2aa64c-d673-47d6-9cbc-c11ab1657eb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 23,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 149,
                "y": 102
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "dc6713c6-8214-45da-9d1b-e70f06384852",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 23,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "7444042d-4d00-4f36-9f33-9b5148131770",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 23,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 220,
                "y": 52
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "b6e32872-6274-4a4d-bb1a-44aef929d942",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 231,
                "y": 27
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "cc59dd5c-5381-4443-b2f4-326d763b7486",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 23,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 2,
                "y": 52
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "282e4c89-9977-440f-a19d-96dca9544fbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 23,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 15,
                "y": 52
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "da8237eb-0ab1-4e13-b37b-6f7f64d0f75e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 23,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 133,
                "y": 77
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "8e69786e-d405-401b-9f6f-e4ebf9ec511e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 23,
                "offset": -1,
                "shift": 9,
                "w": 9,
                "x": 122,
                "y": 77
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "d1854151-8fd4-4842-9e84-82ac8a720a89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 23,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 197,
                "y": 77
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "0154ee34-2554-4963-9097-178d97f7dfd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 23,
                "offset": 0,
                "shift": 13,
                "w": 11,
                "x": 28,
                "y": 52
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "22cf0404-6d26-4de6-a811-030653a59604",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 26,
                "y": 77
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "03911303-6624-4a75-88da-9e749c7d918c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 23,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "d405a62a-fca9-4080-af54-7bb4f5d34ce0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 23,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 228,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "927b7872-4c2b-4179-bd8d-320d055f8709",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 23,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 44,
                "y": 27
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "69a073c9-1114-4418-9fd2-618ae3c160c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 23,
                "offset": -1,
                "shift": 10,
                "w": 10,
                "x": 110,
                "y": 77
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "76332e68-4012-4a28-afda-782812298e5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 227,
                "y": 77
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "685d2b99-49e2-43e6-ad27-a52edb69bb6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 23,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 138,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "13ed40c7-8bbb-4994-b5ad-351e0ef31589",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 187,
                "y": 77
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "0ab616ea-301e-4294-9f6f-39bda96673bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 23,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 57,
                "y": 2
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "b9a13b3b-1f67-4a1a-b768-eebed75c94db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 64
        },
        {
            "id": "97570c89-d51d-46b1-bc52-8f4de0859fbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 44
        },
        {
            "id": "4b1d8dc3-532b-483a-a0f6-8a00bd1936c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 45
        },
        {
            "id": "fdeb38d9-25d1-4bbe-a714-ada0367e48ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 46
        },
        {
            "id": "d88723e7-860e-41e0-b2d9-19cc1a08b430",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 52
        },
        {
            "id": "01a8f4de-9ae4-46b9-9123-562e36ae50d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 64
        },
        {
            "id": "6f9da945-5682-4936-9689-f849b476154e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 65
        },
        {
            "id": "b10ac29b-7ca2-42b9-bf52-afcf09ccbe30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 71
        },
        {
            "id": "8d5fa234-2c78-4bf2-b6b9-d3b5e8c9bdbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 99
        },
        {
            "id": "689c2762-8a55-47d4-a87b-1008add77c53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 100
        },
        {
            "id": "07911fc9-404e-471f-9705-01fbb5e741c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 101
        },
        {
            "id": "b8e1bd5d-0f7a-479e-b34c-bf97cd5643ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 111
        },
        {
            "id": "3d3d0f04-95b5-446c-8a38-5ee407abaaa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 113
        },
        {
            "id": "e6e7c45d-9881-416f-a49b-a8a5928c9acf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 44
        },
        {
            "id": "0e8d6f36-52f5-461d-9315-29cd10f4c03c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 45
        },
        {
            "id": "cb8a4a33-25d1-4e05-988e-4dfeb52b57cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 46
        },
        {
            "id": "a4e65e5b-6f87-41a0-a146-37c5d0170620",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 52
        },
        {
            "id": "c6708134-151e-476b-bd4c-b401c96e0283",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 64
        },
        {
            "id": "b4b68610-5022-4665-8a73-37ac97983986",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 65
        },
        {
            "id": "446f3cad-14c9-44da-8198-33359d294e58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 99
        },
        {
            "id": "5f1e9a4d-8411-4015-b07f-67714320c9d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 100
        },
        {
            "id": "b3919426-db20-448b-a342-449fb88c839d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 101
        },
        {
            "id": "f3412400-e829-4b95-9c9e-369890536ca6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 113
        },
        {
            "id": "6890e987-1d1d-48d2-9ae4-14540106a943",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 64
        },
        {
            "id": "3da85d7e-f127-47d3-bcf4-904500c31bfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 64
        },
        {
            "id": "55c9340e-502e-4cfc-883e-b3c362dabc7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 86
        },
        {
            "id": "af2ca2ca-f490-4f81-8942-f81633d89365",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 89
        },
        {
            "id": "f65f5e2a-c0f3-4c5b-b8ad-56f56800fdcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 65
        },
        {
            "id": "b9e7d1b6-c63f-4e3f-8ab3-1f171d620da1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 34
        },
        {
            "id": "5595297e-8ac4-4da8-8cdb-7d041ef9b7b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 39
        },
        {
            "id": "529ca02b-37e4-421a-915e-9ca08df1d663",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 45
        },
        {
            "id": "50dd997e-779e-4a3c-963d-aebe5cda49e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 49
        },
        {
            "id": "9444697d-1924-4ac3-a6ce-af3954aacd01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 52
        },
        {
            "id": "76804e93-9ff1-4d8d-b381-c4a737d828d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 64
        },
        {
            "id": "855a10f9-4215-4faa-b092-f03027f50b7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 84
        },
        {
            "id": "932204a5-4568-4953-9e47-bdda7808cf73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 86
        },
        {
            "id": "3e7505ea-debd-4345-a4b8-90ef04564e10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 87
        },
        {
            "id": "1cde3063-3443-4875-8a10-93a1867431ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 89
        },
        {
            "id": "32a90a53-315c-4115-8996-33d22daba22a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 118
        },
        {
            "id": "62c4e656-e077-4e87-a7e0-0b5ecc44a8c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 119
        },
        {
            "id": "4f36b9fc-8406-4e83-9f91-8b0c58edee7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 121
        },
        {
            "id": "531eed2b-f3e0-46c0-b7e8-3d59ee9faa90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 8216
        },
        {
            "id": "be7ed371-38da-416a-b782-5bbe12700914",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 8217
        },
        {
            "id": "af8e0854-c399-49f1-bfe4-5451ef24f857",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 8220
        },
        {
            "id": "141bb7ac-9bc4-4f53-b46f-934b7c809f95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 8221
        },
        {
            "id": "ed1cbf60-8f1f-4b1a-a5f7-afadccb68e6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 45,
            "second": 34
        },
        {
            "id": "89a315f4-b3e0-48e6-9c7a-8ed5b560abf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 45,
            "second": 39
        },
        {
            "id": "9f0da568-6540-492a-a76a-a7fb3523e490",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 45,
            "second": 44
        },
        {
            "id": "dda0a8b7-cf44-4e1e-b7bf-49d1760e5b52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 45,
            "second": 46
        },
        {
            "id": "e5bafaae-9409-4bc3-80b5-828b36a25ce9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 49
        },
        {
            "id": "a9eb2399-9b06-4fb1-be35-24506179b950",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 50
        },
        {
            "id": "4b036aee-4e19-4852-890f-9962c9e7c487",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 51
        },
        {
            "id": "57bbb728-5fd4-43d8-8cb7-eecfe9f8dca7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 55
        },
        {
            "id": "8849f0a8-8b90-481f-a72e-dd1ee3dbb5fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 58
        },
        {
            "id": "1feb2347-721a-4f11-8b5e-ab378a81cee1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 59
        },
        {
            "id": "7055e298-145d-405f-8fc4-14b1856604cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 63
        },
        {
            "id": "76ecde0c-bc9a-4e1f-97ff-fe2e4ecdac1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 84
        },
        {
            "id": "570795c8-fc69-4d6d-be83-a3491be27106",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 86
        },
        {
            "id": "a86db22a-9975-4300-931b-5de757b4d4b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 88
        },
        {
            "id": "05ddcb79-a3e9-41c2-b3fb-44726127c5cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 89
        },
        {
            "id": "804ac8dc-7d92-42d7-9d80-ecc64c6f3c68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 120
        },
        {
            "id": "b06b3697-3e90-430b-9482-d621bb0710bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 45,
            "second": 8216
        },
        {
            "id": "981feeaf-2ffc-4a62-bf66-d0e51f685b5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 45,
            "second": 8217
        },
        {
            "id": "9356b1c8-88b3-4ff1-a3bf-ada4bb6057de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 45,
            "second": 8220
        },
        {
            "id": "ff27ec80-6688-44d9-98eb-a5808b790e51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 45,
            "second": 8221
        },
        {
            "id": "0fc053f8-b34b-4617-8931-4429f59dd542",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 34
        },
        {
            "id": "98409672-b460-443e-b848-fdd7e8aaf705",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 39
        },
        {
            "id": "3ec463db-a964-4a11-852c-9f8d0e1957ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 45
        },
        {
            "id": "d5d326fb-34b6-43cc-b8c6-c0b977b05d9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 49
        },
        {
            "id": "ca0b7000-77da-4cbd-909a-e8c674944fd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 52
        },
        {
            "id": "0e30dc5a-dd47-4085-9bf7-08cd4d8eca1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 64
        },
        {
            "id": "cd9804c7-d38b-486e-a35c-0493451c5e92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 84
        },
        {
            "id": "6fbafc66-59e2-4484-b456-c325592c70a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 86
        },
        {
            "id": "94130895-76b7-44dc-a356-308fd2c7e8b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 87
        },
        {
            "id": "81c72785-fadc-47d8-ab9a-705a65f6d7f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 89
        },
        {
            "id": "52624202-c676-47df-90d4-b66f5f2da3a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 118
        },
        {
            "id": "76b51927-c853-4677-9840-2616ee170b14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 119
        },
        {
            "id": "5f3a513b-59e6-4c46-b721-8cd638cd9388",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 121
        },
        {
            "id": "5965fa6c-a35b-4be8-9e69-ab7da2c4f7da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 8216
        },
        {
            "id": "89652363-87da-4c98-b728-417d55dcd827",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 8217
        },
        {
            "id": "3114089f-4966-4ec4-8530-ea2283fc690d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 8220
        },
        {
            "id": "09133340-031f-4464-9249-67c4a7079d6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 8221
        },
        {
            "id": "a5b59405-1bbb-412d-977d-eb8182e0859c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 64
        },
        {
            "id": "f453720f-65ad-4801-b99d-e61e56b18b8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 65
        },
        {
            "id": "427bcbe2-97f2-47d2-88c6-963cc28c2caf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 33
        },
        {
            "id": "3397751c-b1ec-40b1-9d2b-49303961f92d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 50
        },
        {
            "id": "9b7a22c1-901f-45de-afe0-4fb5144b3800",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 55
        },
        {
            "id": "10498d7b-fd96-485f-87e1-b790915cf5ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 63
        },
        {
            "id": "a41e3abc-e8d7-4fa0-8ad3-d749892804a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 64
        },
        {
            "id": "4105b040-6a22-4af5-926d-76cad452e763",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 65
        },
        {
            "id": "af0e1a6d-9060-47e3-8872-cb07ac424cca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 68
        },
        {
            "id": "bbc53ca8-13bb-4220-91bf-a27797f116af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 69
        },
        {
            "id": "22d9ba3c-15ec-4211-9235-2c926c2f3e12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 70
        },
        {
            "id": "c1253044-12d6-403e-a3f2-611163568f2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 72
        },
        {
            "id": "ea159bd3-d50c-48e8-b921-001015d58539",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 73
        },
        {
            "id": "56321435-3d38-4bf6-8e3f-9b20cb3f3b89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 75
        },
        {
            "id": "4a539a17-d9a6-4bad-a1e7-d9b585bb6788",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 77
        },
        {
            "id": "2031ab13-00ff-4e3f-a78f-cab1faf8c436",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 78
        },
        {
            "id": "e1357d2d-58dc-41fc-abf0-5b25e5c0c156",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 80
        },
        {
            "id": "9f126635-8fb1-4772-b7fd-ce558fefa540",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 82
        },
        {
            "id": "e9ebc240-8f95-4060-a85f-0b40cc304892",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 85
        },
        {
            "id": "331bc9d8-cfff-4dd3-a7fc-7c9f27fce855",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 86
        },
        {
            "id": "84db9a26-f73e-4e1b-8e97-693b49f890cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 87
        },
        {
            "id": "055cc63f-c271-41a3-b7fd-fee005b5993a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 88
        },
        {
            "id": "d52e31a9-52c9-455d-9f27-23cd8f71d749",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 89
        },
        {
            "id": "4a76d8d4-32f8-4a11-a6f2-d79390235310",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 104
        },
        {
            "id": "242a7be4-81cd-4afc-8f1b-ccae5c91039f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 105
        },
        {
            "id": "f4ac1968-a65a-40d3-8217-d0bf8649e04b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 106
        },
        {
            "id": "f5970407-7fda-4945-8749-727f7bf5ce5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 107
        },
        {
            "id": "2cf12a9f-a204-4a40-9fc5-b7e382668840",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 108
        },
        {
            "id": "b4bf9fdc-e06f-4cdd-816b-6a229c4b495e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 109
        },
        {
            "id": "0dd0fea4-7cba-4d28-8e84-8e803af8d4bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 110
        },
        {
            "id": "dd3b532d-2312-49c7-9a0b-cfbff0a59f23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 112
        },
        {
            "id": "d05a8bdb-7d21-40e3-9fea-967e1be69dee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 117
        },
        {
            "id": "75f0e1b1-8679-434b-a12c-db49f99edbbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 118
        },
        {
            "id": "a3d17c6a-dc91-48f2-9029-e888228f6b0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 33
        },
        {
            "id": "5693e4c2-8225-41a5-ac19-1f7dbee45b35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 34
        },
        {
            "id": "e07a01e2-ce8f-4c55-9a8c-5b6dfa62a19b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 39
        },
        {
            "id": "e05ce0cc-cce7-4160-a79e-c3df6a931f9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 40
        },
        {
            "id": "0ccd1651-8d7b-4f2f-855f-f144f8ae0dfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 41
        },
        {
            "id": "7c3bc2ba-6903-436b-884d-76eac5f0df9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 44
        },
        {
            "id": "0994421c-6058-4ab8-bc97-45fbab846e22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 45
        },
        {
            "id": "366677eb-7c4c-4195-a3b2-094cbdbeb678",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 46
        },
        {
            "id": "606af15a-7d33-4637-a4de-9197832116de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 47
        },
        {
            "id": "8179621d-e0e1-43bb-838b-703ca34e45d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 48
        },
        {
            "id": "74d50b0e-2129-406b-aad3-400aa9c9b754",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "5437d07c-e1dd-4512-87cb-5655bf6fbad8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 50
        },
        {
            "id": "9b2a67c0-e890-4e03-aaa4-c33445f1dbcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 51
        },
        {
            "id": "72252ac7-3585-440e-9f71-de373ff12348",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 52
        },
        {
            "id": "336c6429-25e3-4213-a9c7-f6ab269f173c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 53
        },
        {
            "id": "8edd8ac1-91ad-45cd-a430-96a8e606de74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 54
        },
        {
            "id": "41697aad-c444-4de3-9c97-89b99f11ee88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 55
        },
        {
            "id": "10f14b59-8dab-4cfd-8aa0-8f4ce97ce1a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 56
        },
        {
            "id": "52f47ad1-254f-4657-b3b8-36e1ecde3e79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 57
        },
        {
            "id": "c395b22d-37dd-483d-a1fe-c992628f066d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 58
        },
        {
            "id": "67df0904-f407-4eb7-87ea-3f0b85053449",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 59
        },
        {
            "id": "385d34cd-7a58-4f06-891b-05c6aec053d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 63
        },
        {
            "id": "1e5a5d28-7820-459b-995d-6d1e98afabb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 64
        },
        {
            "id": "0ec31eaa-b0bc-4504-873e-e3a10ee55238",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 65
        },
        {
            "id": "5afecbeb-424c-4c2c-9608-bc39ad4acfd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 66
        },
        {
            "id": "18d164b4-d657-40d0-aa7e-67ac51b7fd26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 67
        },
        {
            "id": "650c3e8c-1bce-4abc-aa55-e57847c81ff4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 68
        },
        {
            "id": "f7bda762-e760-4de9-be54-d09ecedcbe3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 69
        },
        {
            "id": "ed4e0667-596f-42ca-b333-3ba602f06de6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 70
        },
        {
            "id": "d0af32dc-49a5-4596-b50d-dee36d5aebc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 71
        },
        {
            "id": "53e85306-8bda-4695-addc-ae9ad2987a9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 72
        },
        {
            "id": "1bb41095-e06b-4e97-b457-a2f6d98a73e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 73
        },
        {
            "id": "3412c111-8d2f-4402-9580-d109ebc8acf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 75
        },
        {
            "id": "8b49073c-d56a-4b9c-a8bd-9432a8fd6b50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 76
        },
        {
            "id": "9343f1a7-1109-4edc-a3ab-4a5eba8579c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 77
        },
        {
            "id": "bbaa4339-92a3-4ae5-b42e-200d456f19d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 78
        },
        {
            "id": "a1eca428-991c-4d48-bed8-88ebb0db20e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 79
        },
        {
            "id": "4d088b64-0b67-4a0a-83e2-d1d29ecd9497",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 80
        },
        {
            "id": "a552e2c1-50e8-49bd-9f58-51ecb4b8d373",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 81
        },
        {
            "id": "1bb39b15-d90d-496a-9ee5-20898030a35b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 82
        },
        {
            "id": "df62e354-7802-441a-b5c8-84ef38102b7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 83
        },
        {
            "id": "f48ad123-3dd6-40ce-bc4b-11e0ee9915fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 84
        },
        {
            "id": "f5ecd575-6a5a-4aa6-adab-15af405c4091",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 85
        },
        {
            "id": "d8b1e192-cb13-46ab-903d-de2cfee03c5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 86
        },
        {
            "id": "a151e03f-ad19-4748-8ac8-b7723ddb8e21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 87
        },
        {
            "id": "b9da9dea-5345-4e9b-8bdf-4a786afd8731",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 88
        },
        {
            "id": "089c5c9a-242c-4654-a7ce-4ffc9a93c866",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 89
        },
        {
            "id": "616c2d08-aa84-41e0-b0fa-7b42f0f82b45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 90
        },
        {
            "id": "c8b262de-0975-4dcb-85e7-df25fe7dbe66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 92
        },
        {
            "id": "695c66c4-55dd-41b2-853a-300b6f7c8eb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 93
        },
        {
            "id": "6f9e8048-d754-4b89-9e03-b73ff2b7fbcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 97
        },
        {
            "id": "dc191287-4e14-444c-89b3-ca0da3ebbd1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 98
        },
        {
            "id": "fff17637-d914-4442-9315-aded9b7052f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 99
        },
        {
            "id": "69ce8e9b-a882-415d-b23c-41efab5ddd2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 100
        },
        {
            "id": "b3329d19-2930-464a-89c0-322f688212ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 101
        },
        {
            "id": "3078a0a7-747c-41b8-9c08-121a2204b8ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 102
        },
        {
            "id": "db22a0eb-193a-4f26-9c9f-3548754abbdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 103
        },
        {
            "id": "05e1aea5-9fda-433f-a240-65f9fbb1f1d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 104
        },
        {
            "id": "2d773521-92c8-44f6-bbf1-00f8823eb782",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 105
        },
        {
            "id": "52fd825c-6757-40d9-999c-42e235bce0db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 106
        },
        {
            "id": "42cb87d4-4d6c-4cb8-856d-0cdb57147531",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 107
        },
        {
            "id": "3e960762-3c2b-4e01-80fa-43de25f45995",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 108
        },
        {
            "id": "fab39491-a3ea-4788-9eae-700ede30d027",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 109
        },
        {
            "id": "44355df2-c3f6-4392-89b1-4514280d701c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 110
        },
        {
            "id": "fa05896a-e2c7-423d-9a06-5b973211f306",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 111
        },
        {
            "id": "5d143f25-a41c-4844-955b-db66bb3b18f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 112
        },
        {
            "id": "49839234-2a30-44e3-bb70-de59dfa399b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 113
        },
        {
            "id": "89d3926b-9d6a-4e2d-a9ed-eabb6b6cbcfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 114
        },
        {
            "id": "4c38c0df-4c3a-4104-8a4a-32053ee622a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 115
        },
        {
            "id": "4788c77c-719d-4b25-9a51-0f6a37a34df1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 116
        },
        {
            "id": "c3aac429-b06f-4ef2-8f93-7d0b6152ab42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 117
        },
        {
            "id": "cc7b729c-878c-460b-9275-f09c1bde66e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 118
        },
        {
            "id": "8d712e8c-3b93-4afd-b139-44e6670d763b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 119
        },
        {
            "id": "f4b54742-16cc-4a4b-96e2-87dab20bb992",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 121
        },
        {
            "id": "8f06020c-4da8-4217-a7d2-be6854a8a098",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 122
        },
        {
            "id": "767f932c-3015-4b98-a598-68d459d24a77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 8216
        },
        {
            "id": "4b8121ee-eb5f-4a00-b25f-7d227c921e0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 8217
        },
        {
            "id": "9de25566-a643-4025-b9cd-c3addd80fcd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 8220
        },
        {
            "id": "74aeb55b-f750-4eb1-baf6-9bd4ae84f641",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 8221
        },
        {
            "id": "d66e68ac-e68a-4ffb-9cea-a3c47b67d40f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 52
        },
        {
            "id": "caf09b9b-1ab4-48e5-946f-b58d6d397628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 64
        },
        {
            "id": "776bb177-74e7-4b7e-8bbc-1b9934644791",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 89
        },
        {
            "id": "615f4791-188c-48db-8194-4951378c7c97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 64
        },
        {
            "id": "da2a22cd-1f23-462f-b45d-9a6fb3f7253d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 65
        },
        {
            "id": "d7af0224-73ee-4e2d-9629-aafdd99f8281",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 85
        },
        {
            "id": "40539e6b-eba3-41de-84af-9e276dcdee4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 86
        },
        {
            "id": "a8a02dfb-e826-4bba-a4dc-9db118af7a23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 87
        },
        {
            "id": "a1e814b7-4396-42b4-80df-7f471dac459e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 88
        },
        {
            "id": "1dd20c11-f78b-4515-95d2-cbba43f2e9f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 89
        },
        {
            "id": "eb4242ca-eaf4-43b7-a750-7f0e54c27368",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 105
        },
        {
            "id": "49bd4c0b-982b-40f4-981d-c709e0b5ac2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 33
        },
        {
            "id": "16110c21-a041-43e0-bb8d-75b85679ad38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 34
        },
        {
            "id": "430e2e6f-8741-4577-98cf-059d1a8eda9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 39
        },
        {
            "id": "9d13ec28-206d-4cb8-9e65-3c917e257e22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 44
        },
        {
            "id": "6f32f5d3-8bed-4511-9b7c-42c71e2f4687",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 46
        },
        {
            "id": "61f4225e-41ad-4f5f-a997-580c1cac6d9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 49
        },
        {
            "id": "5eeedaa1-31ed-4071-8527-b41950fa8c06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 50
        },
        {
            "id": "0a52452f-6913-49ce-9d43-e057f5aaf4f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 51
        },
        {
            "id": "5538682d-4ecd-4164-9059-f48999679e9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 53
        },
        {
            "id": "1d2d0310-3b3e-4f14-9995-70b888a1fd02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 55
        },
        {
            "id": "7aaecd3f-0562-405d-8de7-3285725da25d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 57
        },
        {
            "id": "bb4490b6-896b-4d4f-8541-96a4d6214522",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 58
        },
        {
            "id": "97b542e3-e725-4e3c-9f69-8b8d601d5725",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 59
        },
        {
            "id": "cfbf3cb8-dd1b-4011-b65e-fe1320d12a4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 63
        },
        {
            "id": "a392e1bb-6d05-4cf3-a397-7c50225612ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 64
        },
        {
            "id": "aeb1d2a8-b1d0-401a-8a2a-de8917465065",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 65
        },
        {
            "id": "54d5e87f-fc47-454f-b667-188e9b1ea76b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 68
        },
        {
            "id": "20d134f4-5d07-4535-be8e-1f3ac3089906",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 69
        },
        {
            "id": "c86ed4e2-09a7-4ba6-a82e-942fc7123be1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 70
        },
        {
            "id": "e95759e9-f83b-43e5-9988-e5aab08fd4b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 72
        },
        {
            "id": "e552ed43-bb5e-47b2-a039-919b97d29c14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 73
        },
        {
            "id": "bb5aead7-fbb1-451b-b5f0-871e050abc71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 74
        },
        {
            "id": "9ff1c22c-4516-47fe-9931-5b48707f5c0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 75
        },
        {
            "id": "88020eba-d371-4f1c-8e53-d84d7a517801",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 77
        },
        {
            "id": "83a4f3d9-6786-4719-adf3-15f14d037c45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 78
        },
        {
            "id": "3ac8f0e2-1ec6-4612-ac8a-84e13df4da05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 80
        },
        {
            "id": "73e8b220-a546-4b11-97ed-d7525ff6f11d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 82
        },
        {
            "id": "52c8ef45-853a-4db2-bfcd-c8113737e833",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 83
        },
        {
            "id": "4d8a39f8-4e1e-460c-a048-57341cf419a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 84
        },
        {
            "id": "b2052a4c-ce20-4ee5-abd5-fa9075168f92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 85
        },
        {
            "id": "eb573435-0e63-46d7-80f6-164adf8f4a0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 86
        },
        {
            "id": "7f38b6cd-46fa-4ac4-8e32-9ba317eec61d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 87
        },
        {
            "id": "86618f42-b9ff-4a2c-b90e-e5af9f799035",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 88
        },
        {
            "id": "6511da27-9728-4d16-a01a-4924cba9b2bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 89
        },
        {
            "id": "887291eb-aae5-4f2a-87fb-8aeaeadd64ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 93
        },
        {
            "id": "a7ad42bf-8aec-4674-927e-dd462f8f72ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 104
        },
        {
            "id": "2708ccd8-48d4-44d1-9f9b-94c674b78f4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 105
        },
        {
            "id": "cb7fd332-f8db-4666-8ecc-03f76ad6dcae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 106
        },
        {
            "id": "fd13f032-2032-4890-9dfe-b9ab9241168a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 107
        },
        {
            "id": "6ba01a1c-b780-44b1-8352-0a135a47ad0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 108
        },
        {
            "id": "55f4545f-2f57-47f0-9517-2839cd65bf6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 109
        },
        {
            "id": "d068fe0a-2ef0-4a2e-9654-e086cf0f3575",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 110
        },
        {
            "id": "f23335cd-160d-4ab7-9e4f-36c8c2310954",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 112
        },
        {
            "id": "01a3cf38-2729-4c1a-a5ca-9e77c55dcd11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 118
        },
        {
            "id": "c516f890-7021-4b2c-8ba3-2d6c1bf84076",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 119
        },
        {
            "id": "4da73dde-7df3-4aa4-92df-7f0f7d4eda46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 120
        },
        {
            "id": "6f23bd4d-8c77-4d8c-b005-60cea5b8610b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 121
        },
        {
            "id": "640cb494-f6e8-4076-a837-1a1cc218613a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 8216
        },
        {
            "id": "e1445f8b-b7e7-44f5-a124-d729c29559a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 8217
        },
        {
            "id": "8248ff72-48bb-4d76-a88e-d27f61b58a9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 8220
        },
        {
            "id": "a1c76dd2-6600-40bb-b733-47f677a35e07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 8221
        },
        {
            "id": "11e7c8f7-4fc4-4732-8aa0-6fee94482602",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 55
        },
        {
            "id": "404a399a-e3ae-4534-94e4-74497d63c7d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 58
        },
        {
            "id": "d85e5d70-ca8b-4dd1-8b3f-4feb4dd49588",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 59
        },
        {
            "id": "e459ac25-98d8-4fcc-9644-fac4d44f59a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 64
        },
        {
            "id": "073ed095-0019-413e-931f-2960fb507ed8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 65
        },
        {
            "id": "37f07467-239b-47d2-8c97-a53e9c3fe957",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 68
        },
        {
            "id": "29225ac1-660e-40f8-8e14-a19f3d89ca58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 69
        },
        {
            "id": "13d19caf-1247-48aa-8e14-049dc9e99b7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 70
        },
        {
            "id": "0878cad8-3429-419b-8628-c881330e99d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 72
        },
        {
            "id": "0a64bda2-b814-4ef4-a56a-e48c5ee97c0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 73
        },
        {
            "id": "f414b64c-b41e-4d57-a58c-7c6f01f09f59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 75
        },
        {
            "id": "2bd6f7f6-75ff-4e67-800c-57d23e1a4faa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 77
        },
        {
            "id": "5dd272d1-2eb1-4da9-ba86-94689b6a311e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 78
        },
        {
            "id": "73345683-1f9f-4e71-bc90-48be7e7b822a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 80
        },
        {
            "id": "d6070a40-2ec3-4c88-84d3-850f7194cdf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 82
        },
        {
            "id": "8215c8f6-f0b0-49e8-a781-6bb8c88b702f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 85
        },
        {
            "id": "10cda074-7364-4a6f-a336-61960ffb58eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 104
        },
        {
            "id": "7f440ac1-5808-46ae-a23e-6214856483fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 105
        },
        {
            "id": "c4a5083b-880b-4c58-b1d3-100fa6e06182",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 106
        },
        {
            "id": "573f51c8-f61f-4c42-a3a2-f15cfd7d6c88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 107
        },
        {
            "id": "1d02664c-1ce8-4599-b34a-395015399aa0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 108
        },
        {
            "id": "1ab3b354-796b-4a95-a254-83d31191ed19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 109
        },
        {
            "id": "e9b471cc-eb39-45ae-aa66-d59374120f11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 110
        },
        {
            "id": "4a87dac4-3a91-4382-bab1-27df9dfff593",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 112
        },
        {
            "id": "2d600cd0-162e-4f12-8066-53602dd16fc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 118
        },
        {
            "id": "58eec53b-d605-4f71-bef6-9ebc32da3bba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 119
        },
        {
            "id": "449ee49c-7989-4b12-8a9c-a34a7854ca2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 57
        },
        {
            "id": "74b7ea58-d367-46f1-82de-1fac1f593a2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 58
        },
        {
            "id": "7d346665-0dea-4402-bb1c-d011602bde5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 59
        },
        {
            "id": "fe0a3214-3598-4217-9f19-356e4f641c3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 64
        },
        {
            "id": "83f046e7-d984-44d1-bd4b-7f9a9e209ccf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 65
        },
        {
            "id": "ea95b0c7-c1db-4fc6-8f16-29773f6b9fcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 68
        },
        {
            "id": "f7ceb9d7-0173-481e-86f3-7ae1cb310c2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 69
        },
        {
            "id": "c871b33a-3a37-414b-aa74-db3c37fb6a31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 70
        },
        {
            "id": "7699c72f-6cac-46bb-bb52-02012b0e393a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 72
        },
        {
            "id": "c35045bc-9583-4710-b834-4bda0e86eb97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 73
        },
        {
            "id": "ee2def04-ad2f-41f3-84f7-6850da1d271d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 75
        },
        {
            "id": "ba407793-c977-4196-8c31-d8fce886533a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 77
        },
        {
            "id": "87412b37-7c1b-4645-b787-bfb4e6b78d3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 78
        },
        {
            "id": "8db35774-ef39-4adf-a82d-ac4054f06599",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 80
        },
        {
            "id": "cfee6626-77db-4704-81f9-d0a255ebbec9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 82
        },
        {
            "id": "d52d8f81-2a53-4570-aa6b-b04cf26e02ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 85
        },
        {
            "id": "cb2d687b-7a29-49ac-8d3f-c60cdeb86616",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 102
        },
        {
            "id": "c59b7cd1-ef74-48f9-a38f-15160ea7d08e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 104
        },
        {
            "id": "3b643240-cbd1-4f67-81ac-8416b93621eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 105
        },
        {
            "id": "0ae89b68-25f1-494f-af1d-d5f7cee53004",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 107
        },
        {
            "id": "9dd28ce1-f1ba-4a66-898d-0d208da6cbcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 108
        },
        {
            "id": "004064a9-108c-42b5-bbda-19f1319986f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 109
        },
        {
            "id": "205d772f-8a10-4d0d-a33b-68d38c953c0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 110
        },
        {
            "id": "0b258aa2-8fcb-4c2b-979a-d9f2edbb20f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 112
        },
        {
            "id": "2d122bf0-7114-43f7-a84e-a96687357063",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 114
        },
        {
            "id": "c8ac2431-6eac-4fe9-8ae8-37926deb10ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 117
        },
        {
            "id": "2d23c5ac-4a4f-4f9d-8d47-b6f2a03e6195",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 118
        },
        {
            "id": "8dfad560-cb32-49ac-8237-00e9c19302fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 119
        },
        {
            "id": "25e86a53-2c9e-4599-b0c8-6bca99f9157b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 120
        },
        {
            "id": "92cbd3c8-4e6c-4743-9d77-7495c8db5592",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 121
        },
        {
            "id": "16346388-8d17-47bb-a9ca-38a060f4993f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 44
        },
        {
            "id": "245cb7d7-904c-4acf-aae3-d1031e09898f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 45
        },
        {
            "id": "aaf65a3e-e5ab-4871-bc8c-0b3f55201997",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 46
        },
        {
            "id": "0be55be8-25f4-4b23-93aa-7f88137bc442",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 47
        },
        {
            "id": "dc0945f7-ebd2-428a-a3b1-761cebc82ba5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 52
        },
        {
            "id": "89bfcb98-4e58-4c6e-bb09-f1e2efa91e3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 54
        },
        {
            "id": "e3320e1e-86be-46c5-ac45-b3b75e02e6fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 58
        },
        {
            "id": "99488669-1d35-449d-879c-b673d9c50ab6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 59
        },
        {
            "id": "326447f9-3c36-4e0d-97eb-127737a2ec73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 64
        },
        {
            "id": "b8592591-5d11-4337-b369-59ea6993e086",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 65
        },
        {
            "id": "5dab5012-80e9-41cb-9a1d-947e5f5dfa56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 67
        },
        {
            "id": "1e2a85e8-f6d6-443d-8589-8f4c66ad2d3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 71
        },
        {
            "id": "8422ac5b-b0df-4802-98a5-c3f9f4fb6f3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 85
        },
        {
            "id": "c5858629-3bf4-4389-90cb-1111976f3610",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 97
        },
        {
            "id": "77e95330-fb83-4fa1-be28-a60c28c42b70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 99
        },
        {
            "id": "5146646f-1116-40eb-87b9-eb8099bcbf7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 100
        },
        {
            "id": "7d1c796e-26e9-4a89-bce2-bd5cb1d5194d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 101
        },
        {
            "id": "b8d67c44-dca7-401a-b848-f690e78a33ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 103
        },
        {
            "id": "05ac927e-12cb-42d4-8117-f6e0fb7c460a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 109
        },
        {
            "id": "206d137a-0463-4405-8139-db8a119ee529",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 110
        },
        {
            "id": "3c47786d-7d79-4dcd-ad4d-bd26961a5fa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 111
        },
        {
            "id": "db0738ef-11fb-4a6d-ae4d-fb4425466579",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 112
        },
        {
            "id": "01f2e334-91e3-4bbf-97b4-7f5304472c05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 113
        },
        {
            "id": "10b239a4-d298-489e-a000-760f6beee3a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 114
        },
        {
            "id": "ce7ffbe8-e89c-41c6-bce7-d6becbfd691b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 117
        },
        {
            "id": "a5aff3b2-0660-4491-a46e-93b65a874073",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 63
        },
        {
            "id": "974e15bd-7b00-4807-b9ac-2e55898c81f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 64
        },
        {
            "id": "2a57a49b-bd31-48ee-8603-64ef86a1578d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 65
        },
        {
            "id": "ac7dc1f1-72cc-41ff-9215-b4300f2a2dab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 68
        },
        {
            "id": "5aacd378-9f53-4351-a90a-28ae3e770b62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 69
        },
        {
            "id": "5d078df3-9c70-473c-9f35-d704720e693b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 70
        },
        {
            "id": "fad49469-c6bb-4c32-99eb-2a970f94971c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 72
        },
        {
            "id": "28e4ddc9-7b53-4291-8d98-ffa594e987b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 73
        },
        {
            "id": "6a25d56d-d9ed-4b72-8185-a6e114e1679d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 75
        },
        {
            "id": "35aaf1f1-e0c3-47d9-8cce-ca02b6d08cd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 77
        },
        {
            "id": "38e8f25d-7fe1-4fdf-a3f4-444114d464a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 78
        },
        {
            "id": "80110c0d-05d5-4a9c-9ef2-d41055e7e361",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 80
        },
        {
            "id": "229f2d98-352e-47de-831b-d35887311c9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 82
        },
        {
            "id": "9b5c437c-5a21-4ae2-8772-61f1794e464c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 85
        },
        {
            "id": "6e3241a9-52d7-4e9b-b247-7b17fe985e5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 86
        },
        {
            "id": "8b4c3da4-af81-4a43-bf7e-06238bc4e131",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 87
        },
        {
            "id": "5dda2e32-26ba-4db6-8623-a1e1be71953c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 88
        },
        {
            "id": "9d3a7984-3e1f-46cd-a161-af752f4fb24f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 89
        },
        {
            "id": "2830e5f8-1910-46a2-95f3-c1e66a85019e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 104
        },
        {
            "id": "859690fe-b72a-4402-9e2d-f2b394a5569a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 105
        },
        {
            "id": "10f84805-7edf-47b9-acb2-7de2b63294ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 106
        },
        {
            "id": "5d9a1902-5811-4f2a-8404-bbb98200c152",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 107
        },
        {
            "id": "d2bc0193-fc35-4c82-9014-4d714b752bd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 108
        },
        {
            "id": "2b20d148-6f56-4498-bc0c-eea09dd725bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 109
        },
        {
            "id": "07e47999-5b39-4fac-a1d8-448c9d4bcd43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 110
        },
        {
            "id": "4786aa03-032b-4057-8d4f-b65162f571ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 33
        },
        {
            "id": "a2431cf3-b8d2-4576-9808-10bc073b1d0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 44
        },
        {
            "id": "a5d838c3-08e6-4343-8cd5-4da08f476020",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 46
        },
        {
            "id": "486a9bde-d8fa-42f3-8771-28fd36286baa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 50
        },
        {
            "id": "0d60a2cb-1f42-4be6-ab3c-1a4de5b7b789",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 63
        },
        {
            "id": "81dc047d-7223-4e51-b724-a5d08e8c67c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 64
        },
        {
            "id": "8dc8f5bb-a801-4ce2-8e01-92a0f94e923a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 65
        },
        {
            "id": "d711f830-7450-4d6b-afae-b49f5f84d8c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 68
        },
        {
            "id": "151778c8-b4fb-4161-92a0-0815405d4bcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 69
        },
        {
            "id": "010f3158-b3fa-413b-b933-6eb93224cb48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 70
        },
        {
            "id": "1ec176aa-c688-4994-9a86-5de6899c5592",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 72
        },
        {
            "id": "44b2753f-9614-42a6-87e8-ca87195ae0b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 73
        },
        {
            "id": "3cb3c8e9-184d-4242-9e5a-51eec18a5495",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 74
        },
        {
            "id": "f76f4d4d-1656-413e-8950-6d203cb0ee16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 75
        },
        {
            "id": "57ef9b0e-fff6-42e2-b705-cc29bad358c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 77
        },
        {
            "id": "a6b34c45-2aeb-4ee1-9685-919dfdd33966",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 78
        },
        {
            "id": "229acfd3-5fef-40cf-a106-aeb0c386def5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 80
        },
        {
            "id": "7b6045fe-31a2-477d-9404-5fd8a079db17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 82
        },
        {
            "id": "fdcd8895-51d9-400b-8590-6208e028711a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 85
        },
        {
            "id": "b7f070b8-9a45-4a76-98df-70efda8b0237",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 86
        },
        {
            "id": "dd696dbf-11d2-4902-8164-f65a865e1764",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 87
        },
        {
            "id": "feb0a9ae-e6e1-4123-abe1-165bbb1d979a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 88
        },
        {
            "id": "37b990c8-e45c-4eeb-b574-b3f32c160687",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 89
        },
        {
            "id": "28ee2051-e376-4aff-a11c-52e696c74ec7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 104
        },
        {
            "id": "e475c573-b8d7-4035-b1ec-9e1677f16306",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 105
        },
        {
            "id": "471d17e4-ddc9-4f36-974a-1818f27aafc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 106
        },
        {
            "id": "886e1056-ee0d-4cc7-9d9b-8728eb185ee9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 107
        },
        {
            "id": "f2330575-d5ce-4d6d-8a9f-521951bda777",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 108
        },
        {
            "id": "20750197-1aa2-4795-82d7-0c2852696202",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 109
        },
        {
            "id": "e38939ec-b8c6-45d7-a050-08d79be6af2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 110
        },
        {
            "id": "9e10728b-8043-4678-84e7-c815cd5ee6cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 112
        },
        {
            "id": "b6e8ee16-9f90-4793-9ba6-65cb07f3c352",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 117
        },
        {
            "id": "d251a4d9-721a-459b-8013-6a6d653f8801",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 45
        },
        {
            "id": "e7bfad4f-4fb7-4f31-af7b-f11112a53db7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 52
        },
        {
            "id": "349bacef-c19e-45e3-b4dd-fe706bea432c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 64
        },
        {
            "id": "74a0fd72-3df7-4ba9-b4fc-f09ca4a22654",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 89
        },
        {
            "id": "26e17604-2e0e-41e8-bb11-a205f4c86f3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 45
        },
        {
            "id": "0664f5d3-bf15-4b79-a000-964c8ec9912b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 52
        },
        {
            "id": "88a9d5fc-b82d-4054-8d59-f2679828cde6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 64
        },
        {
            "id": "a91e4a59-56d9-4978-ab70-2d4254ff4694",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 89
        },
        {
            "id": "cc6045ea-f241-4966-a97b-ff0b9394354d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 44
        },
        {
            "id": "69f83906-6a81-4231-b778-3a8b4dc97017",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 46
        },
        {
            "id": "9222a693-72ee-4fd5-94f5-1bccda81e284",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 50
        },
        {
            "id": "f10006db-1b0b-4588-9f16-f9fc35f1758e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 52
        },
        {
            "id": "edddf125-f00a-4c44-b3d7-5f995a9fa55f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 63,
            "second": 64
        },
        {
            "id": "8849b75a-1040-473e-98b5-c951f9c6ef1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 63,
            "second": 65
        },
        {
            "id": "a66c1dcd-6dda-4fc9-977c-fda0d07955d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 86
        },
        {
            "id": "aee5bad3-ecca-41f1-88bc-1650533904ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 89
        },
        {
            "id": "47482b7f-8f5b-4f20-9cc1-f56e2379dd3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 100
        },
        {
            "id": "f21e20ea-90bc-42e2-acb3-8b6b53fa0313",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 109
        },
        {
            "id": "0c004af4-7e87-4fbb-a0c2-c1fd239f7379",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 110
        },
        {
            "id": "638649ee-be9f-4f9d-9145-d6480efc58e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 33
        },
        {
            "id": "5d437367-e8f6-428d-bd03-59a5085ef0f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 34
        },
        {
            "id": "ca095c43-abbf-440b-8cdb-f5179d2f924a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 39
        },
        {
            "id": "bd749953-e72b-4211-b5ef-781a6faa1329",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 40
        },
        {
            "id": "f5bd9cf4-0fab-468f-a12e-d3d02bd792d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 41
        },
        {
            "id": "05373cc4-c03a-4ad8-a233-5bc83f854458",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 44
        },
        {
            "id": "a8e82952-7660-4503-8fa0-8e88e5dd7565",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 47
        },
        {
            "id": "899d6736-2b77-493b-a529-41338b59f61d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 49
        },
        {
            "id": "221f83c3-f8ed-4326-a7fc-4fe54d2ea270",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 50
        },
        {
            "id": "3196c8ff-e87a-4171-8fee-1a8fc9583178",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 51
        },
        {
            "id": "00fd93a9-e8aa-4230-a94e-5367f3caf778",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 53
        },
        {
            "id": "c193b2d6-c952-458c-91f1-0ade2c5f5567",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 64,
            "second": 55
        },
        {
            "id": "f1b74f16-a705-4934-a4ea-64892468deaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 57
        },
        {
            "id": "402e14dd-0c37-4677-9200-a18512ea103c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 58
        },
        {
            "id": "79de9238-c72a-439c-916f-9f8778195e39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 59
        },
        {
            "id": "09aba2cc-54cd-4749-b431-137b35554b2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 64,
            "second": 63
        },
        {
            "id": "16944f78-cc17-4a72-9910-05c61d4f3ee1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 64,
            "second": 64
        },
        {
            "id": "4eb99a70-c838-46c9-ad6e-74757338094a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 65
        },
        {
            "id": "570097dc-7d88-4c9f-9dd9-1fb0c810288c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 66
        },
        {
            "id": "5334d00b-8042-4151-bf12-c7de4ac06828",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 68
        },
        {
            "id": "fc9b8777-b0a2-481e-8eaa-fab9e8089694",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 69
        },
        {
            "id": "86f060e0-9dcd-4c97-a7af-afb558768670",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 70
        },
        {
            "id": "27dbd4f1-8cef-47fa-a495-ed43384dcfdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 71
        },
        {
            "id": "2b40f518-6c77-45f2-b135-8d4917e681bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 72
        },
        {
            "id": "4578c758-858b-4b9c-94ef-927362861395",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 73
        },
        {
            "id": "a8d07d3b-b604-4f2d-96d9-2c90d79023f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 75
        },
        {
            "id": "59fe4b04-2063-4b58-84a8-6627f01083b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 76
        },
        {
            "id": "5d7f75ab-9a95-4dfa-b3d6-b8dfb7d6a99b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 77
        },
        {
            "id": "7df1d565-c71b-4c1c-b604-48753b2d4c38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 78
        },
        {
            "id": "1b0a8c10-09c1-4a10-a0ab-a87744859939",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 80
        },
        {
            "id": "0937832a-5067-4a53-9dee-c52f0c51b27a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 81
        },
        {
            "id": "e89e7aad-cd37-439e-9ca8-4845c54f730e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 82
        },
        {
            "id": "76babe78-3a5e-40c7-ae24-bfb721bfce84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 84
        },
        {
            "id": "deca33e0-6444-4d5b-9b3b-fe4a27dd790e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 85
        },
        {
            "id": "0cbe0b3d-4c6c-4f40-a545-05c33fd3c026",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 64,
            "second": 86
        },
        {
            "id": "1d5a722a-7a0b-4363-8e48-b76cb3822151",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 64,
            "second": 87
        },
        {
            "id": "893aa7a0-d453-47a1-a4db-8ba3a8977a8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 88
        },
        {
            "id": "edbb7d2f-03f7-4dd6-a3b2-2b882cda3bb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 64,
            "second": 89
        },
        {
            "id": "d29fbd4d-ccd8-4358-9d73-740163093df6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 90
        },
        {
            "id": "71968e11-5ef2-4ff2-86b6-148b491221ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 92
        },
        {
            "id": "7506b884-f524-469d-a256-98c63f34ec66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 93
        },
        {
            "id": "5dce6a96-062f-4741-a3e1-a3940cfe49dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 98
        },
        {
            "id": "612add89-3baa-4b5f-86c2-b99bb13115de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 102
        },
        {
            "id": "7fd5eb4b-3d85-4ddc-9a41-c32eafe3a1b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 104
        },
        {
            "id": "7752d0ea-cdeb-4638-988f-055435c0ae8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 105
        },
        {
            "id": "c0f03dfb-5714-4fd0-a13e-3a35137013e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 106
        },
        {
            "id": "5b17ded4-7b0b-4717-89de-72abecb932a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 107
        },
        {
            "id": "a5cf0936-429a-4126-81f1-04895696f546",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 108
        },
        {
            "id": "1a420111-244b-430f-a1a4-8c4d454bf01a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 109
        },
        {
            "id": "f2c85270-cca1-43fc-a2ff-a272991daed3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 110
        },
        {
            "id": "fac2ac5c-66e9-475c-8e13-9406520d0a1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 112
        },
        {
            "id": "500f9495-d277-4ee0-97b9-79f154d130e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 113
        },
        {
            "id": "7087190e-200e-4122-b751-d5d5fd0f3da8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 114
        },
        {
            "id": "8d4d5141-380d-4f51-8bf9-61e09595cc8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 116
        },
        {
            "id": "c2e1c3fc-7e3c-47e3-941e-243d00b76d30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 117
        },
        {
            "id": "4a960f86-d761-4acd-ad94-5a2660af9924",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 118
        },
        {
            "id": "6734f8e8-0e23-443b-a6f9-5b24905f18e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 119
        },
        {
            "id": "6e9baa85-b4ac-4c6d-b7f5-1475c7384530",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 120
        },
        {
            "id": "3ae467aa-96c8-4afa-9db6-8c2a19bc5dac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 121
        },
        {
            "id": "019ef41a-519b-4c08-b1f5-168661dd98c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 8216
        },
        {
            "id": "0acaae18-6a36-481d-b909-fc530d817a31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 8217
        },
        {
            "id": "de923cb8-8121-4028-8611-b38768601885",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 8220
        },
        {
            "id": "ebd775ab-a321-4943-8805-de9247f2969b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 8221
        },
        {
            "id": "7107d182-221f-4094-83cc-54e47d859d9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 34
        },
        {
            "id": "d5cc4c94-bb3e-4c5f-8c1b-8dd36d76f31c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 39
        },
        {
            "id": "7ef0a3ac-538b-45ca-9bd3-480f94ffd8fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 49
        },
        {
            "id": "86d1bc25-1a2b-42fb-a4fd-7228e5f9e398",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 64
        },
        {
            "id": "802c8a2e-5659-4a49-add2-357801345bac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "3bb6ea9c-3857-41be-b439-9bcb8488878c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "2b037497-16c5-4774-88f7-fee1b5eb2284",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "ce399f40-9a4b-4acd-a8f8-a1ab53f4cd70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 89
        },
        {
            "id": "9c87557a-7b34-4953-a4fa-fe28aea078c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "49d7bcdb-1641-4e38-b9fa-561b1b4061fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "85c98cc6-281e-4e74-85fa-1a1af9f7f204",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "0bf6a09f-9a73-448f-a6dc-803889190897",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8216
        },
        {
            "id": "bf0bcc2a-dc69-47c7-846d-4ab1790dd280",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "a3b0459a-ed92-4688-a055-c7ce64f5b5b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8220
        },
        {
            "id": "62585114-7d4e-438c-a080-42c85a28b695",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8221
        },
        {
            "id": "3af8b98f-6582-4450-8733-d0075dba9656",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 33
        },
        {
            "id": "ade75d86-e878-4969-8979-761df143437e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 50
        },
        {
            "id": "5ef75437-4059-48e6-b253-2b6bd1353a26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 55
        },
        {
            "id": "44accd9e-4dea-4de3-af9a-dae2cb2fab2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 57
        },
        {
            "id": "dce24100-3067-4467-8af7-3487e179ccba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 58
        },
        {
            "id": "19591a6c-fdd2-4551-a9db-026f58bbf8d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 59
        },
        {
            "id": "4250bb26-36de-4951-8b40-453c2d421349",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 63
        },
        {
            "id": "6eefb58e-37be-4d86-8461-3157ba49f60f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 64
        },
        {
            "id": "55894099-afd1-4b71-b3b3-686acccd8d61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 65
        },
        {
            "id": "ff6c2bdf-b33e-4535-8153-542975f6467b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 66
        },
        {
            "id": "44b02aee-d92c-490e-8071-74fa5b0b92a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 68
        },
        {
            "id": "bb77d852-f809-4425-8365-04bb36c6fc0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 69
        },
        {
            "id": "c5042a54-2ce4-4b44-bf3f-baa4642ec9af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 70
        },
        {
            "id": "ced7938f-e1bc-49bc-96f9-14b9e76b3298",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 71
        },
        {
            "id": "9304cb29-ff6e-4e97-9fc9-d9ba7836bbdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 72
        },
        {
            "id": "7407bf48-9930-40b9-a96a-4ab34f0e56f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 73
        },
        {
            "id": "0d3427aa-f542-4c78-8965-df255a24cd33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 75
        },
        {
            "id": "8769889f-e58c-46c2-a9dd-482e4dc87012",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 76
        },
        {
            "id": "27918420-7a31-471e-9a85-d58f46759d03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 77
        },
        {
            "id": "f490ebe5-0d15-4a4e-8233-419c058decd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 78
        },
        {
            "id": "f12c9965-a91f-435e-b3ee-82bf7cf76194",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 80
        },
        {
            "id": "cc895c22-2b5d-4596-851d-a8aacf1aef9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 82
        },
        {
            "id": "425158ad-ab53-418b-b606-a383b0de0a43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 84
        },
        {
            "id": "c1a127ad-666e-4637-8e6d-9618cd15be6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 85
        },
        {
            "id": "6dc4bf4c-4901-4bb2-abc6-ffef328924a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 86
        },
        {
            "id": "1c6c834b-3317-477b-a07a-934833fae1cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 87
        },
        {
            "id": "cae883c1-370d-497d-a0dc-b22443bfb5f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 88
        },
        {
            "id": "94c513b4-7031-442e-aa1d-c6d0da288d5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 89
        },
        {
            "id": "0d3be033-d053-4b59-aa17-7db990ea2aaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 92
        },
        {
            "id": "a498eacc-9be7-424d-9247-90a4865a3c22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 98
        },
        {
            "id": "33777248-f7b8-4c1a-8c17-684623ad8bce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 103
        },
        {
            "id": "37043eb1-da5e-4696-bb8b-926d370e2561",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 104
        },
        {
            "id": "ac23fb4a-d637-49f9-9905-77671cef68e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 105
        },
        {
            "id": "9d6cb910-3628-47e9-84aa-7d63b559bd8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 106
        },
        {
            "id": "cd7344f0-51d0-4f1e-b501-b5c5fdc8d48b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 107
        },
        {
            "id": "c2683016-ee18-4d3a-bd0d-f55a363f6848",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 108
        },
        {
            "id": "07800261-ae34-41ad-96ca-461ef0c3c822",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 109
        },
        {
            "id": "dddd8565-4b2e-4346-b827-63b00351698c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 110
        },
        {
            "id": "bb108ad8-6f96-4165-ad66-f74263bab1e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 112
        },
        {
            "id": "ec26d666-f6ab-41c0-ba47-c2da04f5023d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 114
        },
        {
            "id": "188c807f-71be-44ff-8a7d-d5d13da8ff18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 117
        },
        {
            "id": "dacc2c68-bb20-4827-9967-00cd66f2b28b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 118
        },
        {
            "id": "eea10885-6157-4629-9271-e5d7d11e4581",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 119
        },
        {
            "id": "887c4827-e66a-47bf-9c54-91791a623532",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 33
        },
        {
            "id": "6dfd234e-ce5d-4609-ae40-a3e7adb00644",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 45
        },
        {
            "id": "870acc19-3fd4-429a-89f9-1b7faf0c0552",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 48
        },
        {
            "id": "d899cde2-8c35-4c50-b42a-1363d8ecf5fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 67,
            "second": 52
        },
        {
            "id": "1da892b7-6dde-4338-998f-b2983dff6c2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 54
        },
        {
            "id": "dcb6fdc1-887e-4685-aaf3-e5cbc245c085",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 55
        },
        {
            "id": "49cec7ab-4a84-4206-a1f7-3fb1200910e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 58
        },
        {
            "id": "ddd69e32-6502-4970-9ac2-bb63504f8947",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 59
        },
        {
            "id": "3ced75f5-bdd7-4552-8188-da5c1c33eda1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 64
        },
        {
            "id": "b83130f7-f321-476a-a6d6-744186a0d2e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 65
        },
        {
            "id": "37daaf0a-626f-4669-9a72-996e8ab4b760",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 66
        },
        {
            "id": "e502c4ad-82c5-465d-ac09-cb9040f2333b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 67
        },
        {
            "id": "6cc72fe8-22c2-4713-9761-66e3934b58ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 68
        },
        {
            "id": "b9bc0c6c-0602-4b58-9d10-be682a264a00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 69
        },
        {
            "id": "23cbf59f-e8e6-4115-9d5e-2e4c2cd364e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 70
        },
        {
            "id": "2dcfc7cf-765a-408e-94cf-c0f50a75b7b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 71
        },
        {
            "id": "64a5bf09-cccc-438a-9651-b046c38a1042",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 72
        },
        {
            "id": "c1437aa4-0fea-4634-ab58-a99ad62e1867",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 73
        },
        {
            "id": "f485aa86-fb90-48c9-b166-9f4b6df3a795",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 75
        },
        {
            "id": "9f5d5897-97c5-4ae0-8d2f-2d8f7e2ac3bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 76
        },
        {
            "id": "857eea57-ff28-4f8e-b65e-4de0764c4ea3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 77
        },
        {
            "id": "a88c9ef1-2a10-4a95-b4f5-1432e8619cef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 78
        },
        {
            "id": "12a5ab82-28ca-4ccd-bf76-253a1a157d6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 79
        },
        {
            "id": "aef6feb6-f0b9-4fb1-910c-53f17f9d4c73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 80
        },
        {
            "id": "f843c716-0a40-43e2-bbfd-fcf9e69d33e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 81
        },
        {
            "id": "e867b879-c640-4df5-9116-71f5fdcdfaa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 82
        },
        {
            "id": "23c3e0eb-fa19-4744-a012-90eef2dd41e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 85
        },
        {
            "id": "5fef7748-e143-41c5-99c2-6219290e273a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 86
        },
        {
            "id": "d7a353f1-1d43-457c-9ba0-eefb1d95a8ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 87
        },
        {
            "id": "9f638fef-58c6-46bf-9e73-7c0bd67cb9b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 89
        },
        {
            "id": "58c146ab-d3bf-4e52-954f-d5fa9e3b9699",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 98
        },
        {
            "id": "0b926d20-9d52-4961-b167-80d6d6278b97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 99
        },
        {
            "id": "582a37f8-0a45-47e2-ab28-a43d303f6608",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 100
        },
        {
            "id": "1ce62f0e-7869-40ac-bfb9-94f9b3bb7d6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 101
        },
        {
            "id": "d971f1a7-4f57-4917-a6f3-a42b64fc78c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 103
        },
        {
            "id": "793983ba-f70c-480c-be8f-f2a505b42590",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 104
        },
        {
            "id": "1b572d4c-ce59-49b4-bc33-9d896f5a97ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 105
        },
        {
            "id": "bec94574-128c-4417-a980-48322f670a38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 106
        },
        {
            "id": "09982d98-ca2c-4dfc-ab72-17f7bb74b51d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 107
        },
        {
            "id": "8fe2c2d2-6ba5-4ad2-a77c-e5f91dd6421c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 108
        },
        {
            "id": "b6146e88-baa0-4c59-89d6-0914fe14c37c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 109
        },
        {
            "id": "5cec1604-b8e6-48f0-9c24-97a4835821a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 110
        },
        {
            "id": "8b431cb7-f85b-45e0-85b3-c4a184737d05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 111
        },
        {
            "id": "83eef83c-2d87-43b6-8e0e-05c36af2a5f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 112
        },
        {
            "id": "78e864b2-5c74-4c4c-9151-8eb1840dbc5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 113
        },
        {
            "id": "aeb02dc1-0a8d-45ba-a2ac-4a9dd21adb08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 114
        },
        {
            "id": "170647c9-e7f3-4ec9-8385-84b5e4751763",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 116
        },
        {
            "id": "3e172030-3e3e-48ee-926c-11c2424b5687",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 117
        },
        {
            "id": "0a6beaa3-5ab6-4f2b-910f-767649beeaf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 118
        },
        {
            "id": "ecc75d1f-d2bc-466f-b6ac-347df8115f4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 119
        },
        {
            "id": "2f08b0f1-d3a8-4519-8974-80350236c0bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 33
        },
        {
            "id": "a8e62e81-7b88-4191-bf72-c9a41599371b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 44
        },
        {
            "id": "51091309-91b9-48ac-87bb-ab0612b10f2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 46
        },
        {
            "id": "8fe4c8e0-735d-4801-8b17-da75dcec2ddf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 50
        },
        {
            "id": "2306fe30-e5c0-4797-8eb0-191f3233a1a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 51
        },
        {
            "id": "4c608cd6-c0ae-457c-8500-d690c1f02aa4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 55
        },
        {
            "id": "26274880-c402-4ea3-bf69-a0fc75272234",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 58
        },
        {
            "id": "4cbd9602-6dbf-460b-b349-861d988ecbe6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 59
        },
        {
            "id": "d48aa658-bd47-4e25-add0-e53d09992936",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 63
        },
        {
            "id": "8a2f9d89-89c0-48a3-aefb-ad9a920a4b7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 64
        },
        {
            "id": "53ac0e26-e6fb-484f-801d-99d04c459eeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 65
        },
        {
            "id": "664d9f0f-c5eb-405f-bbdb-6fd9777472b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 68
        },
        {
            "id": "bd5433b7-6cc1-4052-a40e-3755b4675e92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 69
        },
        {
            "id": "32574ec1-d84a-405a-84bc-0378cbbb6b1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 70
        },
        {
            "id": "b9a737fd-ee03-414f-8c5d-3ba75e57c44a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 72
        },
        {
            "id": "70c8c38c-3886-4f9f-8132-3408f760f8e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 73
        },
        {
            "id": "5f21c9c3-8cbb-42e1-8069-319de41d2dd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 74
        },
        {
            "id": "40f2571e-fa8f-44ac-ae2d-58c59c225d52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 75
        },
        {
            "id": "61e094f8-5c2c-44fd-ba3d-51bacd2eceab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 77
        },
        {
            "id": "e26a5c99-191f-4083-82c6-001877432483",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 78
        },
        {
            "id": "ba676313-f761-4f4d-a80f-2f2fbe051713",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 80
        },
        {
            "id": "3353db8b-607c-4783-aa4e-0fa02a3cad00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 82
        },
        {
            "id": "e9e9383a-d76c-48dc-84c3-1a3747b4a587",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 85
        },
        {
            "id": "faa4661f-d62c-4b32-85a0-d1f6961dda6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 86
        },
        {
            "id": "d75abae6-873a-444b-acd0-de92230e2e18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 87
        },
        {
            "id": "577be7f3-8794-4ca6-a5db-cc9ce5f4efcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 88
        },
        {
            "id": "2133e0ba-c6b2-4826-b563-75483a2ec3b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 89
        },
        {
            "id": "7123bf6c-4f25-4247-bb99-6f84daab7f5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 93
        },
        {
            "id": "4b9fb5a4-9e2e-47ac-bac5-31145378d454",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 104
        },
        {
            "id": "850aeab4-5d3a-4758-b189-bf542403d0b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 105
        },
        {
            "id": "836f1fe4-5df5-4c36-873a-3d281e9deebd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 106
        },
        {
            "id": "4d864d5f-5157-48f3-af2f-8c18fca5b9ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 107
        },
        {
            "id": "78a714c3-fa5d-45d0-ba95-eccfd10a4772",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 108
        },
        {
            "id": "01235ab4-64c6-4c8f-9667-dad6b1b0590f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 109
        },
        {
            "id": "33362f6d-ba98-4059-93a7-27c1f7e37412",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 110
        },
        {
            "id": "0b779b84-814b-4670-bb45-44bfde75a5c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 112
        },
        {
            "id": "4f588b76-3252-4f1a-8b28-ba5b6bc442a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 114
        },
        {
            "id": "9d1be3d7-ffb6-467a-8a35-985a70f18482",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 117
        },
        {
            "id": "60424e0c-bb4a-4d78-b1ca-42b3be8e7757",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 118
        },
        {
            "id": "8441c7da-6e77-4254-a8b4-b33f83a7e720",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 119
        },
        {
            "id": "af45dee0-b50c-4a08-94ee-7aaa1dc0d906",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 45
        },
        {
            "id": "7c086d88-4b8b-4c3e-8364-09f636c4824f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 48
        },
        {
            "id": "185f6597-a2b4-4c04-b553-9fdff7b94bf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 52
        },
        {
            "id": "53160adf-5eb7-4019-8bde-b16aaef4df2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 54
        },
        {
            "id": "0cf8892c-c40a-4e70-a1ed-157f257c791b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 55
        },
        {
            "id": "e1e19a94-6ab4-4e71-9688-df0cb023e55d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 58
        },
        {
            "id": "09e6fe63-fa05-4e70-a58c-9941cd26787a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 59
        },
        {
            "id": "6a5047e4-0f02-46db-a8e3-b4b33f429618",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 64
        },
        {
            "id": "2e142b0e-328f-4503-b9e2-3ca5ead7c535",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 67
        },
        {
            "id": "e1b672ac-32fb-4190-9bac-87979f5c979e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 68
        },
        {
            "id": "aa6d9580-c2ce-45b6-9113-58fd9052a03d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 69
        },
        {
            "id": "c8b33954-1860-45c7-9945-d7169869b0bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 70
        },
        {
            "id": "01160d12-e3b6-46f7-935c-7d1df4d2a43a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 71
        },
        {
            "id": "1668af04-96d3-4498-b031-f010f3922e83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 72
        },
        {
            "id": "3c3a989e-04c7-4217-a326-e96d991b5501",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 73
        },
        {
            "id": "005e3c2a-8dea-4857-895c-decf27ce9ec8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 75
        },
        {
            "id": "0069c27c-586e-42e2-a4c8-7682fba4db00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 76
        },
        {
            "id": "345112bb-cdda-4a66-afb0-9c1f1b1988db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 77
        },
        {
            "id": "520dac1b-94ea-442a-a1c9-9a1d884fa2d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 78
        },
        {
            "id": "12e2329f-7ae1-4a18-b283-0281c634793b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 79
        },
        {
            "id": "2a303fbe-dc20-4da9-818c-5e434634b163",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 80
        },
        {
            "id": "40224a91-dcc6-4e25-a3ff-d62e70122b50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 81
        },
        {
            "id": "eafa56a5-9bca-4bfe-aed5-3c46d16546f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 82
        },
        {
            "id": "7a7f6f40-f716-4a63-b262-a5cf67026f1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 85
        },
        {
            "id": "780c664a-7529-4f86-801b-d2425ead5ec2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 86
        },
        {
            "id": "c34f63d3-849a-462d-9052-a20fab06241f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 87
        },
        {
            "id": "37b682e8-6673-4ffb-8d78-1165298d0d09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 99
        },
        {
            "id": "3b78fa95-7090-49a2-8c2d-84f0a25f6346",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 100
        },
        {
            "id": "edd57060-0741-43bc-9791-ec5e437dc547",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 101
        },
        {
            "id": "ec78551f-1e90-430e-844c-60267fafcdfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 102
        },
        {
            "id": "f897f6e2-f7d6-4e42-bb7f-3226c7138353",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 104
        },
        {
            "id": "2c815cd0-04ea-4f9a-9626-5aba5c3f389d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 105
        },
        {
            "id": "571372b1-f544-45e2-97aa-a80740aef906",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 106
        },
        {
            "id": "5097643a-505e-4897-815b-6335a6b1b53d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 107
        },
        {
            "id": "dda43b1d-fc99-470b-a174-79431818693e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 108
        },
        {
            "id": "85ec6d56-e754-4430-ad97-3f87af189036",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 109
        },
        {
            "id": "cdf1103e-fc6f-4d69-a165-9f67c34d1dc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 110
        },
        {
            "id": "f22fccd8-638f-491a-bf2e-b33e2aad29a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 111
        },
        {
            "id": "7b65b610-2991-4411-9d36-7b556ae74635",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 112
        },
        {
            "id": "91525ede-7827-4266-a53e-b12ca5a9be99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 113
        },
        {
            "id": "4e041589-2a5a-4571-b76f-2c8b8448e3f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 114
        },
        {
            "id": "1fe9935c-9318-4b61-9ddb-b837ce4a3bdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 116
        },
        {
            "id": "50bda6e6-e370-4a28-9429-ca8cc792194e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 117
        },
        {
            "id": "7cc65258-18b4-4604-b2c5-d1ea35e1275d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 118
        },
        {
            "id": "6fa5ab7f-5e99-4a26-9972-1b7aed49b9e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 119
        },
        {
            "id": "0ecc810e-70b6-4be9-98d8-4a7708d40722",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 121
        },
        {
            "id": "1d048f7c-0572-48a2-9597-22cef3deca5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 40
        },
        {
            "id": "8f5ff435-64d2-4f6e-a0ba-f2b51c2d5be7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 44
        },
        {
            "id": "0a2f033d-b4e6-4543-9e7d-32c59905b4aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 46
        },
        {
            "id": "26b97825-c16a-4900-9df0-8dcdb9202468",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 47
        },
        {
            "id": "74b897ad-a455-4f4a-ba79-1feb2e28e6ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 48
        },
        {
            "id": "7a7329cd-4f5e-4624-afd4-709fc96ebc07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 50
        },
        {
            "id": "c0957dfa-0bdb-4fdf-bb82-d88bddd3818a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 52
        },
        {
            "id": "b0a42740-2b44-476e-8a12-143a3f5891d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 53
        },
        {
            "id": "5da32345-640b-4941-8dea-cbd15657bf77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 54
        },
        {
            "id": "a22d57b9-af57-46dc-a442-3b3bcf6a4c52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 56
        },
        {
            "id": "42ac1968-47ea-42fe-beb8-3e62fd4d3c0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 57
        },
        {
            "id": "291f02bd-0895-4e93-b0e3-3b8c0d0384ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 58
        },
        {
            "id": "6c7841f3-385b-4b88-aa41-42eee625da40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 59
        },
        {
            "id": "489bd14f-af78-4ca6-bd31-087715065c49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 64
        },
        {
            "id": "7c0d72d3-302a-44da-a379-1cd1cae20f81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 65
        },
        {
            "id": "1f47e44b-a3f6-4f66-9fa2-fe02f6051764",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 66
        },
        {
            "id": "759465df-23de-4907-9620-49e2d2ba06f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 67
        },
        {
            "id": "ae336c95-9a21-44ff-a462-97049b8aed53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 68
        },
        {
            "id": "d38ec7b7-332e-48f0-9ef4-b891957a36cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 69
        },
        {
            "id": "0f245a6b-0dec-4d1d-a713-ce26db0a0bf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 70
        },
        {
            "id": "0ade6a4f-706b-40d6-acd1-1dc9fdfdcf76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 71
        },
        {
            "id": "6f703459-2fe0-4e87-b177-ada26b55479a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 72
        },
        {
            "id": "e75b47e0-95f8-4142-b828-de16ee407f7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 73
        },
        {
            "id": "5177ec6a-16dc-4eb0-a50e-b2d03846faa6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 74
        },
        {
            "id": "5935b2cc-804c-432a-bf9b-4bad615f8b6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 75
        },
        {
            "id": "c4b8d73b-68ee-41b2-aab2-0592aff9bd6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 76
        },
        {
            "id": "b5d22d4f-effe-4ac8-863b-d4b6dfb7bacc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 77
        },
        {
            "id": "6f649bc2-fcd6-4d73-9b65-ff200c23eb56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 78
        },
        {
            "id": "7b34db29-c12b-4849-b054-49931be58329",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 79
        },
        {
            "id": "094fd691-7eaf-4264-8eca-a403020d26d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 80
        },
        {
            "id": "adf46960-d807-4203-a14b-fc542e8ea25f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 81
        },
        {
            "id": "43ca4fc3-fae7-44b8-84c3-91afc6dc8d6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 82
        },
        {
            "id": "aa84279e-6ace-4b84-871c-483db7048f5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 85
        },
        {
            "id": "125f93e3-782b-4c2f-b81a-e82568032e5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 97
        },
        {
            "id": "b99ffca3-198f-47c9-993f-6aed05ca96b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 99
        },
        {
            "id": "86297f7d-96e7-4587-9c81-e746b01aebf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 100
        },
        {
            "id": "1e3c7af8-6d18-4d58-a7d9-b37e37f61014",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 101
        },
        {
            "id": "9a1859d1-2ba7-4758-8531-adb42de46e09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 102
        },
        {
            "id": "2895b067-44ee-4186-83ea-2bdbf5700ccf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 103
        },
        {
            "id": "1a36580c-464a-4bb8-bc25-847deaff24e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 104
        },
        {
            "id": "6446e15b-da66-40d7-ad20-a7bccd68843b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 105
        },
        {
            "id": "48e30126-12af-4d4f-8972-3d16ce61b1ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 106
        },
        {
            "id": "4ec68c70-9396-4776-b1f3-056c36708f67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 107
        },
        {
            "id": "b83d02e3-58de-4435-af69-4e740a3af775",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 108
        },
        {
            "id": "9eda6d24-f39f-47db-9ec5-2ded7dc94221",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 109
        },
        {
            "id": "dc18cec9-bbf8-475f-96cc-9220b32c055e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 110
        },
        {
            "id": "0a0ccab0-0941-433c-9809-1967e2c129e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 111
        },
        {
            "id": "4f4611ed-da22-47f3-88c5-f4cbb38617e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 112
        },
        {
            "id": "dc8878d1-5be9-4b24-92c4-4872e1f15c27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 113
        },
        {
            "id": "4bf65053-b3dc-4550-8527-1dedfc4aa24b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 114
        },
        {
            "id": "2544778a-0378-4c3a-bc31-905b0b29de47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 115
        },
        {
            "id": "6a7ddfc0-34b5-438d-89ee-d3f0899a124b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 116
        },
        {
            "id": "3e6cc22d-0083-44b2-b048-674679171ccd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 117
        },
        {
            "id": "83d54d14-2a5e-45c2-a4de-cdfc36baa789",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 118
        },
        {
            "id": "c87a6db0-4f5e-44cf-8312-c5b55feeff0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 119
        },
        {
            "id": "a952b0a1-0cac-473e-8774-bb98710fe3bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 120
        },
        {
            "id": "3a5255ea-30f8-44ea-b027-aa289aaeac65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 121
        },
        {
            "id": "c4410929-6bd6-462b-a84e-d0d0be8979ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 122
        },
        {
            "id": "4b32eb16-e6b5-4d89-bf4f-de94b6a5fb49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 33
        },
        {
            "id": "718cf76d-f385-47f6-9e52-a2963a41f18a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 34
        },
        {
            "id": "4cc3880a-9e9a-43e7-9053-f5e8422764df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 39
        },
        {
            "id": "ba2be261-b926-4103-ac18-f1d8d52f6dd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 40
        },
        {
            "id": "38936565-6201-411f-ba4b-1cfc49b1ea96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 47
        },
        {
            "id": "d6711809-9a8d-458c-aa1c-866418650750",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 48
        },
        {
            "id": "6aab0aec-5ee2-4aed-9894-c548d3e089ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 49
        },
        {
            "id": "1d1f4796-2aa6-4344-ae34-23321636ce91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 50
        },
        {
            "id": "3cd69b8a-77d4-4142-8d8c-7d19bf45f32b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 51
        },
        {
            "id": "14823bd6-214f-4df9-b262-4cb85a600a79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 52
        },
        {
            "id": "393cbebe-e046-421e-b03c-7af20ad17e8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 53
        },
        {
            "id": "6cfe7456-6add-4b50-925b-9c22d211cb81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 54
        },
        {
            "id": "c2672f06-8f83-4ba7-90cd-ae9666308eaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 55
        },
        {
            "id": "c52eae63-8412-4223-9a14-507c136c868e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 56
        },
        {
            "id": "35dfde67-2738-44b1-883b-35ea350910ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 57
        },
        {
            "id": "1f985400-a2ae-4eab-b267-b4c466a15ea1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 58
        },
        {
            "id": "5c92ee19-414a-40f2-948c-ec00d97f10ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 59
        },
        {
            "id": "3cd6012f-c924-4636-8b60-87151b3dccb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 63
        },
        {
            "id": "66635498-aef4-49d5-9c40-771146967568",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 64
        },
        {
            "id": "a31deb11-0e3e-48d7-9c80-4e810ee01687",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 65
        },
        {
            "id": "ebca6515-b07c-43e1-aef6-810313c6cddc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 66
        },
        {
            "id": "4bd2dc5e-0171-4040-8881-57221010d37e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 67
        },
        {
            "id": "f2d0c82d-896b-44ed-bb82-4792275cebe3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 68
        },
        {
            "id": "c0b8ffa3-b31c-4871-b828-b818ede1a17c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 69
        },
        {
            "id": "5d6d741c-05a7-49c3-ab4c-236b166b976f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 70
        },
        {
            "id": "604d2614-6740-4477-ac11-fc5ee671ba46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 71
        },
        {
            "id": "983f9393-a7cb-4c7a-9b63-a43fe28680c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 72
        },
        {
            "id": "2d509ff5-30bc-4987-a834-2965fdb140c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 73
        },
        {
            "id": "8f1d6b16-37d6-4aeb-8f90-7d8d1988b721",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 75
        },
        {
            "id": "26bb7aab-20ec-48da-ae7d-fc8022d11fe2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 76
        },
        {
            "id": "2c8c73d2-0a6f-4a31-9e17-9c8acefe948e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 77
        },
        {
            "id": "189da048-3fbd-449f-bd7c-4d175fe1f191",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 78
        },
        {
            "id": "7fae6b45-0ded-47f7-a16a-19f6c885c14f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 79
        },
        {
            "id": "6a72af0d-1ed2-4693-9cd8-672e0800a49b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 80
        },
        {
            "id": "bf538e4c-1dc9-4c1b-abb4-c2e941648f0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 81
        },
        {
            "id": "8ad41f1b-00d1-4f2a-a801-a90241b4bfca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 82
        },
        {
            "id": "8ad1fbc0-15b4-44bf-9e68-a743655fb7aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 83
        },
        {
            "id": "5d11241d-2704-4442-b5fc-2346b160717e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 84
        },
        {
            "id": "6544bb97-49a9-4817-8be7-6a1c5043c139",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 85
        },
        {
            "id": "e68c71d0-0a04-4791-af5b-06bd7d1e5280",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 86
        },
        {
            "id": "a1aab5cb-2898-44ea-86fc-452d30f19eae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 87
        },
        {
            "id": "44de937a-32db-40e7-8404-e9138b3f5445",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 88
        },
        {
            "id": "1f3c521c-d46e-48f8-8ff2-b1ee6c940b46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 89
        },
        {
            "id": "65ee0b8f-1950-4a4e-a2bc-6c1de17ab67d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 90
        },
        {
            "id": "1e73f07a-21a8-4850-91ab-878ba6ee4867",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 92
        },
        {
            "id": "a867a9b9-e21b-4733-88dc-e2dc0b473caa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 93
        },
        {
            "id": "be9aabdc-0352-499d-962d-ac81aee0b787",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 97
        },
        {
            "id": "1a4dcdc6-f89a-4cb5-b0cf-03369db51007",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 98
        },
        {
            "id": "a9240768-8f86-4b2e-a455-91f48e470c1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 99
        },
        {
            "id": "4a894134-e379-4530-85fd-57ab654b0cf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 100
        },
        {
            "id": "8332c318-08d0-4ab6-995a-c82952c706ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 101
        },
        {
            "id": "4e36f257-e3fe-4854-b7c4-19c580105fea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 102
        },
        {
            "id": "724bcd32-e33c-4d98-be90-c768b902b9e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 103
        },
        {
            "id": "86899306-0215-4c9a-8147-da254758e433",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 104
        },
        {
            "id": "46834627-13de-4df6-bd28-c190a1a8f747",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 105
        },
        {
            "id": "0749a643-1a01-4ae0-bdf9-ee1360cb19aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 106
        },
        {
            "id": "9a27631a-d289-4599-ac91-517aacbc8d25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 107
        },
        {
            "id": "bbfd10cc-d8e1-4090-8f31-86a23764c761",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 108
        },
        {
            "id": "3426beea-128e-45e6-aca0-d634455d35db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 109
        },
        {
            "id": "44e28fd4-734e-42ab-9e8f-9f0e13f9b2fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 110
        },
        {
            "id": "bfb30aa8-199f-4ab5-a61c-74fb6316597e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 111
        },
        {
            "id": "007b132d-942e-4d00-9c9d-571e2a7c478b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 112
        },
        {
            "id": "cdf4ea87-aaa3-45e9-81fd-c5a5555a3e71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 113
        },
        {
            "id": "06b1ef32-0cf3-46a5-beb0-e87cca806cfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 114
        },
        {
            "id": "6be67b89-f78a-43a9-997c-5e054767abc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 115
        },
        {
            "id": "1597731a-d2b9-40ab-a126-1cf0672f810c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 116
        },
        {
            "id": "cab32d5e-2a6c-4d7d-bb4b-89532668b5aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 117
        },
        {
            "id": "a44dee09-1652-4e69-a241-64048d834f3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 118
        },
        {
            "id": "4560ecd2-6d01-403f-9799-0d1343920b0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 119
        },
        {
            "id": "6a501c79-3b3d-4053-96ed-25240b4544df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 121
        },
        {
            "id": "c78030b9-cd1e-475e-9966-d2bc5969fd86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 122
        },
        {
            "id": "4c1b3072-d8aa-444d-85a3-90c291d710e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 8216
        },
        {
            "id": "519cc486-9f42-407c-afaf-e8c88142687e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 8217
        },
        {
            "id": "b3dbf330-e272-49a5-97c8-bba62c482d2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 8220
        },
        {
            "id": "2624afb2-e560-463f-a45b-c8636e38ff1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 8221
        },
        {
            "id": "3a7c33c1-16b6-4311-b9ad-78f40e125c23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 33
        },
        {
            "id": "d42fe164-56f8-4260-9466-5c2213ce6a2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 34
        },
        {
            "id": "405e7e04-368f-4dc3-a790-5362a8d5f900",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 39
        },
        {
            "id": "52eeb812-f44b-4d28-949e-f1d35fcf25ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 40
        },
        {
            "id": "d1c4f504-0b3a-4613-9a6e-af03220cb8b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 41
        },
        {
            "id": "27122b60-1b95-441c-a612-fa9d15536e92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 44
        },
        {
            "id": "a8cda298-ff7a-4966-ac00-f64f968eb21b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 45
        },
        {
            "id": "49d307e4-7f40-4f6d-b197-1030097d287c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 46
        },
        {
            "id": "58597a98-8ff9-4e48-8068-374f9a8e2ee6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 47
        },
        {
            "id": "990d550d-786c-47b4-a127-26c314701841",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 48
        },
        {
            "id": "67ad25a6-a939-4b39-9202-75df452cf708",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 49
        },
        {
            "id": "95a6d59a-2858-4ff9-bbe3-8fbb54ef7e8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 50
        },
        {
            "id": "44f7f10a-9903-4815-b110-a3a500d8317c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 51
        },
        {
            "id": "9604f12a-336f-45a8-82f7-33a0973f54d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 52
        },
        {
            "id": "a08afdb5-6629-4182-b641-949d6f45cfc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 53
        },
        {
            "id": "f22635f8-1331-46f6-943d-8bcdfbd963e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 54
        },
        {
            "id": "836383d2-d65c-45ea-90e3-fe90f35ec1cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 55
        },
        {
            "id": "82170779-13a0-401b-8bb9-2ae334e293b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 56
        },
        {
            "id": "17d1b1cc-abfa-4e5e-aa60-16b41b6a77e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 57
        },
        {
            "id": "fe71e160-7a97-4e70-a25a-58e8114f1aa8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 58
        },
        {
            "id": "b7c80e18-f819-46a8-b43a-af0dcafb16ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 59
        },
        {
            "id": "c79a6416-33bf-4e86-81ec-65c5bfbf75ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 63
        },
        {
            "id": "ff289d67-c483-4b76-a66f-1de793b3c8db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 64
        },
        {
            "id": "7ad9279f-ecdc-481b-81bc-7b656408b45a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 65
        },
        {
            "id": "aa12b3b2-a4ff-4edd-ab38-052801d76450",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 66
        },
        {
            "id": "a388a8ab-ccaa-4949-9a04-2056fed05f46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 67
        },
        {
            "id": "33a92204-c411-4c79-a02e-9fad0db304e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 68
        },
        {
            "id": "a55d5c04-6495-422f-9b67-33c6db42791b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 69
        },
        {
            "id": "199138f3-5dcd-4eb3-bd00-60c506c6df75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 70
        },
        {
            "id": "265bbaae-5cf6-4189-9e03-20f76295a5e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 71
        },
        {
            "id": "7f217bd0-e3f8-477b-a45f-215077928662",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 72
        },
        {
            "id": "51735eeb-4850-4890-bd05-d174c25a425d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 73
        },
        {
            "id": "89e5c226-f8b5-4819-8682-4102559a8a42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 74
        },
        {
            "id": "1ca70dd5-f870-4c6a-80bb-d8ddd75e1cfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 75
        },
        {
            "id": "775251c4-e91b-4e5f-baec-ba42c63fad19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 76
        },
        {
            "id": "9a8080b7-4da4-4d9b-ac54-996b29ef0ec3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 77
        },
        {
            "id": "db4af7a6-bf0d-4f77-aef4-fce7e2b70a5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 78
        },
        {
            "id": "be89a5ef-f740-4787-bf74-67f69064763e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 79
        },
        {
            "id": "eed39eef-a2c5-48a1-abbb-7330940cb7aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 80
        },
        {
            "id": "36eb24d9-181b-48bd-9d3c-60904dfe81c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 81
        },
        {
            "id": "a9dc1a5f-5529-4332-89e6-47b6cfde0bb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 82
        },
        {
            "id": "01829b96-21ea-4202-b359-8f2dda5703e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 83
        },
        {
            "id": "cca2f53c-8030-4b29-b4a5-7f3ff28f61b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 84
        },
        {
            "id": "389f40bc-1a68-4cc8-8035-f161d8378586",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 85
        },
        {
            "id": "94ef60e8-6b91-4601-8425-a559dfe4e6d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 86
        },
        {
            "id": "876fbe93-5da8-41bb-8fee-2989850cc41b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 87
        },
        {
            "id": "121d106c-d9ca-4383-b401-740ea867294b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 88
        },
        {
            "id": "eea845da-76ea-4820-b962-77d89b88339e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 89
        },
        {
            "id": "95afc140-7b88-4277-82d2-b733fb0ee37c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 90
        },
        {
            "id": "420a45fd-0270-4b6c-8430-bf8d5f02e321",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 92
        },
        {
            "id": "ef8c2679-95ca-45fc-a3a6-d89000b85fc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 93
        },
        {
            "id": "fe11575d-2cce-46d8-9661-3816e37b43d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 97
        },
        {
            "id": "ab1129b9-2107-492f-9d4e-c7f50407c859",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 98
        },
        {
            "id": "b4596454-bbd8-4a9e-a7dc-892f571fc5ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 99
        },
        {
            "id": "84c9d58a-dcf4-44f8-99fe-7050da2a95d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 100
        },
        {
            "id": "89a53f80-a685-49c4-a3f5-b00444bd358c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 101
        },
        {
            "id": "769cb7d7-b8da-4e81-b232-1b38bb175bd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 102
        },
        {
            "id": "136de797-f993-42ef-8f4b-a4e2136cc9b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 103
        },
        {
            "id": "3b0b9915-c89e-490c-a791-adb2c31f0091",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 104
        },
        {
            "id": "093c1760-2e78-4c5d-8f23-77c3e68cc6fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 105
        },
        {
            "id": "7d67a654-e07a-4568-ac14-9b66238cc5dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 106
        },
        {
            "id": "5744b551-3b2f-49c5-b812-0c7f0c37e950",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 107
        },
        {
            "id": "fa803926-6f9b-4e8e-8d85-82c794f5ba9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 108
        },
        {
            "id": "2ae79f23-da93-4af0-b3f0-85d7b62f7018",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 109
        },
        {
            "id": "e2c0e93a-f151-4d62-8881-21be77955b1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 110
        },
        {
            "id": "576170a0-01bb-4dd6-93d9-6c432fd84838",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 111
        },
        {
            "id": "fe88262b-57c8-4c0b-b6dc-1c0c56b49878",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 112
        },
        {
            "id": "87def544-0730-4a1a-b13e-e55f0b254b30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 113
        },
        {
            "id": "e7113188-18c5-4bc5-a7be-2eae55080804",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 114
        },
        {
            "id": "1101fa5d-70bc-4e58-b4b8-f9d99df38785",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 115
        },
        {
            "id": "e6bee8b2-8718-4d55-ba3f-fef05aa5760d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 116
        },
        {
            "id": "00e8f865-9720-4e73-8280-4137379aa485",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 117
        },
        {
            "id": "98972456-291b-43d1-93f5-15f51115d96c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 118
        },
        {
            "id": "57050128-738c-40c2-93ef-941d1b8164f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 119
        },
        {
            "id": "81719ef1-28fa-47f6-b3be-34b7ee22be4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 120
        },
        {
            "id": "59627879-828d-4690-ad03-f79a9362239c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 121
        },
        {
            "id": "7c56dc14-4320-4aa9-b0b6-2dbe099f324b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 122
        },
        {
            "id": "e17b4296-8f7e-46c0-b8d9-b35440cf216c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 8216
        },
        {
            "id": "8219808c-e6f3-45da-9225-bf6a5b1715a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 8217
        },
        {
            "id": "a3d035be-79fe-4e6c-a616-9884523eed18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 8220
        },
        {
            "id": "0d9a469e-9eb4-4e73-8144-352f60fc099d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 8221
        },
        {
            "id": "8aa7cfe5-f2d1-420d-8af7-ee94400ad5d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 58
        },
        {
            "id": "25db32a3-0391-427e-8df0-1072b80fda8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 59
        },
        {
            "id": "deb1d2d2-95eb-4724-ac3d-cff7ae76d7f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 64
        },
        {
            "id": "ea7957f6-1349-440b-bcb9-4ffc301ea1b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 66
        },
        {
            "id": "50ca08ba-5ec1-41e5-9a68-e92a6dbacf02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 68
        },
        {
            "id": "8368077a-9e71-4d2a-a209-bd511c92b008",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 69
        },
        {
            "id": "4a862d02-fcea-42a8-9c5d-1d19068c14ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 70
        },
        {
            "id": "339a39b5-3a75-49eb-92c2-c6f685e23532",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 71
        },
        {
            "id": "f00f9325-985d-41a9-8e11-871c5adc19a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 72
        },
        {
            "id": "1fac6efc-5b8a-4811-aa94-eb04e505b467",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 73
        },
        {
            "id": "429f0d64-2e53-462b-8f85-531feb6a6b9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 75
        },
        {
            "id": "72bc8f3d-8477-4aaa-b110-f583aee8044a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 76
        },
        {
            "id": "5bcf8cec-ac7a-4f92-8756-70ca09b89827",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 77
        },
        {
            "id": "3abe6974-6b89-4c9e-8e8b-c95510ebd173",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 78
        },
        {
            "id": "b6853881-1446-4daf-addf-f0836777bfbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 80
        },
        {
            "id": "30b22d69-21b8-4dde-9865-3ad21d3f3d29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 82
        },
        {
            "id": "50565eea-bd78-46c8-9d1e-57f6c746eb1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 85
        },
        {
            "id": "fb5fc13b-3c92-4e43-bbad-9e78d3578230",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 86
        },
        {
            "id": "d3ef73d3-6ab1-49fe-988a-3850fa1da0bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 87
        },
        {
            "id": "b7c1d427-7660-48bb-9ecf-93fcf0d7fbef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 104
        },
        {
            "id": "d69cb792-d679-4b5b-8cfd-abbd02feeebb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 105
        },
        {
            "id": "938f90ca-7e73-4915-b87d-18c1e580fe35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 106
        },
        {
            "id": "94b34311-e9c0-417b-8df9-18254516461e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 107
        },
        {
            "id": "3a30be27-0d76-4912-91a5-90f70f0e2071",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 108
        },
        {
            "id": "458b5701-5f29-406c-92a7-f4c2b10d4c39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 109
        },
        {
            "id": "7d1e01e6-f101-42f9-9a56-21b937ea2e70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 110
        },
        {
            "id": "5ca3ffa4-7c67-4907-9d76-04d3ba60cf78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 112
        },
        {
            "id": "f9cf3fc4-1fab-4072-9856-23fe31597052",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 114
        },
        {
            "id": "71539037-8f76-4122-a380-218ee2b88a83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 117
        },
        {
            "id": "786d93e0-4e73-4edf-a867-8da557fb67a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 118
        },
        {
            "id": "6201d8d8-104e-4015-b84d-f4bcd4429068",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 119
        },
        {
            "id": "b5a142cc-9d41-4a74-a36e-40d26dc3fa60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 50
        },
        {
            "id": "069a5ba3-1dde-4897-a818-7a9830348047",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 58
        },
        {
            "id": "f082b3e2-ad34-47b0-9104-68c1cab4090a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 59
        },
        {
            "id": "34a66269-6f8c-4266-b048-a1610ed5d131",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 64
        },
        {
            "id": "10da39a0-eb8b-4b0e-a1d9-0777b2d86788",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 65
        },
        {
            "id": "7973545c-cceb-41d7-8015-d76ed81969c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 66
        },
        {
            "id": "3aea5254-1c1c-4fdd-bb68-b56234ab4749",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 68
        },
        {
            "id": "0873659e-c6c8-459f-ad42-f2cb10aacd2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 69
        },
        {
            "id": "6fbc1430-31ff-40bc-9761-6957d387f54c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 70
        },
        {
            "id": "e1f44ddc-29f1-4d82-9a7a-55b1e13c453a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 71
        },
        {
            "id": "85fc8c34-2d8e-44b7-a32c-d4c0750862ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 72
        },
        {
            "id": "48baaa29-f97d-4e2c-aea7-48fd9ddf0606",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 73
        },
        {
            "id": "93657dcb-dd63-4943-a6dc-cf1b6890e526",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 75
        },
        {
            "id": "c045d2fe-8618-4bcd-a6cb-3c69be7fac8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 76
        },
        {
            "id": "f405dbbd-db4d-4bbf-994f-ac3e5fca5035",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 77
        },
        {
            "id": "76b06362-fec1-4d1e-9147-2720c1b1951e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 78
        },
        {
            "id": "f4fb006e-d0e5-405e-82b2-55157ab30d50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 80
        },
        {
            "id": "e474bd77-0767-4017-b7b5-b410573949cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 82
        },
        {
            "id": "c57cb1d6-60d3-476a-9dac-b444a9566a1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 85
        },
        {
            "id": "6eae9533-9d95-4d17-a192-23d20988008d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 104
        },
        {
            "id": "19450741-5d6a-4e2a-ad52-075096f228fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 105
        },
        {
            "id": "6c52abe3-1661-4717-b6e9-a10f6850d18f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 106
        },
        {
            "id": "25434d07-f22e-4219-90ea-7c47819b3b6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 107
        },
        {
            "id": "361ac362-ccf7-4c4f-99cc-331d6d6af6af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 108
        },
        {
            "id": "1d634cd4-e4c6-4b5b-8088-41a59fecab39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 109
        },
        {
            "id": "f26f8c2d-f1df-43c0-a06f-69a5675cb4f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 110
        },
        {
            "id": "fa54d3da-e6fa-4747-ae96-efc122936680",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 112
        },
        {
            "id": "2c9c49c8-badd-4009-b0a0-65b4d0ea47c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 114
        },
        {
            "id": "076823bf-7b98-45d4-aec8-b9cf16b8df75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 117
        },
        {
            "id": "df02176f-20ed-47ae-adf7-58c89f5e5934",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 33
        },
        {
            "id": "78dbb736-e68c-4d78-bfa7-73915487c41b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 34
        },
        {
            "id": "249f6e9f-d79f-47a2-a288-a9e387034555",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 39
        },
        {
            "id": "46d29eda-0ba8-46f8-856c-b01734212a9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 40
        },
        {
            "id": "6d76a27c-9b73-479c-9669-c30238b9f4d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 45
        },
        {
            "id": "78fa717a-daa0-4894-bfa1-90db862c94f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 48
        },
        {
            "id": "9c9be2ec-d92f-49bb-beae-dab116803973",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 49
        },
        {
            "id": "d0ebcffe-a6ce-4c99-be69-85c40fa18ae0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 52
        },
        {
            "id": "13ec786d-d8dd-4948-984d-eee87ade8cd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 54
        },
        {
            "id": "d3267d00-c110-46e0-9440-171748959bd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 55
        },
        {
            "id": "2ec50377-2838-4bcb-88c5-e6107664a3da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 56
        },
        {
            "id": "d22c1d74-9f5d-4634-986a-6e7f1be6a66e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 58
        },
        {
            "id": "394b5732-b17b-4dcc-9a30-29ee24103c51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 59
        },
        {
            "id": "bcb532d7-1fcd-4948-8b1d-37fb0524c247",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 63
        },
        {
            "id": "837e8c11-dfbb-4c4a-a407-f5d3064310b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 64
        },
        {
            "id": "32b2235c-1e7e-4f65-9ab2-05487c6aed7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 66
        },
        {
            "id": "7bc95e14-48cc-4223-af10-e673088f42c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 67
        },
        {
            "id": "47e20667-ff0c-4adc-9a43-d3c09796c4a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 68
        },
        {
            "id": "98792f55-033b-4bdb-8f10-2b7e16fec0b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 69
        },
        {
            "id": "00ab6bb8-6a1b-4126-92bc-be4184aea931",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 70
        },
        {
            "id": "f87266ea-8c13-4ad1-8e0e-36dbc3810de1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 71
        },
        {
            "id": "ad7a9c25-8dde-4d30-a562-3aff09f29d41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 72
        },
        {
            "id": "394d912f-eae6-4158-b62a-f0507af5df6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 73
        },
        {
            "id": "55cf7312-440b-44ee-a961-101e18296e83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 75
        },
        {
            "id": "03d6747a-c364-4fe1-9b6b-d8eef44b676f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 76
        },
        {
            "id": "fa0798be-df9b-4fe2-a9e5-b32bf716304d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 77
        },
        {
            "id": "9bcafbe8-5c37-4a20-801f-f8eb3869b270",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 78
        },
        {
            "id": "debfead1-6227-4e73-b256-1c0afaa7e66a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 79
        },
        {
            "id": "98139ed6-2546-4df7-bec2-234eb1043daa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 80
        },
        {
            "id": "98ebd200-da47-4943-b298-e38f9c8d1d53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 81
        },
        {
            "id": "0f03d020-af50-495d-b598-b0b31e511c0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 82
        },
        {
            "id": "d50702a9-eecf-4db1-adc5-b929b6d72c24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 84
        },
        {
            "id": "8e2ec6da-5add-4563-856b-98f5962d7e2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 85
        },
        {
            "id": "928d4db1-41fb-4d08-86dd-bd9608f69e5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 86
        },
        {
            "id": "beaee609-83d3-45d0-93fa-44173a592080",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 87
        },
        {
            "id": "fbcba808-74ec-4108-ae97-0221a840227f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 89
        },
        {
            "id": "ff0cf5f9-20a2-4f9e-867f-8cfb7e6c2f43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 92
        },
        {
            "id": "926831c8-87ef-4dfb-8374-1f52e246b2bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 97
        },
        {
            "id": "d447b0af-ec4d-457d-a8f6-f007e4168f59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 98
        },
        {
            "id": "bce72063-41a7-46f6-b1f6-20975e84ea0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 99
        },
        {
            "id": "dace7e5f-e87d-4209-b47d-45333bb372c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 100
        },
        {
            "id": "74d821da-ea1e-41bf-a26f-3d126c141f99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 101
        },
        {
            "id": "a2be5e22-395e-401d-95fa-7c6e3b80c6d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 102
        },
        {
            "id": "c4436d77-ae72-4009-8106-1f02cc1ed5a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 104
        },
        {
            "id": "d9f1052d-b142-4f22-91b1-d933a98f275e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 105
        },
        {
            "id": "344b5fd6-cb03-4af5-a766-a360007b633c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 106
        },
        {
            "id": "b12d55c4-6297-46b1-b946-cf447271e2ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 107
        },
        {
            "id": "40f23d7f-d1e1-42bf-83f5-0a2dbe5bf742",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 108
        },
        {
            "id": "d820b01c-e846-44c0-ab13-bb2f72892bff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 109
        },
        {
            "id": "14708183-82a7-4faa-8ba2-db391cd4f172",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 110
        },
        {
            "id": "b53f864d-e553-4397-8681-441a619475dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 111
        },
        {
            "id": "32dfe87c-97f9-433c-99db-d683fb0c4f48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 112
        },
        {
            "id": "284c2168-9833-466f-ba2c-43523412748c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 113
        },
        {
            "id": "dc9f25d7-4440-465a-bfbe-c5341433d06f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 114
        },
        {
            "id": "bdfb3b24-93a2-44c9-b2b4-d98a70a02968",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 116
        },
        {
            "id": "01f0336e-f2bb-4d31-8187-55516465a84c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 117
        },
        {
            "id": "11ba6458-cf29-4554-882c-e64757d3ab17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 118
        },
        {
            "id": "1d0c9ef7-3b33-4967-b801-30ff08681195",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 119
        },
        {
            "id": "fac573a4-2459-4dd3-b63c-a2bd55c06518",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 121
        },
        {
            "id": "10733d39-29f6-4002-b2a2-ac36f9582abe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8216
        },
        {
            "id": "02da2b3f-ae80-43d0-878a-5f7b92f8d175",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8217
        },
        {
            "id": "2b209902-6c3c-4302-b0c5-b3f8ed5d9a23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8220
        },
        {
            "id": "a3653ef4-8e78-4433-b765-b1a946614dae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8221
        },
        {
            "id": "79dbfb70-c336-4046-bd02-abde1adb6dc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 34
        },
        {
            "id": "ae99e4f8-9fba-4cd4-8840-971003758232",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 39
        },
        {
            "id": "c063599c-cf4a-42c8-bd3a-5b6f60ff1767",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 45
        },
        {
            "id": "bc16ca11-ec3a-4104-8fcb-bddc7d93f9c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 49
        },
        {
            "id": "8a70b145-5ce5-4691-9b3f-0ff1b3c649cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 64
        },
        {
            "id": "28a42cf3-8272-4521-9796-89f869d975ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "67f421c4-7011-4c3f-b4db-703985508011",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "3bf569a3-e3e3-4a4b-aed0-addfc5a1dca4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "dc2eb5a5-b60c-4913-9b00-fbd498f1e6b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 89
        },
        {
            "id": "c0ffaabf-aa99-4704-b425-ca798d3b637a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 118
        },
        {
            "id": "6aeb9fce-99cb-41c5-80fc-cf93891d8836",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 119
        },
        {
            "id": "2cc07332-03b8-4aae-8c45-8eb8d8d58bde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "1542599c-8513-4478-95d9-ac7a8915cf3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8216
        },
        {
            "id": "be960b16-9d23-414f-b6ca-fe1f3262fd03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8217
        },
        {
            "id": "886bbc71-96e6-4523-bdc1-4be615d64f52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8220
        },
        {
            "id": "b82810e3-d847-44ff-9eca-14a6e4f92cfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8221
        },
        {
            "id": "30f328ec-6881-4db7-86fa-7b6bd749d392",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 33
        },
        {
            "id": "2b82b8d1-d536-4135-a682-37bf59ecd57a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 34
        },
        {
            "id": "01c944fc-29ac-48af-a557-10dd0ede71b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 39
        },
        {
            "id": "84931f63-e2f4-4810-9b18-c60bdbb0739d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 40
        },
        {
            "id": "236bf858-c4bf-4d72-8124-246dcd61d7cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 41
        },
        {
            "id": "44a06589-819d-4321-99f0-eaa102b2b8b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 44
        },
        {
            "id": "a4925be0-0d6c-4f5d-be2e-b423ff658514",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 45
        },
        {
            "id": "0c0d5299-5fdf-4ae5-b030-64aa109302cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 46
        },
        {
            "id": "f22cbdf4-ba17-4622-b829-88b2dd191d63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 47
        },
        {
            "id": "4df4c945-98e2-403a-9e74-60209ecd6e98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 48
        },
        {
            "id": "cf0cbd8a-e044-4972-a0ae-699147a85a1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 49
        },
        {
            "id": "444aa825-0c9a-459b-b23e-5d393bf50ead",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 50
        },
        {
            "id": "8bf32d51-e851-4730-902d-71174ba8a021",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 51
        },
        {
            "id": "a701ba86-fce6-4986-8a05-8512698142a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 52
        },
        {
            "id": "14103817-76b2-4faf-9614-5f1261d28337",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 53
        },
        {
            "id": "48ae221a-ca1d-4859-81ee-bf61c05be71e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 54
        },
        {
            "id": "d4f02cd5-cee0-458a-a53e-d35cd105e7ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 55
        },
        {
            "id": "101f7d0a-7bfc-4e73-aada-c453fbb5d007",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 56
        },
        {
            "id": "feed0ad3-4589-4e08-91fd-4d8fbd4e8736",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 57
        },
        {
            "id": "94fedeec-8577-414d-82c4-1551280411b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 58
        },
        {
            "id": "d651aaab-1e1c-4e06-b115-30d122d25359",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 59
        },
        {
            "id": "357264f9-b54c-47b9-85f5-165baa3e3649",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 63
        },
        {
            "id": "e94dd505-ddb4-4d98-8a8b-d3afc7bd595f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 64
        },
        {
            "id": "2e3974fb-fc85-404b-b448-dbf259acf55e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 65
        },
        {
            "id": "c7c75b94-e17e-41e4-87cd-84651a67135f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 66
        },
        {
            "id": "6c278e97-13ce-4ba0-a16c-be714c8c7327",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 67
        },
        {
            "id": "6b07373a-e0e3-4f00-b5bc-c3015c520e45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 68
        },
        {
            "id": "b02df1b3-ddfe-4d2b-91e8-bbf5abc0de94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 69
        },
        {
            "id": "e90f8c3d-07c7-423a-b3dc-401bc78176b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 70
        },
        {
            "id": "7fd77988-7333-4c30-ab2b-d83c269d36b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 71
        },
        {
            "id": "f39a90f4-31be-4b3a-bb4a-d3cf681a4ca7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 72
        },
        {
            "id": "230f80a8-1186-44bd-86e7-35d498e19dab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 73
        },
        {
            "id": "6674d6bd-203f-486d-bd58-b21f3b1e2aad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 74
        },
        {
            "id": "766e59d8-a3d7-4da9-8067-e57c24f2716d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 75
        },
        {
            "id": "ac2693bf-c669-4c82-9b82-618f053a42d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 76
        },
        {
            "id": "8ecbe86f-0b65-4502-96df-637e694b0a20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 77
        },
        {
            "id": "1761f775-da32-445b-9e44-94fcec61a7b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 78
        },
        {
            "id": "e61d08eb-baa3-488d-9d4f-b72030d24d68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 79
        },
        {
            "id": "27863879-9b2d-4495-88be-8dea93929cf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 80
        },
        {
            "id": "e4ad87aa-dc7f-459e-8a1b-505d72c59a31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 81
        },
        {
            "id": "5d22a294-94a9-436d-90f3-2e859b4ef52f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 82
        },
        {
            "id": "dc4c2e3d-1f96-47c8-9d35-25196cd9c3bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 83
        },
        {
            "id": "df012cf8-6f42-44bd-86bc-7f154b9d6b93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 84
        },
        {
            "id": "b6a0b0b7-0bf9-496b-8995-a9940b762006",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 85
        },
        {
            "id": "81a4587a-af36-4ccc-bfa3-5ae6f383afa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 86
        },
        {
            "id": "e5c5f78f-b00c-41d9-bb17-2479cd3def52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 87
        },
        {
            "id": "5db78909-7e4c-48a8-90d8-789496499d26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 88
        },
        {
            "id": "1d94b561-f320-4365-b345-835eb7390e6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 89
        },
        {
            "id": "22866b6f-27ba-4c0d-b927-f8cfb0b82e2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 90
        },
        {
            "id": "c82b1fc6-c67c-4946-a506-467da4e2dc43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 92
        },
        {
            "id": "f6dd470f-b49f-4504-8879-df096c5de2e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 93
        },
        {
            "id": "d341f91f-3f34-4fd9-ba8c-fd2336ef5957",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 97
        },
        {
            "id": "1e6546a5-a8c9-484b-a449-f9d78998e31f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 98
        },
        {
            "id": "b274d9ad-868e-4da0-906a-93bce4f9c4c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 99
        },
        {
            "id": "a64338c3-2ace-4bb6-a0e3-ecb9e892334c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 100
        },
        {
            "id": "bd7a6d57-1b62-440c-84f7-35cac065d683",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 101
        },
        {
            "id": "bdb19311-9951-4f3b-8d6c-86ff38f3a85d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 102
        },
        {
            "id": "e99abf4a-e382-4e5d-9a4a-8563099b5b84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 103
        },
        {
            "id": "0d1a6bc3-93f0-40c3-96e7-d7d2ae172517",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 104
        },
        {
            "id": "1c051634-904f-4d27-84ce-3df6ffbdc5dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 105
        },
        {
            "id": "1522cb77-da62-4b6e-b1b8-74ac3fa90d77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 106
        },
        {
            "id": "ff1ac8d2-04de-4577-9dbb-1e1641f319f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 107
        },
        {
            "id": "1f869ac8-c832-440c-a577-93ec47f1f6ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 108
        },
        {
            "id": "b5ac606c-de1b-49fd-8f44-ed7f5d7a6919",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 109
        },
        {
            "id": "9c7303d6-22ce-41ea-87da-2e3a8059ba7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 110
        },
        {
            "id": "af4172f8-7e84-467a-99d3-885c5ac0d671",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 111
        },
        {
            "id": "9b175431-7f9f-45ee-8f17-383506f5c89e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 112
        },
        {
            "id": "16f66340-9001-4971-8984-b89c68c94cdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 113
        },
        {
            "id": "1e21e618-5e81-407a-889b-64b7752fa38b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 114
        },
        {
            "id": "352a8128-a1e0-44b5-a6f6-9317bbb3cd4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 115
        },
        {
            "id": "67588088-a027-4cec-9d37-1a730bd2030c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 116
        },
        {
            "id": "031a857c-a47b-481d-a493-3d31c6719741",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 117
        },
        {
            "id": "c3cbfc27-9432-4372-af30-e155b95d2ae2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 118
        },
        {
            "id": "602f66e0-6db2-4ad9-a36a-62501e3029a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 119
        },
        {
            "id": "b8a7cc27-a8c8-417f-8154-623ff542adb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 120
        },
        {
            "id": "732c5a4a-60d1-4c12-87b6-2dc2d04a924b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 121
        },
        {
            "id": "73f32319-69b5-41a4-9fdc-ecf52d724224",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 122
        },
        {
            "id": "ec1cdd8d-0783-41d7-a9e2-60186721fb23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 8216
        },
        {
            "id": "c998572e-b197-464c-a27e-5c9cb55f5562",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 8217
        },
        {
            "id": "c72d9a02-c016-4fdc-aa93-5d2d9d8e0680",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 8220
        },
        {
            "id": "6e8a26ae-c25b-41ba-9f78-7114c78c620d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 8221
        },
        {
            "id": "3f8a534d-6b17-4b96-a913-0eea98cd49d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 33
        },
        {
            "id": "9d15ea2e-9b6b-420d-94dd-6ef7d96bd6bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 34
        },
        {
            "id": "0b4a1116-d262-4aaa-a33d-d6b206596f07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 39
        },
        {
            "id": "efb3eee8-4e04-4251-bdf8-aadf35c9bfe1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 40
        },
        {
            "id": "1d14612a-e5b3-43be-9e81-bdeb9f3f506a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 41
        },
        {
            "id": "0541c54b-c45e-4461-a00c-add605cb8d38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 44
        },
        {
            "id": "a411d81d-eb33-43c1-acfa-066a06373392",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 45
        },
        {
            "id": "6f0462dd-9b6e-45f1-92dc-b0ca36dc1750",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 46
        },
        {
            "id": "976eebb0-b5c7-495e-b197-95b3d935133d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 47
        },
        {
            "id": "6cf042fb-e073-4254-86b8-5348f676b5eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 48
        },
        {
            "id": "d10091e6-1d94-4732-93f3-938e5f816571",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 49
        },
        {
            "id": "f2c19cd6-b1e9-4b77-aadd-d0a4db7c302f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 50
        },
        {
            "id": "b9c9298e-7cf4-428b-924f-77d08c5f7bb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 51
        },
        {
            "id": "440a1773-6263-4e98-b447-400d46203879",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 52
        },
        {
            "id": "fd383c18-8858-428d-a68a-7c17f74b28c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 53
        },
        {
            "id": "21b42d64-dec5-408b-b2f0-4a4300d857ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 54
        },
        {
            "id": "c2d1840f-2e49-41a5-823f-5e730c9f2240",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 55
        },
        {
            "id": "fb6de258-88c5-4d12-8a5c-91ba2824af47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 56
        },
        {
            "id": "63ab2140-7dad-4dce-83e5-aed0029320ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 57
        },
        {
            "id": "4625af2e-c9de-4d73-b821-dc39407e14f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 58
        },
        {
            "id": "e7b08463-01fc-4912-ad52-a9aea0c84278",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 59
        },
        {
            "id": "25d860b8-a6e0-461d-873f-2c01b1d2ff4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 63
        },
        {
            "id": "5defefa3-8515-411f-b06d-ea9e22953f6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 64
        },
        {
            "id": "f7ba1b78-e03c-431e-bbc3-8c977e2cd3a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 65
        },
        {
            "id": "a87b913e-eb31-4cc4-b704-ef80d9e63517",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 66
        },
        {
            "id": "e955ff62-8b5e-4f59-9dc8-bfd653030f93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 67
        },
        {
            "id": "769bdf22-f9fa-4a86-8181-cf54fa358eff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 68
        },
        {
            "id": "d268c124-3d6d-4ab0-9b4f-8304bdd31949",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 69
        },
        {
            "id": "3b1e90fc-763f-4131-ad42-59864a9be763",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 70
        },
        {
            "id": "d62a676c-4a93-4bb7-b751-245d8da7ffb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 71
        },
        {
            "id": "98647331-7e58-4d21-8861-190b0cd91f25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 72
        },
        {
            "id": "319ee7e5-e55b-4cc4-85a2-473c0afa3131",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 73
        },
        {
            "id": "ac1506ce-b4e1-473c-afd9-d4f42f89e5ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 75
        },
        {
            "id": "44ded966-cde4-4849-b240-094c0e3c012a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 76
        },
        {
            "id": "fb596197-717e-41f1-a0ca-624b1772c970",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 77
        },
        {
            "id": "10b8f00d-6c92-4ab6-bfed-1be7b3d63ea1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 78
        },
        {
            "id": "1840b58b-5965-4ad5-a669-3195b591b171",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 79
        },
        {
            "id": "3ded49b2-3fc5-450d-abd8-2146b5460631",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 80
        },
        {
            "id": "fa8fb28a-02dc-4a7f-affd-ae818e34505f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 81
        },
        {
            "id": "3c09c538-f58e-4999-8669-09bd61f18f0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 82
        },
        {
            "id": "4538e46b-29fb-46fe-be06-b15e15030b30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 83
        },
        {
            "id": "66ef0e64-407c-44de-bd56-99451b287de3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 84
        },
        {
            "id": "fb32e5d8-0017-483e-b56e-b2c525c24df8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 85
        },
        {
            "id": "a505e0f2-6242-4c74-a9c3-c1bc8cb289b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 86
        },
        {
            "id": "7a1354c9-fc33-419c-ad41-c4f026ea2ac3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 87
        },
        {
            "id": "8c343c2e-3bc1-49b7-9071-ec4501acda3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 88
        },
        {
            "id": "a3503830-bb05-4fad-8d20-60fb1ed6c50f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 89
        },
        {
            "id": "45efaf63-a4eb-4b0c-9e0d-71ce57d8bdf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 90
        },
        {
            "id": "321c79c8-f2b7-465b-abd6-32dd8f04370e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 92
        },
        {
            "id": "4cf25dfe-11f0-4943-95d1-ef59c89a5e65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 93
        },
        {
            "id": "65f1b291-ed31-41ac-86ae-c3df4c4cefc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 97
        },
        {
            "id": "6b95311e-bc65-4165-9e1b-5c757adb43f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 98
        },
        {
            "id": "86cb5983-bd76-4654-8558-a4f13dd2519e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 99
        },
        {
            "id": "2ba0c0c1-cef2-434c-ab19-c3becfb850f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 100
        },
        {
            "id": "0595494e-1085-4e79-9a79-532850b35086",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 101
        },
        {
            "id": "26bd87ba-aa61-4071-8cdf-8ee96be3021e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 102
        },
        {
            "id": "ac1764ea-dd13-48af-9c9a-4e461631f868",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 103
        },
        {
            "id": "bbb79e68-e138-47f8-acf7-e34d17c50c1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 104
        },
        {
            "id": "b1a5049d-5dc3-45ca-8399-17c237b03f9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 105
        },
        {
            "id": "12f97e31-1d59-4ef9-ac77-e46d133cc6c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 106
        },
        {
            "id": "e95dea05-3afa-4e48-bfca-dd9d4421219b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 107
        },
        {
            "id": "6ddba3f1-993e-4383-bfe4-3b57d482ad9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 108
        },
        {
            "id": "81dfb267-6489-4fde-994f-081749b216b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 109
        },
        {
            "id": "b01ea1ae-23f0-4045-8f25-df6a5a225fe6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 110
        },
        {
            "id": "a28566ee-80b3-4e62-a682-04d1b7cf9817",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 111
        },
        {
            "id": "5bc42bdd-2068-4f93-b6b5-90cc504c3d5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 112
        },
        {
            "id": "ea25f919-1222-4496-b3f4-9d9791d100c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 113
        },
        {
            "id": "fcd330c4-e680-48b0-a14b-1f5ad4b31ce7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 114
        },
        {
            "id": "7294c0c9-e1d4-4522-9000-fe96c2240c96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 115
        },
        {
            "id": "58073eef-7846-4915-ba47-e78647dbdd34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 116
        },
        {
            "id": "1158fd85-a292-4c9b-9d1b-b05ff3422a73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 117
        },
        {
            "id": "ae4721bb-a187-4423-bcda-1be0f48ac39c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 118
        },
        {
            "id": "2227306c-016a-4aca-9c7f-c04bb8de4832",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 119
        },
        {
            "id": "b5b50d6a-f3c7-49e6-b41b-f0f5782668d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 121
        },
        {
            "id": "c5dc45a6-dcb4-4f5e-bc3e-72db709ca0a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 122
        },
        {
            "id": "baf2516a-daaa-4289-8bbe-ecb2e0e840bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 8216
        },
        {
            "id": "1e196a38-46d3-4b6a-94dc-9e416a2b291a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 8217
        },
        {
            "id": "64a0e4c6-7cf6-42e1-af41-9a0553721bb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 8220
        },
        {
            "id": "17baf7d6-62be-49e0-aaa5-2f0361b5eadf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 8221
        },
        {
            "id": "b69693e5-8528-4ef0-9422-bbbbfba49272",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 33
        },
        {
            "id": "173f75dd-6a48-416c-968e-c698d221956b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 44
        },
        {
            "id": "0aa0c1d5-ee89-46ae-aacc-09460e9e0195",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 46
        },
        {
            "id": "826dfd3e-3bf4-41ae-b336-6031e00ebd3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 50
        },
        {
            "id": "c28f9b03-8956-4f40-96a1-9c798a4f62cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 51
        },
        {
            "id": "a58910a0-0d34-48db-81d7-8bb4baeaaf9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 55
        },
        {
            "id": "cfa1179d-b132-4285-9b98-d3969941c630",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 58
        },
        {
            "id": "eeac4e31-6886-4bc2-9e72-67ccdd685afd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 59
        },
        {
            "id": "d85b8bcc-359d-4b0f-8911-247d060b33ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 63
        },
        {
            "id": "1cd9f475-83e9-4b3d-abca-a89c97700129",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 64
        },
        {
            "id": "f192e134-1e33-49a6-8b39-d6e60ce22041",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 65
        },
        {
            "id": "1d2b8b0c-5251-422f-9fa5-9604e400e125",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 68
        },
        {
            "id": "03d3f49e-7767-43d6-8edd-1283e06ee8d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 69
        },
        {
            "id": "b6684858-a912-4f04-9655-5243cbab6427",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 70
        },
        {
            "id": "305f730d-6b26-4a15-aa6b-3b568eee1c92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 72
        },
        {
            "id": "0bb535fb-c850-4e10-9be5-452313503aa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 73
        },
        {
            "id": "c8aa1c9b-95bb-41be-bcfe-f792a3bc73ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 75
        },
        {
            "id": "47c44063-62ef-4045-bdd0-8bed91290d42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 77
        },
        {
            "id": "26451816-5939-47f3-92c8-f54ccaf48722",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 78
        },
        {
            "id": "529a7a12-0a69-4bb0-9608-24a6d6e64608",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 80
        },
        {
            "id": "5be4c1b6-3666-411b-ab0f-b4e327c2dddd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 82
        },
        {
            "id": "ab1acac0-71a4-41f9-a9ea-a68fb0433c5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 85
        },
        {
            "id": "06e8f7b0-789e-4ee8-ab2f-6331e2bec601",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 86
        },
        {
            "id": "99ec4ef3-e287-459f-a87e-f716c0bbf23e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 87
        },
        {
            "id": "9bd7c0fb-eec9-4c4d-b58d-45daa78144c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 88
        },
        {
            "id": "807906af-00b1-40ec-bc02-dc34237243bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 89
        },
        {
            "id": "eccd4d5c-a021-43a3-86d9-956fae10226c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 93
        },
        {
            "id": "1f078cfc-3f8f-472c-ac28-180df2e3b533",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 104
        },
        {
            "id": "ce0761b6-2543-40bd-8532-72dedac16484",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 105
        },
        {
            "id": "95b2ee35-6181-4409-a34e-1f0ae2eaa979",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 106
        },
        {
            "id": "72609f27-21d3-4996-b0d4-28a3cf7d1e5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 107
        },
        {
            "id": "0ceb7061-74f1-460b-818a-b841efb0b658",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 108
        },
        {
            "id": "2a8103be-5c8a-478e-8764-2d4af944f861",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 109
        },
        {
            "id": "72120bce-9dbb-4513-b6b1-6bee9567c4e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 110
        },
        {
            "id": "9b3aa067-a3e3-4df0-b6dc-db78047c2008",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 112
        },
        {
            "id": "7c8e8d27-d094-46ec-b773-601e813632ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 114
        },
        {
            "id": "b8de32a7-8668-412f-8236-6e2bf36759e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 117
        },
        {
            "id": "2506618b-701e-4cb9-9615-3003cefea813",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 118
        },
        {
            "id": "fbcf4e5c-83db-49f9-afa3-8f84649dd53a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 119
        },
        {
            "id": "eda39c57-f703-4f4d-bca5-30710d48ae30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 33
        },
        {
            "id": "f659d489-f4c1-4cf2-bfbe-4518706c5710",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 44
        },
        {
            "id": "86642235-ad96-4567-9f76-7a2372b0f0df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 46
        },
        {
            "id": "12b22b86-f486-4816-8750-5db65afb5758",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 47
        },
        {
            "id": "e52d1676-e321-4b2f-93d6-f0e0d08dbb72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 50
        },
        {
            "id": "98bbee13-0f46-4478-afd7-ed66193e62e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 52
        },
        {
            "id": "beaf0d7b-78f9-4eb4-8fb9-c738f65957b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 58
        },
        {
            "id": "fa71d160-ee4c-493d-933b-5e3603508969",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 59
        },
        {
            "id": "ca338398-3e83-4e2c-b9bf-84ea06935f9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 63
        },
        {
            "id": "c2be0d66-3d7a-4b7c-afaf-f7b697501783",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 64
        },
        {
            "id": "cfa0ae2a-d465-4e56-b89f-8a9eebe253d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 65
        },
        {
            "id": "73afcea5-a51c-4993-8c51-4cc235faff19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 68
        },
        {
            "id": "0c6b9d0d-2265-49c8-98bb-a444e799ac61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 69
        },
        {
            "id": "be4f2d37-4426-42d9-af06-1b97fc42cbeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 70
        },
        {
            "id": "444e8866-eb43-48ce-831e-37dcb1dc3283",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 71
        },
        {
            "id": "a2be4aa4-20ee-4dbb-a252-4b0b526c3b0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 72
        },
        {
            "id": "a4a3129d-8fec-48f9-bb88-8131ba64d470",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 73
        },
        {
            "id": "fb1b1d1f-9ccd-455e-84d7-aa4008f0a332",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 74
        },
        {
            "id": "c7da051e-708d-447b-ba44-942adfb850a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 75
        },
        {
            "id": "b3c7e778-198b-4dd3-9c11-59e80fc10628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 76
        },
        {
            "id": "96cc2bdf-394d-4094-bcbe-365af8418c82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 77
        },
        {
            "id": "570dde82-5fa8-465f-8553-f8f4b0af3a4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 78
        },
        {
            "id": "6fb6be7d-3a96-41f3-809a-acd813351687",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 80
        },
        {
            "id": "50d3e1dc-b3ae-44f5-a762-9bfeab05c56a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 82
        },
        {
            "id": "46e26db6-7f56-4a05-b5f2-15a694b2903c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 85
        },
        {
            "id": "3bb9b25d-4749-4775-96d5-c390ff8a0dda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 86
        },
        {
            "id": "f81fc774-b2ce-4e95-b648-e7f28f3e3220",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 87
        },
        {
            "id": "28f79af6-c06f-4d29-9dbb-b4690ce37048",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 88
        },
        {
            "id": "1044b9e0-47f9-4d41-acd8-c9a91b2f300c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 89
        },
        {
            "id": "8f4c1099-d5f3-443d-a50a-e24aece2bcec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 97
        },
        {
            "id": "a169c09f-9030-476b-9756-efce814d3ec5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 99
        },
        {
            "id": "8e44ff2c-da8a-4ae3-8dd1-738c55a7115c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 100
        },
        {
            "id": "b83ffd0a-274a-46ad-a52a-34957f16a7de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 101
        },
        {
            "id": "4caf6d7a-8870-4fef-84d5-c203a4b0e6f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 104
        },
        {
            "id": "3b1f7472-9e82-4480-ace8-5753c951d225",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 105
        },
        {
            "id": "e4a44184-49e3-4a59-9aee-d1fa975c3533",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 106
        },
        {
            "id": "dcd5e1b0-dd65-4cf8-b053-77432c860944",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 107
        },
        {
            "id": "f45a8629-bde3-4ca1-b3a5-9b1127b0b3bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 108
        },
        {
            "id": "fcc16d48-22a2-41ce-b64b-f0c637724cf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 109
        },
        {
            "id": "a7b5fc20-fc0d-457e-b0e9-cf758dc7e5c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 110
        },
        {
            "id": "bb0ad4dc-957b-455d-979f-fb0d4c9e0494",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 111
        },
        {
            "id": "5371769f-291b-4956-bb36-48f80523afd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 112
        },
        {
            "id": "8078ecd4-a25c-4901-8453-66716c0ae520",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 113
        },
        {
            "id": "f784d3a9-4c93-4c92-9d1f-a761fca977cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 114
        },
        {
            "id": "5460b466-7537-46c7-9aa6-2a2141a190c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 117
        },
        {
            "id": "6b7dbc62-3e5c-43d3-8a5d-2377bd841934",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 118
        },
        {
            "id": "6dcfe52e-9cc4-455d-9f1f-1479649930a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 119
        },
        {
            "id": "01b60c6c-d44d-4251-a77c-4b15cc2e0fd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 33
        },
        {
            "id": "9d5db4fb-60a6-477c-92a1-ed054c672c4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 44
        },
        {
            "id": "0d6b5c54-1004-49aa-b47d-0a19573a2782",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 46
        },
        {
            "id": "c81277c4-5b4b-4f2d-b6b5-c29b27be7fd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 50
        },
        {
            "id": "48772ce3-3781-4396-ac04-46380d6d51a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 51
        },
        {
            "id": "71eaeec6-e65d-49f5-82f6-87401dcaa166",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 55
        },
        {
            "id": "0110db1e-c59b-4ff0-a9ce-ee87d5716bf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 58
        },
        {
            "id": "c3c608e2-4cde-4b41-9f3e-61ad6d10a130",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 59
        },
        {
            "id": "e0d41dcb-b1cb-41ad-a7e7-6c397dfc7ab4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 63
        },
        {
            "id": "dc5d29ba-d8ed-4981-adfc-4045038941d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 64
        },
        {
            "id": "46bf07c3-0d09-4dda-b6e4-fb412c9008b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 65
        },
        {
            "id": "d43e8237-dc99-4c84-978f-e27d9acf722b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 68
        },
        {
            "id": "1ccca017-baf8-4c8a-bfbd-3bb7b314df90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 69
        },
        {
            "id": "d3554fc0-c13b-4260-800a-93a65d08cb1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 70
        },
        {
            "id": "f940ef2d-d212-4833-910c-4771b035dfd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 72
        },
        {
            "id": "e2e42570-0b52-4a35-a92b-f9c2220b1d46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 73
        },
        {
            "id": "62415ea7-b6e0-4d36-b7ca-029dfdb59605",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 75
        },
        {
            "id": "91cd1705-77bf-44f2-b80b-35ab4b7c0d35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 77
        },
        {
            "id": "e161bd11-4a89-4120-9716-eda53d0224b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 78
        },
        {
            "id": "ad681e2e-65a8-44ea-8328-7f07d22892f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 80
        },
        {
            "id": "3866ee37-3f13-4b93-9bac-48092dc63cca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 82
        },
        {
            "id": "16f0f9c0-5c4e-49b2-bfb2-2964e081e491",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 85
        },
        {
            "id": "e8c8abea-f420-4606-9a72-1fe29ed9ebe7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 86
        },
        {
            "id": "dfaf0844-323d-4097-a7c5-709e3359b5f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 87
        },
        {
            "id": "b37946d4-5b78-47b6-a2bd-78c68074e6ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 88
        },
        {
            "id": "07bf5371-2ab0-4702-bfad-014d1a7c92f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 89
        },
        {
            "id": "2517e206-b495-4047-9593-45af80613299",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 104
        },
        {
            "id": "89e20067-4c30-427c-be62-ac58bc70a979",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 105
        },
        {
            "id": "3cad52c2-c835-4c03-8432-bb38b936d699",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 107
        },
        {
            "id": "47f62f40-823d-4339-b0b1-d149d17f67d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 108
        },
        {
            "id": "195d47a7-7527-4d52-bd95-34194983ae28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 109
        },
        {
            "id": "0b684ffd-37f2-435a-806b-3253e2c26ea2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 110
        },
        {
            "id": "0d6dc92f-39db-4487-9b7e-2627be567708",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 112
        },
        {
            "id": "450bbf30-3d8c-4120-9887-1929874dd2a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 114
        },
        {
            "id": "1715eba8-4357-4263-a0ac-7639c7de2cd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 117
        },
        {
            "id": "c09ccfcd-284b-40bf-a1fa-6434d6e433b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 118
        },
        {
            "id": "aee1be82-86a6-48a3-a188-3eeafedb0f79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 119
        },
        {
            "id": "cf0b5395-3d43-4cba-9e78-7ee63f40a9e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 33
        },
        {
            "id": "8285a3d2-6561-4f94-af18-534b530b4bd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 34
        },
        {
            "id": "a2d11454-56a3-4334-b3b2-755c628aa5e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 39
        },
        {
            "id": "611febab-585b-4bdd-85b8-9b8b38421082",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 40
        },
        {
            "id": "550daf2c-b01c-4ca3-9350-6054ca69c243",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 45
        },
        {
            "id": "fd2ff5ee-34b5-4547-9901-20d0fce4a6e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 48
        },
        {
            "id": "f56ce47d-9ecf-40a2-ac64-528c7ae3924e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 49
        },
        {
            "id": "f2262e1d-2822-4736-a457-46685a539869",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 52
        },
        {
            "id": "1ec84230-ecdc-40cf-a94e-fb40ab1e1441",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 54
        },
        {
            "id": "11047cef-8d3a-4723-80f4-1049c5608b15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 55
        },
        {
            "id": "3ba053af-f751-4bed-981e-69deee5827aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 56
        },
        {
            "id": "8c742c9f-053a-44fb-b5f7-ca7f1bd82fbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 58
        },
        {
            "id": "f913c104-5935-41bd-986d-f509e5322bef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 59
        },
        {
            "id": "fbe89eae-9f62-4c98-acb2-9f9ff9a278c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 63
        },
        {
            "id": "cd95db47-421e-4c6e-8b7b-7f5bd40ed175",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 64
        },
        {
            "id": "fd7089ce-89aa-4f77-a736-fd0f8045340b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 65
        },
        {
            "id": "06b42451-6b7d-4cab-90c3-653da8e27064",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 66
        },
        {
            "id": "f1588172-ffae-4f8a-939a-548daae70508",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 67
        },
        {
            "id": "c8446405-922e-48fd-a58a-5ee55ff8312a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 68
        },
        {
            "id": "1e811556-d99b-4e72-9de1-b01eaf278aca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 69
        },
        {
            "id": "a81bf861-c073-436b-b681-cd49590f1411",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 70
        },
        {
            "id": "f9793ca6-f1d2-4857-85a3-1ecb0a69142e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 71
        },
        {
            "id": "701f4a85-3846-4a31-a081-f5c377d6e224",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 72
        },
        {
            "id": "7663b2c1-0925-4483-a5c1-291384b5d28f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 73
        },
        {
            "id": "a2da1b5c-6f63-4ddd-ae42-a7a51f35f376",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 75
        },
        {
            "id": "12baf217-bf28-49b7-bcff-72aa8c9ebb05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 76
        },
        {
            "id": "f6fc0b6e-c018-466e-90ea-00e8cd209e0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 77
        },
        {
            "id": "4138e573-73c0-44c2-8ae4-a7ad3908a88a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 78
        },
        {
            "id": "02f33201-03df-463f-b1c8-c31d7bf507a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 79
        },
        {
            "id": "90e13b9f-26a3-49fa-a80b-a5669ba7bc16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 80
        },
        {
            "id": "755b5935-d711-43f0-82e1-5a94032f56de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 81
        },
        {
            "id": "59ea80e5-6f04-46d2-a054-1149808ce232",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 82
        },
        {
            "id": "3fbfc717-27e9-4fe9-ba28-daa3f00c10d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "5ddc950f-dfa7-4295-adfc-23c9777e4a21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 85
        },
        {
            "id": "42f11b24-6698-48a8-b5f0-b8ff652350c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 86
        },
        {
            "id": "e76a9f51-4c86-4099-a9e0-b79b78f1ea2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 87
        },
        {
            "id": "c146e481-824b-4d73-93ac-6b97bd81024d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 89
        },
        {
            "id": "6f84bea6-7bb2-4be5-a102-116f91522620",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 92
        },
        {
            "id": "8a7fffde-fd2f-4b54-a24c-e84ccf699b69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 93
        },
        {
            "id": "1f58849f-44dd-471e-b421-db1d0f51cfa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 97
        },
        {
            "id": "98fb7e84-cabf-4005-be34-65998226a7d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 98
        },
        {
            "id": "0d46bc80-f449-4c64-8bf2-6da1dc387883",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 99
        },
        {
            "id": "46652859-1191-4813-8c5f-ad031120cde9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 100
        },
        {
            "id": "634159c2-dd08-47cd-b503-955f36ea0213",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 101
        },
        {
            "id": "3d48318d-0ff1-4a69-8a16-a8d151d9a177",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 102
        },
        {
            "id": "1e614880-47d6-4490-b4a5-ee3839bcfd4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 104
        },
        {
            "id": "aa16fe4b-dcf9-4352-a7ec-cbb0de31ca03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 105
        },
        {
            "id": "1c692304-8af3-41c7-b7ec-d1b6070adba1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 106
        },
        {
            "id": "31a65a9d-c6cc-47b7-b1d8-4f489345a762",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 107
        },
        {
            "id": "9de37f6a-25cb-42b0-97a7-e6b56358b55c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 108
        },
        {
            "id": "bd42ddd1-3d47-498c-8dff-5e2b90aa5d7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 109
        },
        {
            "id": "4ea490f6-90df-4903-900a-eee75121be32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 110
        },
        {
            "id": "b9f426c0-fe26-44aa-bfd3-459b94d30b95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 111
        },
        {
            "id": "0716eed1-3777-4b17-adc8-b3d416c13999",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 112
        },
        {
            "id": "96da4621-05e6-4434-828b-3b3ab1fd854e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 113
        },
        {
            "id": "a17041f6-931b-4dcb-8adc-b6fe895f4e2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 114
        },
        {
            "id": "fff1bfbf-d90b-475a-9162-a0df53177d39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 116
        },
        {
            "id": "0edb0359-ea4c-45d4-b009-14b6056c04b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 117
        },
        {
            "id": "dd584a70-8af3-4bb7-aa63-8e2b5252e00f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 118
        },
        {
            "id": "a12fce9d-a64a-43e9-9e6d-89987faafaca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 119
        },
        {
            "id": "b59644be-19ad-4eac-8fd6-2c861af0d286",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 121
        },
        {
            "id": "534bfb84-bd64-43f6-92c9-a2e99071b5ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 8216
        },
        {
            "id": "3e4f7bd1-d1f9-4db7-bb0b-ef81b55bc584",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 8217
        },
        {
            "id": "c11a9ff5-a4c2-4754-a263-e8b8a86c6926",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 8220
        },
        {
            "id": "acd706bc-1c4e-45df-8b7b-66405deefa60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 8221
        },
        {
            "id": "b4775b51-09c0-4a1f-87ac-1762cc256f41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 64
        },
        {
            "id": "eb1a881d-9cc6-4fd9-9425-029a62258c8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 65
        },
        {
            "id": "ec8ebff1-76af-4fe6-af45-b16a5af8abbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 105
        },
        {
            "id": "68a1ae6d-737f-4b6f-baf2-3e62501a6ee5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 109
        },
        {
            "id": "8dec245b-3304-4091-96d8-ed216b1c70ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 110
        },
        {
            "id": "e7dcd11c-e29c-4998-8844-2630d6c1714a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "670b25d9-995b-48aa-b83a-f0ac1d8fff02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "2cc97430-ef2e-4c39-b252-5233015cc73f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "c796f4cb-52d4-4a3b-9176-659aa44b138e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 52
        },
        {
            "id": "432f9e53-0d6a-49f1-929c-ca4bff213e19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 64
        },
        {
            "id": "e4f2469c-c23d-4bf4-9033-d4a7c151ae51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "7d8b2fbf-9d9e-4233-a8fe-74b134c8bd7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 196
        },
        {
            "id": "cb0262d3-310e-4e87-bafe-6f84f450a2e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 33
        },
        {
            "id": "836f860a-708f-4051-b01a-f532896bf53d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 34
        },
        {
            "id": "e96690ba-be13-4270-b5a8-3adccad2e373",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 39
        },
        {
            "id": "b8a8f10b-21c0-4a94-be6b-051521366001",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 40
        },
        {
            "id": "e8fb6fd3-c752-4356-91f2-e276ddcfc69f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 41
        },
        {
            "id": "b3793849-9c82-4e14-a3fc-fd54f14109d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 44
        },
        {
            "id": "4776e738-bbf2-48aa-99a7-d050533a356f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 45
        },
        {
            "id": "184e1826-f017-449d-83a3-f4a6427d4d18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 46
        },
        {
            "id": "0238ffab-ad35-4dcd-9757-3c35627365bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 47
        },
        {
            "id": "b29b70c4-0e0a-4b69-ac3c-d055fe23c6a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 48
        },
        {
            "id": "d119cb65-8095-4a22-a888-165b127a5af7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 49
        },
        {
            "id": "9d7030f5-ae66-4301-9c5c-d76c7b3fcf64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 50
        },
        {
            "id": "5eb32014-9549-4b97-9416-3612f1e35170",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 51
        },
        {
            "id": "b19ae54d-6ca0-445e-9594-592a9df939da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 52
        },
        {
            "id": "93c3de54-d7c8-45b8-95ae-71c9976676b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 53
        },
        {
            "id": "7fae1c1b-9c6c-4c47-b0ae-8654b2d873b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 54
        },
        {
            "id": "193b03b8-5b9b-45da-88db-89d6218e5037",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 55
        },
        {
            "id": "164847e2-f20d-4da6-88ec-b938046c0036",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 56
        },
        {
            "id": "27aec865-75b0-47ba-a2f7-e60157628703",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 57
        },
        {
            "id": "e2b067e7-4f8f-452b-8284-ae3636826ffc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 58
        },
        {
            "id": "5f5295b8-cdef-4f05-9969-b10c624acc8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 59
        },
        {
            "id": "e0aeb413-ce1e-4b78-ba12-a9000cb003c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 63
        },
        {
            "id": "aa572a96-64f4-461b-b538-6a492dcbc475",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 64
        },
        {
            "id": "fe0c3531-4baa-48c7-b9cc-9ebe7861b796",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 65
        },
        {
            "id": "442ed110-8088-4a32-a913-c912d5719526",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 66
        },
        {
            "id": "2efa4e36-aaa5-40b5-970f-97362c31739e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 67
        },
        {
            "id": "ff0014f4-fd11-4b6b-9a36-68e1dfd8adce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 68
        },
        {
            "id": "78e2c344-d3b1-424b-8685-546f20383e6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 69
        },
        {
            "id": "e392f1b0-85f3-4c58-8d11-0714fb14a810",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 70
        },
        {
            "id": "f2aab716-b3e8-465f-9768-a95c9f035947",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 71
        },
        {
            "id": "c8303b01-066f-4999-a96b-f39b57fb3d19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 72
        },
        {
            "id": "75467dd1-a0c9-45df-9726-2302d31086ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 73
        },
        {
            "id": "e46eba47-6141-4755-a5f9-f6d48963ea12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 74
        },
        {
            "id": "050a3338-7ede-4e7b-b7c2-20e5141219d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 75
        },
        {
            "id": "c5edf002-ce2d-442f-ae37-08d1ea803cc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 76
        },
        {
            "id": "5b3c968c-7e59-41c9-896b-68b29cefdb8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 77
        },
        {
            "id": "162677e3-2e03-44a3-ac42-86ef0594cef3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 78
        },
        {
            "id": "9fb39674-ac92-49b4-bac5-85433c18fa3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 79
        },
        {
            "id": "1965466f-f22d-4c6e-a4f0-695d84536ec9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 80
        },
        {
            "id": "96eefbcf-b9d3-4d46-8de9-eab52ebbf71c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 81
        },
        {
            "id": "75826174-4066-4d96-9f51-ae5be8195a73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 82
        },
        {
            "id": "229528e0-1e31-4ae8-ad3f-a25ca42da639",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 83
        },
        {
            "id": "63b0e0b1-a353-47cc-bd1a-9cae2b21b7de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 84
        },
        {
            "id": "e611089e-38e0-40b7-b712-6908f1fc8a01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 85
        },
        {
            "id": "d5effd71-4a1a-47fb-bd76-3c91dfceeabe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 86
        },
        {
            "id": "d05312a1-c0fc-43e5-ac9f-4caea3461486",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 87
        },
        {
            "id": "ee25a7d3-4376-4d03-bab8-97bae92b5aa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 88
        },
        {
            "id": "77bdc420-6093-4f47-af1b-5a471bca0e35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 89
        },
        {
            "id": "4865246b-55d8-4327-bddf-7c8f9465ce5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 90
        },
        {
            "id": "ef38276a-19cb-4991-9981-0ee4d190504c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 92
        },
        {
            "id": "85eeec33-5097-4768-88e7-ba2ba3eb1de3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 93
        },
        {
            "id": "5535ba69-ce32-4a39-b279-1e94d48f7450",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 97
        },
        {
            "id": "48b54d88-6a9c-411d-8fed-a4ef35cf7575",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 98
        },
        {
            "id": "6a06bc36-8189-43ed-9c7e-fbd377be04d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 99
        },
        {
            "id": "778437f9-15d9-49dd-b38f-7843a979e212",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 100
        },
        {
            "id": "70fa836e-b150-4d4c-aadc-9aef0196e142",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 101
        },
        {
            "id": "e338f7b3-990b-4207-8699-0cb8a56fe3c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 102
        },
        {
            "id": "203c46bb-5e24-426a-9f73-951a95f8b35c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 103
        },
        {
            "id": "f72fe93a-6c41-469f-a14e-6af791e2d6d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 104
        },
        {
            "id": "ff15c044-6a5b-4430-a2f7-9f29698b235c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 105
        },
        {
            "id": "a93dcbcb-bb50-482b-b960-84e4bf935464",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 106
        },
        {
            "id": "0d1cc979-f728-499d-afae-bb108dfadecb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 107
        },
        {
            "id": "62f36753-10c8-4e76-8d62-e211b3f597c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 108
        },
        {
            "id": "5e46077c-9cfb-42a2-a00c-1742056c4873",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 109
        },
        {
            "id": "bbfc6b7c-8ca5-4fa1-a5a1-95aac58f0d51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 110
        },
        {
            "id": "391d7ec4-a85f-49cd-bcc2-7638d8ed8125",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 111
        },
        {
            "id": "b1ec1659-fe8a-4996-b786-a5ef69409c1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 112
        },
        {
            "id": "f6920511-c16a-46f3-b106-3f9caee9fc2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 113
        },
        {
            "id": "cafd239a-92e7-47a8-b084-ac4314e1c7ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 114
        },
        {
            "id": "3246abf1-6207-43b8-bce9-1b56adfdc5e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 115
        },
        {
            "id": "8036fd60-c22b-468f-98e6-68b14cd8776f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 116
        },
        {
            "id": "7be8d395-b308-4822-b9d4-f2a550489c7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 117
        },
        {
            "id": "8859d347-98e9-40de-b0f2-53533d8457cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 118
        },
        {
            "id": "5e4abca8-aeab-4e70-b792-4da82bc15ef0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 119
        },
        {
            "id": "e9c0458a-7809-4720-b5dc-f165bea09bbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 120
        },
        {
            "id": "db9cf64d-928e-4ab3-b5fd-577d710e5aee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 121
        },
        {
            "id": "b7341d4b-9777-42b2-b488-7cae7c97cd27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 122
        },
        {
            "id": "f1246b84-8d92-4ce8-bb01-7498fca3ae07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 8216
        },
        {
            "id": "86367941-3080-446d-8a2f-244a240a338a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 8217
        },
        {
            "id": "c69bc0cb-65cd-4d1e-a442-6f49a7391db3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 8220
        },
        {
            "id": "de795d73-b508-40e5-8c24-6a2499a3f14c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 8221
        },
        {
            "id": "1ff81730-7e81-4af8-af08-fc4259aa618b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "1427d5fe-e6ed-44d3-8044-52f17e82c765",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "fe63acb8-d3a0-4836-b34f-eb27a8ed7175",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 52
        },
        {
            "id": "6caf0c92-4b5e-45bc-8be0-acb0d79bde23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 64
        },
        {
            "id": "c4cee774-287d-4e17-bda7-1986881989f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 65
        },
        {
            "id": "5536018b-dbd3-4922-8342-484f85c3d90b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 100
        },
        {
            "id": "531162f9-b13d-4d93-96ea-7224f061e0ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 196
        },
        {
            "id": "b724250e-3373-4c6f-a7a6-2370594a3a6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "b7c6fe52-78b0-4913-a7f3-0ada5096f141",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "0881b733-c55f-4b72-a06e-5d87a4ce69dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 64
        },
        {
            "id": "150733ab-9c75-4143-b200-052de5e8f04a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "7afbf829-3197-4a4b-a303-71599c938e96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 196
        },
        {
            "id": "d9b8838f-a3d8-4edf-b577-8aa5c9555586",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 33
        },
        {
            "id": "e8240bad-dc6a-4de4-8a6f-6e16ba1275f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 45
        },
        {
            "id": "3e7b30fb-db3d-4634-be96-9ac1f1725d42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 48
        },
        {
            "id": "4ef91d02-3360-489d-b852-75dda0bd86e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 49
        },
        {
            "id": "dc50b689-69eb-4cd0-b329-e11a46b8c041",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 52
        },
        {
            "id": "30e8197b-fa8b-4f43-b9a4-63b61447cd21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 54
        },
        {
            "id": "83010332-e7fc-4899-99a3-dec028d206ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 56
        },
        {
            "id": "8c4cdfd2-11fe-4636-8dd3-441ee2933589",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 58
        },
        {
            "id": "474bb83d-6c1b-4bf5-b39a-5c3ff98438df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 59
        },
        {
            "id": "ac3a79cb-0627-47e5-9388-f11e398a068c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 63
        },
        {
            "id": "89a426c1-da00-4100-b864-aeaf3533728f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 64
        },
        {
            "id": "fc63b0ee-d05a-4c01-b6a0-3bea75ea01c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 67
        },
        {
            "id": "a1d64736-01f7-4330-af41-f5400e7dccf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 68
        },
        {
            "id": "b76a36d9-db51-4973-94fe-55267e89d96b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 69
        },
        {
            "id": "9fb2526c-9407-4436-bacb-2ecef963e062",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 70
        },
        {
            "id": "b22f0e50-fd27-4d7e-bf65-9db7653dcb66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 71
        },
        {
            "id": "a16569e9-9bbc-4491-bbc3-eff280c41441",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 72
        },
        {
            "id": "d85c7036-c0a0-45f0-8eb2-1a0a07eaf4c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 73
        },
        {
            "id": "bf268d9f-8cd1-4be5-908a-6f9ada4e2356",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 75
        },
        {
            "id": "b2391831-15a9-46d5-b86c-ba81a33369f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 77
        },
        {
            "id": "48de26ff-8181-4bf3-beb7-c9c6af361087",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 78
        },
        {
            "id": "60f33f63-612a-48a6-b866-fc0ec7a7fc51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 79
        },
        {
            "id": "28f7723e-0399-4fc3-b109-105968b3b4f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 80
        },
        {
            "id": "d6bc1880-c647-4d7e-ab42-797ef64cdc0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 81
        },
        {
            "id": "96b87556-f22a-465d-84a7-e452060cfc99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 82
        },
        {
            "id": "e63c78df-8400-4524-80ac-1aa3a592b491",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 84
        },
        {
            "id": "76505f7a-03dc-4d3a-84a6-b19763cda19d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 85
        },
        {
            "id": "8f50e692-4c4a-4188-b4e0-3434fc3921fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 86
        },
        {
            "id": "1e96b526-c009-4b77-a206-67f58a661734",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 87
        },
        {
            "id": "5fb8692e-e526-4bd0-b17d-efb5f36ec97d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 97
        },
        {
            "id": "f3dccb1b-bdd8-4e57-abea-22abd4da3042",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 99
        },
        {
            "id": "ed587528-c726-4f17-b577-0b5cb6653d75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 100
        },
        {
            "id": "f3cca389-1594-4eb8-83a3-ac7b53936fe9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 101
        },
        {
            "id": "de8069c1-4d3c-437a-a12c-e91c93231498",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 104
        },
        {
            "id": "25b50940-f4aa-4683-a9c9-cf1516fe11d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 105
        },
        {
            "id": "6143fb56-ffb0-4c5f-a172-3abcdd06b763",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 106
        },
        {
            "id": "e9778290-0df9-4af3-a348-b000571b4d27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 107
        },
        {
            "id": "05a69a7f-c7e7-4ca5-a039-24a768da90ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 108
        },
        {
            "id": "0d1e1861-7f46-4d12-831b-cd6d19e73339",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 109
        },
        {
            "id": "430a88ad-3044-4b33-877d-41f58607e71b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 110
        },
        {
            "id": "a693969a-2368-43d7-b550-caa94092dc47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 111
        },
        {
            "id": "2431fb10-513d-4a80-ae7f-53bc6b4d95fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 112
        },
        {
            "id": "473eb681-0787-4abc-8437-4007a56c84e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 113
        },
        {
            "id": "c6a16cf0-bf1d-4b27-98f2-b1ce619ba923",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 116
        },
        {
            "id": "169d4ab1-cb91-4f93-a2e7-15060de9c3f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 117
        },
        {
            "id": "8b29c579-1534-433c-ba82-d8ef77046dda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 118
        },
        {
            "id": "d41f68f9-bc3a-43b2-ac64-c6467b2a259c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 119
        },
        {
            "id": "385487db-337b-46e8-9328-8ff51567af99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 121
        },
        {
            "id": "7e557b66-3018-439c-90db-ec4c8e9b1ac7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 8216
        },
        {
            "id": "7b4dc438-2675-49e2-a631-efa95efef6f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 8220
        },
        {
            "id": "a1d322ee-b03e-4fa8-aa72-c962cc636bb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "a8171516-1a30-44e1-a3df-410810d8553a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "3c67a7fc-66e1-4b6f-ac45-86c81d43ee2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "5672e1e1-f82c-4243-8f92-78ac1c4d11b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 52
        },
        {
            "id": "890fbb1f-eb10-4450-a9bf-2d30a9da0627",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 54
        },
        {
            "id": "be155b4b-3ec3-4da6-9cd0-75ee91e4c2ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "934ee42f-a921-472d-bb9d-356b839b04ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "c7449275-9448-44ca-b890-ccc78973ecfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 64
        },
        {
            "id": "12b3705e-def6-4d84-8f7b-240f1df881a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 65
        },
        {
            "id": "fdcf0671-f21a-460e-8317-41dc801e39d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 67
        },
        {
            "id": "e741c3a7-4c61-4e67-93b4-2430ffc5d863",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 71
        },
        {
            "id": "7349b50d-74cf-4bb4-9e26-653b47abfeaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 79
        },
        {
            "id": "c96fe17c-93cb-4fde-bf46-eb11cdbffd5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 81
        },
        {
            "id": "81259371-142c-40d1-863d-9c764c32cddc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "0e9a397c-b3a7-48b7-ac3a-70056a898964",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 99
        },
        {
            "id": "873aafcf-ca21-45d9-931e-3c6abbaf190a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 100
        },
        {
            "id": "7eb5803f-270e-45c1-809e-6d7c52caf030",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "81b7db84-c938-4b53-9f4a-82852f29bcc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 103
        },
        {
            "id": "fe12a810-f5be-41e7-ade5-c3cb21ea0599",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 109
        },
        {
            "id": "932df858-d700-4f4a-b64f-89d8ccb0e36c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 110
        },
        {
            "id": "da2392e9-e108-40b0-b5bc-579d98486aff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "69e3280c-9585-4c8b-a35c-2e5978e693df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "4e28e330-adf9-4ef4-9045-c543e3b5862c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "0e56b0b7-3c2d-4582-8293-2bf57e92f12a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 114
        },
        {
            "id": "7172596c-e48f-4f81-939d-d90b522c806a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 115
        },
        {
            "id": "74710763-2e9a-40cf-b924-890ad48df7b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "7ebeabad-8dbf-4e31-8d7f-6b8c61e99422",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 45
        },
        {
            "id": "505ccac3-8527-4563-9aa4-bb90c80cdb14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 64
        },
        {
            "id": "7e3c70de-2f73-45b2-b673-42ff277d5666",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 85
        },
        {
            "id": "3c9d5d29-fc78-43d5-86fd-6c64028c0674",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 109
        },
        {
            "id": "5cdb1c48-2a8c-408d-8593-75f8d7ffcb0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 110
        },
        {
            "id": "6ebe7a58-b741-4252-862f-bcfd7e018c11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 64
        },
        {
            "id": "e56ca9f4-2e84-49e1-a773-21fa069d8959",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 86
        },
        {
            "id": "c9069ea5-e66b-467c-b626-1a6269d15752",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 87
        },
        {
            "id": "97f17140-fc35-4622-a6b7-bc2506fb6d78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 64
        },
        {
            "id": "1d6aead1-475f-4099-b2e0-539665a5f314",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 33
        },
        {
            "id": "2003f661-b569-40e8-b681-b81da369b6f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 34
        },
        {
            "id": "545e05d2-cbff-47cb-8c63-5988cfcb5a6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 39
        },
        {
            "id": "05a8999c-156d-4ae1-9e66-c93d17d12bcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 41
        },
        {
            "id": "f92edf50-aa61-4ed4-a276-a1512191b044",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 48
        },
        {
            "id": "4388426f-5543-4bf0-ace4-ea83d0e4f392",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 49
        },
        {
            "id": "a633dff0-1d0e-429a-92a1-5473cb006464",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 50
        },
        {
            "id": "fe35151c-dd09-4387-b299-605f85e80f1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 51
        },
        {
            "id": "67aa4448-09cd-4890-929e-8edeeb796e34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 52
        },
        {
            "id": "8ae67117-e607-4fdb-b903-daf32bb5a986",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 53
        },
        {
            "id": "9732b59f-a4fa-4b22-8dfd-62d40a2da6d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 54
        },
        {
            "id": "2dcd16c5-696d-45da-9f09-7a67bfe4bd23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 55
        },
        {
            "id": "debfbd91-cbb1-4420-a5b5-8ab431285d59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 57
        },
        {
            "id": "4070b31f-6739-4ea5-8056-cba686fcc0d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 58
        },
        {
            "id": "5540aec1-c40e-475a-8d7e-f36910ed8568",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 59
        },
        {
            "id": "b3d5fa67-ca16-482e-a2f3-129323b5f08a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 63
        },
        {
            "id": "06fb824d-3586-4383-bf4b-55bc0fb07d36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 64
        },
        {
            "id": "ff548e13-82c4-41f6-947a-8f56c44e9189",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 65
        },
        {
            "id": "6faecc62-c520-4f97-a7a5-915cb03e359f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 66
        },
        {
            "id": "c0976d1e-5387-4037-9043-fe95a0ac0d42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 67
        },
        {
            "id": "9707fe68-85ec-4d44-8139-4c798a98f719",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 68
        },
        {
            "id": "ded8c411-f17b-48da-9391-a6e665599e33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 69
        },
        {
            "id": "03217218-9562-41c3-a533-f00e341d21fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 70
        },
        {
            "id": "bf6c42ff-49c8-4ff4-8864-e9d75c564ec4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 71
        },
        {
            "id": "a46e9b8c-babf-44e5-a97e-e29ff6008353",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 72
        },
        {
            "id": "4a125943-b636-4286-b867-42502a6e1ee2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 73
        },
        {
            "id": "bbfe1a51-17be-47cc-b937-ea1102f87eec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 75
        },
        {
            "id": "dd624dc6-d608-4a06-9763-51ed3a222aa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 76
        },
        {
            "id": "561490df-0f52-4443-a9e5-5a99ad71b52d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 77
        },
        {
            "id": "f5228535-1108-4849-acfa-de20b3479a95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 78
        },
        {
            "id": "f8124cfd-a21d-4bc2-843a-d0c0a475977c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 79
        },
        {
            "id": "d9242b00-74e7-4a37-905e-fd1fd24c9c4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 80
        },
        {
            "id": "ca098fb0-e779-444c-9dca-8f3e72f68b3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 81
        },
        {
            "id": "3d5b53aa-a2b8-4313-a320-c811841a45e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 82
        },
        {
            "id": "bcb797b0-a109-470c-9560-dca26fa525d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 83
        },
        {
            "id": "a4ae97cb-aa96-49c0-8c06-27a87f01d910",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 84
        },
        {
            "id": "eb7fabfd-9a31-46c4-bf6f-67f132be7c73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 85
        },
        {
            "id": "3aeb18ff-2ac8-401b-b7b7-bc902355c2d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 86
        },
        {
            "id": "fbb5dabe-6115-4eff-8e3c-7bbb699fb8ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 87
        },
        {
            "id": "f6d683b3-7bd0-467c-b19d-0118e330e51e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 89
        },
        {
            "id": "52a81ea2-cf05-4f4e-b35f-47fc61073f95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 92
        },
        {
            "id": "c9bad49f-073e-4029-9aba-3bff1a6aec89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 93
        },
        {
            "id": "3fa72c21-ef01-49c2-8237-ce8cfb9d3ecf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 98
        },
        {
            "id": "3a36ea88-953f-4b5a-bab7-f91bea3316c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 99
        },
        {
            "id": "90b4342b-399b-4b80-9162-e77147792144",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 104
        },
        {
            "id": "e908d887-976b-4bc8-913f-b8d86aea590b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 105
        },
        {
            "id": "8b9b652c-68b2-49bc-96c2-3833ec9832b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 106
        },
        {
            "id": "d85a9ea0-58da-4b6e-95cc-5f7fc9cfc244",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 107
        },
        {
            "id": "450f6b66-1da3-451f-88f5-b67318ed5e56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 108
        },
        {
            "id": "cd6d402f-0426-42f6-902f-70b16f5b06f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 109
        },
        {
            "id": "897ad2fe-ad5d-428a-8e24-2c5d012cffda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 110
        },
        {
            "id": "d2925801-78c2-465a-baea-43c709a629c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 112
        },
        {
            "id": "d9a5fe8c-34c2-4f74-9c22-814785a24fdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 113
        },
        {
            "id": "9c46b080-a5f9-4cdd-91aa-7b58d6c50dbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 114
        },
        {
            "id": "793aaccc-bc4d-466d-99bb-cff3d9649146",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 117
        },
        {
            "id": "52682b57-a525-4823-be2e-964db1396c14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 118
        },
        {
            "id": "a5347e90-c592-4638-aa8a-bc1a3d043556",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 119
        },
        {
            "id": "a43d95bb-8f0b-49d0-a65b-c0909e62aa93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 121
        },
        {
            "id": "c97c0656-758c-45be-be6a-02197b3d978c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 8216
        },
        {
            "id": "0456e129-ac03-44bd-94ed-29d7fb504ea6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 8217
        },
        {
            "id": "1a18e331-4e1d-43b6-843c-73fe2cec6d52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 8220
        },
        {
            "id": "db760ad4-822e-4389-bdcd-9b722e8856d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 8221
        },
        {
            "id": "d535b1c9-af11-4cc5-b254-e28a01d1c98b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 33
        },
        {
            "id": "a9cfe72c-2267-4f71-870d-0950e16d3537",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 34
        },
        {
            "id": "dd889a17-bfce-4161-85f5-724d94ef73e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 39
        },
        {
            "id": "fdb3d4a4-53f4-489a-8a12-e6a0c10c4a6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 44
        },
        {
            "id": "6cb5057b-09f5-4fe7-9325-57f1b1b351fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 46
        },
        {
            "id": "0ab71a4d-8035-4fd4-bde5-69e27adedd14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 47
        },
        {
            "id": "815143e4-98e1-491f-a55b-414824f6376e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 49
        },
        {
            "id": "3a65c0d5-5166-4a0a-8860-8feeaccfa4f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 50
        },
        {
            "id": "74bd4bf3-f6fa-4b54-86bc-d068980f9f5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 51
        },
        {
            "id": "7645ab6c-615b-42b0-a10c-2361550135c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 98,
            "second": 55
        },
        {
            "id": "3163a3bd-c618-489c-857d-fb1f490200f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 58
        },
        {
            "id": "d77a7cb7-8e97-4e65-bbb6-941ac46ffe99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 59
        },
        {
            "id": "95ef7ffa-a928-4e54-b299-572bd106d971",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 98,
            "second": 63
        },
        {
            "id": "39cd912d-5ec7-4f8b-9729-eed1e14ef71d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 64
        },
        {
            "id": "71b2cea1-34bd-4107-aea0-27b0983e294d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 65
        },
        {
            "id": "2ae431d6-86bc-4ef6-948c-d13af49d7551",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 68
        },
        {
            "id": "bb618a3a-7f4f-4c01-91d9-f80e0f5a3b35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 69
        },
        {
            "id": "c150a624-b4b3-421d-855d-10ba2cc92b9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 70
        },
        {
            "id": "2edbe83c-a8a6-4014-8936-fd9e66a7fac1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 71
        },
        {
            "id": "aa4c0c48-c03b-48e4-997f-3bcf6d1adf02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 72
        },
        {
            "id": "8e9bfa2b-00f2-4df4-bc70-87c722df7601",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 73
        },
        {
            "id": "57d21ecf-09c4-48e2-9b5d-861afc9e0f27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 75
        },
        {
            "id": "dc77ba66-7862-44c6-af38-d0a023b170b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 77
        },
        {
            "id": "a37c416f-ade3-48a2-8569-195bf4493261",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 78
        },
        {
            "id": "b9cc58e8-d7b8-4f31-b7b0-f230aba72c09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 80
        },
        {
            "id": "e556e211-bfd9-41a6-a5b3-a1348e4e3ce6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 82
        },
        {
            "id": "27cebe19-d3fc-48c0-804f-c5196b47ca3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 84
        },
        {
            "id": "7f83149e-f1f4-4cbf-b389-54daccb28790",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 85
        },
        {
            "id": "aa4c9a99-0f2b-46c2-9926-417a2433df5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 86
        },
        {
            "id": "29a8b1db-f431-4ffe-a2f3-526192825954",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 87
        },
        {
            "id": "347ade3b-2149-44b7-b409-c510ba9247f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 88
        },
        {
            "id": "1f3375c1-6e54-433b-ad8f-45665a5d60e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 98,
            "second": 89
        },
        {
            "id": "00478fa5-654d-4f62-a608-ea65729e3041",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 90
        },
        {
            "id": "9f6fa5e3-51e9-44c3-a20d-8a06cbb5ec78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 92
        },
        {
            "id": "d7f3cad4-3f6d-4bfa-a2d3-71fb5bc8c415",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 93
        },
        {
            "id": "6135fa13-97ce-4566-9a69-935f31b49e65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 104
        },
        {
            "id": "3ecade90-17c7-4dc6-85a2-ec9dbc542b9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 105
        },
        {
            "id": "b1d82954-5b78-4c93-bb6a-c5a4b8dfc423",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 106
        },
        {
            "id": "f8e6f728-62d2-4425-88f3-750eac63548e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 107
        },
        {
            "id": "4acde58f-3468-48fb-a256-9eaff89cabcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 108
        },
        {
            "id": "fbff2305-40ca-4611-9e86-2eaede58ce64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 109
        },
        {
            "id": "09373767-b55f-44ec-a168-be3ef128e85e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 110
        },
        {
            "id": "4ad6640a-9b0b-44e4-93b3-ffebf66cabfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 112
        },
        {
            "id": "f778d60e-2eb5-4cf3-8b71-581dcd53f36e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 114
        },
        {
            "id": "4184dc0b-8fcb-4ba6-a757-bb718dc56543",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 117
        },
        {
            "id": "0fb7a6bc-5d6a-43f2-8a7e-4c5c94fc13df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 118
        },
        {
            "id": "584bf8a6-c6d4-468d-a409-d7dfb361f128",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 119
        },
        {
            "id": "0d218f0e-4cdb-4507-ac44-d705d0bee949",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 120
        },
        {
            "id": "ff571bd6-4e87-40ae-b197-05f0b7834acd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 121
        },
        {
            "id": "987f3976-4c4f-48fd-b5a9-78a9f65ceb2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 8216
        },
        {
            "id": "e7350811-adaf-47c6-a508-25cd117d0f51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 8217
        },
        {
            "id": "5e66bc9d-82bf-458d-88a0-7d822d2953e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 8220
        },
        {
            "id": "e3025f60-6f7e-40d5-80b8-cdb994db9591",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 8221
        },
        {
            "id": "a46b10ac-b8c8-4c68-9bae-0b01f968715b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 99,
            "second": 45
        },
        {
            "id": "da56dc2d-0911-49d3-bb13-e95c8d3cb0fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 52
        },
        {
            "id": "aa624247-1690-4bda-bf99-b7b55e2831c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 64
        },
        {
            "id": "191c3b4e-7c45-48e8-8fbd-72dbc4516916",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 85
        },
        {
            "id": "08686806-577d-47b3-99aa-29fb2abcb1dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 86
        },
        {
            "id": "dc5b4dbb-b258-42ae-bfae-34cb04cadeff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 87
        },
        {
            "id": "ce7511d8-88a5-4086-a2c0-12b7943aebcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 89
        },
        {
            "id": "6d04542f-bbe4-44e3-9392-0f1b64e5d09c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 104
        },
        {
            "id": "70ef758d-078c-47c9-850c-47e1554930cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 107
        },
        {
            "id": "e92ee847-64e4-42d2-a0bd-9a13fa183b13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 33
        },
        {
            "id": "25216890-c3ed-435e-bc35-c0113cf0873b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 34
        },
        {
            "id": "b95500c2-905a-4c97-8103-f5321710fe4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 39
        },
        {
            "id": "606ff92a-6e6a-4992-bd07-6c3bf520d72e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 40
        },
        {
            "id": "805e6c0d-3eb0-489f-87af-598f98a78a91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 44
        },
        {
            "id": "ea01ad8e-5f22-4cae-a91a-118083ba81ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 45
        },
        {
            "id": "00eedc92-8419-4514-8003-938318e753ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 46
        },
        {
            "id": "ce19ffd8-a8b9-428c-8707-cfa71f3e1fbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 47
        },
        {
            "id": "939d0c07-758d-4087-b5bd-82089c6bcbd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 48
        },
        {
            "id": "0fb45ab0-651f-40af-ba30-8c7884789bbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 49
        },
        {
            "id": "59bc0643-2627-47a4-9cb4-af889ccea35f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 50
        },
        {
            "id": "8936ee90-e730-4f32-a6b1-e72c980c4447",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 51
        },
        {
            "id": "091ab30b-f7c3-42a7-83e6-60c63ae3cf6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 52
        },
        {
            "id": "a55954d9-8105-4452-86ad-ebcc1ca6a277",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 53
        },
        {
            "id": "466f6e7a-0583-4461-bdc8-2ef9474835ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 54
        },
        {
            "id": "ee2ed585-112f-4eff-bd79-321377b8e631",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 55
        },
        {
            "id": "5ea63250-bb6d-4b00-b446-d03ad621ba61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 56
        },
        {
            "id": "c845d085-a530-4787-8b10-718f26ae3dbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 57
        },
        {
            "id": "1eee4167-4bbe-4e29-a937-f9e01b040657",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 58
        },
        {
            "id": "3dc391a2-a14e-4f55-8cbe-4cf3469d843f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 59
        },
        {
            "id": "058dc687-3c90-4f4d-bbe3-297839166530",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 63
        },
        {
            "id": "73138dc1-7e06-4df7-a5ef-ee64911f43c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 100,
            "second": 64
        },
        {
            "id": "6cf3355d-6dd1-4ea8-b0ad-13138d8e67a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 65
        },
        {
            "id": "1c2a3b88-98a2-4d11-ab99-36ae674dc9d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 66
        },
        {
            "id": "32e6f70f-2139-4fcb-abf0-fbe5631018c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 67
        },
        {
            "id": "e0b4bd27-9373-43d7-a86f-28539cf8d23d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 68
        },
        {
            "id": "4e90fcca-9835-4de8-88c4-634adb90db26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 69
        },
        {
            "id": "bfbd9496-396a-4446-af28-5b791e90a7d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 70
        },
        {
            "id": "f1a3d7f5-a7f9-4331-a97b-f2d06f7d3719",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 71
        },
        {
            "id": "a7c7cae6-dc45-44ef-bdea-e51f6e38bd0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 72
        },
        {
            "id": "3377302f-7639-4da9-9223-fe3a346c448c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 73
        },
        {
            "id": "15cc8a35-6e07-4a62-afb4-5e64801b486e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 75
        },
        {
            "id": "72143b96-3367-411f-a723-0dff09405aff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 76
        },
        {
            "id": "d8194ce2-9d69-4e40-9154-277f81b88d90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 77
        },
        {
            "id": "d405e2be-1c77-4424-8bd2-78cbf11ea2c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 78
        },
        {
            "id": "bc426b11-77e7-497c-871c-75541e68a8a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 79
        },
        {
            "id": "72feab3c-456f-4cea-9e51-a4a0890d4209",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 80
        },
        {
            "id": "939468e0-6dc0-4146-86e8-3ed30b3082f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 81
        },
        {
            "id": "3e19116b-2646-4828-ac00-a548b5e0431b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 82
        },
        {
            "id": "519fc476-f4f2-458f-93d8-afb26b400d84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 83
        },
        {
            "id": "5e177249-532b-4cf5-b85a-c95fab5ebdd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 84
        },
        {
            "id": "611d7627-e51b-4328-b119-6e075a7299c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 85
        },
        {
            "id": "428c2287-7650-4f27-b9df-36445371e2e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 86
        },
        {
            "id": "6cf4a9b3-154b-4144-9d4f-d3202fbaea46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 87
        },
        {
            "id": "14e2410c-9f9d-49b6-b95c-8fdc495af5e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 89
        },
        {
            "id": "92d4cdd7-4036-40bf-81cb-cd270738c48c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 90
        },
        {
            "id": "10530098-85c1-477b-9897-5f45a420571c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 92
        },
        {
            "id": "9e5bbc82-f4f9-4742-885e-1a8e04b71f14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 93
        },
        {
            "id": "3fb06d11-3020-4214-af4d-3a52f286cf93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 97
        },
        {
            "id": "173f3f29-ea0a-43eb-808b-65b7eac6027a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 98
        },
        {
            "id": "e07860f8-010f-431f-a9f4-75ed5c807c3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 99
        },
        {
            "id": "1af4ef06-76c2-48b7-b5d7-2bb5b1f15dbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 100
        },
        {
            "id": "0a316af0-e472-47e6-a667-fd306ee5050b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 101
        },
        {
            "id": "6b8648e4-5c5e-4e53-bf08-4e4590d03d0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 102
        },
        {
            "id": "2de9ad80-f57f-4352-8021-d9085b7ba5b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 103
        },
        {
            "id": "bf35e774-1e52-44f8-b1b6-b47eeb9af76e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 104
        },
        {
            "id": "b591a629-0bd3-4053-9b9b-73ffafd215e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 105
        },
        {
            "id": "4d99c984-b7ee-43aa-93eb-05c1093d8607",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 106
        },
        {
            "id": "4de3cc5e-4064-4865-96a7-2e06cabb4115",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 107
        },
        {
            "id": "b277605e-9515-4d74-bb1b-ba030e71694b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 108
        },
        {
            "id": "2430a763-50ec-469d-a06d-15257f814961",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 109
        },
        {
            "id": "6df595ac-9e98-4b60-8f1d-63c5bfd81c95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 110
        },
        {
            "id": "22042cc9-e2d8-440a-97eb-cfd7d758806f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 111
        },
        {
            "id": "fe8dc765-41bd-42f4-9a6f-089c3f97fb18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 112
        },
        {
            "id": "40e707f8-ccc1-4e01-b7e8-807634075654",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 113
        },
        {
            "id": "6df0b7f9-9b4b-4b19-acd8-cc6aeec2455f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 114
        },
        {
            "id": "3dc4ca64-c5e8-4677-b618-a7c48cdd8f29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 115
        },
        {
            "id": "3299e1db-6372-434d-9aba-2782c17c9c92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 116
        },
        {
            "id": "2640796c-44e3-49d7-a26c-d22dd3bffa40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 117
        },
        {
            "id": "bb79644d-6e94-4968-b6a9-86664cc89cfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 118
        },
        {
            "id": "ebf8983a-2aa2-4c30-9d81-55ef1da99501",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 119
        },
        {
            "id": "3a3ba4de-c3ce-4f51-b83e-a85ce6f6146b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 121
        },
        {
            "id": "89e40da7-d71b-418c-a754-ee0baee7dfe4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 122
        },
        {
            "id": "174ad611-b30c-4076-8242-5e3f889df11d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 8216
        },
        {
            "id": "774aebca-78f9-4120-b60c-28eebd03aceb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 8217
        },
        {
            "id": "5a5c00c9-36e5-4baa-ada1-aaffe013bc80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 8220
        },
        {
            "id": "ac5e6eed-55e8-456a-90ac-56c58bfe6135",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 8221
        },
        {
            "id": "b3bb32d3-9044-43cd-a90e-3f53490a67c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 55
        },
        {
            "id": "b9f6ab7d-ba2c-4a24-bf82-8211f9dad4cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 63
        },
        {
            "id": "1a11f63a-3f75-4006-ab85-ef3e442c6135",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 64
        },
        {
            "id": "dadd54f2-24ec-4526-b5a1-242feb296244",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 68
        },
        {
            "id": "7c6a0c0b-cb5b-424a-bee9-001e7c6b285c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 69
        },
        {
            "id": "c68c14fa-266d-497f-bf7d-be8523c34c91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 70
        },
        {
            "id": "c9aa4d59-fcee-4fa3-bbba-14d82eb0b4f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 72
        },
        {
            "id": "2d9ab95e-e2a9-4922-a807-9451aaf7e716",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 73
        },
        {
            "id": "53919106-8f9d-4dd7-a1b8-0f14eb45fd11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 75
        },
        {
            "id": "70afe8a3-0271-4782-995f-075fe75416cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 77
        },
        {
            "id": "5dcedcf1-99af-4615-9813-f62409d4791a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 78
        },
        {
            "id": "73bce4d0-cc29-42af-b84b-49d2bdfc951f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 80
        },
        {
            "id": "4bee709d-338f-4aed-bc51-26289e353cea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 82
        },
        {
            "id": "0529a277-1c93-49fc-9abd-4049d040dff6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 84
        },
        {
            "id": "0dad7017-687f-4501-bd14-4cadb4684a64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 85
        },
        {
            "id": "b4df35f2-5c5b-40c4-afcb-c0da1584c010",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 86
        },
        {
            "id": "528dcc5a-58c6-404d-8ea7-fd1e84b77c46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 87
        },
        {
            "id": "f6acc156-9898-471f-a8f1-d1d0a013b89f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 101,
            "second": 89
        },
        {
            "id": "92e04ddb-589f-44c9-b84b-44233a265e8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 93
        },
        {
            "id": "2e2d91e4-37f4-447f-96cd-fbc13ad6a958",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 104
        },
        {
            "id": "fd8ac610-ac17-4574-87e1-80ed4bf2b671",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 105
        },
        {
            "id": "ea7dde97-66ca-4a0e-92d8-16cae2c6b410",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 106
        },
        {
            "id": "adb87134-38d4-474c-8c0b-927786a3570d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 107
        },
        {
            "id": "48b29bba-494f-4389-89cf-5caa7cf83ca4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 108
        },
        {
            "id": "5f82d693-8110-4c70-87d6-27f6f7d267db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 109
        },
        {
            "id": "fa5d1c66-d8c6-4b9f-90a3-664ac9526ee2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 110
        },
        {
            "id": "f50fec58-ea0b-4766-a91a-6e176d12ae57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 112
        },
        {
            "id": "10b8b291-aaa8-4ce1-b70b-0c099cbf993a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 117
        },
        {
            "id": "00f3b612-3f58-4b2b-9ea6-0b15a149f329",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 118
        },
        {
            "id": "224e4cb2-11e1-48e2-b539-68106015b828",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 119
        },
        {
            "id": "2c1ecc0c-669e-4ba9-9d35-8e3361679558",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 121
        },
        {
            "id": "e8e9db42-e566-4500-9a93-690a4ef6b220",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 52
        },
        {
            "id": "9fc65c8a-1b75-433d-bcc8-85069b970dd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 64
        },
        {
            "id": "1be3d77b-dc35-4b3c-92e7-217ab6eeee50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 65
        },
        {
            "id": "9432fc8a-09b6-45ff-ab83-66ddeb1231b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 45
        },
        {
            "id": "6dbf0574-4d01-469f-b3ce-2cfd433e63ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 52
        },
        {
            "id": "c880786a-928d-4b96-82bd-7fdebeb36dca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 55
        },
        {
            "id": "cac96761-cb03-4364-b6b4-047692e5ad4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 64
        },
        {
            "id": "2b7f723a-95f3-4edc-91ae-b286c4b74c95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 86
        },
        {
            "id": "b4c319c7-416f-44b9-b918-201be00e01f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 89
        },
        {
            "id": "99ac58c6-880c-4519-823f-2ef6ba71a5f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 33
        },
        {
            "id": "fb0d4167-ccfb-422b-b518-68d4b0ba64df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 34
        },
        {
            "id": "144e4923-95b7-443b-baf9-4fcb2261d2bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 39
        },
        {
            "id": "2e5298b0-2cbe-44c2-a522-cfdbd8182d60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 40
        },
        {
            "id": "015c278e-8eeb-43f0-8de4-da72a0e29fba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 41
        },
        {
            "id": "b017d8c7-41e6-4480-a993-cdf37eb586d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 44
        },
        {
            "id": "8f0d38f1-4454-4997-bd0b-a147640fda7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 45
        },
        {
            "id": "aeca5feb-1dc5-4759-bcde-77864bf1b506",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 46
        },
        {
            "id": "faf5600f-bd8d-4640-9c84-3f1c6200c9a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 47
        },
        {
            "id": "e3dc88e3-5f21-46eb-b1a3-1267d6a2164b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 48
        },
        {
            "id": "d0d559b4-dee5-4eb2-926a-4645c18f9af1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 49
        },
        {
            "id": "769fc538-9001-47f4-8343-8f1ad61ee36f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 50
        },
        {
            "id": "97cf0dfa-8797-4df1-b166-4b6f0adb71d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 51
        },
        {
            "id": "034b9687-9451-4546-b4b3-9e942dc0c316",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 52
        },
        {
            "id": "d651d612-6632-4447-ae79-1f7b4593e8a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 53
        },
        {
            "id": "153b16a3-4449-481e-a277-325222cf5068",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 54
        },
        {
            "id": "fd000d12-27eb-47d7-847b-21f98cd4aa7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 55
        },
        {
            "id": "7f4bedbb-9526-4ad6-bfbb-1f5e048011e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 56
        },
        {
            "id": "0980758a-83ee-4dc9-a574-004e9dd98b60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 57
        },
        {
            "id": "6f5d9017-72d6-41dc-8a75-ca514648dbbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 58
        },
        {
            "id": "6aaad36c-c980-49f2-ad19-2864f571cda1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 59
        },
        {
            "id": "a58a6779-a61b-4c10-a2c4-06eb6283afb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 104,
            "second": 63
        },
        {
            "id": "6fbc10cc-c3b5-4ccd-b824-067f0d68665d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 104,
            "second": 64
        },
        {
            "id": "6a163234-c4ef-43bf-870e-e85038fbfd63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 65
        },
        {
            "id": "a765cce9-5b3a-48b9-a16f-7826b14cc531",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 66
        },
        {
            "id": "fad363f1-91a4-4d71-b2e6-1939832fd635",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 67
        },
        {
            "id": "ee1b09d4-8467-4b00-be97-bea56219b203",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 104,
            "second": 68
        },
        {
            "id": "29ea2430-7bc0-4116-9d3f-cba238fbd49a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 104,
            "second": 69
        },
        {
            "id": "06e772ae-2c86-40a4-83d0-31822c3d1703",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 104,
            "second": 70
        },
        {
            "id": "24b63d7e-c5a1-4725-a605-72ab75b0004f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 71
        },
        {
            "id": "ee22a39c-f02c-495c-b197-61d425d0862d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 104,
            "second": 72
        },
        {
            "id": "46337b71-7c57-4091-b4d0-1de6283db04a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 104,
            "second": 73
        },
        {
            "id": "95f872ed-1792-4fd4-9314-d8cc8211624a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 74
        },
        {
            "id": "5e0f0fce-6ddf-41e4-a3e3-e3680811c5d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 104,
            "second": 75
        },
        {
            "id": "b0609992-20a4-46a5-aabc-66dd79125f7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 76
        },
        {
            "id": "6d067053-0e87-453e-b8ac-19c0335bf79d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 104,
            "second": 77
        },
        {
            "id": "f96f5aa8-0a05-4e89-a68d-83e3c5882635",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 104,
            "second": 78
        },
        {
            "id": "fd66cb30-3bd1-4044-a955-f33f9d2d0d2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 79
        },
        {
            "id": "a9e3ab2f-edd8-482a-927b-7a2c5f8b05a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 104,
            "second": 80
        },
        {
            "id": "fee5f8e3-60b3-40d4-9201-a7f7a83dffc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 81
        },
        {
            "id": "20cd72ad-c616-4460-8cec-e879be9dae05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 104,
            "second": 82
        },
        {
            "id": "639459fb-7ea9-47c4-a344-da7b643d1a51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 83
        },
        {
            "id": "5066909e-c751-400a-991f-bb51439ba5f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 84
        },
        {
            "id": "b710dac0-de95-4cdc-a8c7-24cd60e5f9c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 104,
            "second": 85
        },
        {
            "id": "39a3d3ab-3ff7-4974-bb85-7f6a4335f966",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 104,
            "second": 86
        },
        {
            "id": "182cb573-a2b2-4d57-ba52-8ef92be718cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 104,
            "second": 87
        },
        {
            "id": "2897cae2-f623-44fd-8862-490a3fef35d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 88
        },
        {
            "id": "6af8af8b-77ec-4794-8d56-67b389f3c296",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 104,
            "second": 89
        },
        {
            "id": "ccb4d03c-8cf9-418e-b227-32c0192df92d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 90
        },
        {
            "id": "9f05a855-b448-44a7-8458-8a05aaf51d2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 92
        },
        {
            "id": "1277558c-b8e6-4d27-ab9b-f1cd848cc7b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 104,
            "second": 93
        },
        {
            "id": "5bffc4b1-4cee-4554-87fd-0a76a15bbdc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 97
        },
        {
            "id": "a148f834-07d6-4e37-bdc4-b47d17e91395",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 98
        },
        {
            "id": "876456d6-84d9-4130-a4cb-df0b88a5d0ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 99
        },
        {
            "id": "57ecca95-31b2-4308-b96f-31e934dd5e3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 100
        },
        {
            "id": "123e4c9c-eb89-4b86-985f-0599b6ecb2fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 101
        },
        {
            "id": "cfff8588-e6a5-4645-8024-95dbadd586f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 102
        },
        {
            "id": "025e3a0f-4afa-4795-92fa-ea1f889860bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 103
        },
        {
            "id": "24d6351c-73f3-447d-a460-921c7519fce9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 104,
            "second": 104
        },
        {
            "id": "59b0af82-3c7d-4be0-9a6d-599492c51fc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 104,
            "second": 105
        },
        {
            "id": "eb759470-f220-4117-b247-ce0d7bcdc58e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 104,
            "second": 106
        },
        {
            "id": "1b8cc405-4478-411f-a231-6d38f6ca590c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 104,
            "second": 107
        },
        {
            "id": "0efdef14-0abb-44db-801c-fc06a54f4944",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 104,
            "second": 108
        },
        {
            "id": "7b567cb3-2272-4f76-ae13-a98e09b6234c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 109
        },
        {
            "id": "dd545363-85c4-413e-ba29-3bfc5ad49fed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 110
        },
        {
            "id": "8c0767dd-c5ea-4225-97ea-79ca7055b30a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 111
        },
        {
            "id": "122c784c-481d-4796-94dc-537a2891977e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 112
        },
        {
            "id": "8379a136-4c29-4fa8-91f4-0aa32f0b0c6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 113
        },
        {
            "id": "44d5775a-a1cb-4213-aaec-c1893a87fa9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 114
        },
        {
            "id": "b453ea00-10fa-4b19-8127-1692adee13a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 115
        },
        {
            "id": "b9c91a9c-7935-4e67-85f2-5ae52b743988",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 116
        },
        {
            "id": "db2f63d7-4764-4f30-be1a-c38b740c9c31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 117
        },
        {
            "id": "73758671-eb0e-401d-9ab3-d6d9811e2712",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 118
        },
        {
            "id": "4cd2c7c8-9327-4ab7-8034-2cf14bbcd541",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 119
        },
        {
            "id": "a32a0cdd-d4b6-429f-928e-7a86ed97f41e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 121
        },
        {
            "id": "4eb65f32-2ea3-4e35-be86-687a4dbd5a3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 122
        },
        {
            "id": "aa95fb85-d80d-42e2-806d-252e02d1b29b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 8216
        },
        {
            "id": "eb55e850-773f-452e-ac66-6898e9bafdfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 8217
        },
        {
            "id": "948e99bb-64cc-4d42-94f1-bf33442f0e0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 8220
        },
        {
            "id": "8b98e03e-fe7e-4c69-9bb1-fd67ccef87c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 8221
        },
        {
            "id": "df5b3ab4-9357-4ea3-b9cb-28f9d7feff82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 58
        },
        {
            "id": "062b4a24-f476-4f91-888c-903c2d673f94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 59
        },
        {
            "id": "cb563022-e19d-431c-92b7-0edb7ee8b2cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 64
        },
        {
            "id": "513957a0-f702-4fe2-997d-b7bb3d3a079f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 68
        },
        {
            "id": "cb059915-33d3-46cd-9501-7064b52610df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 69
        },
        {
            "id": "416a9a21-7efc-47c1-8370-2066d0ea6a73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 70
        },
        {
            "id": "962c997a-5955-4121-a820-9caa9d6cf971",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 71
        },
        {
            "id": "234abe08-3caf-4153-b660-4f972b7c5906",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 72
        },
        {
            "id": "eebee8f2-bb49-403d-8063-1e3ba182d0d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 73
        },
        {
            "id": "dfd6bc4b-e353-46b6-b415-eacd7e0ce594",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 75
        },
        {
            "id": "e2977db2-e788-4d2c-baa6-10e34f248361",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 77
        },
        {
            "id": "62c81b9e-95a3-43a4-963b-3784564cc0e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 78
        },
        {
            "id": "5b89eec5-f41f-4a93-be61-883e21cb5fe1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 80
        },
        {
            "id": "e07fd2e2-5bd7-405d-905a-3d48a156d260",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 82
        },
        {
            "id": "2c084c7f-137c-4924-8e99-f8d1738d6c81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 85
        },
        {
            "id": "785c23e8-3bdd-49e7-8204-acb75d8acb08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 104
        },
        {
            "id": "bd685073-674f-4c30-993e-e55f50870f05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 105
        },
        {
            "id": "a9edce04-24cb-4c91-ba19-8aa2f688b05e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 106
        },
        {
            "id": "d3de49d3-fff9-410e-a912-3b2da643d8ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 107
        },
        {
            "id": "694719dd-ee00-4c2c-afd3-e87ff48be66d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 108
        },
        {
            "id": "fe7734c1-a6e8-4a9d-b262-35997730aff2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 109
        },
        {
            "id": "eb526634-20c4-443c-a023-9f85783d6818",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 110
        },
        {
            "id": "fb6a16fd-d828-49cc-ba7a-66a0001ac09c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 112
        },
        {
            "id": "fe2630b8-3f07-45a0-b1ce-c6daf73b5a52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 114
        },
        {
            "id": "c24b2d6a-ac0e-48af-8d56-9f44475ca888",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 117
        },
        {
            "id": "3ed0f07f-6d61-4f0e-9959-cf84eaea9fb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 64
        },
        {
            "id": "ee4026e3-03c8-41d3-8940-3dc5c767509a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 109
        },
        {
            "id": "cdf25525-ac73-43e6-9eb6-4d719cfc1af5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 110
        },
        {
            "id": "43bc2503-b85b-4172-b7de-99bdb1713a23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 33
        },
        {
            "id": "732868c8-5d76-4938-8f6b-52f49d26214f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 34
        },
        {
            "id": "b7fed84d-f3d8-4597-954f-ac81cf0bbba3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 39
        },
        {
            "id": "efa945da-8173-4c92-98d7-ee8c1c38f5e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 45
        },
        {
            "id": "ef1c5fa3-ab16-4d52-a046-3823ceb8e70e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 48
        },
        {
            "id": "9f58779b-45d7-4391-90e5-c3334e8a1e05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 49
        },
        {
            "id": "603f8d39-9701-4634-bb39-ebf2b2930b68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 52
        },
        {
            "id": "575cfddc-0cbf-4163-b41d-5ac83d89cbc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 54
        },
        {
            "id": "c16329c2-d853-4f77-891a-4d826969390e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 55
        },
        {
            "id": "359d9529-de37-48a4-8e38-ffe49942a986",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 56
        },
        {
            "id": "629c5af3-14b0-4623-bc45-a1ab4e8e8b71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 58
        },
        {
            "id": "25620b15-7916-460f-9d5b-ec4e040ca8fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 59
        },
        {
            "id": "772fdca5-d482-4d2e-b34d-45d25dc8fbb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 63
        },
        {
            "id": "7eca4535-61ee-4f77-bd53-a43f50c07711",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 64
        },
        {
            "id": "2dfebbe7-b952-479a-abc8-578851b121f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 67
        },
        {
            "id": "5028f4e3-8884-4a25-a063-cd4d27233fb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 68
        },
        {
            "id": "57df46d4-7aae-42d9-9803-bf44082252f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 69
        },
        {
            "id": "4940354a-d533-431c-82fb-624801e9ef8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 70
        },
        {
            "id": "9535a629-d038-4b5d-8477-693117dfdc88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 71
        },
        {
            "id": "af22406c-9207-49f9-8d6d-d9890b66f35b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 72
        },
        {
            "id": "6e180ff4-e079-4644-ac03-4e6d199951ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 73
        },
        {
            "id": "ab9cc361-356c-4868-948f-113a5d6d3419",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 75
        },
        {
            "id": "140d8e83-335b-49c7-ab35-7ea9425c83d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 77
        },
        {
            "id": "9e87ba08-c22b-49f5-834f-4535ad7a6714",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 78
        },
        {
            "id": "52bc1bf4-076e-405e-853d-8ef6b228cd89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 79
        },
        {
            "id": "3572dc22-ee8f-49c3-a31a-7e04fa5381d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 80
        },
        {
            "id": "31f9c61d-3ed7-45a4-aaf0-92fec89b8042",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 81
        },
        {
            "id": "b65a53aa-77c3-4f1d-a2e4-919e55fe4e2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 82
        },
        {
            "id": "595e5e1b-1c97-41ef-80b4-dee35ce0c1d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 84
        },
        {
            "id": "099d90c5-8b66-4738-9639-6c77cfe609bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 85
        },
        {
            "id": "e6222d8e-f6b2-491f-aeea-ddf04bf57427",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 86
        },
        {
            "id": "4adfa062-c2a2-4b7a-a745-fffb089f8592",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 87
        },
        {
            "id": "b333e77f-6f6e-4fbd-aad4-40dfe924598b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 89
        },
        {
            "id": "21b492fc-12b1-47bc-bc34-796946aa7dc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 92
        },
        {
            "id": "dfcde4b0-2351-4f80-b500-102a250b1cf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 93
        },
        {
            "id": "c3854cc1-c416-45f8-a2d1-adc523f0853f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 97
        },
        {
            "id": "7766daff-9241-4acf-96f8-dcac6f9685b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 99
        },
        {
            "id": "3c928d57-05b7-4461-9f72-4b92be5c5f06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 100
        },
        {
            "id": "ece9e299-936e-453e-b5cf-19e9ebb53f59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 101
        },
        {
            "id": "45bd1762-1268-47bb-97ab-edbe635d0e3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 102
        },
        {
            "id": "2bdf0a0e-57e9-4376-bb55-9ef9a9ef3fce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 104
        },
        {
            "id": "271d0777-b630-4f28-a687-dec56b72e760",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 105
        },
        {
            "id": "e48afa25-125b-40f7-ac9e-612c6b56c994",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 106
        },
        {
            "id": "43772d71-2549-419e-8ccc-9f14e5ed83b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 107
        },
        {
            "id": "ae16251d-5742-41d8-b963-a4c043e084fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 108
        },
        {
            "id": "98987469-045d-44a8-b00b-be5a2956f5fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 109
        },
        {
            "id": "f287804b-a52a-43a5-b565-b67929761c0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 110
        },
        {
            "id": "9b8d8fec-042d-44f5-946f-844f1a674286",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 111
        },
        {
            "id": "f120ae7b-38e7-44ae-8ed7-684d4ca13dd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 112
        },
        {
            "id": "2cf00990-a672-4319-9211-f599b789a9a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 113
        },
        {
            "id": "789615fe-7cf8-41f1-8135-76b431b8a2af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 116
        },
        {
            "id": "4a0c884b-8835-40a0-aa8a-bb6a2548e891",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 117
        },
        {
            "id": "9c4098e3-4894-4dfa-a793-ff33af3e5294",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 118
        },
        {
            "id": "12497eee-6ddc-4796-8911-38f7701de61e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 119
        },
        {
            "id": "a859dd92-8261-4346-9ac6-fdd3b7a2fd2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 8216
        },
        {
            "id": "917242b6-9438-4ccc-9ef9-90c4c594e0bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 8217
        },
        {
            "id": "10b58c9d-95a4-47a9-88ac-1ed6cd3712cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 8220
        },
        {
            "id": "49fd7958-af6b-4619-b659-2b5d1213df0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 8221
        },
        {
            "id": "e0a0d30e-ffe6-4f45-81b1-bb69d09292f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 58
        },
        {
            "id": "fe7ce7af-df35-47e3-b7bb-45f9860b27bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 59
        },
        {
            "id": "0592d62f-59b7-4e81-a595-b20615eb913d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 64
        },
        {
            "id": "7926d564-08de-4672-bf8f-42dd578d5b33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 66
        },
        {
            "id": "3b4eb2fb-cdfd-4b48-b14c-c02dc3ab4a49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 68
        },
        {
            "id": "33bd643e-2b81-41a9-8d9b-78a9247fb030",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 69
        },
        {
            "id": "2ace36db-14f4-4fd9-924a-8cea66e67052",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 70
        },
        {
            "id": "98a8fab9-1312-4826-b871-9cfcf218a6ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 71
        },
        {
            "id": "0081a6b8-495a-4f2e-b886-eefbcda9073a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 72
        },
        {
            "id": "f9a12b7f-527a-4e58-919d-c4fb0fa5903b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 73
        },
        {
            "id": "889848a8-978e-4d56-a2c1-172b54c16d16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 75
        },
        {
            "id": "eb59675d-ae15-456d-8638-52f53813894d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 76
        },
        {
            "id": "a598e2ce-26d5-4fa0-9441-425c1ef0078e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 77
        },
        {
            "id": "7f3f2920-7e80-47dd-858d-b7117594ba55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 78
        },
        {
            "id": "eea4b53b-f6a2-4a02-bcb3-12ea1b19c0b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 80
        },
        {
            "id": "ee4292ab-14ca-40c3-a1e8-2d35a78539a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 82
        },
        {
            "id": "216791af-7364-44e1-8719-e7c2de05cf4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 85
        },
        {
            "id": "6eefa96d-8365-4509-b625-0a5ca1f7be3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 86
        },
        {
            "id": "be187204-9cb0-467b-83f1-f3d2d6e8e687",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 87
        },
        {
            "id": "df023a63-d34d-4d34-ad54-add548846236",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 104
        },
        {
            "id": "387fb2e5-a442-458f-a90e-f8181a682884",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 105
        },
        {
            "id": "c593e3ef-e077-4014-8618-3fe50fa443ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 106
        },
        {
            "id": "79dc6a4d-eb64-48eb-af19-3c15e87c40b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 107
        },
        {
            "id": "505bd52a-edd5-4f0d-9737-7804479e5529",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 108
        },
        {
            "id": "b50cc0e2-257a-419a-a26a-194d41d5be6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 109
        },
        {
            "id": "257a5134-16f9-430a-8caa-491b69dd945f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 110
        },
        {
            "id": "04f0f0c0-94e3-43dc-907d-33cf06ccca4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 112
        },
        {
            "id": "fd95b2f1-38c2-4ae1-bd34-a65c23abb37b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 114
        },
        {
            "id": "acdc9b75-b665-4084-905e-91e455541399",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 117
        },
        {
            "id": "de80fdaf-83a1-4010-b3bf-b4bff4c58990",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 33
        },
        {
            "id": "86afb25d-c58e-44c8-a576-55fd2af0546d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 34
        },
        {
            "id": "01ed72d4-817d-4d2f-9a3f-a38bffd26951",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 39
        },
        {
            "id": "8a1582f0-92cf-4b20-946c-0654d22ced07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 40
        },
        {
            "id": "e36e4ef9-f8d6-422f-b491-bc0f31947905",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 41
        },
        {
            "id": "e90c8288-7ce4-4450-89e1-fb79b3cf8685",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 44
        },
        {
            "id": "9a0ad923-3f97-4c8f-89d9-30b5c6299014",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 45
        },
        {
            "id": "faa2d87a-0f04-44db-97d9-98ab53ae00b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 46
        },
        {
            "id": "203026b4-415b-46e9-b758-46c61afd52b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 47
        },
        {
            "id": "5dc33b04-ba51-4304-b20a-a58aa02fdc69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 48
        },
        {
            "id": "66db9901-685d-45d8-8d14-d42a1136ae9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 49
        },
        {
            "id": "adb87c11-730c-4873-b542-0335765c3af1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 50
        },
        {
            "id": "3f399e7e-50d8-4018-9f69-f5aa96744da1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 51
        },
        {
            "id": "96c58937-7a0e-4e7e-94e3-adf1e55b1cb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 52
        },
        {
            "id": "e9e200fd-adec-48d8-9d7f-bfb1729f2001",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 53
        },
        {
            "id": "6ca7634a-8329-45ab-bb24-be743b98bfe8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 54
        },
        {
            "id": "6b5c8ee3-ea46-462c-8fd3-1663e817a580",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 55
        },
        {
            "id": "f9e4979b-b2d3-48a0-a1aa-a7ac80d9bf63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 56
        },
        {
            "id": "e2b438fd-2edd-41ec-80e2-4a6cf2dd68ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 57
        },
        {
            "id": "3caac195-989c-4ba9-bf67-6a1c31f2b292",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 58
        },
        {
            "id": "4cb1facd-dd87-492f-93bb-2d22a578c6db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 59
        },
        {
            "id": "2edfe225-18f3-4540-bd63-a6c1e8d83cc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 63
        },
        {
            "id": "b72ce08e-a8b7-4aae-9d4d-eb5716f5297e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 64
        },
        {
            "id": "af3e7e3e-ca7f-4dfe-8991-1da33f29f48e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 65
        },
        {
            "id": "b61c4e33-418b-46a4-9525-9b2534dfc549",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 66
        },
        {
            "id": "8dc4706d-161b-4846-9adb-149719100ca6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 67
        },
        {
            "id": "bdf72303-dcfc-418a-bbad-16b0bbed8a68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 68
        },
        {
            "id": "34cda6c4-78d3-4e3c-8a8e-b5bf24623a15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 69
        },
        {
            "id": "6dda69aa-fbc8-4e78-bde9-a4896e0c26bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 70
        },
        {
            "id": "0bbfab68-23ea-4832-b612-c839d4cdcb4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 71
        },
        {
            "id": "938c18ac-ca2c-4cd7-bfe0-844687290c2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 72
        },
        {
            "id": "1b64fac0-e598-42f6-9c6f-753e74a35af6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 73
        },
        {
            "id": "be8b651f-34ee-4d1c-8d00-94a417360c32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 74
        },
        {
            "id": "de6d032c-1dcc-4b74-9115-942ece5ce68c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 75
        },
        {
            "id": "bcfe860f-84d6-4527-be88-bb27cca3ce52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 76
        },
        {
            "id": "f6feaf97-f01a-4038-8097-0dd8ac7ab5ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 77
        },
        {
            "id": "cb400c4a-8d54-40ef-8345-30828d765d04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 78
        },
        {
            "id": "0ee5fbc9-14e4-451e-b4f7-c1581438e718",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 79
        },
        {
            "id": "6ce3c671-7205-459e-b77c-e51e61ab9de4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 80
        },
        {
            "id": "37f197ff-dd71-40a8-b31c-e1dbc96992b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 81
        },
        {
            "id": "1b8147cc-5806-4799-a835-14c04ed8c84c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 82
        },
        {
            "id": "3157c6c0-b4d4-46d0-8df2-eeebd9c0fbad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 83
        },
        {
            "id": "a6a11caa-bfe0-4665-abb5-78d563cb017c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 84
        },
        {
            "id": "0a58be6f-491f-4209-96e8-775a3a918e42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 85
        },
        {
            "id": "24a7b7cb-fc72-4e9f-93fa-a71c0efed5be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 86
        },
        {
            "id": "6a8f1a1d-09ec-4a86-acd8-989e5e8d7aef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 87
        },
        {
            "id": "b0ca8707-b251-46cc-8ea0-813b994e41fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 88
        },
        {
            "id": "fabdfeba-01ed-44ff-bab6-5072cd7c853f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 109,
            "second": 89
        },
        {
            "id": "4964338c-2d09-4986-abe0-64d606a2d26f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 90
        },
        {
            "id": "fc5de1c2-c9ed-4ff2-94c0-ddd8f233debe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 92
        },
        {
            "id": "5a2346e5-d724-46be-bc5a-5cebb26d6374",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 93
        },
        {
            "id": "6519f9ab-cf49-41ed-84b2-65219a730b0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 97
        },
        {
            "id": "b04121fa-ca72-4a93-b15c-e73c31078ae7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 98
        },
        {
            "id": "a116e14e-0fcc-40dc-8c36-9ef60bd53a26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 99
        },
        {
            "id": "5ef1f0e5-9d4a-4cd1-9481-7ee76adc67f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 100
        },
        {
            "id": "4c0a6b7b-253f-46aa-ace1-4605398410dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 101
        },
        {
            "id": "71740c39-1a11-474b-b28b-85cdfe3420b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 102
        },
        {
            "id": "438e2446-a1cf-4b68-b2d6-0d5b8d74780d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 103
        },
        {
            "id": "41552161-bf75-4fcc-9b70-d64dbfd50908",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 104
        },
        {
            "id": "643386bb-aad3-4334-b6f7-0193da1130ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 105
        },
        {
            "id": "e0dc2ff8-cd11-4ca5-b6ad-7bd2b7d373fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 106
        },
        {
            "id": "0c020fa7-f828-4afb-a507-819869949263",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 107
        },
        {
            "id": "6ffca13a-84f9-47f9-917c-2be5957143ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 108
        },
        {
            "id": "98d56d36-a637-4f99-845c-11c9cb92fd4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 109
        },
        {
            "id": "d159b78b-b0e1-4fb4-9cb6-fe8382471599",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 110
        },
        {
            "id": "c584a88d-ecd7-4c2b-ab9e-3772cc9fd82b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 111
        },
        {
            "id": "181a858d-9c67-4255-a7e4-ccb6b412d9d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 112
        },
        {
            "id": "952f1973-bbcb-4826-9bff-2c3c179dec29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 113
        },
        {
            "id": "6de626d1-4a40-4c9d-b257-1c1ea5415e19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 114
        },
        {
            "id": "769be178-b528-4a95-b327-5a5cde624b53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 115
        },
        {
            "id": "53a07175-9fd0-4e27-bcb4-9a42ae04669a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 116
        },
        {
            "id": "3b485181-a49d-4197-9700-b4117dd93965",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 117
        },
        {
            "id": "b6f14817-b6f9-49ab-a889-11feec4a880a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 118
        },
        {
            "id": "cfaf1402-12b6-4677-803e-d9b64aa0f705",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 119
        },
        {
            "id": "66bbccdd-5f5c-460d-b14a-97bc2496d385",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 120
        },
        {
            "id": "480c8540-053f-49cb-a2fc-55ae7ecc893e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 121
        },
        {
            "id": "87661caa-d50c-4e77-9dc5-200a69a84403",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 122
        },
        {
            "id": "fbb04e99-510d-4f24-bb59-fbceafe87618",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 8216
        },
        {
            "id": "6fb7a822-9964-40f6-a45f-14083b400c47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 8217
        },
        {
            "id": "699e8439-ec91-4707-8f78-f5e86f8525fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 8220
        },
        {
            "id": "f9afcc4f-cb1e-49d9-b7be-16e0020aa399",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 8221
        },
        {
            "id": "91ad366c-5004-422f-952e-76f70a19b8d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 33
        },
        {
            "id": "bdd8e4f2-54c8-46b8-ac4e-91e32ad4ef6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 34
        },
        {
            "id": "e9e0eab7-f511-4a98-bc2f-c8220d81ed25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 39
        },
        {
            "id": "4ec64497-1304-405c-a52e-42d88e66aa0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 40
        },
        {
            "id": "f8950e1b-b371-4e43-bf5a-e37871d972de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 41
        },
        {
            "id": "75eb5713-a6b5-4614-b06a-c865afc7157b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 44
        },
        {
            "id": "dacc023f-2ceb-4629-8697-98eb6bc5d185",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 45
        },
        {
            "id": "4f4b8021-0b01-406f-bba5-09744114f5b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 46
        },
        {
            "id": "4b3534b4-025a-43bc-a841-444934cc38a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 47
        },
        {
            "id": "5186da75-e600-44e6-ba62-1f84fcee2120",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 48
        },
        {
            "id": "5a227805-db22-4a3e-b707-d5df8d2f4c17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 49
        },
        {
            "id": "55420b4c-c75b-4b8e-b5de-b9aa10c8474f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 50
        },
        {
            "id": "4faacc66-165a-44ab-847c-25841b5e31fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 51
        },
        {
            "id": "0cbc5997-2a38-470e-ba21-4be6ab84a0c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 52
        },
        {
            "id": "e83d5aa3-9ecd-442b-9a7f-17c24ae93aae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 53
        },
        {
            "id": "53d8a46e-3205-4a8a-9c3e-8ba55cbdde30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 54
        },
        {
            "id": "cf0be8e2-58a3-4b98-bf5f-1c5dbf1476a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 55
        },
        {
            "id": "091e97ab-f972-40ba-ab11-07bdd4032b81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 56
        },
        {
            "id": "5e47288a-55f9-4ea0-a66d-f2e9e70dd76c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 57
        },
        {
            "id": "170df5db-8f71-431e-a742-22c18b79bfaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 58
        },
        {
            "id": "a47e82f3-2ab3-4cdd-88af-9687e2a226cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 59
        },
        {
            "id": "d5b16496-cc01-4459-b42c-d7b3e55ae92c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 110,
            "second": 63
        },
        {
            "id": "ae24c47a-4808-496f-9d11-5f2ee48954e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 110,
            "second": 64
        },
        {
            "id": "c58785fe-8d90-49ec-8287-30d644e6b3ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 65
        },
        {
            "id": "e9ad951a-6497-41e4-9027-06bd4b2b5899",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 66
        },
        {
            "id": "3c9c8638-2c6b-4b9f-a279-4162af046bf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 67
        },
        {
            "id": "2742e2c0-b788-4cf2-ac65-f4093be2f35b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 68
        },
        {
            "id": "a133407f-cb4c-43b5-98a2-f8a9296c6220",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 69
        },
        {
            "id": "4bb502e6-35a2-469f-ba58-99553bb941a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 70
        },
        {
            "id": "9357b271-d769-4de3-a8b7-5cf8a0da831d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 71
        },
        {
            "id": "218d345e-2641-42a6-9ff1-4be950b2c5ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 72
        },
        {
            "id": "94ae1e14-322d-4719-9632-151e5259818c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 73
        },
        {
            "id": "b0611a3a-26d4-42a0-ab36-50d19ea526ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 75
        },
        {
            "id": "6bf86d65-2492-438a-81b9-a6b1bbf5dad2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 76
        },
        {
            "id": "a925e0a2-3ab1-4ade-a49d-57a48d45cd48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 77
        },
        {
            "id": "32e236de-f6fd-45aa-b897-43b399742143",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 78
        },
        {
            "id": "45e009b5-a2a6-4ae3-8311-f6b715ef5dc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 79
        },
        {
            "id": "3b1fefac-fce1-40ea-b054-150e88d873ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 80
        },
        {
            "id": "1894d69b-5a35-4762-9983-2fbc2539b4dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 81
        },
        {
            "id": "d5fbf134-e3a2-4d34-ad1e-91e20869d11d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 82
        },
        {
            "id": "3a40bed5-25c4-482e-9d6d-134e849d9fde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 83
        },
        {
            "id": "95d6c6fb-7b0a-40ca-988a-559d9a9503ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 84
        },
        {
            "id": "d6545421-e92a-4f4a-bbb6-d5df37e5b167",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 85
        },
        {
            "id": "b3af7dba-3ad1-4ba8-8fd9-d2a3b0f702ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 110,
            "second": 86
        },
        {
            "id": "baad1913-23ec-4367-8fad-f7e4499c539a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 110,
            "second": 87
        },
        {
            "id": "1a7d0dcb-39a9-4fb4-83da-91761474b4e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 88
        },
        {
            "id": "0acfcf2b-8dd5-49e9-bf70-7c6b203a2f3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 110,
            "second": 89
        },
        {
            "id": "dffbe0fd-7d72-4452-bc0c-ffcceac6a11c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 90
        },
        {
            "id": "f9529fb3-77eb-4865-bb79-cbab393da25f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 92
        },
        {
            "id": "b07c99ed-0bd0-4760-a266-aaeb71fe3eca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 93
        },
        {
            "id": "3468c231-1cbe-4e16-bf29-0ad3f9e85b09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 97
        },
        {
            "id": "79ae3b3f-6294-4b09-a959-73ad516ece41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 98
        },
        {
            "id": "8a70c732-9a41-4623-acf4-559a59717173",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 99
        },
        {
            "id": "3630ddba-e217-46e7-91b6-078ef12b0996",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 100
        },
        {
            "id": "7e23e917-8609-4539-926c-e3b5deedba56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 101
        },
        {
            "id": "f1786a38-1db4-4479-9514-9acdffa8c66d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 102
        },
        {
            "id": "a1315405-f4a6-449d-a6c4-a9012743d6f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 103
        },
        {
            "id": "81d0c6d1-22d8-4659-9ef5-10c1cf304906",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 104
        },
        {
            "id": "0c186ce5-d3aa-408c-98bb-861042ad948c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 105
        },
        {
            "id": "4bd1a845-017a-4ae0-99c3-5f67fef48677",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 106
        },
        {
            "id": "61583746-2424-4f1f-be7b-eed9a5293cec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 107
        },
        {
            "id": "dd61cf51-27b7-49a6-9b10-e6b8cf1588a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 108
        },
        {
            "id": "f9e40af5-c0e7-4639-aba9-5b2787b35379",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 109
        },
        {
            "id": "b9c0340a-c8cc-46fb-b95f-d8e00a24768b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 110
        },
        {
            "id": "e5c636f2-c24e-49c9-aecd-77e2a739c472",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 111
        },
        {
            "id": "c4752d20-2d0d-4e8d-a6f5-3fac176712c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 112
        },
        {
            "id": "7a0768d8-6a3f-4623-9d47-102623d361d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 113
        },
        {
            "id": "b63c979b-c98d-45b3-aa81-7863a63d0a5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 114
        },
        {
            "id": "7e6262aa-7603-4be6-a67a-cf6bbb0726a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 115
        },
        {
            "id": "f99bad03-ef80-4c71-9c23-060566f50df6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 116
        },
        {
            "id": "ecad9006-4449-46e7-a9c6-b3ba35d5e22d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 117
        },
        {
            "id": "940b63f7-fa04-4ef5-8e52-591f3310e150",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 118
        },
        {
            "id": "7486a7d9-0a13-48d8-bfe2-d62f907ece40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 119
        },
        {
            "id": "94d9a72b-7ab4-4695-8f82-3bcf0ec28db3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 121
        },
        {
            "id": "6bdaa749-1128-44c5-8d75-477e33a5b3fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 122
        },
        {
            "id": "5ce4cf12-2b98-446a-b732-6fb85b10306b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 8216
        },
        {
            "id": "1ea32a6d-1d53-4431-9195-09ad5f0af2e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 8217
        },
        {
            "id": "1c0dc385-f7ff-492d-b2c5-7fd628b112c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 8220
        },
        {
            "id": "98b9e008-e0c7-4b7d-9312-22a00e556b40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 8221
        },
        {
            "id": "acbf24c3-796e-4b01-993a-c168bf8e1d36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 33
        },
        {
            "id": "38573daf-5a12-4c9c-8da8-c870d638aa30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 34
        },
        {
            "id": "5181bc5f-97cc-480f-8234-9a0eb9697f7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 39
        },
        {
            "id": "3aa344dc-64a8-47d1-8252-c9e0d87ebdcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 49
        },
        {
            "id": "5f7a8603-0803-44d3-b317-3ccda4c4ca8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 50
        },
        {
            "id": "0d7cf7af-1be3-4c01-8118-f14e7c975309",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 55
        },
        {
            "id": "56e7c733-8874-4cfd-8916-9880a479be4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 58
        },
        {
            "id": "f0422113-cac2-4349-ba9c-c92ba66353c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 59
        },
        {
            "id": "e6351926-8ebb-4956-8f40-a9c012badac4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 63
        },
        {
            "id": "99d3d310-f4eb-4fdc-9a35-6f37a2dd397a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 64
        },
        {
            "id": "ebe25315-b53a-4762-8461-67b9ffe5dd2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 65
        },
        {
            "id": "ce746ac2-575f-44b3-b294-468358b292fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 68
        },
        {
            "id": "9b1300df-8e71-48d5-a2b9-82542356db2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 69
        },
        {
            "id": "b7d2db84-06fa-4290-bbf8-dee9a10b9fab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 70
        },
        {
            "id": "f0413245-9ff3-48a8-b88c-7763e72bb7db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 72
        },
        {
            "id": "9448131e-42fe-4657-b2c2-a4540cf56214",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 73
        },
        {
            "id": "1ad17db5-99dd-4152-9612-a94fd998fb6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 75
        },
        {
            "id": "f8d01687-2aae-4083-bf26-96276b01c574",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 77
        },
        {
            "id": "3174d8ca-994c-4062-b175-f79bc2dca586",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 78
        },
        {
            "id": "307bf475-2cd3-4c1e-8704-9ddabd95a62e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 80
        },
        {
            "id": "6918a696-c60f-4ab0-a5d3-0293b6da54ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 82
        },
        {
            "id": "fb19dce6-8542-4767-9e97-7ce31e73c501",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 84
        },
        {
            "id": "e143e675-5a7f-480e-be47-e043d1e27c82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 85
        },
        {
            "id": "98d7d19f-e9a0-48e1-a1b4-6dc49f2eb358",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 86
        },
        {
            "id": "ee0b89c3-281f-46d4-9ddf-1e86a6af06af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 87
        },
        {
            "id": "d574817d-82eb-497d-80da-09fd0bb07838",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 88
        },
        {
            "id": "3833f352-81b2-4ea9-a714-1242a34e06ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 89
        },
        {
            "id": "30de51d3-552e-4282-bf81-c87113392415",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 93
        },
        {
            "id": "e9692793-996d-49a8-acb3-954fbb40f2a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 104
        },
        {
            "id": "772026cd-9157-4b65-a02b-d0f935d09320",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 105
        },
        {
            "id": "323f9229-193a-4cc4-a1a0-028da8cb4989",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 106
        },
        {
            "id": "1d73b18e-ac2f-4b24-a500-a5c8d1274705",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 107
        },
        {
            "id": "437e2eb8-4df3-4650-adab-65a6960b6b08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 108
        },
        {
            "id": "565eaf0d-5c1d-4869-91cb-0898e90f2d3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 109
        },
        {
            "id": "3b8150ca-1dc2-42c5-a24e-d728a40f897c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 110
        },
        {
            "id": "35c22ced-23e3-4a86-8886-c48fab01659b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 112
        },
        {
            "id": "53c72659-654e-4700-91f1-dc4b7f9bf2ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 117
        },
        {
            "id": "9c166cde-070a-477f-ad9e-eb8b14b9fab9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 118
        },
        {
            "id": "ce2ca86c-bccc-4690-b92b-1a8adf31bd03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 119
        },
        {
            "id": "fa0bb5f9-4d4a-41a6-ba4a-cff340b2960f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 120
        },
        {
            "id": "060f6772-e127-47fe-9ac0-49e56ff0a1b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 121
        },
        {
            "id": "44f75ce3-fc29-4f4f-a103-c397e289a12f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 8216
        },
        {
            "id": "febc242f-ea37-4fc3-af3b-ed97ed06d3e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 8217
        },
        {
            "id": "32aaeb5b-c397-4c0c-a6d9-cc6a16fd8e2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 8220
        },
        {
            "id": "6ca64c2b-4b0a-4c7e-ab82-735b45c7a566",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 8221
        },
        {
            "id": "ce210fd9-6334-4c93-ab67-120a50c6bf56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 33
        },
        {
            "id": "5909ab41-03dc-48fb-a4db-b35f9017787a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 34
        },
        {
            "id": "011fc12c-4545-4da6-82ed-6f994a2f9173",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 39
        },
        {
            "id": "e5ea8bc4-491b-4b98-a1c6-c4cb3ae17d8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 44
        },
        {
            "id": "ec7d792a-bfab-4811-9b03-69143773e262",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 49
        },
        {
            "id": "43e2d1f5-150c-49e3-bd9d-10945b75181a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 50
        },
        {
            "id": "f79f7c4f-cf4f-4d4d-b1aa-44a48eba561d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 51
        },
        {
            "id": "41cde7dd-e4fb-40fd-ad9a-38c8f7921e32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 55
        },
        {
            "id": "acd1fc94-cf2c-4a6c-b351-d899e18433eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 58
        },
        {
            "id": "5ce2d4bb-34a9-4287-8146-b774b80cb8d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 59
        },
        {
            "id": "8ed04125-9ad4-4cad-806b-af7a6262a72d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 63
        },
        {
            "id": "fdaf5578-b93e-4aa4-bc03-4b60ffbc5fb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 64
        },
        {
            "id": "1b0eefa0-eb5b-43ae-9bd9-a72cd3e7c89c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 65
        },
        {
            "id": "d6a17a86-64e0-499e-a82a-203768d9808b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 68
        },
        {
            "id": "4b96a20c-9c50-4bcd-9bc8-4164b6c0c738",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 69
        },
        {
            "id": "a037bd23-b9b3-41bf-a654-ecc970e0b8f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 70
        },
        {
            "id": "102cb99d-7571-42f5-9b76-2545f2f067c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 72
        },
        {
            "id": "1da7367f-4743-4ef8-8a9a-bfddc3e0f6f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 73
        },
        {
            "id": "e5b1a828-93c8-46f7-b932-dd6569126d17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 75
        },
        {
            "id": "eaa5ebff-5d8e-436a-9345-88542a13a825",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 77
        },
        {
            "id": "881f46e1-19b7-40a2-ab9b-2cc41f995608",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 78
        },
        {
            "id": "9bcd171f-e916-4965-bd7f-672078239b5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 80
        },
        {
            "id": "1d5bce84-6817-4787-9bc8-ff1d30367914",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 82
        },
        {
            "id": "e52f73a3-7550-49be-bda5-add385898010",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 84
        },
        {
            "id": "c24f544f-3ef0-4f12-ab8e-d7ce6d5612ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 85
        },
        {
            "id": "2a72746e-93de-459c-983d-c97b7f6a2b59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 86
        },
        {
            "id": "af4a77f7-c715-464f-b3b6-899d1e11ca8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 87
        },
        {
            "id": "929e3a4f-4ecd-4af5-98ac-86c5eaf066d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 88
        },
        {
            "id": "61c0b0cb-2432-42e0-a91d-3879c07ce537",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 89
        },
        {
            "id": "77c0c11b-3a77-4ed4-81fe-f591b493c713",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 92
        },
        {
            "id": "01dc7e56-967a-46a0-a51c-f900bc55c3fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 93
        },
        {
            "id": "e35acd62-a2e1-485d-8c1c-acdbf96aaa26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 104
        },
        {
            "id": "a53bc70c-696e-44bf-b6c2-ed764c1ce162",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 105
        },
        {
            "id": "fa7e35a8-864f-4ab0-bf20-c37cf7b3dc03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 106
        },
        {
            "id": "8d44b344-0c62-4a59-ae4f-6a035ecd9958",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 107
        },
        {
            "id": "9a21ef78-5d58-4ae7-9d0e-f277bb66358b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 108
        },
        {
            "id": "c48acbdf-9f2b-47b6-a148-09dbca5e5834",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 109
        },
        {
            "id": "bfc42a42-16c5-4893-909c-f475b205aca9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 110
        },
        {
            "id": "8a3a3df1-1c7b-422b-95d3-f454f566e144",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 112
        },
        {
            "id": "11f19e21-d792-4f09-9b5c-ac249d1855c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 114
        },
        {
            "id": "7eb2d923-caa1-4e1b-a48e-2be8304691be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 117
        },
        {
            "id": "bc3bfa8f-4553-4d59-a102-e406be950538",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 118
        },
        {
            "id": "bab314f8-52fe-4c1d-8741-0419a3e2cfff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 119
        },
        {
            "id": "7dac459b-c93a-48a4-be73-dc885ac04043",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 120
        },
        {
            "id": "36ac0412-46a2-47f1-9dd8-6bb07f5ebfa4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 121
        },
        {
            "id": "47985f8d-b75e-470f-9453-d263b827ca45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 8217
        },
        {
            "id": "aed5d8d2-7d2b-4963-869a-80d9a15222c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 8221
        },
        {
            "id": "bee6604f-513b-4616-9483-0d189239a878",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 33
        },
        {
            "id": "59081868-2678-4565-81f8-c37f8995e1da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 58
        },
        {
            "id": "0add8a99-cc08-40bd-9247-df16ec81adb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 59
        },
        {
            "id": "bfcb95c5-4442-467c-93dc-3eb028975542",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 63
        },
        {
            "id": "38ceb67b-5db3-4647-8db0-0ddf8cde24c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 64
        },
        {
            "id": "8480b648-bf03-4b38-9b75-8db5b37a5e6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 66
        },
        {
            "id": "5d87c461-ed17-49ad-b572-c67558a2f73b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 68
        },
        {
            "id": "59005301-e487-4630-9943-e8ad0d3a4c22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 69
        },
        {
            "id": "7f676b7a-7373-4489-bd45-cd9db17647a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 70
        },
        {
            "id": "faf01339-d3be-46af-bd68-5f9f6feeb0c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 71
        },
        {
            "id": "7d4093df-2c33-483f-8d5a-82622833049d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 72
        },
        {
            "id": "4ceafa9a-4103-46b9-8dfc-9dddfb20bb90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 73
        },
        {
            "id": "f4696102-791e-4207-9ab9-77413493bf7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 75
        },
        {
            "id": "78d424e9-630b-4f56-8757-21fe9ad62286",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 76
        },
        {
            "id": "b19b52ed-6fd1-4498-99ca-bd1b7269ca53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 77
        },
        {
            "id": "9da8e293-4e47-4522-bcbf-01be9ef52b02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 78
        },
        {
            "id": "c01b5b60-ab43-47a6-b331-d882cd2cc30f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 80
        },
        {
            "id": "2014dcd0-e1ee-4fe7-a217-4cfd139b142c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 82
        },
        {
            "id": "4cf5967c-45cb-495c-a629-7f1951f4d464",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 85
        },
        {
            "id": "31c6da38-75b7-4d33-9c64-5b68a9ef6885",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 86
        },
        {
            "id": "41f425af-b608-44cd-b3e7-9d15223abfdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 87
        },
        {
            "id": "621f8d8b-6659-4f9e-be30-df7d428bc014",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 89
        },
        {
            "id": "5b1426f2-0ce1-4383-b5aa-6eeba15182bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 104
        },
        {
            "id": "9117b01e-5c13-4560-9c58-b1258e99bfbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 105
        },
        {
            "id": "7eb6f9dc-5872-4f94-a937-4bea9c997614",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 107
        },
        {
            "id": "a2cc32c4-00a5-4273-8068-4d78e24aa7b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 108
        },
        {
            "id": "73531282-2f32-4ddb-9da9-3c382115934d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 109
        },
        {
            "id": "6b3d1236-6c2f-4199-9574-ccfd43c29e54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 110
        },
        {
            "id": "228a71e0-5ca1-4571-8c4d-148ae0aeec5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 112
        },
        {
            "id": "c0719215-ee47-4dee-a033-de4bd18ac916",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 114
        },
        {
            "id": "5c0930dd-6e7a-4538-80db-444ff39f891d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 117
        },
        {
            "id": "aae73592-3fc8-4211-ba2e-2b812fa88b8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 44
        },
        {
            "id": "10ded3a5-063f-47cf-b84f-5c0b7d41fc77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 46
        },
        {
            "id": "1de9873a-c2ab-4311-9412-03988143fa6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 50
        },
        {
            "id": "855dba72-a07f-4633-ac15-19724465b9c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 52
        },
        {
            "id": "fa12d2a5-79f3-40f9-9229-1a08cae47bb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 64
        },
        {
            "id": "61248b1a-acda-4c1b-a127-adbec5e6ac2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 65
        },
        {
            "id": "1c2fe49a-361e-46dc-816f-2cece58d5cbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 68
        },
        {
            "id": "b4e0d449-4d31-45f6-9265-edc75e819de5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 69
        },
        {
            "id": "f7780e41-7e66-4fc3-a03d-3acacad28747",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 70
        },
        {
            "id": "abec4972-c99c-4fab-94df-cc9f20f158a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 72
        },
        {
            "id": "98fbe8e7-abc0-459b-84aa-d8b978228dd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 73
        },
        {
            "id": "e56b1dc3-3dcc-4003-8584-653ff4f09335",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 75
        },
        {
            "id": "70ce13d8-a228-4ca9-a4cc-e9bdc6a0b879",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 77
        },
        {
            "id": "fb5af80e-8b79-45f2-9732-e1734c1a4741",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 78
        },
        {
            "id": "4dafde27-b171-4566-87b8-9ae631fd5b01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 80
        },
        {
            "id": "f572b002-7df8-456a-a424-3a72227e4a5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 82
        },
        {
            "id": "5f68870d-1fa0-4451-9ef0-eb0143f8b893",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 85
        },
        {
            "id": "791883f3-1ed4-4690-946f-92ef38a918a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 86
        },
        {
            "id": "5a399d06-c9ca-4288-b6dc-cde5e79bc527",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 87
        },
        {
            "id": "614c414a-680c-4da3-aff5-9185514c84df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 88
        },
        {
            "id": "9899f427-ab35-4377-8a2d-23afd85a680b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 89
        },
        {
            "id": "16993e59-813b-4ca3-afdf-92e552805d67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 93
        },
        {
            "id": "f4de4d81-53c4-4c58-ac69-1e9d13b1d5a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 104
        },
        {
            "id": "d85cb60d-3e20-411e-a0e9-d876108a9910",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 105
        },
        {
            "id": "a46ee594-77df-435d-ac8c-2456704339f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 107
        },
        {
            "id": "3c48add0-eda6-4471-8f5a-836368e475d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 108
        },
        {
            "id": "56ba542e-7e00-4f32-95d4-4593f426bb22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 109
        },
        {
            "id": "9ef06305-bae0-4607-bf37-9b3595a782fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 110
        },
        {
            "id": "ac419d54-743f-4d81-9240-48c57a50b78a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 64
        },
        {
            "id": "5c757e38-50b9-418b-98d8-9a467a72f90d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 86
        },
        {
            "id": "d2ffb545-7101-4ccb-90d5-84db0ba1a872",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 87
        },
        {
            "id": "05ea9bf0-5aab-4308-a294-bcddd3dbfdea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 89
        },
        {
            "id": "34c21244-ba96-4110-9934-9ad02da32855",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 45
        },
        {
            "id": "a78fb7d9-4012-4b54-aff8-459577b61935",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 52
        },
        {
            "id": "04180dec-383a-4231-b982-74cac46a0309",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 64
        },
        {
            "id": "918a5e1a-3bde-47d2-a05b-b6058ade4be0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 33
        },
        {
            "id": "a76be68c-7763-4b74-a171-1a60385adfa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 34
        },
        {
            "id": "ec9dc06f-7518-47e9-8294-ba5aa8b7b6f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 39
        },
        {
            "id": "b0bc8d2e-da97-4c9e-9a54-5c271147d7fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 40
        },
        {
            "id": "af501e28-3fc7-4e57-8119-598750c4f1f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 41
        },
        {
            "id": "b57112a2-fe35-4b33-8a5f-5e7830b336ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 44
        },
        {
            "id": "a46ce01d-722f-4163-88d4-29d69b2be2ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 45
        },
        {
            "id": "181ae759-58d7-4ac8-8053-6303960f2195",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 46
        },
        {
            "id": "9e9f3ad8-fdb6-4c1a-8546-ba4a5f47fd78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 47
        },
        {
            "id": "2ea3859d-6b73-4bc2-bdd7-83d5be2a7388",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 48
        },
        {
            "id": "bdaa7d29-25d9-4587-a87e-d02f5a30f0cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 49
        },
        {
            "id": "682c9a0e-17d7-4812-8852-bb8175cf2962",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 50
        },
        {
            "id": "ad7ff263-4890-4674-b8ab-d2e02077760f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 51
        },
        {
            "id": "6ce0ebdf-8ef9-4ba4-b90c-f3a4c5e27b6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 52
        },
        {
            "id": "77bba6b7-9f4d-4546-90fe-b4f90237d3c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 53
        },
        {
            "id": "a9318d14-e75e-4117-8736-7db9a44f5132",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 54
        },
        {
            "id": "9f7199c3-fc5b-4941-abe1-666a55fe18f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 55
        },
        {
            "id": "70ac0069-2f37-4f12-a336-a83541d1ced2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 56
        },
        {
            "id": "dcd85b61-2229-4cf4-98db-8c1c572f2de4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 57
        },
        {
            "id": "f686e1d6-a2a6-4f52-aec4-0071702b7bbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 58
        },
        {
            "id": "64e44032-b741-4ab7-a29a-5c4e3338c20b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 59
        },
        {
            "id": "7ce4e923-e9d3-4c54-a074-4dd826702855",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 63
        },
        {
            "id": "91c060a9-e4ad-4ae3-83e7-00d57620e3fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 117,
            "second": 64
        },
        {
            "id": "33e21616-1f66-407d-9e9c-2b7d3d2b20b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 65
        },
        {
            "id": "1a760ec6-9110-4c35-acba-00c08726422c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 66
        },
        {
            "id": "c0c6725b-ed69-44c9-9b93-309bba9881f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 67
        },
        {
            "id": "d645d159-4241-447e-a696-0946efdba749",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 68
        },
        {
            "id": "aa204603-43bd-4f89-b5fd-0d4c25e8dbcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 69
        },
        {
            "id": "4ff25d35-4b6e-44de-a24b-29dc921d07ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 70
        },
        {
            "id": "0eb4271e-f641-4cdf-969e-6d97b95e5c71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 71
        },
        {
            "id": "2a11dfb7-ac52-40c9-9b4b-a4c0125270f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 72
        },
        {
            "id": "ea06796a-b6b7-4697-a91f-3acfbadc2472",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 73
        },
        {
            "id": "51e44cd4-7c1e-4f7c-9f2f-7387eb80dd68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 75
        },
        {
            "id": "d92a23f7-3779-418e-8a55-7ab5ca14a199",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 76
        },
        {
            "id": "32f9178c-618c-4c5d-afb0-92914392c3cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 77
        },
        {
            "id": "b2979996-8bde-4923-a5d0-0baa69cc0370",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 78
        },
        {
            "id": "08dbb32d-6bab-4020-b865-d4210c4d336d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 79
        },
        {
            "id": "0bf2fb70-cf05-4861-8a03-43cfe3404f4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 80
        },
        {
            "id": "d12644d1-cfdf-4756-82bb-b40c12c517b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 81
        },
        {
            "id": "ed1d4f9f-93c5-4885-bc72-4bf005b46d91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 82
        },
        {
            "id": "88ac3946-540f-4fe4-85d9-79e2e2c5f27d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 83
        },
        {
            "id": "f70e80b9-29a1-4de1-b403-c7031b2a9269",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 84
        },
        {
            "id": "03e19a49-fca5-47c7-a756-8ec758c045df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 85
        },
        {
            "id": "e528ec27-cd8f-40dd-a193-a8a1e4630af5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 86
        },
        {
            "id": "08c8b913-22a0-4753-bdf6-9111308c17a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 87
        },
        {
            "id": "3a847de4-2a01-4be9-aa66-2bbdd5c26746",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 88
        },
        {
            "id": "58ba8dbf-d832-49ab-a88c-dd9f4d3b055a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 117,
            "second": 89
        },
        {
            "id": "17667ad6-25a1-4a6d-8262-89c5bf975a0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 90
        },
        {
            "id": "cc4a738c-65f1-473f-8c7c-24e544777d5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 92
        },
        {
            "id": "960a19f3-26a8-4add-98d9-61973a064e24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 93
        },
        {
            "id": "105d0bb5-0fa7-478e-b471-8370d607f021",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 97
        },
        {
            "id": "667db54d-2c09-45b1-916b-ee5814f2511b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 98
        },
        {
            "id": "02d0034f-6112-48f9-8d78-9ac3cbae0c3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 99
        },
        {
            "id": "1cbedfff-71ab-4bab-9a2f-6360425bec21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 100
        },
        {
            "id": "d3179603-8ab1-4b31-b0e8-fb389831e36c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 101
        },
        {
            "id": "d4b5f6e9-48e2-4f19-bba2-758b15eadaec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 102
        },
        {
            "id": "2037fad0-a37c-4fd9-94db-15d81fa36bd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 103
        },
        {
            "id": "b4a96eda-a257-4432-864d-ecba10bd5d98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 104
        },
        {
            "id": "2e01cf70-7b1a-4de4-8110-0c906f701aac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 105
        },
        {
            "id": "c193d9ce-6895-46c3-9f2f-407ec68fcd06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 106
        },
        {
            "id": "673d2779-bd47-414c-bcc1-58aaea98cb30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 107
        },
        {
            "id": "14655a4e-6a91-45f1-a45b-291a8bda839b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 108
        },
        {
            "id": "ff6af0fd-889d-4be7-a0ab-88b24a6ebf6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 109
        },
        {
            "id": "3f6c3167-cc9a-4c1b-b15d-01802b80d42e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 110
        },
        {
            "id": "452a0e5f-b573-4eff-9b32-a273e8de8d2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 111
        },
        {
            "id": "7d70fe4a-6e85-4aac-8dbb-63b178c784db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 112
        },
        {
            "id": "ee17000a-24c9-43bb-b888-50d1e314882a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 113
        },
        {
            "id": "23fcf92f-f12b-47c4-99af-6bc484ecaf40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 114
        },
        {
            "id": "7acff15e-8d89-420e-85af-85ff6bd77028",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 115
        },
        {
            "id": "9ca7e6e1-6f19-4fe1-93b7-ae84e231c232",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 116
        },
        {
            "id": "55ec5820-543b-46e7-a272-72f0ef83b576",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 117
        },
        {
            "id": "4317bbea-af57-47b1-9cd2-e482ef465c99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 118
        },
        {
            "id": "40c7890f-6ec5-43ee-a36a-a29391ecbac0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 119
        },
        {
            "id": "70c69ec4-76c6-4cea-abf1-f399c859911b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 121
        },
        {
            "id": "f8e30480-8a30-48f0-8c28-50bfd13fce34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 122
        },
        {
            "id": "69e3aafe-d36e-4a1a-adc5-f1c2fb4f4944",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 8216
        },
        {
            "id": "5f733087-f548-40ab-a354-a54a6d4a69fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 8217
        },
        {
            "id": "23beab30-770f-48e8-a272-5800a516c76f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 8220
        },
        {
            "id": "e92eeb68-f736-42f0-b40a-1acd9d8047f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 8221
        },
        {
            "id": "6ef5e69b-92e7-492c-a897-53a922193f83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 33
        },
        {
            "id": "bb839d5f-5403-4603-a12e-c7aa950f37e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 40
        },
        {
            "id": "dd5e48c2-bf2e-4e9c-9bd1-ea9bd69af224",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 44
        },
        {
            "id": "735590c1-b099-4521-9b4d-6bae26ab47d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 45
        },
        {
            "id": "cd073779-a693-47a0-b2a1-5637129bd52d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 46
        },
        {
            "id": "b98c6207-c39f-4f8f-b9f7-86e1386f1ca5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 47
        },
        {
            "id": "eab7feb5-142f-4552-a75a-82e22b0ac7e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 48
        },
        {
            "id": "0eb9425f-cbec-4216-93dc-741e610ce2b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 50
        },
        {
            "id": "bf95a64b-a6c0-4a1f-8e34-b300a0c6e720",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 51
        },
        {
            "id": "63534bce-7924-48a5-b824-19d3466962f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 52
        },
        {
            "id": "c34a4140-d671-40ff-b464-ed96b5ef06ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 53
        },
        {
            "id": "47ddfc0e-d4da-423a-9855-bfa7bf9e342c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 54
        },
        {
            "id": "d3e3a699-f63f-43aa-ab19-49472f03195c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 55
        },
        {
            "id": "50e2eb71-02e5-4111-b452-190123ea940e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 58
        },
        {
            "id": "ac738b62-f700-43d0-b89e-e8d24a2ff03b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 59
        },
        {
            "id": "e6c9eee3-7e6b-4685-ae46-cf20d9943a2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 63
        },
        {
            "id": "807d8a1f-060a-4e3a-9d8d-48fe010ac8c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 64
        },
        {
            "id": "74601af8-e57b-4c63-bd7d-d8a106932a0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 65
        },
        {
            "id": "66fccf11-3bd9-4876-b7e1-37d7ed5dc26b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 66
        },
        {
            "id": "b9049581-6d32-4f0a-9289-57674a9a04cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 67
        },
        {
            "id": "e68d5faa-7981-4a44-93c1-81b0512ae73a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 68
        },
        {
            "id": "0a473977-bdcf-4074-b073-6b679a42ce67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 69
        },
        {
            "id": "13f2bc4f-859f-4636-9fc3-cf36d37c5cf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 70
        },
        {
            "id": "d8facbd6-4846-4aa1-9011-38e8d5654617",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 71
        },
        {
            "id": "009a118a-b16f-4c87-b5cd-837643014327",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 72
        },
        {
            "id": "06c03da9-2cec-464d-9775-9dcd4e4031ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 73
        },
        {
            "id": "fa003ea4-db03-448d-87bc-cd9a7d3cbdc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 74
        },
        {
            "id": "8af71d75-1c23-4b59-9310-73cbf4a952e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 75
        },
        {
            "id": "fc226d87-b577-43fa-8733-59f3097be700",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 76
        },
        {
            "id": "f32a2665-5b7b-4204-bf21-8905d07ed292",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 77
        },
        {
            "id": "d0e05edd-ab2f-436e-ab28-d64c0165bccf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 78
        },
        {
            "id": "2c2be87f-9a46-4592-81c4-08eb8cb2df6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 79
        },
        {
            "id": "268655c8-dd0d-469b-9802-b2d787a286c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 80
        },
        {
            "id": "2dcabf4d-760f-44ef-aa23-d951d10807e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 81
        },
        {
            "id": "a450d809-1067-43f0-b684-0f3ef9133f71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 82
        },
        {
            "id": "896f197d-d04c-46ea-b481-c6b1dd098459",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 85
        },
        {
            "id": "f8535174-0965-4ade-8ca1-2e4adc64c178",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 86
        },
        {
            "id": "30a79587-93f8-4a54-8620-a39e4a9450c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 87
        },
        {
            "id": "8f99288f-f555-46ca-861f-80d988a137bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 88
        },
        {
            "id": "252babda-e24f-421d-bce1-e23ba5ca1776",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 89
        },
        {
            "id": "4d0e32a3-beb2-4001-a281-14bf4d0235b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 93
        },
        {
            "id": "a0fbbd57-bd1f-42a1-a4a5-4ba8fe015b67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 97
        },
        {
            "id": "124760e7-51e4-4681-8dc3-b859602eadc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 99
        },
        {
            "id": "16d13e6f-8695-4302-bf5a-acb7e35ec986",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 100
        },
        {
            "id": "dcb8d55b-3740-4344-8863-cc03e3f47a67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 101
        },
        {
            "id": "a7a92d83-905b-4571-b8cb-619bc576b323",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 103
        },
        {
            "id": "805de933-b585-4aa6-a118-99ef68fe0d29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 104
        },
        {
            "id": "ae828e05-15e5-477a-ac3e-472669b48ce4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 105
        },
        {
            "id": "8aad38b0-7512-43f9-a7fa-4359a6376b19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 106
        },
        {
            "id": "52ca0f66-a8df-4a32-b34d-a2befbbe4469",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 107
        },
        {
            "id": "b08cca44-69aa-437f-80d0-6181f223ea89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 108
        },
        {
            "id": "39973e9e-8ba7-4946-8cf2-350db868933d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 109
        },
        {
            "id": "d604eea6-2a87-4a6f-8d9e-59af61ee631e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 110
        },
        {
            "id": "f9f440e9-ea23-4369-82cd-6ef61eaaf71d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 111
        },
        {
            "id": "14906044-9134-4b4c-bc0a-228884c61f9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 112
        },
        {
            "id": "ca217ece-e54b-4c1e-9bbc-48f53cc135b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 113
        },
        {
            "id": "2c74a274-62a8-4b86-9a0e-11d0d90ddd7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 114
        },
        {
            "id": "01ffc3d3-d4dc-4059-ad03-6f8ad3b874e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 115
        },
        {
            "id": "3c90b871-0e9e-4600-85a1-96c643db367c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 117
        },
        {
            "id": "0663db18-4509-48b1-88e3-600a4079d54a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 118
        },
        {
            "id": "f0ad32db-76c9-4244-bdaa-29134a1eb364",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 119
        },
        {
            "id": "66999bf9-0973-4c50-9713-5e12e3875704",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 33
        },
        {
            "id": "b696716c-70c7-4d3d-bc9c-833ff28acd56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 40
        },
        {
            "id": "c6ef17d3-99a5-4519-b142-be418b886e94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 41
        },
        {
            "id": "05c2db4e-d142-4a8c-a347-1d6a9cba7987",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 44
        },
        {
            "id": "bda8fa9c-2595-49a2-8ff8-e707b7781764",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 45
        },
        {
            "id": "0a5a87fb-281b-43c8-a5c6-7218e064ff25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 46
        },
        {
            "id": "783feda7-600a-416d-91ce-0339a8977ae0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 47
        },
        {
            "id": "dc04880f-fcb6-4072-98a9-c5a1e782bdf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 48
        },
        {
            "id": "15afdf0a-ac36-4d41-95d5-54f0163f75cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 49
        },
        {
            "id": "1f33acba-4e18-4534-a9f3-99e427261fe1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 50
        },
        {
            "id": "8f36d3fe-ada2-4007-9521-c85553ae5659",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 51
        },
        {
            "id": "b1176239-7b50-44ef-a82e-d055e87ddbf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 52
        },
        {
            "id": "f18f36f5-da9c-4339-a429-74aa1024fb3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 53
        },
        {
            "id": "fb558ec5-e6a3-4fb1-ac50-19a108cda855",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 54
        },
        {
            "id": "be034c96-d44a-40f3-af92-57a8e96ac083",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 55
        },
        {
            "id": "253f519f-0552-4631-9526-962a0108f469",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 56
        },
        {
            "id": "3f65eaa9-dc1b-4881-819a-8217bc6e9120",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 57
        },
        {
            "id": "30a9d8a2-622d-4dad-b872-90fce62fc113",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 58
        },
        {
            "id": "8d91b454-8bb1-442b-8a77-3f710657479d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 59
        },
        {
            "id": "de7e22eb-95fc-4dc1-aaa8-7e208cbb3f38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 63
        },
        {
            "id": "17f319a6-b07e-4a25-81a6-5e288348c905",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 64
        },
        {
            "id": "2828057f-e162-4934-9aa5-aa55f6137f8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 65
        },
        {
            "id": "55dd9a2e-e199-4426-9698-1107b10d1426",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 66
        },
        {
            "id": "dc9808a0-334c-4105-ba88-5af9151ef744",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 67
        },
        {
            "id": "6bf49106-da6a-4c85-a3fb-f5d9f546d045",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 68
        },
        {
            "id": "6de219de-910c-40d9-b1a8-27783445120e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 69
        },
        {
            "id": "9f5908d4-f184-49f0-a9f5-d7e797512668",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 70
        },
        {
            "id": "c1bbec94-7330-41d7-a15e-4dbbef900ef7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 71
        },
        {
            "id": "5cad4a22-909c-4c49-bfee-98c867a6c666",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 72
        },
        {
            "id": "f1b96b0f-a901-4629-9cf4-8225df24f377",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 73
        },
        {
            "id": "8f4918b7-d301-4a1d-9d0e-dfbed1b1bd5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 74
        },
        {
            "id": "862ae884-b3a9-485d-90d0-73b0a3279b79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 75
        },
        {
            "id": "eee2a973-30c4-4d80-a6f1-d38dfd5d0e4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 76
        },
        {
            "id": "c6921af9-42e5-44c2-b6ac-a55c9fc77d76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 77
        },
        {
            "id": "4906f681-dc0e-4774-9740-f8f0160779ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 78
        },
        {
            "id": "68b65c5b-d586-463c-a0fd-ecaf7c719c6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 79
        },
        {
            "id": "f020e5f4-5c93-479a-897e-3c1d25214fcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 80
        },
        {
            "id": "a0d25a56-b905-4c08-84fe-c579bcc5b15e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 81
        },
        {
            "id": "954930c1-b182-408e-9af6-035751f5aa5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 82
        },
        {
            "id": "b06edc5d-4d43-4e63-806d-2a044678e139",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 83
        },
        {
            "id": "6230ab9b-875a-4fa5-8f48-aab8fb7c4dd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 84
        },
        {
            "id": "94cfe915-c223-4430-acad-00af099594b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 85
        },
        {
            "id": "18562552-0254-423f-85de-822f27126453",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 86
        },
        {
            "id": "2925383b-bb53-478d-a72f-a062e5e859eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 87
        },
        {
            "id": "b5eb52d1-b091-4dee-824b-9ec6a505ae3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 88
        },
        {
            "id": "86187bd5-30bc-4f2c-aacb-78d67f0415fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 89
        },
        {
            "id": "213b43da-e36a-4d77-9eea-facb51a96106",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 90
        },
        {
            "id": "dab511c3-7756-4972-ba9b-b5e1fdbc3da8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 92
        },
        {
            "id": "afe72a30-ca68-46f7-9f83-79b407c9955e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 93
        },
        {
            "id": "e12c820a-13cc-46ac-94d8-03d7c5f05d33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 97
        },
        {
            "id": "e75411d4-b077-4d0d-919e-40ba738a86fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 98
        },
        {
            "id": "f2fafa8e-478c-48f6-9c57-37254caa32a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 99
        },
        {
            "id": "e62d34de-d739-44e9-9461-1d73451eb5c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 100
        },
        {
            "id": "a7dde46a-2a29-4f0f-9d1c-626833d1c903",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 101
        },
        {
            "id": "d2212142-cf10-4a6e-a024-79fbd8e73416",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 103
        },
        {
            "id": "05b5c288-fa26-4470-8679-515b9f2bada5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 104
        },
        {
            "id": "e21a505d-3256-4cd6-beec-011ccdbca054",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 105
        },
        {
            "id": "243d6c61-c6b0-4a39-a2ca-a40887baa198",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 106
        },
        {
            "id": "f8a59490-23d3-4f1e-8de7-5912058b4ea0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 107
        },
        {
            "id": "13db3893-676b-4472-87bc-b36005ed3aa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 108
        },
        {
            "id": "0bba382e-0cc2-456f-8ed9-dfff5ea597ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 109
        },
        {
            "id": "e0913673-673e-42ef-931e-5ea3438d01c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 110
        },
        {
            "id": "3e9a62a2-5674-4622-9934-0904399337d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 111
        },
        {
            "id": "3590f0c8-8b65-40b2-af80-8f59b969e3bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 112
        },
        {
            "id": "2e8a63ef-3419-4e11-aa4c-843c6112535b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 113
        },
        {
            "id": "12870e71-7ec5-4574-a131-7d2c2c215b1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 114
        },
        {
            "id": "272c1f69-26e3-460e-b393-f423cb5b7005",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 115
        },
        {
            "id": "c67a6592-8f0a-43fc-b21e-571603a697b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 117
        },
        {
            "id": "30ccf8eb-8637-4be0-aa39-9c744684b236",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 118
        },
        {
            "id": "53eeed15-7309-48b5-a229-a8e06872ac39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 119
        },
        {
            "id": "9e31c090-852d-41c2-892d-6d13eb625cef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 121
        },
        {
            "id": "d3f62907-35c9-47f8-b6bb-d3ac1ce6721e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 122
        },
        {
            "id": "c5bb4094-bf1e-4ddd-b2a6-160049aabe68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 45
        },
        {
            "id": "ceb1a6dd-941c-4750-88b4-799b71d95b36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 52
        },
        {
            "id": "9594cc30-0dd9-4768-a38c-f740c623aab9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 54
        },
        {
            "id": "18d537a4-69e6-4ae3-b8ac-662be47599c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 64
        },
        {
            "id": "8521f26d-88fe-4cf0-8c1a-79058c393127",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 67
        },
        {
            "id": "a33ad6d6-da08-4d99-b6c0-acd713179406",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 71
        },
        {
            "id": "f381d543-e205-46b6-a44e-ed8c1f1fbeba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 85
        },
        {
            "id": "3e1df362-a6ed-42c9-962e-9a75e446e69a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 86
        },
        {
            "id": "07c760f1-45c9-4ddb-97fb-d6feb6436b50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 87
        },
        {
            "id": "6094df49-58c1-406a-9ee3-7ab21b2273c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 89
        },
        {
            "id": "24e562c0-56b5-4ebc-ac6e-5a65943331b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 99
        },
        {
            "id": "619c76dd-7cfe-4451-b441-16904b4f0d5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 100
        },
        {
            "id": "1935a82e-d73c-4249-b3a0-5d6ffeb41659",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 101
        },
        {
            "id": "8aa72b5c-493e-4d26-9239-5b5351b0cad6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 111
        },
        {
            "id": "e46ce3a0-c67a-4032-adae-a2b6e7dbbe9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 113
        },
        {
            "id": "c0194da1-dc91-4420-8751-067855058a15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 117
        },
        {
            "id": "76586e3a-9ecf-4845-8e86-9f79a69ef8d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 40
        },
        {
            "id": "510b8059-696d-4ca9-b098-010b2efae28c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 44
        },
        {
            "id": "90d81ed8-19df-4eed-aeec-c8fc11e156f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 45
        },
        {
            "id": "d4e195de-4f15-4aa0-a865-94fdb15ccfec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 46
        },
        {
            "id": "89307922-0aae-4556-8b35-f3556bd80482",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 47
        },
        {
            "id": "c356dd50-4802-43a0-b60f-6a8a5a0d903b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 50
        },
        {
            "id": "f591dc50-58b4-4904-94be-6169c2f67622",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 52
        },
        {
            "id": "0239a52b-e73e-4c17-859a-8a19900e4fd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 54
        },
        {
            "id": "a52aba61-a882-46c5-a9aa-252f86af35cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 58
        },
        {
            "id": "a730849e-a68a-48fb-8ac7-5059ecac1abf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 59
        },
        {
            "id": "66dc7590-a8f2-4278-9065-eda686e746ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 64
        },
        {
            "id": "e8e4cf9b-0fb1-40ea-911f-a410ee7b47b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 65
        },
        {
            "id": "e0359dad-7406-40de-b458-68eb49fc8de4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 67
        },
        {
            "id": "5800f5fb-725d-4efd-b5e2-9fe9726b383f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 68
        },
        {
            "id": "a9a66295-1333-4af5-8d6e-c42125c233aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 69
        },
        {
            "id": "741236c0-e9a8-4467-82d3-00f5f56de2e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 70
        },
        {
            "id": "ff4e3d9e-2aad-47b2-9cf5-4e940622bfe4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 71
        },
        {
            "id": "9b52d28a-b77c-45ee-a84e-abf7eea7b9d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 72
        },
        {
            "id": "14974a0e-80d9-41ea-904c-b296cbc7c32e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 73
        },
        {
            "id": "4f23f56f-b027-48ce-8689-5888fe9d79c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 74
        },
        {
            "id": "112fb630-d4c4-4b1d-a360-fb65f2b237a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 75
        },
        {
            "id": "8d8fe48c-d8fe-4398-abe1-85699c37626d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 77
        },
        {
            "id": "4a033071-77ad-4267-8fca-bcce13218dbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 78
        },
        {
            "id": "acaf2877-8cc4-4229-a4f6-55051d3b8dad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 80
        },
        {
            "id": "9d7f8fdc-1646-4289-8b77-e5060e0013fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 81
        },
        {
            "id": "04b0f9b4-5b62-4992-a6a8-416f916f2394",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 82
        },
        {
            "id": "32e6c43e-74f9-431d-bcd4-0b0a500b7ca6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 85
        },
        {
            "id": "6354c44b-5929-40d7-8031-8421483c6b1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 86
        },
        {
            "id": "910bf296-a586-44dc-b40e-2d4d896fa2d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 87
        },
        {
            "id": "377cb740-fc66-4b4f-a0cb-beeec5953b59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 88
        },
        {
            "id": "fc637948-e514-4271-acd7-bcb7e3393627",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 89
        },
        {
            "id": "366d16ba-4e92-4f50-8b3b-2b2baa73d923",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 93
        },
        {
            "id": "3c6b33f6-196f-4ede-a13d-753259a1cfd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 97
        },
        {
            "id": "3ebf53f7-3fe9-4242-95d7-876b876d33e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 99
        },
        {
            "id": "61fc25c3-7132-47b1-bf7a-2cdbe448721a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 100
        },
        {
            "id": "5899c888-c94d-4e8c-88ea-5d8bb3f4a35f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 101
        },
        {
            "id": "76d7a429-6267-49ae-af32-9a7ed7f78feb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 103
        },
        {
            "id": "30ce2a20-a220-407b-ab71-d55c714d0d99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 104
        },
        {
            "id": "ba01ec83-929d-40be-9077-f873f5b3031b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 105
        },
        {
            "id": "8720749c-7133-429f-ae5e-df3bfc5e3db3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 106
        },
        {
            "id": "9449fb87-ca97-43d9-acfb-435cd77ecf54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 107
        },
        {
            "id": "f566f7d2-542b-4d83-86ec-f4e2e9377a79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 108
        },
        {
            "id": "78480015-5c28-472f-878c-d62e780a011d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 109
        },
        {
            "id": "b04a364e-ee5c-4698-92a1-e5cfb8d7a34c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 110
        },
        {
            "id": "688830d9-00a8-490d-93da-55a71f327eb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 111
        },
        {
            "id": "69805ab9-0d41-43ef-8d85-06e60c2e3d7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 112
        },
        {
            "id": "b6d48ee2-32d4-4a83-a0b8-5cebbaca64a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 113
        },
        {
            "id": "e4324dc5-540e-4d96-8f35-bdd3646afecc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 114
        },
        {
            "id": "23f36076-98f2-42f3-a878-eac767cdce98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 117
        },
        {
            "id": "d7ebc5f4-072b-442b-b594-c422e5555c2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 45
        },
        {
            "id": "42a33838-056b-407d-a588-d0d0026c571d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 64
        },
        {
            "id": "fe42acf0-0d98-4b35-a15d-7898764f56ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 68
        },
        {
            "id": "0e01a53e-c625-4bc3-ae82-73b076f9ac31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 69
        },
        {
            "id": "74685ad9-8459-4d61-89f4-538679fa18c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 70
        },
        {
            "id": "54c57e01-6ae4-41a3-a724-38afe796b0d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 71
        },
        {
            "id": "dd285322-9904-47bb-a8b4-7ca0f5617dc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 72
        },
        {
            "id": "0fe32b6b-44c2-47d6-b008-0d074d8bcca0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 73
        },
        {
            "id": "2f05354e-78e6-4009-aac5-d29d49387f67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 75
        },
        {
            "id": "114e630f-5778-48e7-a91b-d7d9996bdf91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 77
        },
        {
            "id": "b0ca5aec-314c-45ee-ae09-23caadd293f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 78
        },
        {
            "id": "be3b0a5a-74ae-4563-8978-6fe9b899fc34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 80
        },
        {
            "id": "7ccfb0b5-c7d3-435d-9fe2-41fc3ee823af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 82
        },
        {
            "id": "54f4d1a0-9afd-41b9-9311-e1878bd084a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 85
        },
        {
            "id": "86e105f5-7f75-4a96-b19c-c21b11681412",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 86
        },
        {
            "id": "d41b3abf-5ac4-4117-b4b0-42b5e2b5ef07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 87
        },
        {
            "id": "1a2f934d-f139-473f-9380-993bee3512d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 89
        },
        {
            "id": "23493199-bdec-4592-b6c1-510df88fc0a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 93
        },
        {
            "id": "7f18dac2-b589-4989-b436-f1682bd8aae2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 104
        },
        {
            "id": "9298805a-1919-4599-9326-98bdda6cf1b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 105
        },
        {
            "id": "a6bb6971-ecc6-42fb-bff3-11ca7149a9ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 106
        },
        {
            "id": "b585143f-1660-42f9-bf3c-3239187d167f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 107
        },
        {
            "id": "4a98f491-7491-4188-b669-3744ea1fd3c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 108
        },
        {
            "id": "33719bc7-74ec-4517-9f5c-b7d97938b332",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 109
        },
        {
            "id": "6c4ff52e-e739-4b4f-be5f-6225939bc588",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 110
        },
        {
            "id": "157bf15a-65ea-4188-9169-8b1565d82f17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 112
        },
        {
            "id": "b12bbf0c-342e-4a2a-978d-d00f4800e741",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 114
        },
        {
            "id": "9a624a8d-a03d-438c-87d9-7288037ce71d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 117
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 14,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}