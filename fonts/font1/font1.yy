{
    "id": "f88e84d4-57e3-48d0-a4a5-37fcabbf3760",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font1",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Felix Titling",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "0f3f9fea-db61-4b1e-b32e-f13115bf60a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 54,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 279,
                "y": 170
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "7459f6ea-8e0b-4c80-9706-4f5ad0d9a98e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 54,
                "offset": 2,
                "shift": 11,
                "w": 6,
                "x": 351,
                "y": 170
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "585671d4-8ce1-412a-b2fc-d02708c58ce5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 54,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 203,
                "y": 170
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "7a6ca80b-2da3-44f7-ac4b-e28cc5ac3f46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 54,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 425,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "b02a54cf-2969-4a76-b131-f32220c5550c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 54,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 215,
                "y": 114
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "9ab0c550-3cfe-4394-9e91-5ba9806d239c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 54,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 173,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "d26e7f3c-8e3b-4d50-a6f9-4feb2d84d7c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 54,
                "offset": 2,
                "shift": 29,
                "w": 28,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "12b38a8f-2683-4e09-85ce-79b388c77828",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 54,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 367,
                "y": 170
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "ca6405c6-11d0-4a0b-8879-a5de0ed12672",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 54,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 243,
                "y": 170
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "5734b0c7-8afd-4173-8a00-5dabe4d09953",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 54,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 267,
                "y": 170
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "04df52dd-9997-4152-89e3-9a24cab7a5b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 54,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 401,
                "y": 58
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "0abaa7a6-f986-455b-82b6-8dff0473a1a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 54,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 236,
                "y": 114
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "151f5db2-d4a1-462d-b2b6-c705b4b950f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 54,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 325,
                "y": 170
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "b1f44d03-95dd-4477-8de7-80ee68e8ae1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 54,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 217,
                "y": 170
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "b8b51c88-e827-48b1-b75b-6871a316754f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 54,
                "offset": 2,
                "shift": 11,
                "w": 6,
                "x": 359,
                "y": 170
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "3abb80df-715b-4a93-a8fd-db89d028049c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 54,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 111,
                "y": 170
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "427b0440-18de-4e1b-8c86-f918f4cb66e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 54,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 194,
                "y": 114
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "e0a5deed-d8e7-4891-a028-5e1160a55fc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 54,
                "offset": 3,
                "shift": 23,
                "w": 11,
                "x": 230,
                "y": 170
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "5ac5c914-09d7-4b2f-83ce-4152f69df35a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 54,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 24,
                "y": 114
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "b404526e-61a3-4260-84a7-a2d16b1f600c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 54,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 2,
                "y": 114
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "92256f66-7c22-48e8-80ac-72f6a53387ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 54,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 2,
                "y": 58
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "d211bb4b-94d4-4097-bd90-bb5a3afd061a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 54,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 257,
                "y": 114
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "9e65e41f-df48-45f8-9a06-9d6a25848482",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 54,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 341,
                "y": 114
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "02981b01-46f7-4291-bcb6-c366c4f09d18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 54,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 46,
                "y": 114
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "0cf52b7a-4401-4752-b7ef-b40125c0c937",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 54,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 362,
                "y": 114
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "d7c3eac1-0d89-4eae-9ddf-3c836707dbee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 54,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 383,
                "y": 114
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "7c3e0f5e-24d8-4148-99d4-7d31425b4c3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 54,
                "offset": 2,
                "shift": 11,
                "w": 6,
                "x": 343,
                "y": 170
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "a24a277e-0b18-4aaf-a6db-d7ae29d65d40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 54,
                "offset": 1,
                "shift": 11,
                "w": 7,
                "x": 334,
                "y": 170
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "216f9c10-bc8a-49e7-a139-3fbd3612f930",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 54,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 320,
                "y": 114
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "60dda7ae-739c-48d6-abe2-40531092dbbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 54,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 299,
                "y": 114
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "0706b350-f66a-4919-a5dc-b6733e43f116",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 54,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 278,
                "y": 114
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "1a5077cf-d8e6-4f93-96a6-a02ce88b49d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 54,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 482,
                "y": 114
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "3f4fc4ea-57d1-4298-8a55-2fb0ca9bbf0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 54,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "544e546f-78ec-4c38-bddb-bb220451b7b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 54,
                "offset": 0,
                "shift": 25,
                "w": 26,
                "x": 292,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "63a58f00-d6eb-4f99-b6de-20597fe6fa21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 54,
                "offset": 3,
                "shift": 26,
                "w": 21,
                "x": 174,
                "y": 58
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "6b79177b-4ecd-452f-9eeb-dbf600900aa0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 54,
                "offset": 2,
                "shift": 25,
                "w": 23,
                "x": 451,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "b9e5b71c-61f8-47ed-b9f5-0e0cf713217c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 54,
                "offset": 3,
                "shift": 29,
                "w": 24,
                "x": 347,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "29c84426-a364-4239-92f1-5298714d19a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 54,
                "offset": 3,
                "shift": 22,
                "w": 17,
                "x": 21,
                "y": 170
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "c597bdbb-8e2e-4e39-9be0-3b27a4a97a2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 54,
                "offset": 3,
                "shift": 21,
                "w": 17,
                "x": 2,
                "y": 170
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "b82ae84d-5545-4a0c-9204-cfac29621852",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 54,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 320,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "f7948c09-030e-4a46-abb9-3f74ccddbb25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 54,
                "offset": 3,
                "shift": 30,
                "w": 23,
                "x": 27,
                "y": 58
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "84460b6a-810f-4280-8446-871d56ad8841",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 54,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 374,
                "y": 170
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "89e87f86-9ec6-4042-a006-d7a0044bf04f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 54,
                "offset": -4,
                "shift": 11,
                "w": 12,
                "x": 189,
                "y": 170
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "f0551d9c-c36d-4f63-b24e-bcf7b549e904",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 54,
                "offset": 3,
                "shift": 25,
                "w": 22,
                "x": 126,
                "y": 58
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "d51c9ae5-3920-43e2-bd06-59015fc03736",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 54,
                "offset": 3,
                "shift": 21,
                "w": 17,
                "x": 463,
                "y": 114
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "72ec8fe1-bdba-4a7a-8a8f-7c7822e9e1eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 54,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 141,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "97bb820f-7ae5-4ffd-b0d4-34995f264cc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 54,
                "offset": 3,
                "shift": 30,
                "w": 24,
                "x": 399,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "a00a01a6-30d4-42a9-a1f1-c972498bf365",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 54,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "2b708b5c-b129-491a-aef5-837758b6bee2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 54,
                "offset": 3,
                "shift": 24,
                "w": 20,
                "x": 335,
                "y": 58
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "90786bb1-7666-4c19-a5d2-96fc17850258",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 54,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 263,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "07eecc40-09e4-46d3-b8c1-e68153ec7dd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 54,
                "offset": 3,
                "shift": 25,
                "w": 22,
                "x": 150,
                "y": 58
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "fcd5708f-1f9f-45ba-aa9b-f7e1fa101dc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 54,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 89,
                "y": 114
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "8fba1fba-017b-43b6-91a2-e177ec350658",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 54,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 102,
                "y": 58
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "087c5aca-5cd4-4d23-b0d0-9e8a3000c4d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 54,
                "offset": 3,
                "shift": 29,
                "w": 23,
                "x": 77,
                "y": 58
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "8c8578a5-9f57-44ea-b4ef-529f4a01ac78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 54,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 373,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "4d7f004e-f2db-4436-b7fb-3f9df81d72f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 54,
                "offset": 0,
                "shift": 37,
                "w": 37,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "c4dcf589-5e1f-4f06-90e7-5840ad097da4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 54,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 476,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "26263fb7-a909-44b9-a73e-50d86b6bae45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 54,
                "offset": 0,
                "shift": 22,
                "w": 23,
                "x": 52,
                "y": 58
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "7ba6c587-81b2-41c9-9b2f-ca86b2fd757d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 54,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 197,
                "y": 58
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "9dc07e57-b9f7-42ef-b820-be20b27e8d24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 54,
                "offset": 3,
                "shift": 13,
                "w": 10,
                "x": 255,
                "y": 170
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "cd6efadb-4253-4057-adcd-a37d85b22947",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 54,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 94,
                "y": 170
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "4b973a82-67bf-4b4a-aeb4-98e9fa496eee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 54,
                "offset": 1,
                "shift": 13,
                "w": 9,
                "x": 314,
                "y": 170
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "0c2b2c9c-aab6-4415-ab23-72ea13a483a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 54,
                "offset": 0,
                "shift": 22,
                "w": 21,
                "x": 312,
                "y": 58
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "0fb5fcc0-0512-4d6d-acd7-2ba2304abd31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 54,
                "offset": -1,
                "shift": 18,
                "w": 20,
                "x": 423,
                "y": 58
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "9d7d440b-75ac-4137-9c6f-cfdf01870320",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 54,
                "offset": 7,
                "shift": 23,
                "w": 9,
                "x": 303,
                "y": 170
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "2603b10a-7159-4382-9759-47ec0316049f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 54,
                "offset": 1,
                "shift": 22,
                "w": 18,
                "x": 424,
                "y": 114
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "e6698d8a-0364-4b0b-b755-bddd565ad97f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 54,
                "offset": 3,
                "shift": 25,
                "w": 20,
                "x": 467,
                "y": 58
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "fa70722c-aa7b-47e4-8f68-9c24ad2208a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 54,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 40,
                "y": 170
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "b6b225df-1211-4774-8905-d162439cf47a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 54,
                "offset": 2,
                "shift": 25,
                "w": 20,
                "x": 489,
                "y": 58
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "83d3f6cf-269c-47fd-89fa-6f0fd398b0ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 54,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 110,
                "y": 114
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "1cbf7bdd-08f9-4b2f-bc6e-95f0bb9c9d02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 54,
                "offset": 0,
                "shift": 14,
                "w": 16,
                "x": 76,
                "y": 170
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "7ad94765-de5f-428d-a6a3-9d04d84ec795",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 54,
                "offset": 0,
                "shift": 22,
                "w": 21,
                "x": 289,
                "y": 58
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "f82004b8-4875-414c-ad31-22aff74ba2ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 54,
                "offset": 3,
                "shift": 25,
                "w": 19,
                "x": 131,
                "y": 114
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "8d27b327-abea-43c7-9d0d-eea848370ce3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 54,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 381,
                "y": 170
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "6df3c2de-781c-4a91-b404-cced88e8f301",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 54,
                "offset": -3,
                "shift": 10,
                "w": 10,
                "x": 291,
                "y": 170
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "7fc0ed45-6955-41d3-9f33-b3b73f81ad06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 54,
                "offset": 3,
                "shift": 21,
                "w": 18,
                "x": 404,
                "y": 114
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "9256fdf9-20bf-4415-8ac2-1dc6fe0c392a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 54,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 387,
                "y": 170
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "36c8dec1-4075-474e-850f-eed5afd5423a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 54,
                "offset": 3,
                "shift": 37,
                "w": 31,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "1c6bd580-73d6-4725-97b2-ff9e2d7167a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 54,
                "offset": 3,
                "shift": 25,
                "w": 19,
                "x": 68,
                "y": 114
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "58b8c420-6388-42a5-889d-c19fa40342ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 54,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 357,
                "y": 58
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "0d3e2a77-2855-45b0-94e8-8d6d412333f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 54,
                "offset": 3,
                "shift": 25,
                "w": 20,
                "x": 445,
                "y": 58
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "91503664-d815-40e2-9c12-7d907cc39419",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 54,
                "offset": 2,
                "shift": 25,
                "w": 20,
                "x": 379,
                "y": 58
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "77dc7c73-25f2-4bd2-bcdc-162b7a836ca6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 54,
                "offset": 3,
                "shift": 16,
                "w": 13,
                "x": 174,
                "y": 170
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "667b27ab-9ea2-46b7-8162-9624947e627d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 54,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 58,
                "y": 170
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "bbf01efa-d3f8-4d54-8e3f-931c16cccf89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 54,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 128,
                "y": 170
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "c219987f-17d2-486c-9760-424c89f153b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 54,
                "offset": 3,
                "shift": 25,
                "w": 19,
                "x": 152,
                "y": 114
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "e4c20c90-3e39-4f83-aa83-d7ad35e71e75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 54,
                "offset": 0,
                "shift": 20,
                "w": 21,
                "x": 220,
                "y": 58
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "68216a43-aade-4a81-8f43-e4bf4edc279f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 54,
                "offset": 0,
                "shift": 31,
                "w": 31,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "34f6c8cc-4e8c-4cab-8603-b5f80456017b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 54,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 243,
                "y": 58
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "1d3a42b1-8b77-42ed-a8cd-73a129061421",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 54,
                "offset": 0,
                "shift": 20,
                "w": 21,
                "x": 266,
                "y": 58
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "6a126f75-1ae4-4442-b2e6-c8347a7bfb17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 54,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 444,
                "y": 114
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "35c03d10-d3e2-45c4-854a-69e441184fd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 54,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 144,
                "y": 170
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "2c54ba89-6ba8-4ae0-a3ac-6002f91c3030",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 54,
                "offset": 9,
                "shift": 22,
                "w": 4,
                "x": 393,
                "y": 170
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "d872ec4c-a437-4c4d-95b2-342b6aa88627",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 54,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 159,
                "y": 170
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "d32934cf-fe82-4b2a-b0e2-b44e68db6b12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 54,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 173,
                "y": 114
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "93ad3c34-85c1-4a6e-89a7-6a8d9870a85e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 65
        },
        {
            "id": "26c8adac-a7ee-4ab8-8bcf-2c398f8802ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 84
        },
        {
            "id": "56639367-bc67-41f1-931a-e5e5dcacba6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 86
        },
        {
            "id": "1737421d-934d-499b-99d1-9d993cff16bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 87
        },
        {
            "id": "84d04d40-1d81-4bf7-83a3-e3e3268be39a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 97
        },
        {
            "id": "7b5349d9-e213-47e6-bbd5-eeedba479112",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 99
        },
        {
            "id": "26166952-f354-4f3e-82bb-b39a0ff8be46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 100
        },
        {
            "id": "95d94988-ad5f-4904-81bd-2e4e0a6d168b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 101
        },
        {
            "id": "dc081bf7-fbdf-4095-963b-0b269b071d2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 103
        },
        {
            "id": "d53df365-2f22-4655-a100-f155916dc572",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 109
        },
        {
            "id": "3cb6714a-0466-4fa0-ae62-35f495ecd9ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 110
        },
        {
            "id": "14c63e8b-47b2-4cb3-a6de-bcb62ce9af2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 111
        },
        {
            "id": "1369f6a0-9b54-442a-b35c-aac18881f99b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 112
        },
        {
            "id": "06b96bf2-bf7d-444d-9d87-a762d480803a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 113
        },
        {
            "id": "b49c21d2-d9ac-4e13-b3df-84ab1a2cbea8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 114
        },
        {
            "id": "5f6f8f0d-1c79-455d-927f-1e6b7a288470",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 115
        },
        {
            "id": "dc113952-7fcf-4cc9-a563-f7afffcf5ca3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 117
        },
        {
            "id": "07402068-24a8-413b-9aaf-46e06ffac179",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 192
        },
        {
            "id": "c2308312-a851-4284-9676-63b6df738022",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 193
        },
        {
            "id": "eb5a8186-d044-4090-afc1-db0491066d0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 194
        },
        {
            "id": "16dad788-d014-4c69-88c0-8eea09637073",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 195
        },
        {
            "id": "a4e85389-13fb-4774-9e30-8eee90980d6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 196
        },
        {
            "id": "c2929e01-1c51-4995-9a15-f6549a0b00de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 197
        },
        {
            "id": "d37bccc7-dc1f-4ee0-9aa7-49c23a6da00d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 224
        },
        {
            "id": "6447897d-821b-4d78-a917-80220044c2a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 225
        },
        {
            "id": "1854c8a6-dff5-4f5a-9588-cea8f0d31641",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 226
        },
        {
            "id": "488d06cb-9973-48c1-b7d8-6ae9a5a10264",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 227
        },
        {
            "id": "be5f2f66-6366-4e88-b0ed-c807de975ab8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 228
        },
        {
            "id": "c7e9b26d-5f6d-4e94-9e8b-58f06fc3656e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 229
        },
        {
            "id": "603fcab9-0971-4d66-9de3-8bdfea5341b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 230
        },
        {
            "id": "f027f34f-a716-4efe-9b98-67d5742b8462",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 231
        },
        {
            "id": "08d828f2-98fe-456e-9037-f8357db7c316",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 232
        },
        {
            "id": "c2b0b571-f03c-4c4d-81d4-09f5d87b90a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 233
        },
        {
            "id": "6bd88d60-b08d-4110-92ac-cf35908a32d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 234
        },
        {
            "id": "379dc503-ed4f-4289-8536-815cad206ea9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 235
        },
        {
            "id": "75c747b5-c1f6-400e-9163-8e0d70d4fb89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 242
        },
        {
            "id": "b6d244d9-7f2f-4d68-aa4a-76296578f4eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 243
        },
        {
            "id": "641ea132-d124-433e-ab81-65cea3567de3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 244
        },
        {
            "id": "c137c9fd-d7d1-4a78-8934-7ecc4423d131",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 245
        },
        {
            "id": "26d4258d-9057-4afd-b555-f6bbc4fc47bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 246
        },
        {
            "id": "6f5de406-a233-41b2-818c-21ab1fac47eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 248
        },
        {
            "id": "49dfea56-ffe5-49b1-b0fd-58049600c74d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 249
        },
        {
            "id": "61a980c2-d187-4bf5-9bf5-0d6527511b22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 250
        },
        {
            "id": "c6d2cc4c-485b-46fa-8bee-bf5a83bf3d8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 251
        },
        {
            "id": "e36f7253-7625-4732-9fc4-cc111003b71c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 252
        },
        {
            "id": "7a81d329-146f-44cc-adf4-b2df5479c8d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 256
        },
        {
            "id": "8af767b7-fb8f-4bb4-a14c-1784b2e3db2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 257
        },
        {
            "id": "c08cbff2-937c-49ba-91bd-9120b4d4d29e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 258
        },
        {
            "id": "3f79ed8e-245f-4668-aefe-5067a5b96a66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 259
        },
        {
            "id": "e9ccd1b3-907c-4a81-8e7f-cdff09be6982",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 260
        },
        {
            "id": "dd073e09-5c69-4f7d-859d-68dbb987bab3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 261
        },
        {
            "id": "67366824-2847-4c9c-bae1-4e8ba6b24374",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 263
        },
        {
            "id": "44d248e8-4fc1-49c7-8e5a-5961eb30419c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 265
        },
        {
            "id": "00dfe8f9-3775-4375-a357-096be2d4b2c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 267
        },
        {
            "id": "ca8aab46-f201-4ad7-afe9-987d74ba19df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 269
        },
        {
            "id": "e7f57bf9-1e3a-44f5-8fd6-b028f00a0c3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 271
        },
        {
            "id": "48a8f0ea-b1a0-44c5-8b3b-92f57ce0d372",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 273
        },
        {
            "id": "1648f86f-18e3-4950-9739-a863403ee502",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 275
        },
        {
            "id": "d303e405-55e9-4eb2-be8f-15210f110ab5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 277
        },
        {
            "id": "098ccfaf-c98e-4900-88f6-ba3fb3c41258",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 279
        },
        {
            "id": "7ae17d84-89be-4eda-a2c2-772302cca505",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 281
        },
        {
            "id": "30b14706-d37e-4cc0-b53c-45563cae85eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 283
        },
        {
            "id": "d83cee84-8d5a-4a3a-8d78-3587c86a16c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 285
        },
        {
            "id": "883a08c8-04f3-43f0-a3e6-f4c44d158575",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 287
        },
        {
            "id": "c1cd0ccc-b4d8-4a19-b08f-9c1ea36a1855",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 289
        },
        {
            "id": "cc657d17-5738-4574-851b-dabce0b6aa48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 291
        },
        {
            "id": "1d152708-67ba-4776-b549-9520ca9e3234",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 312
        },
        {
            "id": "02fdbd0b-ec95-473b-a553-2e5dfe0a0f01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 324
        },
        {
            "id": "2e23b4a5-569e-44de-aca9-ba4c1499f783",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 326
        },
        {
            "id": "9f0a9b46-8476-4ee0-98a1-40744c8cd512",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 331
        },
        {
            "id": "cdbc7ac2-2fb8-47b8-9ce8-08312b2a4c71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 333
        },
        {
            "id": "81675f01-ff5b-4d0e-b849-f1110e8d5844",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 335
        },
        {
            "id": "7262d6de-089c-4bfe-9510-2bd2e775d3ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 337
        },
        {
            "id": "335e08ce-2b67-4bfe-950b-4e1d28855e1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 339
        },
        {
            "id": "22d76a26-de11-4bb6-8ba0-545ae2cf035f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 341
        },
        {
            "id": "b00e5e72-d227-49d8-8a15-319eb3e09bf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 343
        },
        {
            "id": "47f49ded-dbfe-479d-8913-7c48e8e7bd0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 347
        },
        {
            "id": "c7919a60-df41-4d23-acd1-1a15d5fba60f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 351
        },
        {
            "id": "6e386a7b-34a6-472f-894d-8eabbdd41136",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 354
        },
        {
            "id": "08c2ad04-1ec8-4af3-b24e-ad8688d005f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 356
        },
        {
            "id": "eee8973c-7199-479d-a1af-4e0bfe8b19a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 361
        },
        {
            "id": "04402b78-18bf-4349-aed9-df7aba3270d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 363
        },
        {
            "id": "83e8cd5e-ea5b-4d61-b5b2-f03a6f06c9ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 365
        },
        {
            "id": "e9db3f25-9506-4ecb-9680-392ebbdde645",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 367
        },
        {
            "id": "f5a72c1e-45a0-4dd7-824e-c671aa62f0f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 369
        },
        {
            "id": "e3cf8dc5-b8c1-4c21-b648-952397da92d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 371
        },
        {
            "id": "fb3ab8d9-4ee6-470a-862b-ff02b129c2a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 372
        },
        {
            "id": "39d80a59-188b-4bab-914b-4004f19d0d7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 417
        },
        {
            "id": "c8560d44-bbcf-4911-8b32-5a1eddb1b200",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 432
        },
        {
            "id": "67ef216a-9c04-40bf-a61b-f1238843257c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 506
        },
        {
            "id": "642664dd-a461-42f7-9417-29daa9748cae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 507
        },
        {
            "id": "65dc8d55-0d92-42ca-bae9-38eec94e3e4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 509
        },
        {
            "id": "f448c8ef-2dda-4fb1-9aa9-414d628359fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 511
        },
        {
            "id": "c0c08256-a4ea-45ef-8dfa-7a6f22b69e16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 537
        },
        {
            "id": "07da716a-93bb-47df-b91e-2c5496b88e02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 538
        },
        {
            "id": "47daf49b-54ba-4967-9880-f8d2a65c2ead",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 902
        },
        {
            "id": "4fbf5bb7-8bb9-4b53-9ff3-f31bb399c052",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 913
        },
        {
            "id": "3353270a-9465-4f5d-b660-aeed4163d92e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 916
        },
        {
            "id": "26ca8c2e-ef28-4d13-853f-8c0b0958320c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 923
        },
        {
            "id": "07b1f94b-a6f9-4fdb-8f80-13aada58b1a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 940
        },
        {
            "id": "34d09138-0b0f-4b1e-9583-6e10ee5fad17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 941
        },
        {
            "id": "af050e5f-fbfc-4d58-a1ed-d3483074a38e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 942
        },
        {
            "id": "bf415661-961c-455f-88f6-f7a8118d4d4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 945
        },
        {
            "id": "663659bd-792f-4a7d-a258-7631ea48db45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 948
        },
        {
            "id": "1d6b7272-b033-442e-bcba-c5c7a1ec9936",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 949
        },
        {
            "id": "dbd97d6e-d1c6-4488-9fe5-04550b23254d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 950
        },
        {
            "id": "d779cbe3-0d63-4a05-a0c9-d32a69641121",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 951
        },
        {
            "id": "41f16d47-8a7c-438d-be6a-085d7b8d44a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 954
        },
        {
            "id": "367d3ad5-f769-4f9a-a7d7-1190f29c5fd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 956
        },
        {
            "id": "719e8e25-62ec-4c0f-8883-bd17abd106a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 959
        },
        {
            "id": "23b031f3-4da7-4f5f-9c0d-0acbaa57f76f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 961
        },
        {
            "id": "41f6149b-170d-4d63-96c9-0e31475a9460",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 962
        },
        {
            "id": "212e97f1-e51e-4693-968b-2737f2e755b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 963
        },
        {
            "id": "44b9074b-414d-40d2-8b8f-664ca0bbc7e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 966
        },
        {
            "id": "f37635ac-3f02-461a-94a7-699fe99bf6ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 972
        },
        {
            "id": "e74dbc20-e165-4d36-b134-44da2f194f82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1033
        },
        {
            "id": "e0b6eeef-6de7-4c4d-bcfa-8903ba216157",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1040
        },
        {
            "id": "e532505e-552a-4893-a4bb-4a2f9a3705fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1044
        },
        {
            "id": "96f5b6c0-1c1f-4ee0-abd6-6dd92669d56b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1051
        },
        {
            "id": "dcc734a2-00dd-46b1-82a0-95496236b4ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1072
        },
        {
            "id": "8d5f8b92-7275-4c43-a5dd-7e01c9714dbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1076
        },
        {
            "id": "fb12c8e6-5dea-4d8d-979e-eed971c7706f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1077
        },
        {
            "id": "f9d8e71f-952c-45f2-9122-8d11b47e9b47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1083
        },
        {
            "id": "109032ce-c596-4027-b0b3-2611327c6783",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1086
        },
        {
            "id": "c2f83606-0153-4278-923b-c3f24308e7e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1089
        },
        {
            "id": "a0230b1a-4725-4779-939e-f51dffd771d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1092
        },
        {
            "id": "95bdc5df-71cf-4085-acd7-e3fb669a49f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1104
        },
        {
            "id": "bf99d984-0e0c-47df-9bc4-ce1d3ca17835",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1105
        },
        {
            "id": "54191778-c147-4fd3-90f6-17b889496de9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1108
        },
        {
            "id": "f588e862-4aa8-4d18-885d-1afa72ebbc30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1109
        },
        {
            "id": "b18b3580-a5a3-41f8-95a9-bb3f25698efd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1113
        },
        {
            "id": "53085128-4777-42a8-8030-cc9255708f5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1126
        },
        {
            "id": "7e400287-4be8-44f0-b28a-5aba18e5171e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1127
        },
        {
            "id": "fe1e717c-1caf-49f1-ba19-ffa5568aeff3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1139
        },
        {
            "id": "c6d1cd1f-6e20-415f-95bd-f50bd594eb0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1145
        },
        {
            "id": "ab4fdc00-60d2-4d71-8998-1c940150de10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1147
        },
        {
            "id": "b3ad4ee5-8043-4754-ab64-754cb061855c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1149
        },
        {
            "id": "467f938d-7f7b-4445-a197-002c06b1d2d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1153
        },
        {
            "id": "e979464f-38a0-42d3-b26d-3bcf5d92750d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1193
        },
        {
            "id": "de8426d9-39a6-408e-b1c6-381703130cb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1195
        },
        {
            "id": "77b974d1-0848-4869-ba00-5e54b2c06183",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1221
        },
        {
            "id": "313154f3-1ed5-42f8-8e0a-f805afe1c193",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1222
        },
        {
            "id": "2b3f2bd8-d780-4a45-b0bb-134ab1f75ec5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1232
        },
        {
            "id": "033c2eff-6aab-46f0-87ad-f3ef5c4f6a44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1233
        },
        {
            "id": "93f44544-1f67-47d1-a11e-f85c354d9a8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1234
        },
        {
            "id": "9b5de764-a923-4690-8e3a-d98f6950e030",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1235
        },
        {
            "id": "ba1213ec-620f-4dc8-ac90-ffc3befee6f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1236
        },
        {
            "id": "7f0b0fb8-e78e-4cd9-9072-4dc0df762516",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1237
        },
        {
            "id": "291362a5-61b9-4980-b2f5-9e9a19defd7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1239
        },
        {
            "id": "fbe45f88-1262-4da0-b9ed-634a545d875c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1241
        },
        {
            "id": "d169e1e6-b82d-4bf2-9198-e24029053580",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1243
        },
        {
            "id": "c87c3433-ed51-49d8-8a99-1a520ee2e403",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1255
        },
        {
            "id": "f518bf56-f21c-44ea-9d33-9960947f80a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1257
        },
        {
            "id": "1301667b-355c-49dc-ac81-45b42f9bb304",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1259
        },
        {
            "id": "892b3cf3-2861-44ee-b8d4-8a5366759aef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1280
        },
        {
            "id": "ec411c83-c24f-4a41-af95-7ccef3e27425",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1281
        },
        {
            "id": "da2e8b90-7d8e-4afd-a7e2-77bd53ef36c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1282
        },
        {
            "id": "77cb92da-8398-452c-9596-d00c2321ac86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1283
        },
        {
            "id": "8e7643f0-0535-48a4-a806-4067ce15943e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1288
        },
        {
            "id": "cf7c7ec3-a2f9-49cd-9b25-1637613d5dce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1289
        },
        {
            "id": "366f6fe8-3552-47aa-9eb4-4c4735bf2ee2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1293
        },
        {
            "id": "56fd15e5-8bc3-46b8-91e4-6971db45e79b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1297
        },
        {
            "id": "9582b1c2-ca64-4600-9827-e4f7bcc25fa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1298
        },
        {
            "id": "a204b9f8-5963-4e4c-a5f2-9cb89ea6150a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1299
        },
        {
            "id": "01d87b68-d511-41d7-b73f-069559bd1a0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7680
        },
        {
            "id": "2545028c-1f0d-4d38-bf74-b6c59af87e5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7681
        },
        {
            "id": "e086001c-0dc9-4eb9-8846-0aec3a3b7bdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7743
        },
        {
            "id": "fa74f056-1d84-4042-b7da-3b4bd566cf19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 7808
        },
        {
            "id": "daf02e7a-4ee8-4dfe-97f0-8163ca30259e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 7810
        },
        {
            "id": "56b7a572-4248-48e7-a732-d1be88b57fcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 7812
        },
        {
            "id": "41c60c19-88ab-4b20-8b21-34e5881a39f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7840
        },
        {
            "id": "d3da1675-8488-4e26-afb1-455f1e99efdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7841
        },
        {
            "id": "3a925602-92ad-43e5-9cca-2966dec3477f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7842
        },
        {
            "id": "f8fedc27-f6e9-47b0-8db1-e6627080020b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7843
        },
        {
            "id": "c7866259-49f8-4efb-88c5-b4aca24dd701",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7844
        },
        {
            "id": "b77b21c6-dcf3-4d31-bd12-fa807705c9c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7845
        },
        {
            "id": "946d1b07-db60-4c47-9b2c-b3cfce6b1110",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7846
        },
        {
            "id": "f385b112-24dc-4ea1-a439-37a83348d01d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7848
        },
        {
            "id": "f9df2cca-fba5-428d-8fa1-b1cb0db26526",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7849
        },
        {
            "id": "90facd2e-b779-4679-9028-1ace3fe7ca02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7850
        },
        {
            "id": "2a02b6b3-5c55-4d47-8895-1b6ca5ce4e9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7851
        },
        {
            "id": "975e47aa-ddb6-44c5-8b97-534448645f13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7852
        },
        {
            "id": "f417455f-7c48-41f0-9a14-34805a53ad6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7853
        },
        {
            "id": "efae5830-5ea0-44fb-91ea-f7165f46ddfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7854
        },
        {
            "id": "00e49813-f84c-43a3-9730-f60d83812d24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7855
        },
        {
            "id": "84ab1228-798a-4578-bec8-2485d3ca6ae5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7856
        },
        {
            "id": "72a72477-a079-47c9-a95a-3f4eb6cc1b48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7857
        },
        {
            "id": "07f17549-5102-44ce-9d3b-2d99a75ea45b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7858
        },
        {
            "id": "05a56b12-09b4-4f26-824b-8ac4ee8a395d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7859
        },
        {
            "id": "ce3cdb2d-1b7d-4534-86b1-841937e11cfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7860
        },
        {
            "id": "e892d9d7-3644-4bfa-9548-3294db4906b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7861
        },
        {
            "id": "4aea52e7-35e8-4620-af80-99f69809cf8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7862
        },
        {
            "id": "c579330e-24f7-4f3f-9ef7-a6b688ace6d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7863
        },
        {
            "id": "61216b05-64d5-4cee-a6d6-5df77839ff99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7865
        },
        {
            "id": "1c4430a4-f26a-4b6b-a6ba-d1e725830703",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7867
        },
        {
            "id": "87e115f2-9985-458f-98f2-8684dbe4d01e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7869
        },
        {
            "id": "727fa0f2-4757-4063-9f83-4f9859de650a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7871
        },
        {
            "id": "b76be31e-dc51-40fe-b486-6fc902c21aa8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7875
        },
        {
            "id": "e2a76ede-c4a7-4502-8cd3-467ab00f8f80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7877
        },
        {
            "id": "61c98163-ed0e-4ebd-be4e-4e5548b208e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7879
        },
        {
            "id": "9a924734-ffe6-4c9a-8674-cbb9548293b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7885
        },
        {
            "id": "a7c55e3c-50bd-44e5-9d1f-b44a1b6e8467",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7887
        },
        {
            "id": "5b4dd858-b50f-402b-af14-ded48bac96cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7889
        },
        {
            "id": "f5c49d7b-7cb4-482c-a50f-2ddb2ee1d5bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7893
        },
        {
            "id": "0b04823d-7482-422a-a9ed-32e22c29ef73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7895
        },
        {
            "id": "092ff424-2eb3-4856-b807-d3f0c794bf54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7897
        },
        {
            "id": "864e3ddd-8c19-4445-b063-e94d69e36e95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7899
        },
        {
            "id": "28a7a68e-25ff-4b1b-8126-6d9e2e39f445",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7901
        },
        {
            "id": "64338c82-d9a2-478e-800e-8fd2140deea7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7903
        },
        {
            "id": "44f8b4ce-972b-46fa-aeda-9b51dccff599",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7905
        },
        {
            "id": "65b08837-0e67-4696-8532-7344b141e7da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7907
        },
        {
            "id": "34e24ae2-b9ee-40fe-8010-571371f24e1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7909
        },
        {
            "id": "2eb1b24c-cb80-4082-957a-3e03ab28dbe9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7911
        },
        {
            "id": "2085e9b3-6d74-423a-864e-a96474660891",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7913
        },
        {
            "id": "f40e856c-b14c-46ba-8688-500a87a61279",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7915
        },
        {
            "id": "545c3d9b-4f27-4456-adea-381061866ca6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7917
        },
        {
            "id": "4d3026e6-d6ae-4d8b-bf78-e8f7cdcbde92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7919
        },
        {
            "id": "64e42c1c-d7b4-4763-860a-3bd25c450216",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7921
        },
        {
            "id": "fdf05351-2a20-4f30-95c5-39ed6946461e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 65
        },
        {
            "id": "32c4dc84-e2d8-4e8b-8c53-82cbf11d2ab7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 84
        },
        {
            "id": "9f349915-37ae-43ba-ac7e-b21bc7090aa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 86
        },
        {
            "id": "f6102641-e07b-4dbd-b59d-f4e48526e189",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 87
        },
        {
            "id": "c3ff0959-cda4-4e14-a2a8-b2edf585ba7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 97
        },
        {
            "id": "7e79ecfb-272d-49f9-ab78-dedd5c55c619",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 99
        },
        {
            "id": "2c25789f-7639-4548-ba2b-1287069206b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 100
        },
        {
            "id": "bba113b1-2e7c-4404-9765-925932ffa057",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 101
        },
        {
            "id": "e34723ee-a971-4afb-b639-1d7415b28d75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 103
        },
        {
            "id": "fcc3d37f-5b43-4d62-a13d-c0560cc414b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 109
        },
        {
            "id": "1aab81f1-8b5e-4f15-9d9d-dad0c0f5dfd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 110
        },
        {
            "id": "12d9b5dd-5917-47e0-a3aa-7e23247f9e1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 111
        },
        {
            "id": "812a84d4-359a-427f-830d-208d003634a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 112
        },
        {
            "id": "098c4f4d-1804-4f34-a583-201f1ba44816",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 113
        },
        {
            "id": "2336f99e-f6ef-42f2-b3a5-dc038101e09e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 114
        },
        {
            "id": "b8955d6a-0ec6-4f02-bf32-111ba14a5404",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 115
        },
        {
            "id": "08b501b9-f235-4d68-9b55-a18b7b95ab88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 117
        },
        {
            "id": "dee6997f-d1bc-416a-a618-3525db5b0f28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 192
        },
        {
            "id": "77eabc56-85ce-414c-bbfb-37aaa4178005",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 193
        },
        {
            "id": "d9ace398-0a26-4772-bdde-0c29292da12a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 194
        },
        {
            "id": "c0b4e4ac-2c0d-4277-bc0a-50e5edac77c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 195
        },
        {
            "id": "75dea7b0-6790-4450-a3a4-7d8d1ff01013",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 196
        },
        {
            "id": "e8d4816b-9bef-40b5-b3cb-c0314bb0589e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 197
        },
        {
            "id": "1f1a7696-c89c-4cda-9e8e-99d738eead2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 224
        },
        {
            "id": "20f4f8f9-330b-4dc0-95d5-d801f3bc72ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 225
        },
        {
            "id": "79debd21-5b1b-4dfc-825a-bccf351013a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 226
        },
        {
            "id": "51bf72d7-1238-450d-860a-6a15e15d6b2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 227
        },
        {
            "id": "bf31fb20-54d8-463a-a5b3-0e23c792ad78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 228
        },
        {
            "id": "470d10b6-6e17-4495-af0b-373203f3db12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 229
        },
        {
            "id": "b354968d-b7d6-4499-bc57-7103961d53a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 230
        },
        {
            "id": "21c0b078-cde5-4653-b1f5-e6c439595a6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 231
        },
        {
            "id": "f0d3419f-e08b-4e6d-b6a8-e8aa3b31a1da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 232
        },
        {
            "id": "aa24134d-9ac2-41a8-bdf8-de81582b90e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 233
        },
        {
            "id": "fa9868cf-7efa-46d8-9726-b68f4933f2fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 234
        },
        {
            "id": "6d8eca71-0fe0-4d8a-88ec-71a300426656",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 235
        },
        {
            "id": "66f33e90-6625-43b2-85f0-53c379eeedbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 242
        },
        {
            "id": "138459b5-55ac-42e3-aa64-38b1c0ef3046",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 243
        },
        {
            "id": "c255d822-5145-4c84-9d55-58663909fffc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 244
        },
        {
            "id": "9774f246-02de-4cd1-ac0b-8bc318d7c145",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 245
        },
        {
            "id": "cd471f85-bfba-4bfb-a41a-129185ea8e6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 246
        },
        {
            "id": "9dafcf19-34fb-4cd0-8288-039a48e5d81e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 248
        },
        {
            "id": "1d723a89-d24c-4184-ba1a-f48dcb0afb40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 249
        },
        {
            "id": "d16a77d1-6025-4f17-8903-8df9215fa3d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 250
        },
        {
            "id": "d6ba36c3-b347-4652-9fc5-c0592f1450b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 251
        },
        {
            "id": "4a155d56-6885-437c-96fe-d9787a0870b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 252
        },
        {
            "id": "7b6dedd6-7905-4429-b329-142765cddec2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 256
        },
        {
            "id": "f67fe423-1565-4bb1-a4c2-d82b6bf7d8e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 257
        },
        {
            "id": "03599394-586d-46eb-8676-f605a613896e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 258
        },
        {
            "id": "7e97b3ac-927b-4184-8a3d-db8f04d0b6e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 259
        },
        {
            "id": "0aff0f3b-122b-49ba-9cbf-05b8d8adb11b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 260
        },
        {
            "id": "54648ca7-a526-463a-980c-cf88b1c3e5b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 261
        },
        {
            "id": "2827a234-9a61-48c2-b528-fe23dada875e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 263
        },
        {
            "id": "51915318-ec75-44c9-8554-2ca825599031",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 265
        },
        {
            "id": "4435740c-cfc6-4fd4-bd46-6888f7e797f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 267
        },
        {
            "id": "7832aca3-a1b4-437c-bdbd-75d8f6e076bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 269
        },
        {
            "id": "75fb14a9-2e7d-471a-9bca-b783bd7b6c91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 271
        },
        {
            "id": "803eb55a-bdeb-41b0-b16f-abbd6aee581b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 273
        },
        {
            "id": "94a32735-ef86-41cf-9d2b-b089c9f13b54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 275
        },
        {
            "id": "e5d0dcde-a992-49a6-a2c2-2529b372585c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 277
        },
        {
            "id": "d197ab35-c25b-422d-9010-1f7bd792305e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 279
        },
        {
            "id": "0ea7258f-114c-481f-8df9-13c5b4ff0461",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 281
        },
        {
            "id": "a4de0592-5366-46b7-b839-87437b5a8b88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 283
        },
        {
            "id": "6d83c994-67b3-4e5c-8941-de20e6d94640",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 285
        },
        {
            "id": "686d6826-74ee-43b4-9c06-b01168a46ee1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 287
        },
        {
            "id": "183eb342-2e6c-4cef-a653-58ef0cac77e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 289
        },
        {
            "id": "16c58534-030f-453f-aa77-d9ceaeccb6fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 291
        },
        {
            "id": "61ee4d43-d1fc-4b93-9581-9d432fb796c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 312
        },
        {
            "id": "59f12351-d292-4be3-99cb-acdbb0104348",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 324
        },
        {
            "id": "ce26ace1-7f8d-4ecd-afdf-d4a74f28b003",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 326
        },
        {
            "id": "69041d88-acd6-4e0e-a39b-a88f52939bd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 331
        },
        {
            "id": "38692e4e-9ca8-4c23-bab5-d1c4d8a3135c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 333
        },
        {
            "id": "709448d4-7247-405a-9828-99efd3f0de17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 335
        },
        {
            "id": "c7e8da5d-f81f-4d48-ab4c-87c7998f34ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 337
        },
        {
            "id": "cbef276b-5878-4e33-8f5e-aadb7b17d8e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 339
        },
        {
            "id": "2717da8c-3baa-4bc9-91da-7f4f75fde10f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 341
        },
        {
            "id": "c4fcd5a3-2324-4b4e-aa0e-47a2065cc52d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 343
        },
        {
            "id": "32780e54-bc36-46bd-b6ae-d0fa951b7492",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 347
        },
        {
            "id": "fc224716-6d62-4bbe-ac1c-d6c3d8e8bc00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 351
        },
        {
            "id": "5d3456be-a30f-4fec-8df3-f139c2b91283",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 354
        },
        {
            "id": "0dc78f12-3441-4f51-bfe4-80a3aa8cbe86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 356
        },
        {
            "id": "5588f702-fe34-4a16-94aa-20cd237c48e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 361
        },
        {
            "id": "a205c3dc-1a37-4324-a80f-4a683a1ac959",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 363
        },
        {
            "id": "15e791a1-066e-4869-8c12-aaebab779413",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 365
        },
        {
            "id": "c9dc242f-cbdf-433a-9e58-de760e3628b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 367
        },
        {
            "id": "1b732760-1b93-449b-b9b1-e09c94d2d4b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 369
        },
        {
            "id": "ffb32d6e-5d9d-4d9a-8654-941a1f6a1eba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 371
        },
        {
            "id": "e13183a4-6493-43ec-b1eb-353499bcac9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 372
        },
        {
            "id": "8c43a058-70a3-4d22-bf39-5a8f06585f61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 417
        },
        {
            "id": "87cf8878-1d64-4786-a3db-77eb2c6843b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 432
        },
        {
            "id": "ef300c44-c59a-4266-ad6e-fd6be2ddbd1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 506
        },
        {
            "id": "37fdc8aa-4ca5-40ed-bab5-89e1fe413fbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 507
        },
        {
            "id": "a60d7717-34c6-4dd3-bf30-9e9c599cb7e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 509
        },
        {
            "id": "3a1c16a4-f37c-4ffa-ab4b-cb5973173647",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 511
        },
        {
            "id": "8e77b9c7-3b2a-49e3-bbb1-ef813be68c7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 537
        },
        {
            "id": "5db62aa7-9a2b-4618-8ecc-0704b12f42c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 538
        },
        {
            "id": "d2472a54-6fec-4324-92cb-bacdcdbb4640",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 902
        },
        {
            "id": "73424e01-f351-4395-bffa-85f4c49953d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 913
        },
        {
            "id": "1b529e6e-41d5-49ce-ac0b-5ec49031cd29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 916
        },
        {
            "id": "60a91113-cfe8-41f9-90c1-8ee484cd2524",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 923
        },
        {
            "id": "03a28833-f9a5-4003-9167-478e84e91f5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 940
        },
        {
            "id": "e4dfc842-ad5b-4804-857c-7da86e5b6852",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 941
        },
        {
            "id": "c7c9b8f2-6a55-4643-ab8c-5131624d0e38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 942
        },
        {
            "id": "d4a3239e-ce02-4e8a-bd3c-a55a06419d5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 945
        },
        {
            "id": "c6921f4c-5345-45f4-b5b4-53ecaaaa8705",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 948
        },
        {
            "id": "126805db-1338-47e0-abb5-1ef6f2a1398d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 949
        },
        {
            "id": "c6e723ea-e278-484a-b1f3-526d4eed668a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 950
        },
        {
            "id": "6ee6b231-9299-430b-94ff-e409e463a9f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 951
        },
        {
            "id": "6c8f9e8f-4582-44f1-b17a-7b2dd6e9e052",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 954
        },
        {
            "id": "291eb948-d70a-46df-87f1-a49c74d7f51e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 956
        },
        {
            "id": "a68317d1-2e00-401a-aed8-b182ee9dadf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 959
        },
        {
            "id": "2c302720-05b9-4a90-8590-e6416d50115a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 961
        },
        {
            "id": "0554762f-aaf7-4ddc-8f57-fc73cd99c48b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 962
        },
        {
            "id": "8004e047-c259-4b39-a6b6-cef50c577aef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 963
        },
        {
            "id": "b3b68453-6ee6-4d5a-a923-4973c93c1c0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 966
        },
        {
            "id": "e6168c24-3d34-47dd-a145-0a678f1de22c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 972
        },
        {
            "id": "1e7cb930-9eb2-434b-bc99-e079405fab7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1033
        },
        {
            "id": "82cb77b2-5624-42d3-8a82-d913ff91ddd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1040
        },
        {
            "id": "abd515fe-7dcf-4511-b6a4-4b4360d30720",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1044
        },
        {
            "id": "3977399d-82a8-4e3b-8149-fabe93215931",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1051
        },
        {
            "id": "34a32002-37f8-40a1-86e9-cd9a4ff2ef05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1072
        },
        {
            "id": "b8f5d411-8aec-45c5-a0c1-be4245fb7256",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1076
        },
        {
            "id": "eb78add9-96f3-4fcd-acbe-fd8321dc3a50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1077
        },
        {
            "id": "658a4b01-f643-42f5-aa44-4ec592f3f3a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1083
        },
        {
            "id": "38972740-4388-42f7-b57e-961a8423f5ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1086
        },
        {
            "id": "21ce5324-27dc-45e0-a2df-846f9e4c85c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1089
        },
        {
            "id": "9e94f4cf-b3f7-4fee-907e-b54896e4043c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1092
        },
        {
            "id": "e6c263d9-b46a-4dab-b909-0d2f18516227",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1104
        },
        {
            "id": "74bd2bc8-9169-4a3d-a0f1-fbccd0d94355",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1105
        },
        {
            "id": "5b700634-0ba5-428c-aabc-6461d3f58b8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1108
        },
        {
            "id": "45e45ab1-fe9c-440b-89a9-588dc0f39f1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1109
        },
        {
            "id": "9eb9ec02-fcbe-492f-bae8-508c2b61bf73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1113
        },
        {
            "id": "c989fc93-e467-4a7e-b049-1b303346b56f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1126
        },
        {
            "id": "305bc06a-9cac-41f0-81bb-6a58acf7bcd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1127
        },
        {
            "id": "08f5059c-c8cb-4e7f-b7e4-71f8d998a58d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1139
        },
        {
            "id": "886f1e48-521a-49b2-967c-0a30510cf82c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1145
        },
        {
            "id": "5fd52c98-a4f6-41b3-aeed-7bd445833654",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1147
        },
        {
            "id": "82594e61-33a8-4361-ac24-ef4ef288cec5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1149
        },
        {
            "id": "9502f003-eac1-4e7b-901b-4cf0e767b7fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1153
        },
        {
            "id": "83e566e9-a702-40bb-9219-6ff43841f2ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1193
        },
        {
            "id": "cc7832ce-7270-48c4-9cdb-f68c983b84c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1195
        },
        {
            "id": "8d2671bc-b3ce-45aa-9f39-a1dbe49cc8f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1221
        },
        {
            "id": "3421daaa-8e55-42a4-a268-d19bc9390b82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1222
        },
        {
            "id": "5753d473-7894-4c25-a4d7-491ece203586",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1232
        },
        {
            "id": "357c3162-61ee-4616-bc08-20d03282ae11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1233
        },
        {
            "id": "48870e60-7508-47b9-89dc-ad4e6f602f55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1234
        },
        {
            "id": "5203205e-74db-4622-bf92-32ec74dd5780",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1235
        },
        {
            "id": "ddc4f2b8-87d9-4ed3-8ad2-d64908556b80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1236
        },
        {
            "id": "eba8436a-69a1-44be-b7fa-1ad25f9ce093",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1237
        },
        {
            "id": "2d45dc3f-bda0-497a-9c76-1dc5d78dd3bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1239
        },
        {
            "id": "42974034-2da5-4b8c-b26a-e8a917e97753",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1241
        },
        {
            "id": "f4f37865-b78a-4e79-880c-cc2a6dce5768",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1243
        },
        {
            "id": "4bf0fe11-857c-4035-ad57-8723566a16b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1255
        },
        {
            "id": "4c810115-308d-421d-86fa-565bfd361a49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1257
        },
        {
            "id": "eb38dac6-40f0-4c1b-9f24-b84ffd023182",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1259
        },
        {
            "id": "522ca5e8-4b23-4b2b-aff9-56ee779425f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1280
        },
        {
            "id": "8ed44bbc-1406-42ec-8f83-9b7f9e6c47af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1281
        },
        {
            "id": "d92beafb-5438-4117-b244-dcbef346d2e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1282
        },
        {
            "id": "9f1833ed-1db7-43f6-8e53-d14928a05b77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1283
        },
        {
            "id": "fa59e82f-7164-41d1-9e42-d9d95fa33f8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1288
        },
        {
            "id": "6189f537-0ddc-4df9-b945-c882d568e599",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1289
        },
        {
            "id": "e464a0da-f884-46a2-b549-b8dd19b7f84e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1293
        },
        {
            "id": "10560459-9f7a-4127-8590-16064b99d8a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1297
        },
        {
            "id": "08babde1-fa3a-49b1-9858-d34a5e70ca88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1298
        },
        {
            "id": "13871ee3-027a-4cd5-9016-5612368f2e79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1299
        },
        {
            "id": "c101bfc9-e9fc-4517-a786-97c66db7e291",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7680
        },
        {
            "id": "5d9fba47-69e1-44e2-8250-4fc14a88b318",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7681
        },
        {
            "id": "1bec9960-d255-4fef-8315-9eb02a3f9033",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7743
        },
        {
            "id": "9d9115f5-9864-4bc5-9deb-a1b8ad2f56f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 7808
        },
        {
            "id": "75bd745a-99fc-4bd0-b9e9-d34b3e468e34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 7810
        },
        {
            "id": "370a6a46-ebbc-4601-a625-d09053592d37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 7812
        },
        {
            "id": "7b2e597b-ac28-4cef-befc-c8ca6256ef98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7840
        },
        {
            "id": "3421c51f-5b34-4a57-90ff-51c2f073e0cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7841
        },
        {
            "id": "8b64266b-c101-45a7-bff7-3f5a0bcba4c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7842
        },
        {
            "id": "4ded7554-0b30-42d6-ad50-5345177afee5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7843
        },
        {
            "id": "2faebe60-61ed-4574-abe1-7228c84ecb2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7844
        },
        {
            "id": "e5bd3217-4ac8-4fd2-8af4-89a6507b3073",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7845
        },
        {
            "id": "19ca3e8d-04b2-4165-b3ad-d4896bade659",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7846
        },
        {
            "id": "4adbcdc2-4ff0-493f-8e85-53be3d1b061f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7848
        },
        {
            "id": "91b4f911-cc01-4413-8359-3e9adf60eb14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7849
        },
        {
            "id": "7e5cdf67-ad38-491d-adb7-0ab3382810ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7850
        },
        {
            "id": "94d3569c-9884-4675-889b-bea5985258e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7851
        },
        {
            "id": "ed2d2085-24d8-4831-9192-ab93b48187ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7852
        },
        {
            "id": "b8854ad3-de55-454d-8345-f3c8d6410d1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7853
        },
        {
            "id": "9d370138-36f9-4604-8980-9013a9108f82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7854
        },
        {
            "id": "39b4b3f1-43cf-4198-8790-7d2c2eed9350",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7855
        },
        {
            "id": "d74d0f1a-7038-474d-9891-5e98b88b5aa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7856
        },
        {
            "id": "5efa6422-0a89-4e7f-8698-67bbcfd167da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7857
        },
        {
            "id": "1698fc7e-d37e-448d-ac97-0b42307bd11e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7858
        },
        {
            "id": "2f8e42d0-596d-4507-a5e8-9610ddd51488",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7859
        },
        {
            "id": "2a014203-1aa1-4d7e-8b90-63095214fb07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7860
        },
        {
            "id": "0eeec530-8e71-40c5-a7c7-48f9891a7151",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7861
        },
        {
            "id": "4ca905a1-62b1-42f6-b154-d296be631758",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7862
        },
        {
            "id": "af3916a7-190e-406c-a9e9-05afac8b20fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7863
        },
        {
            "id": "fc064f42-a4b9-40bb-a83c-909475a9e8c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7865
        },
        {
            "id": "54022b2a-07c8-4ced-88e4-d75e82ddc800",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7867
        },
        {
            "id": "a342af3f-447f-43f9-bf48-1063ce52a448",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7869
        },
        {
            "id": "59187f2b-58a2-49b5-a40a-a1835078897d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7871
        },
        {
            "id": "94a06e7a-c177-4797-aa69-a9bddb8970ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7875
        },
        {
            "id": "5bb1aa9f-648f-40b6-b414-bb2cad63e5f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7877
        },
        {
            "id": "3a844e5b-5592-4098-a1d6-3cdee244e209",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7879
        },
        {
            "id": "da0df92d-b864-4445-b286-bfe6435741f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7885
        },
        {
            "id": "4f928e6c-c1d0-4e11-8da9-b9595a9b54fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7887
        },
        {
            "id": "5990ef8e-d7f3-473f-bf22-c3f494372c40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7889
        },
        {
            "id": "c41b0d55-ddab-47d3-bf23-c47e76d5df3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7893
        },
        {
            "id": "7c9350b0-0758-4335-9224-4870bf39aef3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7895
        },
        {
            "id": "d8f7b102-b2f9-49a4-a603-020502e0dafc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7897
        },
        {
            "id": "7155a70a-17bc-4b93-a1b4-bd7503ec6008",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7899
        },
        {
            "id": "b81f5857-08a7-4152-bb8a-9db141f5f575",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7901
        },
        {
            "id": "41b8c3bd-6d7f-4f52-87ef-c9d74b95de39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7903
        },
        {
            "id": "dbb2d0a3-0838-4812-afdb-6b269bceec8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7905
        },
        {
            "id": "645ee8f2-c4b9-453a-a216-2de53bf0d911",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7907
        },
        {
            "id": "0d68980b-d102-4ebd-8527-2d700cca7608",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7909
        },
        {
            "id": "0915f433-2e87-4e61-9f18-6992a08cb4eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7911
        },
        {
            "id": "cc6dd596-7acf-4358-b825-77ecc83c975b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7913
        },
        {
            "id": "31fbf809-384e-4bff-8b8e-a34971d29e1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7915
        },
        {
            "id": "7217537a-bd17-4eb5-9852-4578470106f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7917
        },
        {
            "id": "a67b0b9e-7c50-4122-b94e-627d3c78c166",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7919
        },
        {
            "id": "3f1c6062-7216-43c7-a61f-130afa36bcf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7921
        },
        {
            "id": "b902cec3-b777-4d5c-9e9b-183edec2e3a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 40,
            "second": 74
        },
        {
            "id": "90917b89-906e-4d42-9a3d-b598fbe1fd2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 67
        },
        {
            "id": "edc555d6-6821-4bec-8b5a-2f5d19ba6095",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 71
        },
        {
            "id": "999d3de3-d70d-453a-8792-330dbb1301a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 79
        },
        {
            "id": "661df66b-23ea-4ed9-a6f9-71a0d3d1263e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 81
        },
        {
            "id": "9490985f-37da-425b-86ce-c2c8a16992e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 84
        },
        {
            "id": "3e34149b-4386-447b-a1f8-ab3d01f48b8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 85
        },
        {
            "id": "6fc80dce-01da-458e-a60d-9f280c1a4dc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 86
        },
        {
            "id": "5399cf18-491f-4c0c-8a74-67611dc3c709",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 87
        },
        {
            "id": "a6c4b375-d9d9-4cdd-b912-b8fcd90c0287",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 89
        },
        {
            "id": "876388e1-eb3e-46ba-ac2e-0553f7fa579a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 199
        },
        {
            "id": "1546d2cd-aa08-489e-823e-d3e9dc3e8589",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 210
        },
        {
            "id": "c8d58346-580d-4f18-84b6-e74fd6f8d06b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 211
        },
        {
            "id": "5d7f32a7-6519-4230-9b7e-0791f2f0284c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 212
        },
        {
            "id": "1c8776bf-293e-4486-8189-ddd3c697b791",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 213
        },
        {
            "id": "950483bb-a9e3-447a-ab62-fc782c078144",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 214
        },
        {
            "id": "0f9300e0-65e6-40ca-a680-91d4cdcde8fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 216
        },
        {
            "id": "dfbfb9a4-340d-4807-8807-5a69796720fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 217
        },
        {
            "id": "1e470eee-162c-4f49-8f15-aea6efd6be11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 218
        },
        {
            "id": "559c369b-2c67-44c2-ac5f-fa7f1b717e57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 219
        },
        {
            "id": "7a7ee958-8e65-44aa-999e-f70d7045e1f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 220
        },
        {
            "id": "1b8e7bfc-21a8-419f-98bc-35a1036a400d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 221
        },
        {
            "id": "94579dc2-cca7-443a-aaf2-789dfb0761f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 262
        },
        {
            "id": "809cc25a-7aec-4140-8b9f-5b6c8e06e656",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 264
        },
        {
            "id": "62ccf425-d511-4109-aefe-4d93c7574a3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 266
        },
        {
            "id": "1f3bae2e-b80b-4c6a-843d-0614a570c435",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 268
        },
        {
            "id": "0c3d1ef5-abd5-424c-8e74-f40add0d2731",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 284
        },
        {
            "id": "dfd2f246-64b2-4384-bc08-53d99ec80510",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 286
        },
        {
            "id": "21dfbfee-a465-4118-8072-f9457eb90852",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 288
        },
        {
            "id": "c38c156d-99e2-46a7-a596-3c6368492ca8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 290
        },
        {
            "id": "5c735932-39b0-46fb-a82b-bc6cd6395ccc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 332
        },
        {
            "id": "b95e5a50-d7ff-4c7a-a024-72c955784106",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 334
        },
        {
            "id": "6d879ce5-47c7-4272-ad72-705405a161b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 336
        },
        {
            "id": "961efb8d-39f1-4f60-9133-950ce30f8a74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 338
        },
        {
            "id": "65df8f74-e72e-4959-8410-e0ef44da55c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 354
        },
        {
            "id": "eb1e8a9a-afaa-4ca4-831e-d846c0da4f5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 356
        },
        {
            "id": "8f49bff1-36dd-41c6-bb09-bd882df03ae2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 360
        },
        {
            "id": "c06c9528-5700-46ba-b997-1c179282c689",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 362
        },
        {
            "id": "fd485444-964f-4d70-8671-0a7a07f492d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 364
        },
        {
            "id": "e5001493-5791-4733-ace5-db90586a84ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 366
        },
        {
            "id": "c45cb6fe-b55a-4b9f-8465-aedec40cb7da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 368
        },
        {
            "id": "c40f8c4d-4a0d-4e4d-8acb-9dfd9ca993a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 370
        },
        {
            "id": "0456a732-cd10-4169-a17e-a2d6cb066382",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 372
        },
        {
            "id": "48111583-be9b-4abd-ab3b-a15d6227a2e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 374
        },
        {
            "id": "17118f41-37c7-48cf-a939-087f7e1a3d42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 376
        },
        {
            "id": "fe516772-3022-4d40-b092-9e45904197f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 416
        },
        {
            "id": "d9aec7fc-118b-48a2-9984-ded30485847f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 431
        },
        {
            "id": "141e9fc3-6f5d-4a9f-92bd-17efabfc187f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 510
        },
        {
            "id": "29cfc012-6f5c-493c-acb2-9194830c7777",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 538
        },
        {
            "id": "7b3263eb-52be-4c53-b88e-539c5299d057",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 920
        },
        {
            "id": "76ad397b-9a83-40d2-be54-01c84e0c65bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 927
        },
        {
            "id": "ceaa7500-afa1-4bc8-83bc-09d601ec0de0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 932
        },
        {
            "id": "6f0b0321-bd36-4c14-a7c3-0150e22903e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 933
        },
        {
            "id": "55348db1-93e7-448d-9a0d-b2751599872a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 934
        },
        {
            "id": "8bc85566-054a-493d-a885-0c83d6b7d089",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 936
        },
        {
            "id": "44a2a017-3003-4ef0-94c5-aeaebbec20bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 939
        },
        {
            "id": "80206715-c475-4e9b-b623-08c65482207b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 952
        },
        {
            "id": "151d5bec-d0d2-403e-a49d-2775d20264e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 978
        },
        {
            "id": "2bb49be0-b4c3-498c-b22c-dc09a7f292ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1026
        },
        {
            "id": "822e0546-86cd-43ec-bbd6-c4c5b24a20c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1028
        },
        {
            "id": "f037a3fe-34cb-4880-8d1d-69f5c437ed8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1035
        },
        {
            "id": "f62f2290-8b9f-4301-9809-eedb2c24f4e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1054
        },
        {
            "id": "d9d5d9be-4e18-40a9-a3ae-77712635f4ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1057
        },
        {
            "id": "3a122248-fec7-416d-adb1-188a0c05f109",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1058
        },
        {
            "id": "6daf11db-1e39-4e9e-94f0-bf017f145b07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1060
        },
        {
            "id": "4df1686b-9389-4d92-8829-9b37f2a715d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1063
        },
        {
            "id": "b38cb3c0-e0e1-4ed1-bfc5-67700b18c5dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1066
        },
        {
            "id": "1f180bf5-bde3-465d-a123-560940e018b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1090
        },
        {
            "id": "78da992c-cef6-4ebe-a7da-32e34c1b9a2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1095
        },
        {
            "id": "5fe45e93-8d1f-438e-9461-e1bc69360422",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1098
        },
        {
            "id": "45c84690-a556-4cad-86da-084b7d0f73c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1120
        },
        {
            "id": "1c5b326e-a94b-48a9-b2c7-7d83fa6293e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1136
        },
        {
            "id": "63b2e327-c79c-4ddc-b566-62058d9c0450",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1138
        },
        {
            "id": "98221200-2c98-4806-b6f9-d4ecff4ac21f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1140
        },
        {
            "id": "43ba52d4-aa97-4010-adf3-da6934849cbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1142
        },
        {
            "id": "76e84e85-0136-428f-a331-322d6c054e35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1144
        },
        {
            "id": "006150be-bd05-4c77-a8c8-75d4e757d766",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1146
        },
        {
            "id": "d9ba4ebc-c227-4765-bf32-c68bba2f7177",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1148
        },
        {
            "id": "13a21cc0-909e-48b2-be98-04ba36da8d82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1150
        },
        {
            "id": "69535e97-5f9e-4bea-ae95-cc82bff34e82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1152
        },
        {
            "id": "0891263c-d298-4601-889b-5044c3fc9ddf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1184
        },
        {
            "id": "dfdfc0c4-aa4f-48d1-a64b-977979b6d66e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1185
        },
        {
            "id": "4726ef33-eafc-4d17-b125-4505ff7df59e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1192
        },
        {
            "id": "3199597e-03d9-4e88-a4ae-e0324e212356",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1194
        },
        {
            "id": "a7814df6-0728-436c-a834-868ac96323f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1196
        },
        {
            "id": "20b7ea59-1a3b-46d8-939e-ea9a6834c097",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1197
        },
        {
            "id": "fd0b894d-7a1a-4dc5-9123-309c4a9278d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1198
        },
        {
            "id": "917886c5-24f9-476c-bfb6-c2efcff2f99b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1200
        },
        {
            "id": "129cc5ae-c71c-4637-96da-d2276aa4afd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1204
        },
        {
            "id": "7a3b7546-702f-414d-a85d-bd7d7b13eade",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1205
        },
        {
            "id": "15e388f9-3a50-4b2e-8d52-ad11b70d5f99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1206
        },
        {
            "id": "98cc3c56-da65-4b12-aa4d-a2fc2ef983eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1207
        },
        {
            "id": "bd3b660d-e6c4-402c-a4fa-67db3b2ac48d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1208
        },
        {
            "id": "ac0f2327-5f46-4ae6-b379-ef749cc409af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1209
        },
        {
            "id": "e299d18c-c5d6-40b1-9bcc-5a63d82192ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1212
        },
        {
            "id": "a725880a-5bf3-4fe4-98c0-01a28a28f401",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1214
        },
        {
            "id": "9f86306e-78cf-4a1e-9f46-86ee3d85da97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1227
        },
        {
            "id": "7717ea04-1597-4826-9fb7-6643005e0e17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1228
        },
        {
            "id": "607904b7-67ab-4df6-954a-27e4846f9ba9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1254
        },
        {
            "id": "81f93218-c349-4967-9edd-ce9d6a3b6df3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1256
        },
        {
            "id": "8487238d-a0c0-4324-bc30-371ebef14ed5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1258
        },
        {
            "id": "11949321-0233-42e3-a2e2-0d90706460cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1268
        },
        {
            "id": "5920f7dd-15b9-4618-b6c2-5f3d16ddd20a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1269
        },
        {
            "id": "f90e7665-f01c-4fd7-8a64-079065695db2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1284
        },
        {
            "id": "ff919795-c0fa-4498-9ce1-347e5f25ecb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1285
        },
        {
            "id": "0eedd34d-6acd-4471-a5ed-ebd4c08594fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1286
        },
        {
            "id": "5f8a06fe-edf3-4d84-9eea-b1751754df88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1287
        },
        {
            "id": "7ed38df0-329c-43eb-92e6-ca0f0cb553cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1292
        },
        {
            "id": "f9e8e760-b387-43c4-8ec5-c0c975b53804",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1294
        },
        {
            "id": "8b0f9c63-78e7-48cb-ad33-7537f18296e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1295
        },
        {
            "id": "5e68b9e9-2c01-446a-95de-559cfb6655e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7808
        },
        {
            "id": "a6a5369d-7ffc-44b8-9a3e-582d864322dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7810
        },
        {
            "id": "b6c53315-9f38-4e40-b492-fdc516eb622d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7812
        },
        {
            "id": "529f4033-163e-43f1-b448-753695269784",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7884
        },
        {
            "id": "478d7787-a619-47f4-a55c-532e1612b058",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7886
        },
        {
            "id": "f0b8f9ea-6615-49c1-aba4-e492ef7f6109",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7888
        },
        {
            "id": "d3cb4f86-c335-4c94-935b-cefaf95e4cbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7890
        },
        {
            "id": "18b7485e-9162-4548-8c42-8589c4ef4853",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7892
        },
        {
            "id": "8a6083f5-0f85-4e1a-ad47-4800a2bed241",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7894
        },
        {
            "id": "f4ed05ee-36ba-4f29-8e54-df36d119bd59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7896
        },
        {
            "id": "96b74029-0edb-485c-83d9-8ae0711dbc8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7898
        },
        {
            "id": "3af4943a-05ac-4a5c-ba36-a97a7da9d259",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7900
        },
        {
            "id": "a53f5f2b-1a27-4473-876b-b0b9b2ced923",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7902
        },
        {
            "id": "4df2d933-1f5c-4654-9e81-f30d4bb20b1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7904
        },
        {
            "id": "976b051e-3ce0-4565-8040-0d39f992b119",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7906
        },
        {
            "id": "20d49cd0-1f15-4536-862e-ac1318acee72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7908
        },
        {
            "id": "10b97231-d75c-4d7c-ad9e-abb244bfb6ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7910
        },
        {
            "id": "03a358db-20b0-48fb-95d2-2c5a21cc4d2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7912
        },
        {
            "id": "e02da41c-6f66-4e7b-9055-897fa47dff4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7914
        },
        {
            "id": "96247a61-0bee-4e10-a5c7-a5711b62f9fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7916
        },
        {
            "id": "e070f18a-3f8d-40c9-9ef2-de34712bb73c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7918
        },
        {
            "id": "64fb4269-25c5-4ada-b680-0d8a037df818",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7920
        },
        {
            "id": "cbb87b58-2784-4e70-9c59-b7c8310987fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7922
        },
        {
            "id": "f40b415a-ab4b-4581-a961-4e52b32903b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7924
        },
        {
            "id": "aeaedf3e-2d4e-435c-8c5a-bea3507b2f5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7926
        },
        {
            "id": "ac4f95d6-a108-4cfc-a43f-c6d9078fbc69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7928
        },
        {
            "id": "0ce89415-e337-4596-9be9-5bd8fe9f7969",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 84
        },
        {
            "id": "001c8ecd-ff54-4e1f-bd9b-1000c371d2a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 354
        },
        {
            "id": "3a6dcda6-b282-4130-a42f-ea7bc576f46f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 356
        },
        {
            "id": "67897ddd-5f38-42c5-96d1-f289cd0d1482",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 538
        },
        {
            "id": "f93c0dfd-46a5-4b9c-a851-a4ed16b77986",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 932
        },
        {
            "id": "a6cc2cc5-df83-40bb-9823-afc442c8b8f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 1026
        },
        {
            "id": "c9d8da46-74f3-4008-bfca-03de60d5016e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 1035
        },
        {
            "id": "c216b145-f3ec-4c13-aec9-d2918a50b1e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 1058
        },
        {
            "id": "a1f05811-6c2f-414e-b99c-e826d2f8f286",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 1066
        },
        {
            "id": "d51de38f-93a1-47f1-ad06-3c31e059dee1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1090
        },
        {
            "id": "e4554f79-30ed-40ba-93f8-6f3ed824c585",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1098
        },
        {
            "id": "cb24172a-e983-4014-9a2a-ba4e21e19873",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 1184
        },
        {
            "id": "a5447589-1751-4988-b84a-460edae52284",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1185
        },
        {
            "id": "fd271a28-bb71-4121-8f02-c821f9844233",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 1196
        },
        {
            "id": "92ce2ba9-4cb7-4a87-9b6d-1392337f88a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1197
        },
        {
            "id": "9a7a5168-1ce6-4b2c-bd37-95e5de68ad58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 1204
        },
        {
            "id": "7918c155-11d8-43fd-87bc-0eaba4f609a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1205
        },
        {
            "id": "180d11a0-eb11-4f6d-8b87-cb85974b8676",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 1294
        },
        {
            "id": "438c1778-587d-4592-95b4-4af9d2bea98f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1295
        },
        {
            "id": "c438e86a-b1a6-4576-ad3a-4461fc7e9376",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 67
        },
        {
            "id": "15d61ad4-92dc-454a-a4ab-406b6b7f07af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 71
        },
        {
            "id": "b614a60d-f87b-4915-8d97-6b1168fbbb7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 79
        },
        {
            "id": "8e68f3d7-88e1-4ce4-81cb-57f976f97311",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 81
        },
        {
            "id": "0e7582fe-4b23-4a9a-87cb-15bca622ac14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 84
        },
        {
            "id": "4f201143-890a-43e1-b65c-472214aca9ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 85
        },
        {
            "id": "e5e507bc-d9c5-428d-93ad-32b2c223c133",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 86
        },
        {
            "id": "fd07c11e-ad07-4d4d-b854-9cf7f5131c58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 87
        },
        {
            "id": "e416652c-8d10-4325-aaf6-5d9ed1f0976e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 89
        },
        {
            "id": "05d2a13a-3433-4157-be7f-200fd7057557",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 199
        },
        {
            "id": "88d6a590-db17-4bae-8a96-286af0b3f901",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 210
        },
        {
            "id": "93837187-7a37-470e-a238-70def88ba3ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 211
        },
        {
            "id": "94c6e64f-9be4-419f-8bc4-80c1398d2b6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 212
        },
        {
            "id": "fbd040d2-cadd-46ed-8d16-60261c42d683",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 213
        },
        {
            "id": "7ed16d56-e4df-4689-8fc2-fb5debaa7b10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 214
        },
        {
            "id": "f3208c49-b491-45fc-b8e4-e26c37a33b50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 216
        },
        {
            "id": "2566bed0-ec27-4288-8314-276343ef4570",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 217
        },
        {
            "id": "40d2d180-09eb-4c75-9a5b-9806f57d5722",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 218
        },
        {
            "id": "f3a86ff4-9d1b-4c1b-984d-eac6f90e1823",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 219
        },
        {
            "id": "d98000b4-8c30-462f-b7fc-43ccf826ed18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 220
        },
        {
            "id": "a5749731-87c8-47ea-b194-2409bfac6223",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 221
        },
        {
            "id": "24d1b43a-7304-4f9f-ad2a-b187f7d7b726",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 262
        },
        {
            "id": "c990dcdc-9dab-497d-9e59-95977982bc26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 264
        },
        {
            "id": "8a20fdf2-4a00-4cfd-b2b2-eb1b15cf3afa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 266
        },
        {
            "id": "d956de8f-4844-4125-a5dd-6f73caa4f3a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 268
        },
        {
            "id": "69ddb91c-40a8-4012-906a-d302feeba5c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 284
        },
        {
            "id": "59388129-34aa-491b-a597-523d0bf36016",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 286
        },
        {
            "id": "d4a7d334-065a-41d9-8d8f-bd8c61e16c45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 288
        },
        {
            "id": "3768cf2f-7208-4902-bb73-b2b500af7d19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 290
        },
        {
            "id": "2bbdd04b-424a-4d9f-8c01-95357989e8a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 332
        },
        {
            "id": "1fe29c8d-c601-4da2-a294-3e3607441925",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 334
        },
        {
            "id": "4aea6deb-3745-44d0-9ece-00462bf61d7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 336
        },
        {
            "id": "65285bc6-1c90-46e4-bb81-9c1d1612de20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 338
        },
        {
            "id": "7051be37-a004-4b8c-93cf-97f556385a27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 354
        },
        {
            "id": "7b7a1a48-02dc-4f52-ae1f-1e367933b562",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 356
        },
        {
            "id": "1c1394f9-491d-4e72-9dfc-9b38cba53088",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 360
        },
        {
            "id": "822f44e7-a808-4ef0-9bc9-433c4106421b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 362
        },
        {
            "id": "1a47ead4-7db1-440b-ad48-c31cbf34dcc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 364
        },
        {
            "id": "09a469e7-be75-4d86-a6af-e33b8bea140a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 366
        },
        {
            "id": "6111ead4-4cc6-4abd-875b-20717e0f7c9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 368
        },
        {
            "id": "7cbc124f-8b46-421c-8a89-1848c6370a99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 370
        },
        {
            "id": "ebb58561-45f8-4c45-8626-928948fe6f71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 372
        },
        {
            "id": "bb6c7809-f0ff-4df3-9626-8c42dcd28c2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 374
        },
        {
            "id": "121865cc-4a95-4b78-894b-110143a9a357",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 376
        },
        {
            "id": "5e7f2107-2ee5-4bf0-8fae-971f31ba24a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 416
        },
        {
            "id": "9a84b195-2721-4fe0-9252-5a1ea17ef57e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 431
        },
        {
            "id": "d0de1289-c4f8-4e79-957a-52d899392e38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 510
        },
        {
            "id": "bcea98be-5f85-4c19-b2d9-5e2fc958f120",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 538
        },
        {
            "id": "8e88847f-b3b4-46d7-ac0a-e784d55469e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 920
        },
        {
            "id": "90142698-c870-4cdb-8553-2a5bbda1c699",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 927
        },
        {
            "id": "659d9df9-6cfb-4833-b29e-c4c730762d59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 932
        },
        {
            "id": "b8289764-80e2-4e05-9e11-a2e29437b230",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 933
        },
        {
            "id": "f4db083f-b984-4cb3-8621-56891120b699",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 934
        },
        {
            "id": "d9425c23-5791-4e1b-be29-5a7b0c25d9f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 936
        },
        {
            "id": "a4ebe118-3bc7-4594-bc72-d0ab8a6340b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 939
        },
        {
            "id": "0ff097b5-525f-4981-a20f-283a468ec402",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 952
        },
        {
            "id": "193ce55d-baa5-4fa1-b7d3-8cfd749a50ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 978
        },
        {
            "id": "fe6687c7-050b-450a-8e3e-0c46be3557d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1026
        },
        {
            "id": "f7da557f-ef4b-49e4-99fd-d127d3f18954",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1028
        },
        {
            "id": "628a9cf9-1931-40dd-bcc6-05044e62a805",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1035
        },
        {
            "id": "4155f7ed-17a9-4cb2-84d9-43d5fbb6adc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1054
        },
        {
            "id": "d5e3fced-6020-41e6-bc72-1a377e18669c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1057
        },
        {
            "id": "d0af5c42-0efb-4f36-b2c3-8b123cec6c65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1058
        },
        {
            "id": "18a57950-206b-47aa-b410-16f4f1619fdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1060
        },
        {
            "id": "cac1f3c0-101b-494b-acb6-df690d757697",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1063
        },
        {
            "id": "9e29045b-b70f-419e-ac53-29901c195b1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1066
        },
        {
            "id": "ed3edacf-534f-41a3-a65a-108fffc1f6b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1090
        },
        {
            "id": "9972925d-d00e-4707-9388-d86abc1c7e35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1095
        },
        {
            "id": "8f8e0efa-1d28-4c68-b17c-798971332124",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1098
        },
        {
            "id": "020c5773-7abf-45b6-915f-e5872aedbab5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1120
        },
        {
            "id": "7dd798ff-c3f8-4fc4-be28-f6f5dd9066ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1136
        },
        {
            "id": "27b961b4-b574-4f47-b2b4-4c58337fbd9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1138
        },
        {
            "id": "622443b9-675c-4590-bfa9-c30c2db2b521",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1140
        },
        {
            "id": "6060b143-8e0d-4446-a1e9-c8a7354e7315",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1142
        },
        {
            "id": "999c61ae-63ff-4fea-acf5-3d05da5c4969",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1144
        },
        {
            "id": "e67d90f2-2f73-4752-9365-f91a77c24087",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1146
        },
        {
            "id": "ea8be86d-b18b-4b16-9b76-185a9798ebfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1148
        },
        {
            "id": "cd9b6f5d-373e-44ec-9395-b66dfde35661",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1150
        },
        {
            "id": "e234257d-ecbd-454e-92aa-0c7949cfd651",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1152
        },
        {
            "id": "cbce61d0-1ce9-4060-a4e2-a1232cf96f91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1184
        },
        {
            "id": "9055362b-0977-44d5-8091-50f20138b8af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1185
        },
        {
            "id": "d6abe4a1-664e-45d4-9378-753127a6d11d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1192
        },
        {
            "id": "0bb8fca3-127f-4d69-a0c4-b599c8704f6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1194
        },
        {
            "id": "4196622b-51ef-471f-b77d-0bea75327fbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1196
        },
        {
            "id": "f6828d24-78c0-4fc7-bf7f-bdb49dad7aba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1197
        },
        {
            "id": "d500d6f9-6aae-4daa-b90a-a93225aa1088",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1198
        },
        {
            "id": "132b39e0-9cd9-4013-b40a-fac0df9beaad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1200
        },
        {
            "id": "5ab5b43d-24e6-4f2e-9929-7a7f904882ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1204
        },
        {
            "id": "6eccb4ec-7ce1-4fb8-8180-af0d8dc22528",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1205
        },
        {
            "id": "ffa3a57c-7a21-434a-84f2-300fac0a92b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1206
        },
        {
            "id": "caf42671-689d-4c2d-bd2b-563db16a4bbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1207
        },
        {
            "id": "1660a7d9-7899-402d-8020-888d24e6fae1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1208
        },
        {
            "id": "6f637df3-1bef-40d8-818f-ba11c4634e54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1209
        },
        {
            "id": "1cef5145-9f81-425b-a032-9f10ce36fdb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1212
        },
        {
            "id": "a0468ecb-223e-4d27-8e4d-837e9b242c5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1214
        },
        {
            "id": "aa6c8ad0-b7f9-49c8-b11c-8c3c999351c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1227
        },
        {
            "id": "fbc6cdcb-aead-4f05-aa21-5da6d6e60a63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1228
        },
        {
            "id": "14f95722-ae99-47f1-a681-2b7bf86fe106",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1254
        },
        {
            "id": "4fc5b917-adef-48a9-803e-e5e96307616a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1256
        },
        {
            "id": "8bb53900-e017-402e-908d-5d685f810a33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1258
        },
        {
            "id": "426b0556-7b89-492d-a258-621edd028bff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1268
        },
        {
            "id": "1e04495a-0475-4495-b1dd-6fc7e749f941",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1269
        },
        {
            "id": "57bb4628-c619-43af-97db-746197330ce7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1284
        },
        {
            "id": "848d914c-8b88-4e20-a01e-727a59b3d63b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1285
        },
        {
            "id": "67930d16-3cd8-45b4-b6e5-37eebd1ca584",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1286
        },
        {
            "id": "5addc680-e85d-4786-970f-5f5051829171",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1287
        },
        {
            "id": "bac02d50-f1d9-4e43-8fe0-8c3fc3aabdba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1292
        },
        {
            "id": "d40f9f26-5271-41f8-a0a8-e0e96d31fe28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1294
        },
        {
            "id": "ab8946f5-4a67-458f-bde7-2d0f009a7003",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1295
        },
        {
            "id": "41c850ec-3657-4b40-a0af-44f33673d4a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7808
        },
        {
            "id": "67146bb0-a74c-46f6-b3a0-c5a79c2509c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7810
        },
        {
            "id": "90448b88-b854-45e0-8269-4ccad824e760",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7812
        },
        {
            "id": "4e9926d3-d2fd-47be-aeeb-c4ea3e7430f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7884
        },
        {
            "id": "09a64111-7e62-4ac0-9e91-d71c82f99dc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7886
        },
        {
            "id": "4a3b6f22-17da-4623-b8d1-df85c210d2a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7888
        },
        {
            "id": "3b1b5ba3-9b2f-4f1d-a4aa-13bc176007d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7890
        },
        {
            "id": "cda8f2fd-c3f7-4a12-87ed-aaabb1b8d116",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7892
        },
        {
            "id": "82d5fd5c-ff90-45e3-9eda-089937926157",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7894
        },
        {
            "id": "249faa70-f1da-496a-a189-0721efeef436",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7896
        },
        {
            "id": "76a6f783-bb1a-403d-aa01-c9cefa2f157f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7898
        },
        {
            "id": "13cf3a6e-de00-41cd-af76-a4ba97ee9334",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7900
        },
        {
            "id": "df38bbf7-8ce0-4a15-912c-593a5156f99a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7902
        },
        {
            "id": "9634fde6-1f5c-4415-8718-b32510f4b7b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7904
        },
        {
            "id": "d72ae7d7-8d02-45d0-8c80-57010f7bb3a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7906
        },
        {
            "id": "4222b974-620a-4efd-afed-b438c090d777",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7908
        },
        {
            "id": "b9776d55-11f7-42f8-b2a7-1805881d5c6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7910
        },
        {
            "id": "1611707e-c74d-486d-9b2c-46c20a8fdaf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7912
        },
        {
            "id": "98f86496-5f94-4cbd-a511-abb7a14719f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7914
        },
        {
            "id": "7ceca832-cd06-4d8e-a9c9-b3c4eca03877",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7916
        },
        {
            "id": "3b3db422-2bba-485f-9c30-7cde0dea6d3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7918
        },
        {
            "id": "34d72a4a-bff2-4386-a34e-81a350a06d39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7920
        },
        {
            "id": "43103da6-03f6-4036-b2d5-7c0404ba37ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7922
        },
        {
            "id": "fc15c579-25e6-4332-8f2c-0742799dcb17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7924
        },
        {
            "id": "168af160-9b6b-4294-8b1d-9721102771d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7926
        },
        {
            "id": "ef3949b8-706b-4c50-8d4d-24ad7260dc58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7928
        },
        {
            "id": "34d521a5-a843-4101-b991-a234f3cc942f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 34
        },
        {
            "id": "3317ca3c-e9a2-4363-9195-eb0985aebafa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 39
        },
        {
            "id": "36db7a3f-e635-4b4c-bd23-ecc811df6dd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 67
        },
        {
            "id": "ffcd8239-723e-48be-b560-df97ac8f8623",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 71
        },
        {
            "id": "254442f4-3ea1-430b-9d13-aba719931c79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 5,
            "first": 65,
            "second": 74
        },
        {
            "id": "321d884d-6748-4be9-8984-d75555a6b9a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 79
        },
        {
            "id": "6d932cc1-283a-413f-ab72-8a3be3c328e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 81
        },
        {
            "id": "cec22cc4-0734-4240-b462-8647f143bcb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 84
        },
        {
            "id": "cacb8fdc-842c-4d4f-a1e1-55c24523145f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "91696784-4096-49e8-8125-135a21ac26ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 87
        },
        {
            "id": "9dc7dc7d-22b9-4880-8fdb-c269db527563",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 89
        },
        {
            "id": "289e94eb-e88e-44b7-8e63-9068f9682984",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 199
        },
        {
            "id": "fe734c65-d2b4-41e8-9eca-aa93c77f3723",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 210
        },
        {
            "id": "82d26e2a-cd6b-41c5-86d0-645de68a8b7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 211
        },
        {
            "id": "f0d79368-9dfb-40dc-b505-fead254c6f7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 212
        },
        {
            "id": "5997930d-c71b-4f25-8406-9ce700fd734f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 213
        },
        {
            "id": "25c43261-d495-46eb-97bb-d2489426f8c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 214
        },
        {
            "id": "55a98b6e-685e-4a87-ab60-0b618f0cfd41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 216
        },
        {
            "id": "8b456741-a03b-47b8-8b7d-5abab3fcec18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 221
        },
        {
            "id": "7adfb119-24c3-400c-9759-9fd442214624",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 262
        },
        {
            "id": "37612aae-51f7-45de-b9f9-8c16fbbd1996",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 264
        },
        {
            "id": "33f3311d-ecff-46c1-987f-07096d9baef1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 266
        },
        {
            "id": "3372b3bf-5599-4c66-aae9-125d2b70bcf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 268
        },
        {
            "id": "021745ff-3675-4fd5-966b-a5b636c71f68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 284
        },
        {
            "id": "0abd28e9-2315-448f-bfd0-c39795c107a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 286
        },
        {
            "id": "f27948ac-22c0-4a3e-879e-cab4f9a6fd23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 288
        },
        {
            "id": "aa8e5a72-fbce-4be9-9927-424a3f7b9146",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 290
        },
        {
            "id": "7134fcbd-9204-43a6-ba8a-491d1e923584",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 332
        },
        {
            "id": "09d91c00-368b-4b46-9a11-d52db30b6e3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 334
        },
        {
            "id": "f67dc3cc-6bb1-48da-ab91-571234001c5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 336
        },
        {
            "id": "5f171274-a3f1-42c4-9330-187ce593a74f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 338
        },
        {
            "id": "d94288c8-32fa-4a28-9282-02e3fd711832",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 354
        },
        {
            "id": "182960c6-a550-4475-9a20-0c406dfdfea2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 356
        },
        {
            "id": "dc5702b6-7768-4405-815e-b888b5948233",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 372
        },
        {
            "id": "7d1d908a-39c1-452b-aeb7-510d64b00ba7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 374
        },
        {
            "id": "df840040-b06e-4d0d-a6a1-270a8e7a963d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 376
        },
        {
            "id": "76d65acd-2414-4ffb-b062-f69887e8acb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 416
        },
        {
            "id": "63e7c0ba-a3f4-4dcb-acc7-300f1a7af884",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 510
        },
        {
            "id": "6235a8ba-049e-4040-ac4c-e916e5b95962",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 538
        },
        {
            "id": "0c190eef-0b32-4dae-8ea1-987718e116e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7808
        },
        {
            "id": "dffcbef0-218b-4ac7-9f5c-e2e797dc4627",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7810
        },
        {
            "id": "6178f0c6-c44c-48ac-b970-9de7aa1f6ff7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7812
        },
        {
            "id": "43a45791-829a-41f1-9607-c3bc575f4ae4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7884
        },
        {
            "id": "c27e8870-28ee-4ac2-a8df-835bec9346f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7886
        },
        {
            "id": "a0e824b1-27d6-4a9d-8c45-464f308faa62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7888
        },
        {
            "id": "666991bc-eb1e-4ffe-9159-8ecc4de27f83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7890
        },
        {
            "id": "3b04b764-7971-4703-8b7a-61548b2c20b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7892
        },
        {
            "id": "b0da216c-5ae7-4daf-877c-a7b9146f299f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7894
        },
        {
            "id": "172dbd16-e68c-48fd-b6b4-03ab4b45a154",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7896
        },
        {
            "id": "429e9165-f3fe-4781-967b-8423a0502268",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7898
        },
        {
            "id": "d999e2b1-9e3c-47f2-9704-b649a243b249",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7900
        },
        {
            "id": "ee91d11e-c7c2-4fcd-a399-9967de9eaf66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7902
        },
        {
            "id": "6b665007-3859-44ae-80e7-f02328b6c308",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7904
        },
        {
            "id": "cbebc679-89c0-41c5-9303-e10af2ff207c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7906
        },
        {
            "id": "c03e0594-9099-4c8a-b375-5451eda7082c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7922
        },
        {
            "id": "fee0b8d3-2f9b-44fb-946d-9b745d3f1842",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7924
        },
        {
            "id": "ebfb1cec-b47a-453e-97a7-117f5e69a2be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7926
        },
        {
            "id": "1481bc1e-7368-433e-b66d-8d0fa7a64fc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7928
        },
        {
            "id": "effe1e4a-a0e8-4e23-8ade-d7ea446155f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8217
        },
        {
            "id": "67a5c23a-831f-432e-b891-f05dcb7097e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8221
        },
        {
            "id": "f258901f-407a-40c7-a959-21a20b5733e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 44
        },
        {
            "id": "ab2816fe-9038-49f5-b1a3-4fee22bb5816",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 46
        },
        {
            "id": "9c075bde-70f2-46ca-8e4d-5adc69fdbee4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 65
        },
        {
            "id": "d2bf6365-dddf-488e-bd64-f7d375e00652",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 84
        },
        {
            "id": "81aea9f3-10c1-4fce-a62d-66c6ca1a4304",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 88
        },
        {
            "id": "84ed506a-f968-40d7-909e-ced3854e1929",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 192
        },
        {
            "id": "77ee9f5e-0e70-4744-978b-e7639f0d3c72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 193
        },
        {
            "id": "7fbdcbef-6cbd-44df-bf7b-4777e42e3e68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 194
        },
        {
            "id": "3ab37319-c0c3-4a4a-acb2-5ff2bb092d33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 195
        },
        {
            "id": "fe7e10f4-e33e-4d00-968e-70f0780e99ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 196
        },
        {
            "id": "01fe3955-49d3-4f0b-9ef9-c9eb30bce658",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 197
        },
        {
            "id": "fdb54ad8-c18e-41d7-ac75-afb32188c4e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 256
        },
        {
            "id": "5238c761-606b-453d-a0e7-13c6a18dc55b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 258
        },
        {
            "id": "d24e950b-8ca9-4e91-a8b6-061a25596e3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 260
        },
        {
            "id": "ff29ec76-9654-4031-9f2c-5084d6580297",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 354
        },
        {
            "id": "d6a23d9f-a464-456e-9d0f-0a11cb140ff4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 356
        },
        {
            "id": "dd69480c-07e8-4bac-bb5b-579f2563060b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 506
        },
        {
            "id": "40102601-ad03-4fe0-89e2-384e163c9b7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 538
        },
        {
            "id": "3e497dc3-3055-4153-aaa2-93d43a6c4de6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7680
        },
        {
            "id": "3afdfd5e-600a-46ff-bd5c-69179b22b381",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7840
        },
        {
            "id": "468a5de4-f509-4342-ac47-12c587816e8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7842
        },
        {
            "id": "bea21313-3977-45ca-a163-c72189e4fb3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7844
        },
        {
            "id": "ba6e0a68-a594-49b6-9d0b-08a00251f910",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7846
        },
        {
            "id": "50ce295a-9de0-4fb6-b6ff-2c366f8d7e47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7848
        },
        {
            "id": "758ed347-e153-4245-92cf-9ab6b04d7723",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7850
        },
        {
            "id": "6f683e32-eea2-48b6-b837-da0aff074a31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7852
        },
        {
            "id": "b2985470-f798-44fe-a5a6-8e93be960e86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7854
        },
        {
            "id": "7d6c819e-8487-41b7-897f-c5b21e548f03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7856
        },
        {
            "id": "c4475c5b-765e-475e-8a9a-b2699134437f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7858
        },
        {
            "id": "52f44aab-34fc-4e9c-bb23-fc89193050d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7860
        },
        {
            "id": "a0290954-517d-4aa4-a94e-a57abdb429a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7862
        },
        {
            "id": "d8f81758-e786-4ba7-a517-986a161c7898",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 8218
        },
        {
            "id": "582e2755-7383-4665-979b-6294fb8f1aef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 8222
        },
        {
            "id": "49e66376-fdfa-4bd7-9f90-eb16f491bdb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 67
        },
        {
            "id": "f7916f38-2ea2-450c-a6b9-54320908eec9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 71
        },
        {
            "id": "89e7abcb-1afd-4177-8eba-e65e97fc5829",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 79
        },
        {
            "id": "12db5e02-2ca5-4269-8686-8c7a43d536d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 81
        },
        {
            "id": "2a979155-8872-45d5-9359-4761b69f34f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 199
        },
        {
            "id": "4a0171c3-f318-4fee-9cd1-60cb077c21d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 210
        },
        {
            "id": "f0a4c7c1-3a26-45e8-9c5e-ddf050394ab0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 211
        },
        {
            "id": "3d9df326-c4f0-4735-8988-4338ffd3b268",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 212
        },
        {
            "id": "1191aa57-68a9-43ac-a6ad-f40822e3faa4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 213
        },
        {
            "id": "0482fafd-8084-4582-9b63-79cbc9a14bf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 214
        },
        {
            "id": "78c436a1-2281-43fd-b312-492071d88637",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 216
        },
        {
            "id": "72c293d4-4cb0-4bea-9dd9-50665ec8c16a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 262
        },
        {
            "id": "2440e997-ca8d-4bad-8886-c3a08372bf67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 264
        },
        {
            "id": "6b6e7910-9380-48b5-aa6f-5008e76b542d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 266
        },
        {
            "id": "88438054-58f0-482b-94ad-f5099571dad4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 268
        },
        {
            "id": "8a4eae05-3ecd-458b-849f-01c00e526bb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 284
        },
        {
            "id": "4b8bd5b7-6322-4a6a-ace0-41b17e7dd2fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 286
        },
        {
            "id": "ee71db40-638e-4490-82fc-91951bd9a01a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 288
        },
        {
            "id": "39d0138d-bb23-41bf-93c8-0f6d76ce2fb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 290
        },
        {
            "id": "cfa3efb6-85f7-4093-8444-b4967c0debe9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 332
        },
        {
            "id": "e93c7622-64da-4944-80c4-09a717b6f50d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 334
        },
        {
            "id": "299085f0-f746-49d8-aae4-6ef01c50a6fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 336
        },
        {
            "id": "5b3c663f-6645-485d-ad12-c2cf85ebd6f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 338
        },
        {
            "id": "5f40ba56-b665-4397-997b-e864f13cc686",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 416
        },
        {
            "id": "a2afd4f1-10e3-4860-84c5-5233e78322ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 510
        },
        {
            "id": "c5a67f54-5816-45fa-b86e-b797312f65b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7884
        },
        {
            "id": "dc979684-b6cd-466e-b0fa-8e0ab81b2bae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7886
        },
        {
            "id": "13081167-1833-493a-8b9d-e7eb78f4f9c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7888
        },
        {
            "id": "8e8f8d01-7e0d-484c-b2af-93f18fd79747",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7890
        },
        {
            "id": "7703a28f-02fe-48d5-b1ef-8c96d8431a1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7892
        },
        {
            "id": "bd748920-aa15-442f-99c4-2a27bccf4be6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7894
        },
        {
            "id": "b8667768-aa35-4989-9c71-06f19459cc6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7896
        },
        {
            "id": "bfcf8b84-ac40-429f-b755-b5dad5a329d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7898
        },
        {
            "id": "5feb263c-f900-47a5-9355-f5c2b2d38524",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7900
        },
        {
            "id": "f20e5daf-28d1-49e6-9840-4b9b6b3054ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7902
        },
        {
            "id": "ee462b2d-cf4c-43f5-a4fe-28c1f6bf2555",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7904
        },
        {
            "id": "7cddff06-0baa-425b-a300-d95271efa6b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7906
        },
        {
            "id": "5b8473f6-863c-4c92-a254-a72415f24ce6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 44
        },
        {
            "id": "c3318884-4b6e-4f0a-b6fc-a40b1705212e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 46
        },
        {
            "id": "f77c8ce1-0e6a-4bc8-8a7d-fd904762d634",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 65
        },
        {
            "id": "aa47876c-8acf-410b-9624-664d93b5de75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 84
        },
        {
            "id": "8bd92d0b-3f29-4def-9f49-b86d55d36ce3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 88
        },
        {
            "id": "16823237-2756-4807-9a65-7944f3fe160e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 192
        },
        {
            "id": "be687f83-600d-490d-b755-9ef5e78cd2e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 193
        },
        {
            "id": "fcab5676-1f61-4de1-a227-db01bd1ee87d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 194
        },
        {
            "id": "9942b675-d747-4d99-9b71-8794c7e1481c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 195
        },
        {
            "id": "ce42f72d-e57d-4a14-893e-15f811e3a8c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 196
        },
        {
            "id": "f0834b7d-642c-469c-be6c-af23899a8521",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 197
        },
        {
            "id": "740cdf91-0710-4322-8109-92ced3236eaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 256
        },
        {
            "id": "f0a3bb75-8ad8-46a4-b219-893d84159f4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 258
        },
        {
            "id": "22f981d5-df49-473a-9dbb-d2e286d0718b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 260
        },
        {
            "id": "061da6bb-6f13-40e6-9684-954edc30837f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 354
        },
        {
            "id": "d329088d-92bd-4f04-9395-e1b89e13ed1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 356
        },
        {
            "id": "4d07d587-bcab-41ff-bc20-aa1fe6bd6c1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 506
        },
        {
            "id": "f7848618-7cf5-4825-a275-63ac6c660468",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 538
        },
        {
            "id": "ddfd5fad-16c4-4101-8cb3-6d1129814d6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7680
        },
        {
            "id": "c188db25-5e0b-4736-a028-577ce1540a69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7840
        },
        {
            "id": "3e4b527e-6516-4b2c-8f3d-fe85005da401",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7842
        },
        {
            "id": "57b69741-e5af-41d4-9253-8423063f0d78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7844
        },
        {
            "id": "d0c9691f-9eac-40f5-a4c9-9d129f261df9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7846
        },
        {
            "id": "91aa1faf-8d1a-45b0-9582-e419801f26f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7848
        },
        {
            "id": "1c75a92b-320e-43e5-b682-74dc45ea7f64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7850
        },
        {
            "id": "17adabb2-977f-479d-acc6-8e38cc42e320",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7852
        },
        {
            "id": "9e3ddf91-5da2-4292-8e9f-0fa7d513d9b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7854
        },
        {
            "id": "caf25853-f8fd-4c2d-8a09-ee8bd5596d69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7856
        },
        {
            "id": "690126df-c215-427b-80db-c64cf97a013d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7858
        },
        {
            "id": "f2e8859c-e177-4a6f-8d85-260e0243c735",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7860
        },
        {
            "id": "71458b10-8926-4df7-94b8-d5de1fa2a4d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7862
        },
        {
            "id": "cbcdd025-e5e3-45dc-8f25-6a9154b62eb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 8218
        },
        {
            "id": "80b68e49-9cf4-4944-acfa-7172920facda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 8222
        },
        {
            "id": "a09e532d-1cec-4af1-97a1-0a0888d027e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 69,
            "second": 74
        },
        {
            "id": "9456009b-cec5-4876-80b1-50b5356bc2fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "11914078-05dc-475d-aa57-fb39798caf5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "4219f045-15d5-4b72-ae14-97099d80e427",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 63
        },
        {
            "id": "67bfb918-0596-4817-9f38-d67818ed2ec8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "7f05ca22-7bad-4891-bcde-ad26bf1ef64f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 192
        },
        {
            "id": "a70c440b-cfb8-4a39-8b33-824a5873a8e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 193
        },
        {
            "id": "7dccbe0b-e806-42d6-baf4-511fbb8bee06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 194
        },
        {
            "id": "8fe77fab-3116-4943-b8f6-1fe081f1fcdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 195
        },
        {
            "id": "9656b205-4a03-4b11-a487-b19dd815398d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 196
        },
        {
            "id": "c97e8d41-3ad7-4057-a17d-d8e27f31c33d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 197
        },
        {
            "id": "bb7b63a3-2f72-425b-b25f-4bd9cfac6a17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 256
        },
        {
            "id": "b73bd839-c988-4377-bd2a-603e2be27a8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 258
        },
        {
            "id": "c24f1f3a-b6c2-48e3-9596-2f5853aedc4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 260
        },
        {
            "id": "c11a4f90-adf0-4e2c-a102-7cea36ba1e43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 506
        },
        {
            "id": "5caac9fe-0302-453f-ac2e-b3cf260c299d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7680
        },
        {
            "id": "87d8d170-62b7-427b-a603-032cd7476215",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7840
        },
        {
            "id": "9e2444fd-4462-431c-b7b8-b2cb2db3c81c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7842
        },
        {
            "id": "5f241a7a-73b2-4a40-9dff-9ab8f18b1b0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7844
        },
        {
            "id": "ff1644ac-909e-429a-a178-2affe08591db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7846
        },
        {
            "id": "a482d687-8d9f-4fe3-9ad6-16e89ec3f86f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7848
        },
        {
            "id": "10b30606-2096-47ab-8c86-f931ea5c7771",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7850
        },
        {
            "id": "97d06936-fe5a-4529-94cf-75e1021e4766",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7852
        },
        {
            "id": "71675dd6-76d7-4210-bcb2-569fa71a50e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7854
        },
        {
            "id": "b82ee0ec-90be-448a-b313-6f2f37400ae9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7856
        },
        {
            "id": "5947a6b9-402f-455d-a6a6-a135e5d87445",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7858
        },
        {
            "id": "42e7816c-9be0-4d22-ad03-98e9742f0e4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7860
        },
        {
            "id": "40cf4711-647e-4171-96a3-2671d38066cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7862
        },
        {
            "id": "eb2a5b0a-980e-4511-9902-51b2569f045c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 8218
        },
        {
            "id": "85160d96-ecb7-4953-9139-90956db1ea2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 8222
        },
        {
            "id": "67e57dc6-7a31-4e6f-b404-05fae8424c54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 67
        },
        {
            "id": "76b85704-bd5b-4e42-99e3-f615bad99e6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 71
        },
        {
            "id": "f49b23f2-d8f7-4fb0-a681-f0faa4826bba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 79
        },
        {
            "id": "f867af42-cfe6-4bef-b57f-96b737cf5a62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 81
        },
        {
            "id": "13250b93-1ec6-4c6b-b698-87980a88a64a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 199
        },
        {
            "id": "c3ba991c-d9e0-45f5-b928-03501d343870",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 210
        },
        {
            "id": "a56d03b3-62f2-4495-9171-5d005e229cf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 211
        },
        {
            "id": "bd383591-36d2-45de-b6b8-c514abbd2aa6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 212
        },
        {
            "id": "96bcaac1-bf39-4f51-a375-a0ab352123d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 213
        },
        {
            "id": "352b09b1-8848-4804-a76b-4bbd2eb8f655",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 214
        },
        {
            "id": "91d3eccb-9188-4433-8615-769de2c8737a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 216
        },
        {
            "id": "402a024b-0eb7-4b07-9250-c649c969f744",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 262
        },
        {
            "id": "f1c5a014-2a9e-431b-92f7-26cb622194e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 264
        },
        {
            "id": "1e59d6fe-cfce-4ffb-b97d-aa873d22d52b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 266
        },
        {
            "id": "5f62dac3-d394-4ebd-9f4a-505050353964",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 268
        },
        {
            "id": "e6aae435-11dc-4cad-86e2-61d916b53376",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 284
        },
        {
            "id": "cfdfed6c-6ec4-4a9b-899a-ed4591551d24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 286
        },
        {
            "id": "575fd617-dda1-4ce5-a49a-98bf65157dea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 288
        },
        {
            "id": "9a529d3a-3e25-4e93-a822-3f6346e03fd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 290
        },
        {
            "id": "ef53944a-2d2f-4bef-bb39-47294782d52c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 332
        },
        {
            "id": "b3fc8529-44bd-4b87-8368-1a6a659f15d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 334
        },
        {
            "id": "f0192488-358d-41d5-b485-bc16573870f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 336
        },
        {
            "id": "c4fdd349-7939-4adc-985e-71f9a7251e36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 338
        },
        {
            "id": "e26869fb-f62a-4862-b2a7-6e3912198604",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 416
        },
        {
            "id": "3a5fb4af-cac8-453d-8dea-02fd266d217f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 510
        },
        {
            "id": "4194f3f1-e546-443c-bc85-614ef81de711",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7884
        },
        {
            "id": "dab0fca0-c621-471b-a878-5cc9a37509c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7886
        },
        {
            "id": "5ac1806e-58b3-4bf9-8374-9bd91f2fcdf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7888
        },
        {
            "id": "cb874347-2a06-480e-87d0-e3819337567a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7890
        },
        {
            "id": "1e8ec71c-e0a8-4b78-b72f-43322b1c4249",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7892
        },
        {
            "id": "c2d8fc86-1ebb-460a-a1f4-49dcc82ea070",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7894
        },
        {
            "id": "3c754040-e119-4ce1-9e2b-0dee1aa0ba3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7896
        },
        {
            "id": "bcadf760-f102-482a-8f86-4c0c8427e6ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7898
        },
        {
            "id": "ee103967-a5c6-48dc-8785-c9706ef41b3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7900
        },
        {
            "id": "97c75041-a8d6-4764-b5f9-81b130853c0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7902
        },
        {
            "id": "56967548-7280-4e40-9fcf-2a4402411041",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7904
        },
        {
            "id": "ad3eb80c-60fa-46f9-ac48-2ff45092c27e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7906
        },
        {
            "id": "5034bc01-38b2-48d3-be50-a653fdcfeb2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 34
        },
        {
            "id": "8c652b7b-7386-4e79-ac9f-6ef7b46bae26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 39
        },
        {
            "id": "d34e389f-5c33-4b7f-8d05-cd8021c67e2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 67
        },
        {
            "id": "069c0641-3fd6-475d-a00d-53adb09173c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 71
        },
        {
            "id": "cf6229a9-a7c8-4787-a778-5e7962538c61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 79
        },
        {
            "id": "ba525de0-cbb6-4fce-a1b2-45a7c8f9e01e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 81
        },
        {
            "id": "b3d71caa-9ccd-44e5-9b3d-a2381a0615cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "dc06fc2f-38a2-4b3c-8840-c48ed4857e5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "6cf6cd94-b8a2-4f86-b2ec-829e215a7c5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "6ae1074c-548c-4c18-9d71-d9c60f624616",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "2d99d6ec-db2a-4a9d-a35f-4c803483e22b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 199
        },
        {
            "id": "bbe5cc20-a132-4b45-bb81-277b9bb12e12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 210
        },
        {
            "id": "ffec1248-731f-46b4-8da8-c00c0c6e95f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 211
        },
        {
            "id": "a0380741-c5ab-4761-928e-f1d277540a57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 212
        },
        {
            "id": "69745608-d8f7-4086-b060-23277b325562",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 213
        },
        {
            "id": "4c4f34cd-94c6-421d-ab34-bec46a6ed6b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 214
        },
        {
            "id": "d70c54e1-bf59-47af-8773-a4ca25a77738",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 216
        },
        {
            "id": "15b1b583-1662-4fce-bc08-badf391588e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 221
        },
        {
            "id": "9bba5f89-64a1-45b8-8633-3a60bf39c47b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 262
        },
        {
            "id": "0be352a9-2a26-4221-b5df-d9e0efb671a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 264
        },
        {
            "id": "d89b5ee3-cde6-4d26-97c0-216a38c42e94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 266
        },
        {
            "id": "e214d15e-cfcc-48fa-bb2f-7d44f6305e34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 268
        },
        {
            "id": "4cafe2a3-1000-4824-94c8-98cf3ca2be0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 284
        },
        {
            "id": "a637175a-cfc7-4466-ab72-2b76f222bd6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 286
        },
        {
            "id": "fc471842-38d5-4a63-afa9-09876a0731bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 288
        },
        {
            "id": "270595ec-9732-48e7-bd45-02e8499ecfa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 290
        },
        {
            "id": "0f258291-88ad-497d-a7dd-ef8ebdd94efb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 332
        },
        {
            "id": "187c4de8-a79d-4ebf-b226-c2674acd6f67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 334
        },
        {
            "id": "b609fea5-e804-4117-b932-4777c87973e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 336
        },
        {
            "id": "2e1ed02f-bb4d-4095-97a3-6199979001d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 338
        },
        {
            "id": "d09fe57f-0df9-49a4-8286-837dbd01b073",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 354
        },
        {
            "id": "16996e5f-760f-4b1e-b4d2-04dd0ca10b17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 356
        },
        {
            "id": "d4266634-bf1f-4eb1-9d5a-4938f8372354",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 372
        },
        {
            "id": "e5412b9f-8dfe-4694-a8d0-3b826872e39a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 374
        },
        {
            "id": "28c5c3fc-afb2-40d5-bd92-f6049c4494eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 376
        },
        {
            "id": "f3a78f3d-45af-4163-bb79-23f5a76a6d40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 416
        },
        {
            "id": "7d5891a0-48c3-4419-a984-54c87e7049bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 510
        },
        {
            "id": "b944a699-b25f-487d-8af1-1b0840076680",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 538
        },
        {
            "id": "8f788c7b-881a-43b6-be7b-12a0c6950480",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7808
        },
        {
            "id": "45b17b96-a48c-4568-bf7b-e192ac9e3421",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7810
        },
        {
            "id": "881096ed-9ab6-4527-b716-0acd6b8b46fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7812
        },
        {
            "id": "6c2cdd80-dcaf-457d-b8b3-ed42567fe2be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7884
        },
        {
            "id": "324bd8c0-e0ec-4a33-9f2a-de3bcca2eb38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7886
        },
        {
            "id": "b409eaf4-e74b-4863-9cb1-bde99f1662a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7888
        },
        {
            "id": "b737ae66-f3c1-4425-8493-f8b8f1705e20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7890
        },
        {
            "id": "0b16eb69-4cc1-4fff-86b5-e66fd7fe1575",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7892
        },
        {
            "id": "e386fd1e-e8a9-476a-b6c1-c5bd9573b0cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7894
        },
        {
            "id": "62ec988b-772a-40bb-b87e-f34b03744b43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7896
        },
        {
            "id": "c4cdda14-445d-4965-bdb6-c31bcb1ce606",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7898
        },
        {
            "id": "3ed20e4d-ab36-4552-ad50-15dfb81be8e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7900
        },
        {
            "id": "2d0dacdc-89d7-41f6-ba86-f531a64ed767",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7902
        },
        {
            "id": "76c35bb2-74ba-4e4c-aadc-2028f20474d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7904
        },
        {
            "id": "2005aeeb-7bc4-499c-bd00-aa711f2eea53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7906
        },
        {
            "id": "74b74c3f-4082-4696-a57b-fbf5b62ea6ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7922
        },
        {
            "id": "3cb76596-629b-41a1-a063-087d9b2b65f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7924
        },
        {
            "id": "d2f7023d-6fa0-4d16-9b59-774956573c34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7926
        },
        {
            "id": "ea8aa3fc-86ed-4e7d-a394-20f761142c60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7928
        },
        {
            "id": "149d4c59-b530-443e-806e-02e4566c75e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8217
        },
        {
            "id": "da9daa37-32b3-431b-86bb-cfce4b0e9fc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8221
        },
        {
            "id": "b8852044-3609-417b-9205-7e8b27dfa2c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 44
        },
        {
            "id": "24a585a9-3a80-4ecf-8d54-bb2e413fa83a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 46
        },
        {
            "id": "9edcdb6e-4cb0-4ded-bfcb-3ae8479b2ce9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 65
        },
        {
            "id": "2a72a924-e404-43e8-bdcd-62ddfdd1eb92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 84
        },
        {
            "id": "06d51954-6107-4193-bd8f-a16df1c648b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 88
        },
        {
            "id": "c2462485-99db-4c2c-8ddb-cfa476fd0b79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 192
        },
        {
            "id": "5429d02b-25e6-4a9f-976a-394b8448b59f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 193
        },
        {
            "id": "18e9437b-c0e8-45bc-a997-731c091d596d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 194
        },
        {
            "id": "72125935-5e19-4a99-98e4-d0f73d215bed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 195
        },
        {
            "id": "213b92e2-209d-4a11-b19d-fe524fb7c7eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 196
        },
        {
            "id": "592bb6f9-5248-42eb-b7f4-456951529c57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 197
        },
        {
            "id": "5d4eb9a2-0218-479b-86c1-8b4281347078",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 256
        },
        {
            "id": "b190a5bc-eeac-43da-87fe-c421f724dff5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 258
        },
        {
            "id": "24a2f31f-a90c-493e-8978-800c8e07d97a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 260
        },
        {
            "id": "c8b3a7f6-deb3-46e9-b3a3-f611c999736f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 354
        },
        {
            "id": "ab2a1a40-6dc2-4fc8-8605-dabf96407afc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 356
        },
        {
            "id": "c7d80ac9-b00c-43d3-880a-772dec19fa20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 506
        },
        {
            "id": "85b44113-d14c-4b0a-9876-e4a9e9cc2c9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 538
        },
        {
            "id": "0a5301e8-cff2-420c-a596-02966990ad6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7680
        },
        {
            "id": "e424fb0a-b9fe-4851-9ab2-f3789a5d0e1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7840
        },
        {
            "id": "2e2d5050-450b-4499-b9d7-aa6b2fa4aad5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7842
        },
        {
            "id": "7de4e606-d1f6-406b-8482-aa2c7928383e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7844
        },
        {
            "id": "6d890783-e9ba-4d5a-8e67-bc1358673df8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7846
        },
        {
            "id": "fb482e53-c0e1-4144-bd67-3c86f0bfb25e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7848
        },
        {
            "id": "ad3c9f5e-94c7-445f-bfdc-0a39e49ac9e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7850
        },
        {
            "id": "47371ba1-35d5-4d0d-b053-114ae51d5624",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7852
        },
        {
            "id": "ae0d88b0-5a0a-43f9-9a0f-5a64bf9d73a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7854
        },
        {
            "id": "fd02b431-e8b5-4ba3-ae7c-4b335dd38557",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7856
        },
        {
            "id": "b4966bd6-76d4-46f0-ba69-913cdcd55f3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7858
        },
        {
            "id": "a139096e-1ef0-402e-a29b-8dfd8b222b9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7860
        },
        {
            "id": "dcd564e2-1816-442c-b968-e54ef44f4207",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7862
        },
        {
            "id": "cfbfb1de-2aba-44f6-a74e-236c3fac2b71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 8218
        },
        {
            "id": "f74e6863-b013-4b02-8b61-09125a25139c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 8222
        },
        {
            "id": "c9947f2b-ad72-478d-9c21-037d0bd9e43e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 44
        },
        {
            "id": "80a9533e-506a-409a-80c9-d25d55c40d3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 46
        },
        {
            "id": "3a7af14a-c0d0-4e2c-bd1a-a77c23b0c74e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 65
        },
        {
            "id": "fa87591c-9bb5-4fa3-86c0-a01b85c5a376",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 88
        },
        {
            "id": "4e1cea66-d860-403f-a176-8522c84b56a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 192
        },
        {
            "id": "86ed355f-3b60-432b-aace-7beba9a45db0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 193
        },
        {
            "id": "c3dc88d3-50c1-4279-b6ea-241e7338db4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 194
        },
        {
            "id": "a75f067b-fbd5-447c-a3a3-71404891068e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 195
        },
        {
            "id": "e04137d1-1050-45b1-a3f3-999a386dd9b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 196
        },
        {
            "id": "4b7fc93a-4eb6-4fc8-a843-673f106382db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 197
        },
        {
            "id": "08c8ec74-72bc-4c6a-8a7a-7f1ff9981420",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 256
        },
        {
            "id": "19cae078-ace0-467b-81eb-267fb35fcba3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 258
        },
        {
            "id": "966a2331-474d-45d5-a8e0-8542f61ed809",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 260
        },
        {
            "id": "7cb400cb-4e3b-47e2-952f-967827c531cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 506
        },
        {
            "id": "6d1ba3ba-d491-4437-aca4-7de7fd77fce0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7680
        },
        {
            "id": "4bbe0d52-21ec-490c-9d60-d8e40089f681",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7840
        },
        {
            "id": "d0ea91f0-17c7-4309-97f8-9ae799bce3d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7842
        },
        {
            "id": "7a44d57e-168e-4a41-8683-8005e5d0539e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7844
        },
        {
            "id": "4e34c0a9-d1a7-45ad-9730-49ae47b0ba95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7846
        },
        {
            "id": "34c546b1-9b1e-40dc-a725-2179eb54f072",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7848
        },
        {
            "id": "8dc7faa0-1690-4f3f-965b-739c77becb38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7850
        },
        {
            "id": "391cf7c1-22b7-426d-979f-86f8382d2378",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7852
        },
        {
            "id": "3d7e369e-02ec-42c2-8d0e-93e7ac1847a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7854
        },
        {
            "id": "73f4faf5-b2a1-458e-9f44-ac24b543c797",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7856
        },
        {
            "id": "ddc15a4e-2de8-498d-9895-3a4c7f6943fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7858
        },
        {
            "id": "6581b788-87a9-47b9-9ff4-dbc56c0fc29e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7860
        },
        {
            "id": "653e3043-7e3d-4dbb-9abb-4ce6870c0694",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7862
        },
        {
            "id": "460255f4-cc4e-480f-88b7-4b6edc26904a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 8218
        },
        {
            "id": "1563ddde-2586-4b6a-8629-14c481684a4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 8222
        },
        {
            "id": "92dc274e-ee4f-4330-9efe-4b2c2f76b80d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 44
        },
        {
            "id": "b27e7b51-be3e-4c0a-8f89-2df848278df8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 46
        },
        {
            "id": "b5b31823-a27b-4181-9d48-a097b4ede255",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 65
        },
        {
            "id": "39a440bb-8aa9-419e-9e0b-8034a04d17d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 84
        },
        {
            "id": "bfb480f8-027f-4562-aaba-42c17656145e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 88
        },
        {
            "id": "caa21bb6-1d8a-433b-bc6e-5264e0198816",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 192
        },
        {
            "id": "a5ba0c38-3958-4ce8-8a2c-df0e47105212",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 193
        },
        {
            "id": "1840ff35-ae54-4ef0-95de-c6eeed4d704f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 194
        },
        {
            "id": "1a9be30c-126d-4238-a44f-3193ddad48fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 195
        },
        {
            "id": "bf59f8d3-3cf9-46a3-a41a-60e3a18b81b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 196
        },
        {
            "id": "17c761ff-4581-4ec0-a24f-df428a414215",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 197
        },
        {
            "id": "8839f02f-3bb9-4cb9-a2bb-e08eeb05ec8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 256
        },
        {
            "id": "f03dd32d-f7e7-4e30-8e67-8d4aeee0eb1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 258
        },
        {
            "id": "a47f89b2-1737-4677-a483-d29d524a5ddc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 260
        },
        {
            "id": "16e985d4-e5e4-4c52-93cb-a9f34a22498e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 354
        },
        {
            "id": "7a373d94-b624-4364-9e2d-f56647df5425",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 356
        },
        {
            "id": "ac21c819-40d9-49d6-869e-8879fe2e3464",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 506
        },
        {
            "id": "f653ac14-180d-40f2-811b-b98d12fbede1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 538
        },
        {
            "id": "2b74f44e-86e6-4bad-be3b-1c54ee3567e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7680
        },
        {
            "id": "fbab345c-e1e9-4278-a4c3-ccd0b2197b78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7840
        },
        {
            "id": "c99e866c-d5e0-48cd-b793-b75c06fac255",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7842
        },
        {
            "id": "1968391c-95e5-4848-bbe3-cfc8064d0367",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7844
        },
        {
            "id": "279c5662-79ba-4c60-a417-2eb85d773bd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7846
        },
        {
            "id": "8a5afd25-a3c7-4db4-a440-8604e0f4fe51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7848
        },
        {
            "id": "eb80f38c-6c0e-48b6-aad3-ad292c037e0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7850
        },
        {
            "id": "1d352da3-92a7-4f72-8ef9-cf2e2edf7ce5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7852
        },
        {
            "id": "d1437312-3296-46df-a269-1fb10e1bf5b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7854
        },
        {
            "id": "7d18920b-78b0-4b8d-b105-bed85b56bc57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7856
        },
        {
            "id": "9fcd93f6-c005-4336-8e85-ba9f7d3a923f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7858
        },
        {
            "id": "3d6a736d-e90c-41ef-beee-4e52cf7d27d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7860
        },
        {
            "id": "fd92dbb9-468a-446b-b419-3800c94cb5e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7862
        },
        {
            "id": "ca70c7ae-fc3b-44d6-a4c9-40b72d0977f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 8218
        },
        {
            "id": "6ff3f5cb-6ceb-429b-be24-f872d0d9fd76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 8222
        },
        {
            "id": "59fa1580-207e-4128-8fa5-82b6d2f5c9a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "70b302ca-1ec1-45e1-b3b9-aa01000a3154",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 45
        },
        {
            "id": "e21d6819-b358-4846-be90-edafb607e689",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "fcaef233-3233-4be2-bb06-3a52d6775493",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 63
        },
        {
            "id": "987e1d2c-077b-4912-9497-09b8f6ab4807",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 65
        },
        {
            "id": "cb386876-a045-4120-a5e8-c1c431c6ec13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 67
        },
        {
            "id": "208cfd05-9e97-4219-b2ad-08ea1d579b04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 71
        },
        {
            "id": "4818b052-355b-41fc-a8a3-45100f77cd41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "5d79d157-1df0-4d7b-a23d-94a3a3602591",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 81
        },
        {
            "id": "8579c445-5637-4d14-afa0-afb7fe651fbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 84
        },
        {
            "id": "3a6b12d0-d3e9-4234-8957-4c7232deeab4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 97
        },
        {
            "id": "dadfe693-0eb7-4eca-91b7-b81f8b053109",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 99
        },
        {
            "id": "a9bbb172-cb8d-496a-9501-4b56c1d24eb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 100
        },
        {
            "id": "d2504eca-a7b9-42bd-8fae-83a50b4b8955",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 101
        },
        {
            "id": "684a9568-350a-4a51-a615-a6b17bc86504",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 103
        },
        {
            "id": "7d989dbc-da49-4642-a9d8-e23ea81ec26b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 109
        },
        {
            "id": "ee7bfb18-7552-4432-9e67-7f0792431652",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 110
        },
        {
            "id": "e128817c-6765-4270-b66e-d2903200fe69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 111
        },
        {
            "id": "9ed2480a-5465-4b68-b3e9-689d167e3c4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 112
        },
        {
            "id": "3eca82ce-d38a-42e1-a747-bbedd4049b59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 113
        },
        {
            "id": "6f0ef82e-b6bd-4ff7-beec-e8331ee71482",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 114
        },
        {
            "id": "8084705c-ba45-4e6f-a57d-e713298c00f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 115
        },
        {
            "id": "573a23d6-4188-4cd1-8ce5-c1fde50d1759",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 117
        },
        {
            "id": "a2a637ac-f2cf-485a-964a-1f1fa1977762",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 118
        },
        {
            "id": "470dbc50-186f-4202-a291-8e933bb2bcbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "9912ab61-db09-4e8f-8770-62438435a2a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 120
        },
        {
            "id": "3b2cb847-7913-45ce-a771-cdb1b924755e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "d295d724-6480-4bd3-80ac-1fda62a83fde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 122
        },
        {
            "id": "4115b031-b9f3-44ad-a4a8-47ea39e084ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 192
        },
        {
            "id": "6d2519ef-26e6-46bd-81fa-a5c3f7ff3768",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 193
        },
        {
            "id": "5d5f172e-e27a-4b90-9b38-1a5d9fe7bbce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 194
        },
        {
            "id": "5e1b841e-a7a8-4ebd-80bd-6bdb380c4bc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 195
        },
        {
            "id": "5b3d1505-0c41-43b0-8c6b-64772c40c754",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 196
        },
        {
            "id": "8114954b-b725-48b8-bccd-a6e84156278d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 197
        },
        {
            "id": "c592367c-9ce8-405d-8e6c-a74921a127df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 199
        },
        {
            "id": "744c46fa-eafa-45e7-be3f-d554b1fd03e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 210
        },
        {
            "id": "c1628758-cd6d-411c-b865-142ea63f5ada",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 211
        },
        {
            "id": "959c5688-30cc-4579-b625-4af3aa7c8608",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 212
        },
        {
            "id": "f35b6de1-6ea8-4a6a-addc-a567441104ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 213
        },
        {
            "id": "f11bc1f0-ffb9-4f6c-8eeb-cbaa255b8e2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 214
        },
        {
            "id": "65c2ff29-7bc2-441b-a945-3cdcc5f580ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 216
        },
        {
            "id": "7dcdf5a9-fe45-4b5a-8b4d-eb0736999642",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 224
        },
        {
            "id": "06929880-9a3f-4af9-875a-224e29e67d12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 225
        },
        {
            "id": "af7ef207-8be3-4e5e-8529-da9e7e49f07d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 226
        },
        {
            "id": "fd113325-44b7-45c2-a125-aa1134dfd90e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 227
        },
        {
            "id": "36d9fbf2-5906-4e3f-b262-19e62dc1a4ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 228
        },
        {
            "id": "8025baee-9de8-459c-a9c7-cf048323987b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 229
        },
        {
            "id": "781fed69-5f97-4746-bccb-00b15b25cc26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 230
        },
        {
            "id": "60c5eb2f-f276-47ee-abfa-823a7ebd8d99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 231
        },
        {
            "id": "7035a6d6-0cbc-4d81-b571-b8819c647cac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 232
        },
        {
            "id": "4e252cdd-c991-4349-92f7-e902800502b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 233
        },
        {
            "id": "2e6b373a-52c0-495d-a556-f45fb81b7701",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 234
        },
        {
            "id": "37d3ae7d-6205-4141-948d-9a5005b7ab54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 235
        },
        {
            "id": "f02e85d1-fb13-4c88-a9fd-a4dde7a032a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 242
        },
        {
            "id": "b611abcb-59e8-4390-b80c-3e760924c3ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 243
        },
        {
            "id": "92698228-3988-479c-968b-5431fba246a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 244
        },
        {
            "id": "8c500535-64ff-43d4-8d9a-95cde30f5afc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 245
        },
        {
            "id": "6af9e5ef-59e7-4f08-956a-b801b73c142d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 246
        },
        {
            "id": "76c8e390-e11c-4f6c-be53-50bad2aa1095",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 248
        },
        {
            "id": "69a92d5e-aea2-45cd-9259-8b3b8f40d8c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 249
        },
        {
            "id": "4f7e7df0-44ae-405e-93d4-6ba015741470",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 250
        },
        {
            "id": "479ecf72-0c10-4636-99fe-39b6ac24e840",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 251
        },
        {
            "id": "07f1580f-bc40-41af-84f0-bd4b034f129c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 252
        },
        {
            "id": "fc047205-4dfd-4a47-8661-638836abd370",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 253
        },
        {
            "id": "41b77073-09d1-4b05-a2d6-527b45ae2c09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 256
        },
        {
            "id": "e28b5754-447b-4b92-aa82-79e1d99a2fee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 257
        },
        {
            "id": "574ca1d9-76cb-4cc8-a4cc-c9fc7ddc22f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 258
        },
        {
            "id": "57382653-dc26-457a-b0b9-ac91da4047b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 259
        },
        {
            "id": "42444951-9f44-4fc1-84f5-1ea6289a78f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 260
        },
        {
            "id": "de44bfba-b783-4cd9-ad1e-1ed98589d0b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 261
        },
        {
            "id": "4710b804-3fa1-43d8-b137-156d30c6f1e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 262
        },
        {
            "id": "0733d635-b764-4ece-a141-567e0c157dd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 263
        },
        {
            "id": "3e41195f-8c25-448b-8fea-a4ac15dcff0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 264
        },
        {
            "id": "26670337-26a7-4121-b3f5-be21e6cb987c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 265
        },
        {
            "id": "1c3a19f4-c651-4de5-990b-1ff3c892c635",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 266
        },
        {
            "id": "b97253c4-2b09-4c44-93fb-6adaff5e3ccc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 267
        },
        {
            "id": "527d819f-b839-46f8-ad4a-e0e83198bf07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 268
        },
        {
            "id": "6d37218b-6dfa-4cfc-81a5-2a93916b5fac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 269
        },
        {
            "id": "8ea7b552-63cb-46b4-b6a8-616a558d665e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 271
        },
        {
            "id": "e4e49296-59e5-440c-a7d5-660b3cd902b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 273
        },
        {
            "id": "8ed9fdd7-c086-4628-b1ed-74ec6f3b9ed2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 275
        },
        {
            "id": "dec83067-819f-47f5-94f3-3fc871c9e7a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 277
        },
        {
            "id": "ec8b5563-2d8d-49a7-8239-c3f91d06db8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 279
        },
        {
            "id": "9f32edf9-671a-4f83-b118-eda42a70a096",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 281
        },
        {
            "id": "dcd0516c-eafc-4b9a-829e-d52df829b1e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 283
        },
        {
            "id": "a5b5b104-a3de-418c-b968-d8a974489702",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 284
        },
        {
            "id": "f01b415e-663c-4771-a3cf-addda0d7211f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 285
        },
        {
            "id": "e7c823af-0465-4273-ab71-c264d4ab52cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 286
        },
        {
            "id": "f7ad5ef4-294f-4e8d-ae6b-9c459965a52c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 287
        },
        {
            "id": "237456aa-e579-420e-91a4-d11aed4650db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 288
        },
        {
            "id": "f9a65e05-2055-4409-8666-77bfaf6d26af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 289
        },
        {
            "id": "048dde09-1de7-446c-bffd-0fdd4b942930",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 290
        },
        {
            "id": "8fda37ed-8faa-4cc8-80cf-a1616a393ade",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 291
        },
        {
            "id": "f6452617-b62e-4314-98d2-26115017088c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 312
        },
        {
            "id": "f5dc96f4-dfc3-4d3f-a445-751558165e39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 324
        },
        {
            "id": "023d73b2-1786-49bb-8223-89de23fb08f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 326
        },
        {
            "id": "d9b7928e-639b-4d24-826f-ec6f70ae6d1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 331
        },
        {
            "id": "7fc86b21-6cca-4fa0-a475-cd62340a77c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 332
        },
        {
            "id": "09b0b7cf-4430-4507-9bfb-37cf11efb560",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 333
        },
        {
            "id": "5759e839-1744-4023-9873-c98f26aa9741",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 334
        },
        {
            "id": "1a409a5b-fadf-4991-b06f-6344bcb1cf80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 335
        },
        {
            "id": "1d4ffcc3-93ae-47c9-abd7-41b010284bb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 336
        },
        {
            "id": "47d11018-fbc7-4693-9072-cdde68a22c56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 337
        },
        {
            "id": "c7b7c36d-ef8f-42ce-b543-4f14229e07e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 338
        },
        {
            "id": "a08a9578-b5a8-4425-83d4-a382c1513741",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 339
        },
        {
            "id": "7741cb7a-7767-4018-a7b7-c8fa3b114612",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 341
        },
        {
            "id": "aeb85bc7-7a33-4911-9267-b42edbab6473",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 343
        },
        {
            "id": "4cd67ac6-a29e-4138-b7ab-b6dc727a9523",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 347
        },
        {
            "id": "e96a3c59-4608-4370-aec9-994d442ef432",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 351
        },
        {
            "id": "649f06d3-aab7-4c0d-8f78-c1101a15fbbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 354
        },
        {
            "id": "e4a36958-22a8-4181-9804-db5d731b4de5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 356
        },
        {
            "id": "651bf4a4-d821-4d77-b25e-e34e46f869c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 361
        },
        {
            "id": "957e31bc-5056-423b-9c09-d91976e96140",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 363
        },
        {
            "id": "f7f89baf-19a5-42ba-961b-901c77aeab03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 365
        },
        {
            "id": "98554655-f9bd-4279-b64e-d621111233c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 367
        },
        {
            "id": "96a692d1-d247-444f-a673-337995dbb182",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 369
        },
        {
            "id": "53c31093-7228-43a0-9573-e195cea01540",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 371
        },
        {
            "id": "fb19edee-b1c2-4760-9a84-a04bac1dfb34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 373
        },
        {
            "id": "b67dd3a3-483f-4993-88a7-712c5d379930",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 378
        },
        {
            "id": "61e392f9-ba02-472e-860d-d508115fabb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 380
        },
        {
            "id": "1034103e-6060-4114-b561-38d7797bcd4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 382
        },
        {
            "id": "f29593fd-b6dd-4374-8d87-1294a55a2832",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 416
        },
        {
            "id": "495a7725-77b2-43b5-a5cb-a8f2cae745fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 417
        },
        {
            "id": "6a90cdf6-2fdb-436a-b452-3fa52f4aefe4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 432
        },
        {
            "id": "73da1e58-4f7c-47db-a189-2959a3a8ba2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 506
        },
        {
            "id": "8a310c27-b814-4c34-9897-5ee745973d54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 507
        },
        {
            "id": "a68feb84-0d93-4587-b891-cd37348979f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 509
        },
        {
            "id": "d0b935b4-2246-46eb-83d5-2c382cfec3f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 510
        },
        {
            "id": "0f7bcd30-5daa-45c3-83a1-bee56de56da8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 511
        },
        {
            "id": "0ea5714b-91ca-41ae-a120-f9197bff7460",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 537
        },
        {
            "id": "e731ff9a-069a-482d-a90e-7d0e532f4033",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 538
        },
        {
            "id": "99f1a847-4974-4487-aac2-4057e755554e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7680
        },
        {
            "id": "10c6e137-bac3-4caa-9db7-8522d3d7f3ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7681
        },
        {
            "id": "1a3b7508-7b14-43e5-bb05-4fb50b4f804f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7743
        },
        {
            "id": "6576a8e2-3cfb-4d8f-abb4-38c71e023051",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7809
        },
        {
            "id": "b7c87323-cbb7-4b88-82a3-daef968b4465",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7811
        },
        {
            "id": "86a00a1d-f270-4f1d-97a8-ee1e6e86ba6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7840
        },
        {
            "id": "c4409a99-744b-45fb-a73a-5c43f5fb0ff5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7841
        },
        {
            "id": "8ffedb9b-7db8-40d8-89dc-eb7502f9b0ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7842
        },
        {
            "id": "308c28b9-6e19-47f6-ab22-2c7b6849c85e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7843
        },
        {
            "id": "e15e0478-2921-4c74-b1d0-22bcfd0a377b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7844
        },
        {
            "id": "86771dc2-3ea3-4a32-bcaa-aed1353b41a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7845
        },
        {
            "id": "22ca7325-ce0a-45fa-99bb-c42c4933fe45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7846
        },
        {
            "id": "68c8cb0c-aecf-449e-ad0e-2c2ad5479acf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7848
        },
        {
            "id": "324ae764-783d-415d-afe7-2f01464a9378",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7849
        },
        {
            "id": "3ba4d986-6352-492d-b949-b47ad7903fc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7850
        },
        {
            "id": "d200b03f-f603-4574-9f78-315bf4bfc60f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7851
        },
        {
            "id": "070c982c-5f32-4559-babf-8aebf18e5e62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7852
        },
        {
            "id": "65443f3e-e8d1-4200-aee2-6f24ffa9622e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7853
        },
        {
            "id": "009cd34b-98d2-45d0-8cc8-d72923f7ff8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7854
        },
        {
            "id": "992d76a8-d9dc-407a-9be2-4923b2326912",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7855
        },
        {
            "id": "0882aba9-db31-487f-bf9b-d4809d1ac429",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7856
        },
        {
            "id": "2663abb9-de3c-41d3-853a-20b6c3aa5aa8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7857
        },
        {
            "id": "82daa55a-83c8-480d-9c0b-bc237838af38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7858
        },
        {
            "id": "ea5ea444-98b2-4d50-9253-f57e2a0e6dc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7859
        },
        {
            "id": "4d031ebf-1779-48ff-890e-83f5b501a837",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7860
        },
        {
            "id": "d8f6a312-05ba-4688-a639-816311d09f8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7861
        },
        {
            "id": "f98cd855-01de-47d2-b7aa-acde4a9e9075",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7862
        },
        {
            "id": "6c507b08-48fe-4f55-93a2-805e46df7ed0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7863
        },
        {
            "id": "d1f4f6bc-7a40-46a7-8a47-08615939c33a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7865
        },
        {
            "id": "1eae7bc9-dbb2-4869-897d-8c7a4de338f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7867
        },
        {
            "id": "eaa221ba-4be8-4e20-9571-5e20d09fa9af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7869
        },
        {
            "id": "87dddc8f-5f5c-457a-861b-d5ad70002d50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7871
        },
        {
            "id": "53f305a3-1ab1-4a2c-85ce-54ca6c25985c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7875
        },
        {
            "id": "ab2a82ef-5683-43dd-8f34-fb1d4c0df16d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7877
        },
        {
            "id": "a1e4dec8-7209-41b3-86a3-110c4e8ef5ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7879
        },
        {
            "id": "add5a933-59ba-4956-be5f-71bc4638ca79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7884
        },
        {
            "id": "ba529da9-760e-41bc-b083-b7ffdc897b4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7885
        },
        {
            "id": "2f342b20-a60f-4e26-84d4-05a6a4b86fcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7886
        },
        {
            "id": "02b48e85-cbd1-4fba-948f-da5f79449c69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7887
        },
        {
            "id": "1151787b-4a14-471f-ab67-46b9d745ecdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7888
        },
        {
            "id": "7722c5db-4209-4652-a829-10bfe43bd4eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7889
        },
        {
            "id": "94fd13b2-6e48-4a30-9974-957133c0d4af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7890
        },
        {
            "id": "8eb11ca6-8154-4d8a-a264-0ae3f911d371",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7892
        },
        {
            "id": "8ed96d7a-e4ce-4d36-8ab5-6721cf54e1f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7893
        },
        {
            "id": "0cbf1b0f-d6f3-4dae-b497-4fef380595c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7894
        },
        {
            "id": "174998df-a8c8-4a64-a4ec-d8cfe0b91c71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7895
        },
        {
            "id": "9dcf6b9c-d928-4654-b21a-7e6a2fcf3c29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7896
        },
        {
            "id": "ee5506c4-2d17-4b64-b019-7951adfcc5b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7897
        },
        {
            "id": "b72b9a8c-2cd4-4977-ad5b-859d5fa79cdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7898
        },
        {
            "id": "072ad5e4-46b3-4d2e-9db3-8b57e4d1e878",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7899
        },
        {
            "id": "807749f9-d9b0-4355-9c12-f2e83a095468",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7900
        },
        {
            "id": "c6e539ed-a0dd-466c-ad48-afa9c58ede8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7901
        },
        {
            "id": "26bebb3b-7f66-41e6-8655-2af398f3cd6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7902
        },
        {
            "id": "409f5160-f626-4e0c-9785-b7a6ad78f431",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7903
        },
        {
            "id": "cd60177e-7123-4e26-897b-6186186ac42a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7904
        },
        {
            "id": "9644779c-68f3-4d77-84ae-6d2d7033e047",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7905
        },
        {
            "id": "0822667f-45bd-421c-98c7-bc90d4590707",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7906
        },
        {
            "id": "49a3e90e-836b-4da0-987e-8406f2638bc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7907
        },
        {
            "id": "59ba31f6-ae4d-4646-a60c-72094fee9b22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7909
        },
        {
            "id": "771b5c0d-bb83-4545-a989-c788cd25aaee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7911
        },
        {
            "id": "5164aaaa-2094-4016-b8c3-a9c28b0f658f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7913
        },
        {
            "id": "6550952e-4da9-4fd3-ae0f-199e498e11c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7915
        },
        {
            "id": "6b7346ee-d5a1-425b-a12b-e5a90cfbcc7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7917
        },
        {
            "id": "ed3bedb1-3d42-46cd-a440-8d6850adaf3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7919
        },
        {
            "id": "0bbaee92-6428-483d-a592-09bc1dca8c0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7921
        },
        {
            "id": "da6aac4a-4b40-4cdc-afb3-100e6dadb9c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7925
        },
        {
            "id": "cf30517e-8234-40b0-a407-043306905149",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8211
        },
        {
            "id": "88d5661a-8798-433c-8203-e00707914f1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8212
        },
        {
            "id": "8217f3c0-11e6-44b1-a695-e45969173422",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8213
        },
        {
            "id": "96ba1e78-f227-4783-aa92-4cdcfe56eb1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8218
        },
        {
            "id": "f65136f8-527c-4492-a692-63dd0b6a7bc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8222
        },
        {
            "id": "90a08d47-4468-4002-8708-0cb4d95a7ad2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 44
        },
        {
            "id": "c266b88f-7731-4b53-ae4c-e8be6eb30765",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 46
        },
        {
            "id": "89c478d3-1220-48e0-9549-2260abdea976",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 8218
        },
        {
            "id": "079ad78a-b107-487f-8767-c938f4321d53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 8222
        },
        {
            "id": "b80118de-6d5a-4804-9c64-c73f3246fd11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 44
        },
        {
            "id": "928c2ae5-04c4-4af5-8f77-f0d261ee56fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 46
        },
        {
            "id": "4592e69c-987d-4e5b-9f7a-fbc4c6d11297",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 63
        },
        {
            "id": "154f83c0-9b6e-457c-9385-ea0f08aef9f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 65
        },
        {
            "id": "947803c2-8947-4d1e-92a1-37e860dc8125",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "37c28b14-ee7b-453d-81b3-be206c2f69df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 99
        },
        {
            "id": "dc96e41e-15e5-447e-8689-ee0eca728267",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 100
        },
        {
            "id": "704ccd12-c7ef-4338-87a3-412ac7a5f719",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "454e8d6a-2e41-4e46-874a-1ca55befc6b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "e92aa848-540a-4fa2-9a00-8df29374da41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 113
        },
        {
            "id": "6402fd91-b16d-4cde-b631-636ba19c8dc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 192
        },
        {
            "id": "8b6c6e7f-f054-4c4f-a35e-1e03b47ee8cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 193
        },
        {
            "id": "228cba2e-2d45-48a6-9077-636b75b93427",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 194
        },
        {
            "id": "a1b472f4-4b90-4b57-bc03-ee5343b5601c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 195
        },
        {
            "id": "6f1fdd66-b087-46b8-bfaf-f2736e70835c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 196
        },
        {
            "id": "f412812f-d08d-4035-8ac6-c2e60e92ec3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 197
        },
        {
            "id": "531ee293-567e-4aa9-a383-19a82d52846a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 224
        },
        {
            "id": "4c03e2ce-7619-45a6-ab75-48b2110db45b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 225
        },
        {
            "id": "dee777cd-5e0a-4d97-a564-d3b35a446614",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 226
        },
        {
            "id": "1ca22f4c-6194-47ae-8f03-36a2f0662af5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 227
        },
        {
            "id": "8f8f1fc1-9845-44dc-8ebe-cd57e801a894",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 228
        },
        {
            "id": "46fcc10b-b2c3-4583-bb99-5c02cfbfdabf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 229
        },
        {
            "id": "3586b6f5-566f-471c-92d1-df55dfbdb482",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 230
        },
        {
            "id": "bbdaf7e7-9e15-48d7-b258-28c649c75e40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 231
        },
        {
            "id": "ff65db55-1a7d-49b7-b2c5-2340ed964df3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 232
        },
        {
            "id": "ca6d572f-42b7-493e-bfaa-60f78e84e204",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 233
        },
        {
            "id": "2a264c7c-98d4-458b-9b49-96105db8a6a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 234
        },
        {
            "id": "94352a4f-97ec-4d3f-ac68-8f8541468dc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 235
        },
        {
            "id": "5581745c-f379-49f3-8b28-a87771268598",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 242
        },
        {
            "id": "93608e73-46bb-4fc2-83e1-df5a3c042431",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 243
        },
        {
            "id": "6c26dbf6-9f79-4daf-ab21-b2266010e2db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 244
        },
        {
            "id": "a902d79d-548f-4bbd-9509-883ef40dd83a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 245
        },
        {
            "id": "5dae3995-156d-4d60-9ab2-ac394db00c8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 246
        },
        {
            "id": "72c1d22b-342a-43cd-9597-1ebda8942e64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 248
        },
        {
            "id": "3136fb13-fd76-4aca-a390-e45d751db20b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 256
        },
        {
            "id": "078eed28-ff3b-40c7-931d-327a118bf83e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 257
        },
        {
            "id": "3e95ee0d-82ee-4d0f-bf9e-c7df85a0cc2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 258
        },
        {
            "id": "e8e1817a-e22b-4b39-8cf1-19205b1e544e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 259
        },
        {
            "id": "4be572cc-0361-4729-a863-4c6139ddc734",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 260
        },
        {
            "id": "56df0bb5-cf0d-4c8b-ab23-ddb601fcd8eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 261
        },
        {
            "id": "e37c2281-793d-4f12-a1eb-22f7a4253e9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 263
        },
        {
            "id": "215e5a91-9456-4bf3-b06d-268c1ebc86cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 265
        },
        {
            "id": "fc8af064-b4e3-4609-b86e-b4b2c1ad36fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 267
        },
        {
            "id": "1b083313-4cef-4c74-9d17-12ae77e14822",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 269
        },
        {
            "id": "f9c5a9e9-bf55-4684-8036-baefd3c49722",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 271
        },
        {
            "id": "e7a0ef27-bf75-4c4c-879e-d8461383558c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 273
        },
        {
            "id": "4c5b572b-db0a-4874-ac0c-16724e9f00b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 275
        },
        {
            "id": "e0789744-90f0-4516-8b7e-2846d40e85d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 277
        },
        {
            "id": "79b745ec-2963-4298-981d-7e93e36a2fed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 279
        },
        {
            "id": "a393c800-6cc1-4002-874d-84c36b1b0cbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 281
        },
        {
            "id": "0db20391-980b-4387-9bd3-401f5ee3f387",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 283
        },
        {
            "id": "4caea87e-f156-4619-b2e0-f0b1f88e8a40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 333
        },
        {
            "id": "b05c43bc-58c5-454a-af97-530c5098b4c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 335
        },
        {
            "id": "af06134b-77f5-41a9-a8d3-1a59758ff8ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 337
        },
        {
            "id": "79bc025a-fd56-44de-a6c3-0a9396214f12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 339
        },
        {
            "id": "0bd577cd-7874-4732-b1bd-4a4cb950ffcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 417
        },
        {
            "id": "d3a765c9-9bad-47f7-bb2e-eddb3b8eb40a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 506
        },
        {
            "id": "6215668f-b5a6-460d-8039-902700d29baa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 507
        },
        {
            "id": "4cbcd33a-a7ce-4d28-b766-f6dae70cb71a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 509
        },
        {
            "id": "9a8b4fd1-7824-4abb-90d1-c0e3470ba459",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 511
        },
        {
            "id": "dd54fd96-4904-40b2-ae3e-5b202c64541c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7680
        },
        {
            "id": "626f461c-c307-4373-9740-fb5f57c30089",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7681
        },
        {
            "id": "211d4229-816f-4daf-bf35-89741a532004",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7840
        },
        {
            "id": "636eb8d7-19b5-48c2-9740-ca7173f02df1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7841
        },
        {
            "id": "22234d2a-6925-4a9d-ba1c-ffd1826c6810",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7842
        },
        {
            "id": "f37cbeff-fc08-4881-aa6a-4625e9597280",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7843
        },
        {
            "id": "fcfc93af-b7d6-4dda-8c75-cb7b95fd175d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7844
        },
        {
            "id": "61380d70-7d63-4904-b6f2-0cd445d29e72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7845
        },
        {
            "id": "759465de-7026-4585-9051-55059febff20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7846
        },
        {
            "id": "8f6c85e3-7b5b-4b25-a8d1-d781fdc5bf8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7848
        },
        {
            "id": "7e481903-ef80-4990-8582-69c9e5d845c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7849
        },
        {
            "id": "f9fb1e91-04a0-484c-ad96-7deac7b9af37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7850
        },
        {
            "id": "0b1cc2a6-554b-44fb-ad4b-0eab9a1a81a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7851
        },
        {
            "id": "15d52541-c492-4ef9-bd04-31a99a16206a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7852
        },
        {
            "id": "5a2081b9-c99d-49af-b822-66506e2d2759",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7853
        },
        {
            "id": "c3bd9373-af00-4dbe-97be-9b5eb2c272b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7854
        },
        {
            "id": "d959a272-1ac1-4479-8241-ad02c8987b68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7855
        },
        {
            "id": "0425cfa3-54dd-4120-965c-daeca379cf9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7856
        },
        {
            "id": "b84f6390-3a45-425e-8f5b-0756f45d3d95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7857
        },
        {
            "id": "7e65e9e8-bd3e-438b-852b-83e3d2e616b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7858
        },
        {
            "id": "5e9aaa5e-2b58-484c-9595-88f1e82d8adf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7859
        },
        {
            "id": "4443f887-ac0d-410a-9bfc-7874d5b762e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7860
        },
        {
            "id": "bc87cce7-bf36-4025-a3e5-3fed0ebecba4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7861
        },
        {
            "id": "9633cf57-cc72-480b-8810-1188a61f58d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7862
        },
        {
            "id": "fd4e86be-821d-4c1a-9fc9-07a5734cc5dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7863
        },
        {
            "id": "241c2da7-af4c-4ecc-a19c-5e24fa722899",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7865
        },
        {
            "id": "b9b54582-3dd9-47ae-8505-b355abcc294e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7867
        },
        {
            "id": "53b6fd46-1c50-4dc7-afa8-559e85680ab5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7869
        },
        {
            "id": "e2579df7-1bb3-41b5-b328-a9d195a27be5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7871
        },
        {
            "id": "36031fed-1a8f-4f58-b074-2ca2135f0cf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7875
        },
        {
            "id": "fbc0e7e9-7717-408c-a1f7-3b823069fef9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7877
        },
        {
            "id": "9b46af92-901f-44b2-8f1d-a4c0fa525a5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7879
        },
        {
            "id": "c7520807-01da-4e1e-b645-22533651f8c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7885
        },
        {
            "id": "3e433bce-1431-4aa7-95e6-70e791d270ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7887
        },
        {
            "id": "e232e824-ad16-41d2-9ee3-7925facdb3ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7889
        },
        {
            "id": "2f2361ed-9ffc-4ba5-a6a8-1115a375deae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7893
        },
        {
            "id": "dcadb2c7-d506-493b-86a1-84c03f283c69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7895
        },
        {
            "id": "1882e4ca-7e07-43de-a431-77adb532a3de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7897
        },
        {
            "id": "498dbfaf-b754-44bc-a070-a688f8185e21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7899
        },
        {
            "id": "357198f2-25e8-4d16-9a5f-12ed6fb184f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7901
        },
        {
            "id": "d9ea6ad2-6463-4b4f-9faf-a7de9cfca8ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7903
        },
        {
            "id": "bcae63c5-d73a-4c40-8171-1c7ba8ee4547",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7905
        },
        {
            "id": "c1de6eae-59b5-4c7c-b8e8-1cf79a18145d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7907
        },
        {
            "id": "41409075-d778-4804-bbb9-df5f997dde23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8218
        },
        {
            "id": "8e096f38-67c4-4173-8a28-91767fc23060",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8222
        },
        {
            "id": "69138f13-8c17-4804-9a93-999f9b8edf56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 44
        },
        {
            "id": "73e792fa-e6e3-4c9e-a9cb-27648fc129f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 46
        },
        {
            "id": "8e6b01c6-2f01-40d1-9cff-1f4812bc8fee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 63
        },
        {
            "id": "749decd0-2efb-46ed-ad11-df0a977a0a99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 65
        },
        {
            "id": "9aff4ab6-5c7f-4408-84ee-edf7021088ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "16f1615a-a70d-4a71-9d5f-d8f16d23893b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 99
        },
        {
            "id": "ba522f36-ae2a-4e46-a184-8946793d3b4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 100
        },
        {
            "id": "883b321c-f84b-4563-9f36-a9e420985615",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "bbe8ad68-d56a-4235-b92e-b9580def8f70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "1b6c7252-306a-4ec3-ac57-a4e12704d24e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 113
        },
        {
            "id": "ad9759da-a4b6-4651-8b90-b3ca9837f92f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 192
        },
        {
            "id": "10dd7ad7-831d-43d2-80ef-e10786516ec9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 193
        },
        {
            "id": "1f55b61d-7b6c-4c89-9f87-53bf91391756",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 194
        },
        {
            "id": "02c662c7-0cf2-439f-9db0-922764b7d49c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 195
        },
        {
            "id": "00289e98-6c30-4472-8ff7-a88ef4a0ae55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 196
        },
        {
            "id": "e728190b-305d-4820-8937-8c702726a511",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 197
        },
        {
            "id": "22b8284f-ac23-4ca9-b67f-0de7f46a2fbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 224
        },
        {
            "id": "1ca61067-0bde-4b1b-9ab3-28449515868b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 225
        },
        {
            "id": "5c625cc5-1b51-40d0-983b-f05e48744c16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 226
        },
        {
            "id": "b8337e37-0422-4c8f-8d65-4c307b2f607b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 227
        },
        {
            "id": "c94493a9-88ed-4731-9d03-8dbf4203c183",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 228
        },
        {
            "id": "29453b45-9a78-40fa-ac28-b842055d28b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 229
        },
        {
            "id": "f4f67c5a-2298-44cd-9b18-89f96c1d8169",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 230
        },
        {
            "id": "be42b6f6-ee91-444c-a977-7053c4c37e99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 231
        },
        {
            "id": "7d9e0199-91e4-4f15-9f74-d02ed642cd37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 232
        },
        {
            "id": "f0677c11-1fb0-410b-bc9f-90aa9853b0e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 233
        },
        {
            "id": "a6ea9512-a2bc-405a-85f3-93a78fa60e6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 234
        },
        {
            "id": "252beaad-588b-41c1-bbe0-f27ab84b94d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 235
        },
        {
            "id": "cc40661a-c87e-4546-9758-f05e9f6ad9f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 242
        },
        {
            "id": "01ff1161-cf99-44ac-825a-216d7ceecdad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 243
        },
        {
            "id": "54ffdec3-0e47-4028-baa3-d76a78d49ebf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 244
        },
        {
            "id": "3a747b2a-c40f-47b7-a2e4-1b56e0c703aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 245
        },
        {
            "id": "0702f119-da2e-4128-ac35-c3ef60dd898a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 246
        },
        {
            "id": "e5c0e3b6-58ab-45f5-b32b-6291cfe4cc26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 248
        },
        {
            "id": "19e2db52-9af7-4ae7-be6d-ff678142a0d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 256
        },
        {
            "id": "87aead69-a632-432e-b095-6d29bc55f21d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 257
        },
        {
            "id": "e54834c7-077b-4acc-a12c-9cbd2f8d6157",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 258
        },
        {
            "id": "deefa5f6-2eba-4ece-a3c8-e1e10d0e7a8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 259
        },
        {
            "id": "5f119985-f410-4d32-a28f-733a054300b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 260
        },
        {
            "id": "d01f1327-b2c3-4504-8321-10998d806294",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 261
        },
        {
            "id": "1ec0aaab-5846-4da1-802e-e35055a07902",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 263
        },
        {
            "id": "f8241d9a-a09b-467a-ab5d-b691888463ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 265
        },
        {
            "id": "c72a193b-37ae-4eb4-a6ed-476ae6efe9f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 267
        },
        {
            "id": "7c64f86b-8b95-497f-9b87-eff5c8174f9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 269
        },
        {
            "id": "efcf1e0b-abe3-4e49-b4bd-2a91ddaad754",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 271
        },
        {
            "id": "889e45c6-9195-4dc3-a17b-bc4b18c6070d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 273
        },
        {
            "id": "dbe475d7-ca50-473f-95e8-11b5644e481c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 275
        },
        {
            "id": "2bae24fb-e7e2-453f-8999-bbe29e6540b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 277
        },
        {
            "id": "bbabf050-f864-482c-b3a3-e1de0c21a39b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 279
        },
        {
            "id": "d3e95cfa-00da-482c-89c6-4cd4e71b0222",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 281
        },
        {
            "id": "816f09e5-a132-4159-8c73-4aa87df2648f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 283
        },
        {
            "id": "9e5b8a72-8cec-46b1-b117-ba3f1a21de22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 333
        },
        {
            "id": "27e7c2b1-779a-4007-85b1-0d114d58acbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 335
        },
        {
            "id": "ab6080e4-35e5-4ef8-b0ba-3951861c9ef4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 337
        },
        {
            "id": "44391fb5-2d63-4c93-bcd2-81dc93e64298",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 339
        },
        {
            "id": "ce1e1c64-5979-4cb6-82d4-951ca86b415c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 417
        },
        {
            "id": "9461dd83-cce3-40d5-af92-a417b74e9387",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 506
        },
        {
            "id": "1b6edf35-21d3-404f-ac29-775a038b22d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 507
        },
        {
            "id": "9fae69e8-7965-464d-9c28-21f82b5d4f50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 509
        },
        {
            "id": "39d05b30-9fed-4a20-ad39-7d6b533f95b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 511
        },
        {
            "id": "b8a0f093-04d2-40fe-bed6-26cd11d1e213",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7680
        },
        {
            "id": "d4ff90bb-4eb5-48a7-a01b-0f588588cf6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7681
        },
        {
            "id": "e8477ee0-b0d3-4fe8-9ff9-d47874b4effc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7840
        },
        {
            "id": "b6ff7c11-3fdd-40af-b434-b87116f27415",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7841
        },
        {
            "id": "1887ccad-c041-4d7b-8b57-1f86c8be8667",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7842
        },
        {
            "id": "33fcfe71-ca55-4d34-818b-cd2ca0dcc998",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7843
        },
        {
            "id": "0e193f72-1d99-4575-a290-2aa047e1d767",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7844
        },
        {
            "id": "194c5ef9-997a-4019-b76b-9b23ec2dbb24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7845
        },
        {
            "id": "3d7025c5-03ce-484d-8409-2169a344713f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7846
        },
        {
            "id": "68f64b23-21d3-4a59-b5e5-98fd2dfa97b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7848
        },
        {
            "id": "f30af223-7b82-4462-8e69-f2cbed5a3ce8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7849
        },
        {
            "id": "60f11238-434f-4e17-a96f-a8595b46cd55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7850
        },
        {
            "id": "25f7e837-aafd-4f2c-b30c-424490e445c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7851
        },
        {
            "id": "18c7ebaa-e635-45b2-a4c6-d76d0b5030bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7852
        },
        {
            "id": "8b74e357-1124-4bb9-ad5f-219da8e5633f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7853
        },
        {
            "id": "980474da-d9e4-46b3-8e11-0dedb7c1545d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7854
        },
        {
            "id": "cdd3747b-31bd-4895-893c-abfad13da3f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7855
        },
        {
            "id": "3d633836-e07e-4c00-8250-b9dd10f12ae9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7856
        },
        {
            "id": "6755d477-4dc4-4e6c-85bf-dd0f2f2acc2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7857
        },
        {
            "id": "40c08845-b491-45b4-8965-db9fb5988c3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7858
        },
        {
            "id": "f680a6ca-8360-4639-bb11-26abb1db51a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7859
        },
        {
            "id": "d28d3297-11de-4262-b88b-9fa63c4eb9be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7860
        },
        {
            "id": "e0d23337-f090-44f7-a039-20fc0199be3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7861
        },
        {
            "id": "bc379490-e475-4cb0-a6cc-adb4b96a8912",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7862
        },
        {
            "id": "c6100684-b64f-487d-8309-2bb58514adfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7863
        },
        {
            "id": "32c1e1f1-5625-4b29-ac12-8dd7e245bd6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7865
        },
        {
            "id": "0495084f-d2f7-4983-b4fa-60853799f114",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7867
        },
        {
            "id": "6e9e0a40-eac9-4ade-9e86-1eab4c47cf3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7869
        },
        {
            "id": "8b5889ad-309d-415d-b1dd-7800e4059b0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7871
        },
        {
            "id": "37e5d607-5086-44d7-a96d-588e3d2f8f66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7875
        },
        {
            "id": "fb7faf90-6490-4c6b-ac8a-bc2b731c329c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7877
        },
        {
            "id": "e2e0de00-e79d-47a9-a105-37ad0bc7952f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7879
        },
        {
            "id": "6ca3eace-2020-44b8-8e78-bfd0fd802288",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7885
        },
        {
            "id": "5eddd859-4d19-44bd-bab8-1e76ee06221a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7887
        },
        {
            "id": "10d68d8f-e451-4dbf-aca6-8929dc67e791",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7889
        },
        {
            "id": "b31220bf-5b02-4d5a-9bf8-9b54b46848d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7893
        },
        {
            "id": "e54495e8-fb59-4cac-88bc-7e0e21fb563d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7895
        },
        {
            "id": "81c7c4c1-198c-4201-9887-42d264d790df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7897
        },
        {
            "id": "75f9cd5a-34d7-4cac-8f10-c96fa8bdefa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7899
        },
        {
            "id": "24efdcda-77df-4968-a67d-0c19fcf155fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7901
        },
        {
            "id": "86bd27a1-1afc-47eb-a85d-1e7babf0f79b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7903
        },
        {
            "id": "3ad371c6-8982-46e4-a303-b91103b38ad7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7905
        },
        {
            "id": "d2e7ec72-d1ce-47c1-8276-de08446d4064",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7907
        },
        {
            "id": "4c6ba63a-8a83-4f1a-b0f4-073937f9ca42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8218
        },
        {
            "id": "3e8fb45b-7525-4f42-a84b-1ac23b09946d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8222
        },
        {
            "id": "fc1cd172-314d-41d5-88ea-815e2616dcf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 67
        },
        {
            "id": "45eb6cc4-3275-43e2-aab6-54731813c707",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 71
        },
        {
            "id": "a52da1f0-2fb8-4c85-9f29-d2a94fe4f41d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 79
        },
        {
            "id": "3ffc42e1-8c14-4451-9c90-747b42c7b66a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 81
        },
        {
            "id": "b7b4232b-41dd-4625-8d70-e8eccd060364",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 199
        },
        {
            "id": "6e081eaf-4674-4515-bf76-c1dfb4e33bfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 210
        },
        {
            "id": "d98e326f-6900-4e3c-a3c2-93ac17e36ba6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 211
        },
        {
            "id": "603ca6aa-c99c-4406-8209-f75854ce61a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 212
        },
        {
            "id": "0ec5333b-9c8c-452a-8860-e704180b4f8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 213
        },
        {
            "id": "20ac5e66-2737-45d4-a84e-45a5f44515dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 214
        },
        {
            "id": "f36e9809-339c-4955-a72b-935bd2b4393f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 216
        },
        {
            "id": "497b7462-371b-4501-80d2-c32eac876516",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 262
        },
        {
            "id": "1fec30cf-29b6-41d2-9195-b5d71c355382",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 264
        },
        {
            "id": "41669535-d500-4bc1-859b-f59324f2415b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 266
        },
        {
            "id": "1bb10f7e-8b97-47b2-8772-b2bec7d5e691",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 268
        },
        {
            "id": "1c60a53e-6351-43a1-b12c-fbdef6f9d832",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 284
        },
        {
            "id": "999aab56-3cb3-419f-8421-688cdb66cf1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 286
        },
        {
            "id": "9e61d3ae-a17b-4660-82cd-36f2ca1e4a3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 288
        },
        {
            "id": "ac5b42f0-6b93-4825-9f48-57fbb4199ee1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 290
        },
        {
            "id": "e047ee33-41a4-4010-b7c9-c6136893e7a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 332
        },
        {
            "id": "d16fdc47-c519-410b-9400-e26aa2fb08b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 334
        },
        {
            "id": "2317dfe2-59bd-41d5-bb59-790645e3019b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 336
        },
        {
            "id": "494a874f-0735-4470-becc-770c8d13c975",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 338
        },
        {
            "id": "ec99fed0-bfc3-4955-b02a-e3cb60c12619",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 416
        },
        {
            "id": "73938ea4-bd28-43e1-aef5-e776aa9ad1c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 510
        },
        {
            "id": "64d76e2e-5e86-4d1f-83c7-7bb4a469b6f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7884
        },
        {
            "id": "81728ace-7b1f-41a7-91e1-b00bb5fa0d38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7886
        },
        {
            "id": "4cad114f-c21a-4fdf-baa0-517e72480be8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7888
        },
        {
            "id": "44e3f03b-e193-45c3-a2e4-cbc63dfe1f1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7890
        },
        {
            "id": "99175395-5772-4f1d-8476-604b5bb94ca7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7892
        },
        {
            "id": "80444ab3-0a4b-4ed5-a7c0-2a56efe06d46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7894
        },
        {
            "id": "566d4314-eba0-4689-abb2-5d03de1c5a3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7896
        },
        {
            "id": "b4dba3ab-9f94-4f88-9f19-f887a7f943b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7898
        },
        {
            "id": "784bdd49-31e7-42f3-b4c4-c1aea92377ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7900
        },
        {
            "id": "4aca8236-1745-408c-b8bf-bbb03271e9f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7902
        },
        {
            "id": "1c46bc00-df6a-4d73-99b5-390dafb8c6b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7904
        },
        {
            "id": "a965033c-c573-4d74-8b40-d625147246c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7906
        },
        {
            "id": "9bedd2d3-ec9d-4205-bdc2-750e17fa0cff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "33354700-aeb3-4d59-91fc-f9bc12daf486",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "1f2e1fde-876b-4ddf-93d1-687aa90cabb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 63
        },
        {
            "id": "865590c5-b592-430a-836a-8f7bcf9644cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 65
        },
        {
            "id": "65c91b53-70d4-4b10-963e-2d013a04bfac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 67
        },
        {
            "id": "7566dab2-1611-4456-9326-9f372fbe34b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 71
        },
        {
            "id": "c3bf361b-fbd8-41f2-8834-02c7f7634e88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 79
        },
        {
            "id": "69862339-b713-4541-a645-61c28f484a8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 81
        },
        {
            "id": "8395a604-c7c0-412a-aeb2-af075e23ba7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 97
        },
        {
            "id": "f9910f54-0fdc-45bb-8eb3-1ed2f94931eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 99
        },
        {
            "id": "c1c2b727-9a48-439f-8422-ac7aae018528",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 100
        },
        {
            "id": "8f5727f3-709a-4909-be3a-7ba643888b1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 101
        },
        {
            "id": "9ad21d9d-6da3-49b0-b9f5-abb74cbbb08e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 103
        },
        {
            "id": "096cd85c-ff22-41d1-9285-1dec2233c4ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 109
        },
        {
            "id": "aea8509c-fce0-46af-9d71-4e161618ce1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 110
        },
        {
            "id": "f3bbd442-7e69-43fd-a788-7cd17614033f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 111
        },
        {
            "id": "b90411f0-66b5-4efa-8e07-770e452da138",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "fcc1b04c-77b6-4bd4-8dec-c8d5fc124d5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 113
        },
        {
            "id": "8158bf87-42ec-4286-9b23-2937973a22e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 114
        },
        {
            "id": "cbc2b53f-b4d4-4991-98cf-66c6f70ac902",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 115
        },
        {
            "id": "80b15e43-01a3-4a99-99ae-84e832858a5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "29886303-f9b0-4b2b-bbd9-c23671eeac10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 122
        },
        {
            "id": "6b729347-a65d-4c65-8be0-053a7ea60f2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 192
        },
        {
            "id": "a842e89f-5f5f-41bd-97c6-2d48cc94769a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 193
        },
        {
            "id": "c84db6b0-4809-4dd2-8b28-fb55c2ed6c45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 194
        },
        {
            "id": "664a2300-38e6-4188-b64a-208bea0677b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 195
        },
        {
            "id": "91f1980b-3875-4187-8278-ead546e87d5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 196
        },
        {
            "id": "5f7e4373-f002-4410-9b47-c77204980db1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 197
        },
        {
            "id": "a0a2792f-92b5-4052-8fde-2ea1d2e1852b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 199
        },
        {
            "id": "d5576213-4fe1-4c5e-a88c-23a398c2f0db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 210
        },
        {
            "id": "2e00b6bd-e898-4f5a-98f6-9b5cc1f01cce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 211
        },
        {
            "id": "02f68e08-a191-47fb-9180-a3d4eaefefd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 212
        },
        {
            "id": "0d6b17fe-f8b0-428d-ba34-2fee4ca0c2d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 213
        },
        {
            "id": "5557fe2b-fc13-4ad6-919c-e45d5038d55a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 214
        },
        {
            "id": "be0c7e8c-b59e-4a8f-a81f-c8c4d7cf6dc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 216
        },
        {
            "id": "bf8954e0-e298-4a8f-8c3c-c44fc2c4b1b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 224
        },
        {
            "id": "234cdca7-9db6-40a5-add4-47105169c4d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 225
        },
        {
            "id": "81e4b022-a20d-4715-993d-9aa552e23747",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 226
        },
        {
            "id": "e78550ba-2d20-45fc-a388-6e2d50bba7bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 227
        },
        {
            "id": "87b62458-521c-4970-b738-6bcfb96fb2ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 228
        },
        {
            "id": "2b11236f-a3ad-4958-8db1-8214a7940d06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 229
        },
        {
            "id": "c4fbd526-527f-4ec4-9b30-d729209a1fd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 230
        },
        {
            "id": "19ec75ef-f067-4206-adbd-8e2b22e4c561",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 231
        },
        {
            "id": "63c6896f-c3e2-46b3-ba33-ffe6eed69292",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 232
        },
        {
            "id": "cd4a2362-3932-423c-b0d4-a3a0b648e85b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 233
        },
        {
            "id": "ce532d9b-0f4d-4925-8a83-a3e398c1e408",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 234
        },
        {
            "id": "071296d4-e891-4eb3-a9b3-1047d9877507",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 235
        },
        {
            "id": "ea5c1ece-e706-4529-bfb5-0f175d35ff7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 242
        },
        {
            "id": "5d1a7e58-a891-4b20-b501-5704078cd1a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 243
        },
        {
            "id": "ce170136-82dc-4c70-94bd-6fdc9efd4a5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 244
        },
        {
            "id": "257fbecd-f758-4df6-9532-7d3b1e441ebb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 245
        },
        {
            "id": "676ce29e-a58f-4797-a2d9-e69de85755c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 246
        },
        {
            "id": "40e1171e-f911-49ed-b14c-ef6ce4953541",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 248
        },
        {
            "id": "54b66eae-fdf0-462d-ad25-6097e565d07a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 249
        },
        {
            "id": "be4f867f-e212-4dcf-bd96-defb4552e761",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 250
        },
        {
            "id": "327724b0-d238-4595-a984-5c1d659684aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 251
        },
        {
            "id": "7a70cda5-9280-4c1c-9e9f-c886b6b4d6dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 252
        },
        {
            "id": "0b25866d-6d0e-4127-afb7-04d294061bc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 256
        },
        {
            "id": "97c0b2de-9ec8-42b1-8d21-dc4df0d0af53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 257
        },
        {
            "id": "c323051f-f04c-42a9-b655-83f1ea6b4bcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 258
        },
        {
            "id": "e5635fc6-30d0-4666-a1b0-c55d5aaa315f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 259
        },
        {
            "id": "d5ae815a-a641-4f5a-a75f-586e27b7f29a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 260
        },
        {
            "id": "af28c26c-6bbb-4fdb-8e1c-bce7d1d2be15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 261
        },
        {
            "id": "de4213a7-80dd-4cc3-bee4-bd3e096efa00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 262
        },
        {
            "id": "4e527d75-148f-42f7-8af0-df0fbdbd42b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 263
        },
        {
            "id": "da5c79fd-5006-49a1-8b6e-a45dcb9116dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 264
        },
        {
            "id": "9164434b-a9c5-4ff5-9313-7d08c60d9f5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 265
        },
        {
            "id": "3ef74812-c521-472e-af72-881ec6395e0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 266
        },
        {
            "id": "3f3ec03f-ed0e-49ce-9505-62a04a437143",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 267
        },
        {
            "id": "23155bc3-cebf-478c-baae-772449034ea4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 268
        },
        {
            "id": "f4ab18b9-136c-4be7-8165-457305f06c8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 269
        },
        {
            "id": "f200b7d9-cb32-45ad-bbab-ced8071c0f70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 271
        },
        {
            "id": "f2a0f943-0733-49e3-8f70-4f0cf9f0941f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 273
        },
        {
            "id": "e474925b-3b07-4cfe-af9a-d085b5f9e0fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 275
        },
        {
            "id": "9dcb6ea2-a18b-44d6-bb24-eed23bbc7edf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 277
        },
        {
            "id": "4a51467c-c0d0-4ff8-9a52-b1f140dc62f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 279
        },
        {
            "id": "0121132c-756b-4f7f-8501-906b5ca626b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 281
        },
        {
            "id": "8856b643-cd91-4146-af6a-bf4207f35afe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 283
        },
        {
            "id": "3f677b61-8e25-4b19-9a96-5d415a3f2a99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 284
        },
        {
            "id": "7e2dc43e-7c9d-4afa-886c-1da3d5937bb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 285
        },
        {
            "id": "06f6d378-5f41-41cb-b69c-540cec4b5923",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 286
        },
        {
            "id": "3c265702-7fa6-4273-bf01-e2c253ec215a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 287
        },
        {
            "id": "3fd9f040-b225-4217-b5ab-0fdc20b40b65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 288
        },
        {
            "id": "0b171c5f-0b20-4d4b-8753-f0b44f90ed1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 289
        },
        {
            "id": "7f812186-6941-41f8-81e3-2777a436e761",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 290
        },
        {
            "id": "c76a7ac8-4fa2-41e0-a487-962b524f6c25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 291
        },
        {
            "id": "810e69b1-6ecc-427c-96c2-a4cd5d069922",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 312
        },
        {
            "id": "2b30045e-676d-4a11-9d9d-d801f1c26c48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 324
        },
        {
            "id": "17349e34-a41f-4978-b9dd-5b6bf9472e26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 326
        },
        {
            "id": "357fb289-40f2-49d3-a25c-e2f52ad78a82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 331
        },
        {
            "id": "125a9d25-db33-485c-8a05-0a28c9cdf644",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 332
        },
        {
            "id": "fac9db4e-8de2-48c7-ae0e-768487e3cfbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 333
        },
        {
            "id": "a6c5bd09-372b-4adf-b7fe-550b633a773f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 334
        },
        {
            "id": "5613660d-96a9-4201-8e46-a4304d6ec0c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 335
        },
        {
            "id": "2748f2fd-9ff3-458c-bf2c-5a1c370593fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 336
        },
        {
            "id": "1791cdba-c935-4bbc-ae8a-dc8f57aecae1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 337
        },
        {
            "id": "b4b93b20-4977-4fd6-bbeb-62873ebc56c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 338
        },
        {
            "id": "0aa7ce6f-ec3e-4bd6-80ab-a34cb9a2890f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 339
        },
        {
            "id": "572d7f03-5284-40f6-b724-01742976c763",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 341
        },
        {
            "id": "e60482ea-12a7-4fdd-807e-f3e8a0d6cd3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 343
        },
        {
            "id": "196aa744-f053-4dfa-be39-46cb27c792d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 347
        },
        {
            "id": "3ad7b38b-93c7-4192-8ce5-ae1b83a7adae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 351
        },
        {
            "id": "3d7133b0-8f80-4bb3-8349-2033b036ef99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 361
        },
        {
            "id": "b1cee5b5-a106-45cc-949e-3ec20250aa9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 363
        },
        {
            "id": "20a2d5eb-cb35-4072-a620-20cb632a39ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 365
        },
        {
            "id": "40177eb2-4f36-4092-8a7f-8aa37bff84b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 367
        },
        {
            "id": "7c58c179-705d-438d-b412-b7406abc547c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 369
        },
        {
            "id": "3afa0e64-d113-4295-b4c1-50ac4cba3598",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 371
        },
        {
            "id": "eb82b6ce-c19e-47c9-85e7-70f0af011659",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 378
        },
        {
            "id": "e9891e23-189c-4f18-b9c0-81de76df2c36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 380
        },
        {
            "id": "66bd9c50-2446-4fd0-8f15-6a0b46cfb10e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 382
        },
        {
            "id": "9f294180-c7b4-479c-b7a8-044fe3d6c190",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 416
        },
        {
            "id": "5f97653a-4b7f-4e4d-b612-0e4dbc5d5368",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 417
        },
        {
            "id": "da1b642d-efe3-4f9f-84d3-3764c889001a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 432
        },
        {
            "id": "4265e540-7ceb-4a47-b7df-552c931e3efd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 506
        },
        {
            "id": "4e51d246-9d1c-455a-9c2a-948d9044146a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 507
        },
        {
            "id": "e36b1f30-f39a-415a-a165-66187007dd5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 509
        },
        {
            "id": "705389aa-3e78-429b-8183-6bc56656d4e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 510
        },
        {
            "id": "a6561ebe-a2d5-460a-b4d2-882600641cd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 511
        },
        {
            "id": "87cecdac-5d08-419a-b287-326bd35a4d79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 537
        },
        {
            "id": "b837a210-b528-4619-bf62-fceeb7993e40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7680
        },
        {
            "id": "32d558cc-2229-453a-8fa2-ce312673dcad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7681
        },
        {
            "id": "40e4753b-1a30-49da-82ec-f549ae896319",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7743
        },
        {
            "id": "f05c0111-bc37-4b98-a764-d67fba6fe3c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7840
        },
        {
            "id": "93659753-9688-42ba-ad5e-4c5e5ca228c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7841
        },
        {
            "id": "74fb69ab-9b3f-42fc-a37f-85acf31d7483",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7842
        },
        {
            "id": "de781e4f-467e-4cfc-8ca4-cc789a60b011",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7843
        },
        {
            "id": "f5f7fc18-aef7-40ba-85f0-87b6db5544d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7844
        },
        {
            "id": "98f34985-6548-4d49-9687-c5b9640a8849",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7845
        },
        {
            "id": "5a8919f8-40de-4170-bcdb-e8cb0b586fe0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7846
        },
        {
            "id": "868faa79-8123-4fe4-a35f-a8c2bd8d4979",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7848
        },
        {
            "id": "8325323f-3f79-4306-b845-0fcbf9a0060f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7849
        },
        {
            "id": "5ded26f9-88cc-47cf-9899-5368628a10fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7850
        },
        {
            "id": "f525aba7-3760-4030-b60c-cee47e7f2381",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7851
        },
        {
            "id": "e070e6c2-b802-428e-b582-258d5732627d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7852
        },
        {
            "id": "25e9a6c9-a3af-46ea-bfdb-05d4d0d527ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7853
        },
        {
            "id": "6d630583-d637-4a7b-b1a7-f7f70febfa15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7854
        },
        {
            "id": "2b247ffd-73ba-40ec-896f-efc8e2916d9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7855
        },
        {
            "id": "6426224d-1230-4007-8171-8e0c2a1af630",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7856
        },
        {
            "id": "26dbcbed-da71-4e93-9a02-f61c536e3365",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7857
        },
        {
            "id": "4166f4c0-da26-4f15-ab74-f952ba84bb06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7858
        },
        {
            "id": "551d6172-4426-48f7-ace9-c0c241d27f61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7859
        },
        {
            "id": "b245c23e-64d7-4cf0-b666-aec8ead68554",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7860
        },
        {
            "id": "abc24b2a-9755-4818-b237-96b907ced61b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7861
        },
        {
            "id": "cc5f3217-e180-4b09-9a9d-f781faec834a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7862
        },
        {
            "id": "20d66069-b6e8-478e-bb23-a7177c3f8685",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7863
        },
        {
            "id": "8ed5cfc4-79d8-46cc-af2d-104e99c7e108",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7865
        },
        {
            "id": "7f28bc01-4e6e-4f25-80f0-95987e27493f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7867
        },
        {
            "id": "e87268a3-c4db-4ae0-b864-068155b77ef4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7869
        },
        {
            "id": "07c0a47d-cc54-42da-b5e3-9f1423e2329d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7871
        },
        {
            "id": "5dd96717-f94e-4d75-b6fe-808b449f89bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7875
        },
        {
            "id": "f63616df-6e14-4c32-97a2-f2fa0097e235",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7877
        },
        {
            "id": "c02758f9-16e8-4219-bda0-7cfc873727a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7879
        },
        {
            "id": "3d673ceb-6a48-42f0-9ab8-d78b54cdcf19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7884
        },
        {
            "id": "65834ea9-e250-4150-b07e-ad7e119fad7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7885
        },
        {
            "id": "97b4e9f5-b868-4529-bd24-8dab9c44cb72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7886
        },
        {
            "id": "d0a57e0c-e799-4b09-8c75-88300e93569b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7887
        },
        {
            "id": "eced2005-fcb4-4810-ba46-bb595bea03b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7888
        },
        {
            "id": "2ec0ab5c-18a8-4857-b7a9-b3d66b7da415",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7889
        },
        {
            "id": "7892c8ab-79b2-447b-b084-010160ceb469",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7890
        },
        {
            "id": "7dbbba83-a164-4cec-a311-c5b866912942",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7892
        },
        {
            "id": "099f24d1-3614-4ba3-a574-8c5930d393cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7893
        },
        {
            "id": "8446cb8f-c1e0-45ee-9018-3dc6e7d5ba1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7894
        },
        {
            "id": "d517d2e4-1b36-4af7-a02f-85e12b56a2cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7895
        },
        {
            "id": "7d9805eb-a429-4153-91cd-22d800c9e7b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7896
        },
        {
            "id": "54eb7f5d-2408-40ba-8ac3-9b6e322f8d23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7897
        },
        {
            "id": "6b8184ba-61e9-42e8-80f8-3a2aabce4e8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7898
        },
        {
            "id": "e251f6d1-b0d6-483b-802c-f5b4f0699480",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7899
        },
        {
            "id": "e73b91dc-beb6-430a-931f-cf024e9f6d00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7900
        },
        {
            "id": "e8785768-db37-4be3-a8ab-e1c9d2b75496",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7901
        },
        {
            "id": "7c3f9594-0610-4c6b-ac21-84caadafd57d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7902
        },
        {
            "id": "8109d391-b438-4c76-8442-a4aa688ca5e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7903
        },
        {
            "id": "1fecb0a9-d52c-4c59-9889-4cf6971adeff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7904
        },
        {
            "id": "1685e631-77a4-4249-b395-9e33f91970fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7905
        },
        {
            "id": "42917292-3ff3-444a-b6fd-dd543c4f2ec0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7906
        },
        {
            "id": "29c1e63a-b494-4ba0-9436-ec76db1148e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7907
        },
        {
            "id": "77b6285f-814d-48ac-a7d1-05f77b2ec4f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7909
        },
        {
            "id": "5d2b5d14-5488-49d1-936c-1f42ab80bd24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7911
        },
        {
            "id": "017a5697-0178-424e-ae00-2d3cbfcc910f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7913
        },
        {
            "id": "f8d2df3d-b7ba-49d0-8c7f-6e464ab609ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7915
        },
        {
            "id": "2779b27b-d3fb-4e51-b98e-63142fbea07d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7917
        },
        {
            "id": "68fd21d3-57ec-4aa4-a492-90b78c3fab45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7919
        },
        {
            "id": "d7e26f78-338f-45bb-93aa-27a2085ceee1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7921
        },
        {
            "id": "35c1923d-87f8-4fdb-8d51-039577b77dc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 8218
        },
        {
            "id": "c10a3904-d46e-46c9-ad4d-7954f1a4e181",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 8222
        },
        {
            "id": "5473197e-98e9-488a-a1de-6e89b000b7b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 91,
            "second": 74
        },
        {
            "id": "183f7c69-927f-4320-8d54-b83f9d615d68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 118
        },
        {
            "id": "4f8eb761-3b7b-4d7c-aff7-0b348ae4d271",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 119
        },
        {
            "id": "3b76251b-2e5f-4950-be91-a5199dffe078",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 120
        },
        {
            "id": "f46f25f9-301f-4455-a72d-87ebe17ace82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 121
        },
        {
            "id": "a93658a7-7a22-42f4-95d4-8aa53d7e9614",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 253
        },
        {
            "id": "24126516-9445-4ffe-8629-982074eac1db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 373
        },
        {
            "id": "539df9d7-20a6-44e0-8cbe-63d3045510c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 7809
        },
        {
            "id": "b25566c9-4fa2-41cb-81fa-551db0f30c93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 7811
        },
        {
            "id": "bae3e50f-4c13-45c2-be0b-850c7a400ebe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 7925
        },
        {
            "id": "6fa2ef2b-f808-4713-9e13-e2e337172528",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 34
        },
        {
            "id": "54287634-aa44-47ff-8922-9e54b00b5fa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 39
        },
        {
            "id": "baafe0ea-70fe-47ee-8038-174c6bc7b823",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 8217
        },
        {
            "id": "607335dd-62ec-41f4-b286-29aaa6d885b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 8221
        },
        {
            "id": "d75183ca-f490-434c-a970-61f5db44b012",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 118
        },
        {
            "id": "cbc2119a-8a0b-4bf0-ab77-529c069ab7c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 119
        },
        {
            "id": "edeba093-6652-486a-aa6e-83a1335fb0f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 120
        },
        {
            "id": "7291ed83-47cf-4bbe-a60e-a71b99eeef87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 121
        },
        {
            "id": "52b09baf-161a-4c32-963b-d543a0645041",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 253
        },
        {
            "id": "1996aaeb-69a2-405d-8ffa-d4f4c417aea3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 373
        },
        {
            "id": "f4712f0c-059c-4d1a-9491-1409ef97f711",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 7809
        },
        {
            "id": "7479a180-b9ef-47ae-817b-e6f7e3936160",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 7811
        },
        {
            "id": "985d91d7-8551-42f7-8db7-b50065e616d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 7925
        },
        {
            "id": "7acc0ac0-20f1-40f8-a8bf-7489474a09ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 34
        },
        {
            "id": "03e37a02-ef69-4e97-98d4-afeac4c3a3cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 39
        },
        {
            "id": "c7c7ce56-fdde-4ffd-beb2-54ac19ade899",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 8217
        },
        {
            "id": "1ac63e1c-03ed-4afa-aefc-1b728b6212f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 8221
        },
        {
            "id": "7c918b88-c2b8-431b-8e5d-73b8d59707b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 99
        },
        {
            "id": "3b599f25-afee-49c6-94e2-ad4e651acccb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 100
        },
        {
            "id": "eebd552b-6b83-43ea-88f7-6581d25e6e89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 101
        },
        {
            "id": "6cfec20e-130d-4e47-b3ab-d5c1c0d4451d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 111
        },
        {
            "id": "d294c5e4-d2cd-4604-800a-3017a27c5ed5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 113
        },
        {
            "id": "f3bb7154-152d-4c2d-a025-d9ea8ca198a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 224
        },
        {
            "id": "db679e1b-9dfa-41cb-8bdf-a92269892f2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 231
        },
        {
            "id": "ce47efb5-850c-4432-be48-f2f869e7df8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 232
        },
        {
            "id": "02784517-b88d-438a-b7ac-ac08ab6a211d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 233
        },
        {
            "id": "7797f319-6a81-4368-9daa-85672f30b2e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 234
        },
        {
            "id": "54c4f4d9-0077-4ec8-ba97-dc5b22e2aa35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 235
        },
        {
            "id": "4b782aa7-df32-4462-944a-f0526306c1cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 242
        },
        {
            "id": "763370cc-612e-4d40-9b2d-0b79b27cb462",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 243
        },
        {
            "id": "48529bad-394c-4b1e-aea2-5e3108821443",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 244
        },
        {
            "id": "717baa2e-c352-423a-a617-816b4f444243",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 245
        },
        {
            "id": "8afe68e3-b2d1-4f58-a3eb-960f80ab9bc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 246
        },
        {
            "id": "96a73c23-b0a0-4f6a-a51b-b6b3d3281ed6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 248
        },
        {
            "id": "362a99a0-d639-4e5c-864c-bc8dba412adc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 263
        },
        {
            "id": "cdacce86-72ac-48a0-a8c0-4964e1a9b3e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 265
        },
        {
            "id": "c449a6c6-af2e-4388-8e2c-cffa6c8108ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 267
        },
        {
            "id": "b10197d5-d0e7-4cf8-b7a8-2fa553be3fef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 269
        },
        {
            "id": "41733907-29be-4280-b3c1-78f9c736132c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 271
        },
        {
            "id": "98b1fc77-f197-481d-a1cf-dafd6953fae3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 273
        },
        {
            "id": "2a4156ac-e37c-4128-8898-bb65319e8ead",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 275
        },
        {
            "id": "e98058a6-f4ef-4412-87ef-cd388fdfc2b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 277
        },
        {
            "id": "9a2b741d-b046-4ce3-86d4-eb80f629cf76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 279
        },
        {
            "id": "c3ecad85-0201-4b2b-83fc-6d6f200133b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 281
        },
        {
            "id": "af9eb75a-94db-4700-9e9a-815364e27334",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 283
        },
        {
            "id": "a8b0b219-d089-48c6-b511-8dfb76b2880d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 333
        },
        {
            "id": "bf3d9d86-96e3-4a3a-adc3-070678b42876",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 335
        },
        {
            "id": "7d5553cc-31a1-4cff-8506-19042af8adc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 337
        },
        {
            "id": "abb2ca7d-a581-49ff-a0af-5f6716ae9bbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 339
        },
        {
            "id": "91e49c07-dc7f-485c-a92a-af29925f336d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 417
        },
        {
            "id": "3810e8a2-7d75-44e5-b1ea-b85b44fe773d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 511
        },
        {
            "id": "8d59b3b3-49d1-4ccc-97b0-062f5c92a361",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7865
        },
        {
            "id": "5f21e8a9-b362-40e8-bd56-99c72b79fec6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7867
        },
        {
            "id": "0c1d4990-cb7e-43ed-94cf-b8281fec2bf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7869
        },
        {
            "id": "dcaac8a1-5f10-47d6-ae8f-e5fbf298ff7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7871
        },
        {
            "id": "566b37bf-9881-4b2c-a7b6-8863d2db24da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7875
        },
        {
            "id": "e8e3d4d8-d324-4326-be5e-58b256279136",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7877
        },
        {
            "id": "2c325623-5ba4-4c54-b174-d57082fc0fbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7879
        },
        {
            "id": "c6311a4e-f7fa-4450-88a8-06f5d24ab284",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7885
        },
        {
            "id": "e11f35c1-2ce5-40f6-9067-027bfec88889",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7887
        },
        {
            "id": "5c3ed71d-8cd4-471e-a36d-69c0c8ccaaf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7889
        },
        {
            "id": "45a9b86a-ed60-40b0-81f9-63ef0d5ccae3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7893
        },
        {
            "id": "3d9ef258-fb12-4ad3-b4d4-ead4c26a7bac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7895
        },
        {
            "id": "b467d7fe-a5ed-4524-a7d8-e23637912733",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7897
        },
        {
            "id": "7ac876a3-5e0c-43d1-8d56-fe13da2293b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7899
        },
        {
            "id": "112147ac-2b5f-4549-9241-e453f88bfadb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7901
        },
        {
            "id": "503d1d3b-b9ba-42c7-a411-4d5479dd67c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7903
        },
        {
            "id": "e9e1872d-983c-4a74-99b1-e7f2d50639e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7905
        },
        {
            "id": "20252043-f33c-48de-b034-2a825c6643cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7907
        },
        {
            "id": "bad724a4-8602-4783-a25f-054851841d2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 118
        },
        {
            "id": "1c09da17-a90f-4132-9376-9850e75bb5c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 119
        },
        {
            "id": "cded0d72-9d70-4a0a-a4a0-bb1b6ca7a009",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 120
        },
        {
            "id": "965620d9-8c3a-4b41-ba29-8fd1c47f520a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 121
        },
        {
            "id": "9511fa2f-c0eb-46fb-9d31-c11ab9bb6a8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 253
        },
        {
            "id": "51b0ca13-89bc-437e-a510-438d0c734eb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 373
        },
        {
            "id": "b35e20ad-a8e5-401a-864c-60d2b9223f3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 7809
        },
        {
            "id": "9c139e7d-49da-470f-994f-4d88234290fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 7811
        },
        {
            "id": "2e194f21-2122-45a1-904e-0b181bfcdf23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 7925
        },
        {
            "id": "bfa4fbc4-41b5-4b1d-b554-aeb0dbac35b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 118
        },
        {
            "id": "9842ebad-b84e-4adc-80ec-58625368f7c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 119
        },
        {
            "id": "e0cdc0aa-9e2d-450f-8b2c-64eeac32b7ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 120
        },
        {
            "id": "e4ceae4d-821d-447d-b5ee-1b85e4180bc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 121
        },
        {
            "id": "7aa7d76f-079b-424d-b9dd-4e51576c9f0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 253
        },
        {
            "id": "23559c18-b307-46f9-a488-a9ad802a421e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 373
        },
        {
            "id": "a6e95c18-90d2-4753-bae5-ba0bcba66f00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 7809
        },
        {
            "id": "34f1fe6a-12ab-4a3c-bcc4-27a257856b16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 7811
        },
        {
            "id": "97c6543f-ec1e-4b64-adad-5e67ea4c35fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 7925
        },
        {
            "id": "0c31ecac-a1af-434b-8379-dd329a7c8859",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 34
        },
        {
            "id": "a2371152-9fda-46b1-bb61-ee0916cf9b36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 39
        },
        {
            "id": "45cf7026-4c9e-4f62-81cd-d1a69a1b6a58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 97
        },
        {
            "id": "38363b64-b8c2-4a09-a3fa-e01626d0d4c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 99
        },
        {
            "id": "42cfc52b-442c-4399-83ef-edd371de9c3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 100
        },
        {
            "id": "af5b61ea-78dd-4df2-ad3d-d83cac50bdc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 101
        },
        {
            "id": "4fae095f-3195-4cd3-b97c-01b3d8e1e01f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 111
        },
        {
            "id": "1efd0ae3-fac2-48ff-9877-85f7096f3e7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 113
        },
        {
            "id": "619d10da-7a54-43a9-8f4b-d22997b9067b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 224
        },
        {
            "id": "45f438fb-5698-4354-9519-3e657a826677",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 225
        },
        {
            "id": "9beab264-0497-47b1-a27c-a64919165501",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 226
        },
        {
            "id": "8a7301c8-5e38-4a01-a378-fec7bc89ed56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 227
        },
        {
            "id": "22597d32-b13b-4af2-9aa1-fef94143afed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 228
        },
        {
            "id": "fb8c08b6-8fce-4d01-ad8e-b078b2588eb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 229
        },
        {
            "id": "6d5f442b-60ad-4a62-a1d2-df3fc2d1a839",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 230
        },
        {
            "id": "1a639b78-5401-47c5-95d6-f474c56ca92e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 231
        },
        {
            "id": "acbdc9a3-bbcc-43df-aecc-7a82f6e4db5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 232
        },
        {
            "id": "39b5bc1d-b8c2-459f-8a96-a3667fcb0d50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 233
        },
        {
            "id": "2102336e-4e7b-46a7-86f0-2d600b6e79f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 234
        },
        {
            "id": "c776a02f-6bf9-44e9-8f85-f91798f9acce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 235
        },
        {
            "id": "c837e42b-8264-4d05-a4cf-1b702f9ad62c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 242
        },
        {
            "id": "e4214b74-3bc9-47a3-a3c2-2fdd784e535a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 243
        },
        {
            "id": "c101b148-2295-4d45-b621-04bc4b5d6bf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 244
        },
        {
            "id": "e4603e14-a90f-45f7-b9ed-d6ae390586f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 245
        },
        {
            "id": "71063a92-58bb-44ef-bd07-43c27987efd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 246
        },
        {
            "id": "58de1af0-10d2-44a9-bb28-2192da432e8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 248
        },
        {
            "id": "7b983082-20c6-4979-b58a-f9a7b38599c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 257
        },
        {
            "id": "a0aeadb6-5979-44c0-a232-aac961326409",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 259
        },
        {
            "id": "81a4c8bc-3f90-4fb1-86a0-e0da48b6c773",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 261
        },
        {
            "id": "e13a03e9-271e-4073-8ad8-31ad8a3cb16b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 263
        },
        {
            "id": "b105b39e-b6fc-4523-bc0d-e7703bd4b9ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 265
        },
        {
            "id": "f081a323-0aab-43b5-852a-ffe7bcf18e73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 267
        },
        {
            "id": "2be4eefd-e1ff-47f3-acbd-3d6d40ca505d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 269
        },
        {
            "id": "929d70c6-37ac-4e66-b0e6-75a6c0f1a55f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 271
        },
        {
            "id": "da8af907-e59b-446c-af43-b4f52491c5fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 273
        },
        {
            "id": "cde5ef47-78e3-4746-b61d-345ada95cbea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 275
        },
        {
            "id": "21bee13d-30f2-4205-a674-2a3457c5c112",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 277
        },
        {
            "id": "33af1353-e27d-46aa-b82b-d77565f6b5b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 279
        },
        {
            "id": "e1af41a7-d808-4ff6-bc14-436e8f3edd31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 281
        },
        {
            "id": "2f4b0974-67bb-4ba5-b06c-7d2b2007a84c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 283
        },
        {
            "id": "b9f6a44d-9297-41c2-88d7-b07aa804f5e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 333
        },
        {
            "id": "bb97f7c6-18d4-4944-9394-0d17569b1257",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 335
        },
        {
            "id": "611c7f2a-aecd-4c9f-b7c1-50193f21d28e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 337
        },
        {
            "id": "300a05ce-7e84-4505-8216-84b0b7b79f05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 339
        },
        {
            "id": "19309f10-f30c-46f1-8518-41a88c2d1c87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 417
        },
        {
            "id": "c4c82a40-3c3f-437f-8356-19c44de7b17d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 507
        },
        {
            "id": "cbf7f3b5-f787-47fb-ba19-ae81342a46ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 509
        },
        {
            "id": "d1ca79cd-0c29-4db5-9e83-c1c85ef0d4d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 511
        },
        {
            "id": "c7df46d8-cf3d-4ecc-acf7-3a268ce52fc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7681
        },
        {
            "id": "2fe7b775-af48-4068-a14d-67f4230f176c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7841
        },
        {
            "id": "57fed419-65a9-440b-8d67-0ba88c67ffb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7843
        },
        {
            "id": "53cacde9-8135-405c-bdd3-b25e86a825ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7845
        },
        {
            "id": "8b1edb67-c95f-40e8-804d-39b1795082ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7849
        },
        {
            "id": "5d0d55d6-ad77-41a1-9928-bddf1c44ae97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7851
        },
        {
            "id": "d23d1032-021b-47a8-9f06-42b16f6c18eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7853
        },
        {
            "id": "386e30c1-0710-4a5f-932c-65101dc53a67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7855
        },
        {
            "id": "bbe39bf8-8aad-42be-87e2-ce4583285224",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7857
        },
        {
            "id": "14eee2a1-2f04-423a-ab32-d6d5ffe9a82b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7859
        },
        {
            "id": "e4971c96-5a40-4f0c-aa76-b308f1c4b7a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7861
        },
        {
            "id": "54ae1541-ab75-4ac7-8cf7-52225fc92ab2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7863
        },
        {
            "id": "91d57066-7c78-4544-b3ea-97bbe79f2180",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7865
        },
        {
            "id": "6fbdb518-927a-46d2-b21c-179050d2f476",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7867
        },
        {
            "id": "70e7b95a-ba14-4f52-9193-d00f598ffb89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7869
        },
        {
            "id": "0aa73ba0-5bbb-4311-ae25-4a6dfeba68eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7871
        },
        {
            "id": "64e037ea-1fa7-45fa-97b4-0ab5afe8e47d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7875
        },
        {
            "id": "990579b2-d78c-4d72-9bdc-bfbcff42d07f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7877
        },
        {
            "id": "32c9c049-e86b-4b1b-9f0a-0528496f17a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7879
        },
        {
            "id": "132c8978-e03b-4918-8a76-b010c8a8706a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7885
        },
        {
            "id": "70584e9b-8523-4a5a-91c4-66742cd87307",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7887
        },
        {
            "id": "aab174b7-904d-4389-8841-8b7e850dd0f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7889
        },
        {
            "id": "7de67e07-b5a4-46e3-abfe-fa3a7b49d73c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7893
        },
        {
            "id": "318e1c0f-2714-417c-86b7-c359f947cd43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7895
        },
        {
            "id": "fdae0e19-8cf2-4980-aeca-118dd418105c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7897
        },
        {
            "id": "bb0b3d15-8324-4a52-b222-e6ac6333ff4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7899
        },
        {
            "id": "1d84dd76-85da-48f4-b2fc-de75a8022aae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7901
        },
        {
            "id": "35123a79-9b5b-4e4b-9ccf-5d32066bbd41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7903
        },
        {
            "id": "3b5f065b-5828-47f4-b44d-892ad7b6589e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7905
        },
        {
            "id": "c116513a-a1d4-4086-98c4-cc83d4364c08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7907
        },
        {
            "id": "b448fc8a-2b40-45fa-a845-6d6e85206380",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8217
        },
        {
            "id": "01de7c21-7171-48a0-9cd0-6560aa5b6eac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8221
        },
        {
            "id": "32c7aa8e-1c0f-495e-96a8-abbb1d16ac47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 34
        },
        {
            "id": "a89f61c2-9ed4-48ea-b955-3bfb31100b74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 39
        },
        {
            "id": "01726e39-4419-45b3-89e5-9bc0b7defebe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 8217
        },
        {
            "id": "05401086-ad2f-46b0-bdbb-da2e7cd04427",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 8221
        },
        {
            "id": "d46066d0-b24f-4fa4-a788-8ac6ef9dd1b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 118,
            "second": 34
        },
        {
            "id": "96812fae-db63-4c19-bd15-aa257c8065c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 118,
            "second": 39
        },
        {
            "id": "3f67825d-acae-47b5-a7d6-e815b543f948",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 44
        },
        {
            "id": "8af81a1b-babb-4d1d-9a19-22c8f7b46a2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 46
        },
        {
            "id": "d6e4c03c-9b1d-4b13-8816-14cff2c7c4c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 63
        },
        {
            "id": "f8419095-cbc2-446b-8f43-4286c7b77e45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 118,
            "second": 8217
        },
        {
            "id": "e8ce4bcc-2b5d-48a0-956c-34fd6dc8e7dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 8218
        },
        {
            "id": "836eb974-2c39-416b-867b-e5b785c7cb78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 118,
            "second": 8221
        },
        {
            "id": "f7573d65-259c-470a-98bf-5dbe456ca9bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 8222
        },
        {
            "id": "7df095ee-07c5-4bc3-85fd-8dbf84a3d5de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 119,
            "second": 34
        },
        {
            "id": "a080a428-351c-4658-9f42-d5f7947207e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 119,
            "second": 39
        },
        {
            "id": "75715b56-e3ce-4b4a-b3c5-61f0966213f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 44
        },
        {
            "id": "1f41584b-9937-4432-89f5-39193747aa40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 46
        },
        {
            "id": "1a1ebd01-cf1d-4fd6-8453-9e07de8d1998",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 63
        },
        {
            "id": "80df5521-33b0-450b-a141-fe9087913393",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 119,
            "second": 8217
        },
        {
            "id": "c04df7b4-adbd-41c6-84ff-0ec08cd3f2e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 8218
        },
        {
            "id": "a5fdec14-7ec8-4467-95ab-2b493f1c905c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 119,
            "second": 8221
        },
        {
            "id": "b7ab12ab-e34c-4593-ab5c-ff755c6272f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 8222
        },
        {
            "id": "b6745aa9-e891-40f1-ae69-ecca31855406",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 99
        },
        {
            "id": "a46cfd6b-2cca-48ae-9e43-dbc9c5cc9f98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 100
        },
        {
            "id": "6f98cf0c-e475-46e4-bd5e-03e1f550bbe4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 101
        },
        {
            "id": "294a65a6-9ed4-46e5-842d-d06c15405258",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 111
        },
        {
            "id": "93409fff-cff9-4fdb-b2ee-f53df61452d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 113
        },
        {
            "id": "a49b7e79-7f67-420f-84dc-79d3b2b7bfbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 224
        },
        {
            "id": "97a35eff-6565-438a-90e6-4e2e421a6332",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 231
        },
        {
            "id": "00fb9aa8-9221-470c-8560-8af618332efd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 232
        },
        {
            "id": "d32327f8-617e-4faa-bc5a-56e7e18bb702",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 233
        },
        {
            "id": "6dd73ede-3616-4513-b797-36637ddb4a65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 234
        },
        {
            "id": "de7de9e6-8e15-48b8-9ad4-886dc6addee2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 235
        },
        {
            "id": "e7c56074-1022-4bbc-883d-131640ed5292",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 242
        },
        {
            "id": "ddc3d6c0-ea68-444d-8640-c684a4c0a288",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 243
        },
        {
            "id": "3b4df263-afd0-416a-bd94-c349985cedd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 244
        },
        {
            "id": "1e048176-1688-433c-8c6d-a254a8fd849f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 245
        },
        {
            "id": "b11178bf-9d04-4354-8f27-fea307eb4048",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 246
        },
        {
            "id": "228c3cb0-ef00-45e8-a4ea-2669f48e7d29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 248
        },
        {
            "id": "9194e57d-aa74-4e85-84cd-6bfb3e72c6ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 263
        },
        {
            "id": "a3d9d140-2a5f-4bc9-bae5-098e87010199",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 265
        },
        {
            "id": "69281ae1-3055-4a88-9248-0837f0e3af53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 267
        },
        {
            "id": "ef041fd0-3abd-4788-92e6-3c7a477c1c8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 269
        },
        {
            "id": "5d54242e-9e45-4946-aa1b-d7e1819fdce5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 271
        },
        {
            "id": "2263eb7b-24d7-4ddc-9ecf-8f8b86ddef79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 273
        },
        {
            "id": "612a74b1-adbd-4b77-b892-a350d1911408",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 275
        },
        {
            "id": "d4b1450c-a680-4305-8ee1-459d4493486e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 277
        },
        {
            "id": "260d32f3-74bb-454e-aeac-a83374e2dd63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 279
        },
        {
            "id": "14ad9d65-133b-493c-b00e-068dc6e28f58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 281
        },
        {
            "id": "f8fc2634-edf4-4a13-9913-6ffb19800bcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 283
        },
        {
            "id": "42854c28-e89e-4f72-8886-684023086617",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 333
        },
        {
            "id": "f8480603-ff19-48ff-a509-f7dc0e021a25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 335
        },
        {
            "id": "4b5bff2a-179d-45a2-9dcf-3e40ab841688",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 337
        },
        {
            "id": "ff7be61c-f1d9-4285-9efc-896719724a17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 339
        },
        {
            "id": "faeafe6c-7b47-40a6-af68-7eb6359b61e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 417
        },
        {
            "id": "1f554e76-6676-4f49-b4ef-adfbdf9194fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 511
        },
        {
            "id": "e62f0e9d-5290-4add-8b40-a7bf3d6fd481",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7865
        },
        {
            "id": "9355b055-fd4b-4513-9dca-dc08ad2c7e35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7867
        },
        {
            "id": "d3291d0b-c1a4-49e7-9396-12ae233d0f37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7869
        },
        {
            "id": "ebf4a3dd-50c6-437f-81ae-854a91509dbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7871
        },
        {
            "id": "0cf51f4a-5864-4356-a61e-032dee687b2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7875
        },
        {
            "id": "6344cc94-1eab-4672-b3ea-d70797e0657f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7877
        },
        {
            "id": "8cfbdad3-9d9a-4a5e-a56d-752fff8b26e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7879
        },
        {
            "id": "ea9512c7-f431-43de-a38e-e3531bcc0e3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7885
        },
        {
            "id": "de62f6d7-a7f5-46d7-b81e-4ed315217340",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7887
        },
        {
            "id": "18327e06-e269-4ee5-8d27-2938f2dcc769",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7889
        },
        {
            "id": "4c915cb7-2492-4ebd-b76e-a1674d6ea8cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7893
        },
        {
            "id": "5a791a6c-714f-4697-b4c3-24b17f53737a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7895
        },
        {
            "id": "8a074b17-292f-4fd5-8e20-43c0995ed375",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7897
        },
        {
            "id": "b12541dc-fca8-419f-b663-aebc2885b809",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7899
        },
        {
            "id": "66c1c86b-31a2-4b06-9e7c-d4c8ab8988db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7901
        },
        {
            "id": "6f1eb8b6-8d75-4489-938a-c21a836693e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7903
        },
        {
            "id": "14bd4bfa-ff52-464f-a8a6-cca47e04a759",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7905
        },
        {
            "id": "b0ef60f7-4288-42d0-9fe1-b070cd59d615",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7907
        },
        {
            "id": "6a2d308d-e4b3-457a-8d9f-f6b186ed9fd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 121,
            "second": 34
        },
        {
            "id": "04d00d3b-a7ef-48c2-9385-8f58c1308194",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 121,
            "second": 39
        },
        {
            "id": "4b22b667-3c2f-4e23-9046-6bb67b9ea4c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 44
        },
        {
            "id": "abb9a31f-1189-485f-aa36-90c589098dfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 46
        },
        {
            "id": "84cb528f-f99f-4e8f-8ce2-67bf7f01b5a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 63
        },
        {
            "id": "e0c0ce8f-9a6e-4b4c-b01b-9fe3e6e244a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 121,
            "second": 8217
        },
        {
            "id": "166f2402-1c82-4f3f-b317-a438ccbfa6c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 8218
        },
        {
            "id": "88f9ad67-6cfe-4d5a-b289-d52ef17f41cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 121,
            "second": 8221
        },
        {
            "id": "fa313020-f7bb-454b-a701-888cdef34aff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 8222
        },
        {
            "id": "470ab880-0ffb-4dd3-8b37-bbd968bd6f08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 123,
            "second": 74
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 30,
    "styleName": "",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}