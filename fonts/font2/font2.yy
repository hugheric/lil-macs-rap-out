{
    "id": "4332900d-4249-4103-a2cf-49b9ec70f6cd",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font2",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Consolas",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "f06ed0aa-32bb-40f6-afe7-0c639ced5295",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "7eb127ed-dd46-4fd0-8a60-d933fd9b0507",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 25,
                "offset": 4,
                "shift": 12,
                "w": 4,
                "x": 74,
                "y": 110
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "25521ad0-82d4-481d-839a-cbdbd4b2c2c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 25,
                "offset": 2,
                "shift": 12,
                "w": 7,
                "x": 228,
                "y": 83
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "2fae4441-c0ea-4d73-92e1-aa7346c50052",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "690d7b00-f1a2-40f9-96b3-46f781ac50cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 74,
                "y": 56
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "a89dcb74-8aaf-44f4-a25b-feb212f9b56d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 156,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "86fdd92e-217a-4e7c-9dae-6e0e520e244b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 142,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "b9efa97a-f460-4b20-931e-5c1b50d7b2e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 25,
                "offset": 4,
                "shift": 12,
                "w": 3,
                "x": 80,
                "y": 110
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "81845d63-1149-4147-8911-613b36a585c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 25,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 45,
                "y": 110
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "cbccb322-9696-455e-9919-3e8a14d707cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 25,
                "offset": 2,
                "shift": 12,
                "w": 7,
                "x": 246,
                "y": 83
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "73339417-9d96-449c-8fba-e768be092c7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 101,
                "y": 83
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "cda4c09b-3434-49a9-b524-f85215cfe431",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 132,
                "y": 29
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "26dd2980-3988-4234-b325-31db13115b73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 25,
                "offset": 2,
                "shift": 12,
                "w": 6,
                "x": 37,
                "y": 110
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "00039872-c6c9-402e-8d6c-7502a934abeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 25,
                "offset": 2,
                "shift": 12,
                "w": 7,
                "x": 237,
                "y": 83
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "097e3994-a79b-4d1e-aa4c-d51b56ebba08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 25,
                "offset": 3,
                "shift": 12,
                "w": 5,
                "x": 61,
                "y": 110
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "ce56ad29-20b2-465a-9b96-a95246bb890f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 156,
                "y": 83
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "de830d6b-dc3f-418b-a92d-56161d046dc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 106,
                "y": 29
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "b5c63d99-9ebf-48ec-83d7-5b511681310f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 169,
                "y": 29
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "38ff5c5c-df66-4ae8-860e-b934becbdd52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 86,
                "y": 56
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "8f2c3ce5-82fe-4773-ad03-447406e9c2b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 134,
                "y": 56
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "16e6926e-07e0-4572-a751-3b201c34fc3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 119,
                "y": 29
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "7962af6b-e766-4df2-9a00-47fe7bf68aa8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 134,
                "y": 83
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "f18e42a7-50a6-4eb4-b0e4-b3289d3809c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 146,
                "y": 56
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "ca38746f-df28-497a-9e15-8e5661160fe2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 170,
                "y": 56
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "2e7f55c1-3c9b-4a72-a90a-982f1d602a15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 205,
                "y": 29
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "203dd116-73fa-40cb-8dbb-daa65d66fa42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 110,
                "y": 56
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "67dfb982-53b1-46b0-92b9-9b0c8747dbbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 25,
                "offset": 4,
                "shift": 12,
                "w": 4,
                "x": 68,
                "y": 110
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "a6fb3568-782f-404d-a7ae-63790a0add19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 25,
                "offset": 2,
                "shift": 12,
                "w": 6,
                "x": 29,
                "y": 110
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "51c8c79d-ffc3-4669-9913-745f55465676",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 13,
                "y": 83
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "b6838050-d56a-4867-b8bc-9c19d6fa7603",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 194,
                "y": 56
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "8769e1cb-a3b5-43e9-9d1f-25bf38af30c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 25,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 241,
                "y": 56
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "f6d21cf7-5790-43f1-9ecc-629087487e50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 25,
                "offset": 3,
                "shift": 12,
                "w": 7,
                "x": 11,
                "y": 110
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "a3700f53-f03a-4c4e-a71e-3ad57f1a8e0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 128,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "bb40cd88-afcb-4555-a6ee-8d9421857402",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "6b6bf33d-090f-47e2-8e2a-80942f539737",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 206,
                "y": 56
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "60f0996f-371b-4423-b6f8-7cc2a9b19ed6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 41,
                "y": 29
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "a57766f3-b150-4cb3-95e2-e73e0b1e7efc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 218,
                "y": 56
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "1d876cdb-342a-41ff-8409-c48075bcfce9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 25,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 208,
                "y": 83
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "4c711e4c-d502-453f-81d2-e9f771f216ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 25,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 218,
                "y": 83
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "cf6b61fe-cc48-47bc-98f1-1b62d8444abe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 28,
                "y": 29
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "0bc8d8c7-efa0-4f85-942b-92cb0341351d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 182,
                "y": 56
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "8e852411-50f3-4d38-b80c-b13cab7af6b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 145,
                "y": 83
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "201fbae9-c84e-4965-b36b-d1c38c30aac8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 8,
                "x": 188,
                "y": 83
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "2f127054-1f58-4ac4-b411-7268b0507d83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 158,
                "y": 56
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "b5ce7962-4717-4e44-a21d-46d0b8cbceda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 25,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 123,
                "y": 83
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "62aeda69-0202-4658-94eb-f7893718b215",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 16,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "f9d3f756-5493-4a64-8d36-1504c012f2d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 122,
                "y": 56
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "86c7e300-0fb2-43e1-8a92-a5e5cb31c29b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 80,
                "y": 29
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "2222c38e-158f-4543-82ca-e8e0c0d4bcfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 98,
                "y": 56
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "59f2bd1e-b507-4a29-89aa-6bab9e831a12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "5765ec11-d9ea-4c2c-a576-62096207c2e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 145,
                "y": 29
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "a3339295-88f0-41ff-9420-998d1b0ff1b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 157,
                "y": 29
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "304ac4c1-09cc-4ecd-8fc0-55fe92fb4bb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 237,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "e675fe5a-9bc2-4df8-8ae5-517e940804d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 181,
                "y": 29
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "8a31686b-43e0-41ab-9e23-f0307950096e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "6f46715c-45d0-4909-ab30-acea0eb927af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "a77fa902-d17b-43dd-8f62-331dc2f4521f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "4d25b092-ea29-4176-9a46-fb751a1411e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "32e9aefa-ae42-4d67-a418-a2b5c76a0ab5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 193,
                "y": 29
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "9a3b6524-537d-482d-bbd6-c5348d51502d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 25,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 53,
                "y": 110
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "424a745d-a9cf-45f8-a20d-e1c8714f2d8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 217,
                "y": 29
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "8ecd8460-9090-4e25-99ad-40135fe93c27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 25,
                "offset": 2,
                "shift": 12,
                "w": 7,
                "x": 2,
                "y": 110
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "67567970-8289-47a8-a96c-c731a2fdfc43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 229,
                "y": 29
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "e4cd0284-19bd-468f-98d9-2690058da1c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "f52c7247-648f-490f-8324-686b049f2a1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 7,
                "x": 20,
                "y": 110
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "543cf692-f6e9-42b4-b520-674e81c32402",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 112,
                "y": 83
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "0a51f655-be42-4dd2-8fa5-2011d06fc8c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 241,
                "y": 29
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "8d05d8ad-5124-4e99-9a8e-9314bfed6355",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 167,
                "y": 83
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "d0ef1a21-dfbf-4289-9c60-831bb9f338df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 90,
                "y": 83
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "a462a6a7-285c-4da1-99ec-7a5a1117a7cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 56
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "f9081a1d-103a-4c26-90a1-047d9f89fa01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 67,
                "y": 29
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "851568b0-68da-4791-95bc-0c2134adc4ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 198,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "a45a879e-752d-4d39-8538-dc40d6e7e37b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 230,
                "y": 56
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "dfb2d006-05f0-4d72-bc3e-4fd09a035ff8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 14,
                "y": 56
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "550defb0-6cec-4b48-97fc-da4eee7f511d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 8,
                "x": 198,
                "y": 83
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "2067298a-d343-4648-a231-5e5365f5f6b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 26,
                "y": 56
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "7d17436f-d9a9-4ba6-bb1a-07fc809699ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 38,
                "y": 56
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "c88c7d59-b30d-49c3-a56d-e492d541e87f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 54,
                "y": 29
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "03198f2c-039e-4f14-b0fa-caac7bed63ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 79,
                "y": 83
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "22d2eeaa-0a52-4d0d-a5d4-6aec8ef5c6c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "b10cbe33-e340-4bf9-8d70-95bc9bd9b29e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 50,
                "y": 56
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "67a9e514-817c-4012-ace6-425fcd6fccb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 2,
                "y": 83
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "5dc49329-89e9-424c-96be-617a11a287b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 25,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 24,
                "y": 83
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "ee67d98c-5b21-4bd0-8b00-07680c1e1ca9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 35,
                "y": 83
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "280e2c5a-c36a-4eed-ac8b-e6244ffdd949",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 62,
                "y": 56
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "ff32d890-7c35-40ef-bddd-6f24844f3d12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 46,
                "y": 83
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "3bdc49fc-b120-4162-b1ee-590e3e7dfabe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 224,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "44810fd6-cd7c-4194-8011-7067fa93e2d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 170,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "65ea3f77-bca4-44dd-be90-647f7e164fc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 2,
                "y": 29
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "f005cf12-0417-4632-91ad-6dd03b1e5823",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 15,
                "y": 29
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "61a912c7-9fb2-48da-8015-5713cbdfc397",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 57,
                "y": 83
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "45b8835a-e3af-479e-b14d-dc19ca21410e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 68,
                "y": 83
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "71b3e9fb-f623-466e-882e-18f5132e45a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 25,
                "offset": 4,
                "shift": 12,
                "w": 3,
                "x": 85,
                "y": 110
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "e62ea479-a29d-4352-bf67-5495d788e17d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 25,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 178,
                "y": 83
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "5123f543-aca1-4e23-a95d-c52bfe0e6bcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 93,
                "y": 29
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 16,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}