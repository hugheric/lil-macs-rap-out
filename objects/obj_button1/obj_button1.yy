{
    "id": "f4564303-b5f2-4ac5-8338-fb564b5b0549",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_button1",
    "eventList": [
        {
            "id": "f3de1ea4-e560-413a-97b9-f193f1bbcdec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f4564303-b5f2-4ac5-8338-fb564b5b0549"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "6023c878-372d-4041-b0d1-5bbca5702d3f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "48d67fad-5268-4316-aa80-70b5a2f113ca",
    "visible": true
}