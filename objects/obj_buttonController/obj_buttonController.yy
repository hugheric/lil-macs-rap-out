{
    "id": "6023c878-372d-4041-b0d1-5bbca5702d3f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_buttonController",
    "eventList": [
        {
            "id": "7534ecda-85c0-4b54-a2fe-375152e5eaf9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6023c878-372d-4041-b0d1-5bbca5702d3f"
        },
        {
            "id": "80862b34-b993-484c-b01e-420da378edb7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "6023c878-372d-4041-b0d1-5bbca5702d3f"
        },
        {
            "id": "ef5b4215-4a5a-4f08-bd10-673632d580a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6023c878-372d-4041-b0d1-5bbca5702d3f"
        },
        {
            "id": "832e3bf6-be87-479c-8596-85633321210c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "6023c878-372d-4041-b0d1-5bbca5702d3f"
        },
        {
            "id": "7f883241-e847-40de-abb3-9c44a3a23015",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "6023c878-372d-4041-b0d1-5bbca5702d3f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}