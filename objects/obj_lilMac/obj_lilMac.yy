{
    "id": "31c758ae-a5e4-4e99-84c7-5126bfd91be3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_lilMac",
    "eventList": [
        {
            "id": "46c7a5ff-6891-4747-9006-a9dc4beac831",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "31c758ae-a5e4-4e99-84c7-5126bfd91be3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "eb039aea-ce5a-418a-8bea-0e3ef6dcb603",
    "visible": true
}