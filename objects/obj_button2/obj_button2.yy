{
    "id": "26c5be2c-6984-4dd3-9b61-f9e1bb5f5305",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_button2",
    "eventList": [
        {
            "id": "bb1b49dc-6f0b-4000-ad09-f6ac8a99f6df",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "26c5be2c-6984-4dd3-9b61-f9e1bb5f5305"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "6023c878-372d-4041-b0d1-5bbca5702d3f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "48d67fad-5268-4316-aa80-70b5a2f113ca",
    "visible": true
}