{
    "id": "4e560eab-c9f8-412e-8107-f20fd2b5fa8f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_lyricsController",
    "eventList": [
        {
            "id": "5978acb7-ac93-4f1b-bc48-c661251fc70d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4e560eab-c9f8-412e-8107-f20fd2b5fa8f"
        },
        {
            "id": "26107e9f-c980-4a02-8212-26b1f5ce48c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4e560eab-c9f8-412e-8107-f20fd2b5fa8f"
        },
        {
            "id": "fa122dae-9b3f-4d02-89dc-3ba4bbcfa03b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4e560eab-c9f8-412e-8107-f20fd2b5fa8f"
        },
        {
            "id": "bbf7f64e-20c2-4cd0-984a-bd0b5c279ee5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "4e560eab-c9f8-412e-8107-f20fd2b5fa8f"
        },
        {
            "id": "a1c70d69-ff35-4d60-a4cb-cadb27640c7b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "4e560eab-c9f8-412e-8107-f20fd2b5fa8f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}