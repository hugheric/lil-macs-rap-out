{
    "id": "b6b8b9f2-d1c2-43cd-bdc4-c027061bb03a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_retry",
    "eventList": [
        {
            "id": "77d0bf12-f0cf-4144-9ec1-222506c518b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "b6b8b9f2-d1c2-43cd-bdc4-c027061bb03a"
        },
        {
            "id": "022c3af3-ff68-4f01-a52b-f711f208ca5d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b6b8b9f2-d1c2-43cd-bdc4-c027061bb03a"
        },
        {
            "id": "30cee2aa-2fdf-4b50-b915-d75acb9446d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b6b8b9f2-d1c2-43cd-bdc4-c027061bb03a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}