{
    "id": "03535f96-88ab-4f71-9d29-de8dcc9cebbe",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_button4",
    "eventList": [
        {
            "id": "a8abf517-f858-4033-bd54-c501f81f6977",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "03535f96-88ab-4f71-9d29-de8dcc9cebbe"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "6023c878-372d-4041-b0d1-5bbca5702d3f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "48d67fad-5268-4316-aa80-70b5a2f113ca",
    "visible": true
}