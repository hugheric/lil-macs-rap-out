{
    "id": "b9db2481-c68a-476c-88d8-81dada8d5705",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_level1Intro",
    "eventList": [
        {
            "id": "0018a17e-bb12-45b4-a2c8-41c2a89f170a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b9db2481-c68a-476c-88d8-81dada8d5705"
        },
        {
            "id": "3885d0c0-2259-4ae6-885a-732f59eef07f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "b9db2481-c68a-476c-88d8-81dada8d5705"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "68304b57-d316-458f-b0ae-96b6d10a7720",
    "visible": true
}