{
    "id": "b039e75c-f41b-4827-baf6-f287a24b7ce0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_wrong",
    "eventList": [
        {
            "id": "cb32e37d-0f45-4e80-aeb6-1ccd4f8d9fc6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b039e75c-f41b-4827-baf6-f287a24b7ce0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "f2f7eda7-1f89-46ac-81be-6e0ee3f1ed1b",
    "visible": true
}