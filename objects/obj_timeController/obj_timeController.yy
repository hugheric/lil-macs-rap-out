{
    "id": "87285bc7-c165-45b5-bc27-4345f8b53911",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_timeController",
    "eventList": [
        {
            "id": "9cab3570-62bf-43bd-b4be-9f84d6a2078f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "87285bc7-c165-45b5-bc27-4345f8b53911"
        },
        {
            "id": "62bacf2a-b993-4e5a-9147-1f8a8ee8bab8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "87285bc7-c165-45b5-bc27-4345f8b53911"
        },
        {
            "id": "85bdf795-4dc4-43e9-a5e0-33709900966f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "87285bc7-c165-45b5-bc27-4345f8b53911"
        },
        {
            "id": "e6260e2c-2179-4f17-adb9-6fa360bf6041",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "87285bc7-c165-45b5-bc27-4345f8b53911"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}