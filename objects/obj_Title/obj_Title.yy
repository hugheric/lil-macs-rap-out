{
    "id": "9fd1410b-4ffb-4003-8cac-5119e0cd65a6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_title",
    "eventList": [
        {
            "id": "82d6c65c-a8e8-4d6b-a0bb-e549500e1857",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "9fd1410b-4ffb-4003-8cac-5119e0cd65a6"
        },
        {
            "id": "4e13afa4-241c-4b95-a0e0-8e6955cb89e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9fd1410b-4ffb-4003-8cac-5119e0cd65a6"
        },
        {
            "id": "f8ca5d71-33ec-43cf-8848-a2832961f251",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9fd1410b-4ffb-4003-8cac-5119e0cd65a6"
        },
        {
            "id": "5ec51166-3c2b-43d0-9851-efa399fc054d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "9fd1410b-4ffb-4003-8cac-5119e0cd65a6"
        },
        {
            "id": "a7d1563c-0c42-4938-b7e0-8120f381245e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9fd1410b-4ffb-4003-8cac-5119e0cd65a6"
        },
        {
            "id": "f75a189c-10b0-4d48-a725-f4ca2a88f56e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 17,
            "eventtype": 9,
            "m_owner": "9fd1410b-4ffb-4003-8cac-5119e0cd65a6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}