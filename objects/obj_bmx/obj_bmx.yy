{
    "id": "90ba84c6-de46-411f-a7bc-b966e29ea146",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bmx",
    "eventList": [
        {
            "id": "b1f7e369-d8ac-49a6-9077-332ffbec843a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "90ba84c6-de46-411f-a7bc-b966e29ea146"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "801b3552-4c91-4eb4-ae09-a6126a2de793",
    "visible": true
}