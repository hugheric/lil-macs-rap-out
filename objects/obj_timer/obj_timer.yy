{
    "id": "44f54150-7d7e-4f16-9548-1d06fb8fef3c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_timer",
    "eventList": [
        {
            "id": "66252a97-1c3b-4104-88bb-5da42b974994",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "44f54150-7d7e-4f16-9548-1d06fb8fef3c"
        },
        {
            "id": "ca4b4398-3eea-4fad-8e9c-36c5c2690194",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "44f54150-7d7e-4f16-9548-1d06fb8fef3c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "f4f833e6-fe97-439f-a75b-06961dd4a58d",
    "visible": true
}