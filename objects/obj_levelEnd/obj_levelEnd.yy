{
    "id": "df15d46a-8574-4f94-b4b6-8beac6f6cbfb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_levelEnd",
    "eventList": [
        {
            "id": "3f791344-3212-4bb9-97dc-6ab33e801e09",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "df15d46a-8574-4f94-b4b6-8beac6f6cbfb"
        },
        {
            "id": "27a10f85-bb67-44b6-8e6d-452d1b90824b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "df15d46a-8574-4f94-b4b6-8beac6f6cbfb"
        },
        {
            "id": "09ac8a84-e1cc-4a95-8a83-bfb09a1226ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "df15d46a-8574-4f94-b4b6-8beac6f6cbfb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}