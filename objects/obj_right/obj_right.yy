{
    "id": "7f6bbc13-f854-4ad5-8aef-31192d289473",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_right",
    "eventList": [
        {
            "id": "d8a4fe48-90e8-44bb-8660-53a125f61240",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7f6bbc13-f854-4ad5-8aef-31192d289473"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "678c03b1-4cdc-45b6-b5a0-743324edc0ea",
    "visible": true
}